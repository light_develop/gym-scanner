<?php
class Offers_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	     $this ->result_mode = 'object';
	    }


	    public function get_gym()
	    {
            $res = $this->db->order_by('gymname')->get('gym')->result();
        	$gyms = array();
	        foreach ($res as $value) {
	          $gyms[$value->id] = $value->gymname;
	        }
	        return $gyms;
   		} 
	    public function get_details($id){
	    	return $res = $this->db->select('o.*,r.gym_id')->from('offer_relation r')
								->join('offer_description o','r.id = o.offer_relation_id')
								->where(array('r.id'=>$id,'o.status !='=>'3'))->get()->result();
	    }
	    public function get_all( $user_id = '0')
	    {
	    	$this->db->select('o.*, g.gymname, u.email')->from('offer_relation as o');

			$result = $this->db->join('gym as g', 'g.id=o.gym_id')
                ->join('user as u', 'u.id = o.user_id')
                ->order_by('o.gym_id')->get()->result();
			return $result;
		}		
}


?>
