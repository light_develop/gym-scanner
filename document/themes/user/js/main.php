
		<script src="<?php echo base_url()?>themes/user/js/slippry.min.js"></script>
		<script src="//use.edgefonts.net/cabin;source-sans-pro:n2,i2,n3,n4,n6,n7,n9.js"></script>
		<meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="demo.css">
    <link rel="stylesheet" href="<?php echo base_url()?>themes/user/css/slippry.css">


<div class="clearfix"></div>
<section>
<ul id="demo1">
				<li><a href="#slide1"><img src="<?php echo base_url()?>themes/user/images/PersonalTraining.jpg" ></a></li>
				<li><a href="#slide2"><img src="<?php echo base_url()?>themes/user/images/izano-beautiful-gym.jpg"  ></a></li>
				<li><a href="#slide3"><img src="<?php echo base_url()?>themes/user/images/gym2.png" ></a></li>
			</ul>

</section>
<div class="clearfix"></div>
<div class="container">
  <div class="col-lg-1 ">&nbsp; </div>
  <div class="serach_gym col-lg-10 col-md-10 col-sm-12 col-xs-12 ">
    <h1 class="heading_gym">I AM LOOKING FOR</h1>
    <div class="col-lg-12 ">
      <div class="search_bg  col-md-12 col-sm-12 col-xs-12 ">
      <form action="<?php echo USER_URL;?>search" class="form-horizontal" method="post" id="search_form" />
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dropdown padding_art ">
          <?php $attributes= 'id="cat_id" required class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
             echo form_dropdown('cat_id', $cat,'',$attributes);
          ?> <font color="red"><span id="error1"></span></font>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding_art ">
          <?php $cntry[''] = 'COUNTRY'; $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            echo form_dropdown('cntry', $cntry, '',$attributes);
          ?> <font color="red"><span id="error_cntry"></span></font> 
       </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding_art ">
          <?php $attributes= 'id="state" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" ';  
            $state = array(); $state[''] = 'STATE';                    
            echo form_dropdown('state', $state,'',$attributes);                      
          ?> <font color="red"><span id="error_state"></span></font> 
        </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding_art ">
          <?php $attributes= 'id="city" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" ';  
            $city = array(); $city[''] = 'CITY';                     
            echo form_dropdown('city', $city, '',$attributes);                      
          ?> <font color="red"><span id="error_city"></span></font> 
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_bg_1">ADVANCE SEARCH</div>
    <div class="col-lg-10 check_box_bg">
    <span class="adv_search">
    <input type="checkbox" name="adv[pilates]" value="1" />
    <label>PILATES / YOGA</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[mma]" value="1" />
    <label>MMA</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[boxing]" value="1" />
    <label>BOXING</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[cross]" value="1" />
    <label>CROSS TRAINING</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[spinning]" value="1" />
    <label>SPINNING</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[gymnastics]" value="1" />
    <label>GYMNASTICS</label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[martial]" value="1" />
    <label>MARTIAL ARTS</label>
    </span>
    </div>
        <div class="col-lg-2"><button class="search_gym_button">Search</button></div>
  </div>
  </form>
  <div class="col-lg-1 col-md-1 ">&nbsp; </div>
  <div class="clearfix"></div>
  <section class="mar_top" >
    <aside class="col-lg-9 col-md-9 col-sm-12 col-xs-12  ">
      <?foreach ($latest as $key => $value) { ?>
      <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_art ">
        <div class="gym_cnt">
          <h2><?= $value->gymname;?></h2>
          <figure class="img_fg"><img src="<?php echo base_url()?>uploads/gym/logos/<?= $value->gym_logo;?>" width="213" height="127" class="img-responsive"/></figure>
          <article class="art_min_cnt"><?php $data = substr($value->description,0, 130); echo $data;?> <span style="float:right;color:blue;"><a href="<?php echo USER_URL;?>gym_imgs/<?php echo $value->id;?>">more...</a></span></article>
          <a href="<?php echo USER_URL;?>gym_imgs/<?php echo $value->id;?>"><button class="button_more">JOIN TODAY</button></a>
        </div>
      </aside>
    <?php } ?>
    <div id="timer_div_sh">
      <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_art ">
        <!-- <div align="center" style="margin:25px 0px"><img src="<?php echo base_url()?>themes/user/images/time_counter.png" class="img-responsive"/></div> -->
        <div align="center" style="margin:25px 0px" class="count_time">
        <h1>Gymscanner.com goes live in: </h1>
        <div id="countdown"></div></div>
      </aside>
      </div>
    </aside>
    <aside class="col-lg-3 col-md-3 col-sm-9 col-xs-12">
      <section><img src="<?php echo base_url()?>themes/user/images/mobil.png" class="img-responsive"/></section>
      <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:25px 0px;" >
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4"><img src="<?php echo base_url()?>themes/user/images/ios.png"/></div>
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4"><img src="<?php echo base_url()?>themes/user/images/android.png"/></div>
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4"><img src="<?php echo base_url()?>themes/user/images/windows.png"/></div>
      </section>
    </aside>
  </section>
</div>

<script> 

   $( "#search_form" ).submit(function( event ) {
      var cat = document.getElementById('cat_id').value;
      var cntry = document.getElementById('cntry').value;
      var state = document.getElementById('state').value;
      var city = document.getElementById('city').value;
      $('#error1').html('');
      $('#error_cntry').html('');
      $('#error_state').html('');
      $('#error_city').html('');
    if(cat == ''){
      document.getElementById('error1').innerHTML = "Select any category";
      return false;  
    }
    else if(cntry == "")
    {
      document.getElementById('error_cntry').innerHTML = "Select any country";
      return false;             
      }
    else if(state == '' || city == 0)
    {
      document.getElementById('error_state').innerHTML = "Select any state";
      return false;             
    }
    else if(city == '' || city == 0)
    {
      document.getElementById('error_city').style.display = "Select any city";
      return true;              
    }
    else
    {
      return true;              
    }
  });

  function load_dropdown_content(selected_value){ 

       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_states/' + selected_value,
                   'async' : false
             }).responseText;
         $("#state").replaceWith(result);
      } 
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
   
</script>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					transition: 'horizontal',
					useCSS: true,
					speed: 1000,
					pause: 3000,
					auto: true,
					preload: 'visible'
				});

				$('.stop').click(function () {
					demo1.stopAuto();
				});

				$('.start').click(function () {
					demo1.startAuto();
				});

				$('.prev').click(function () {
					demo1.goToPrevSlide();
					return false;
				});
				$('.next').click(function () {
					demo1.goToNextSlide();
					return false;
				});
				$('.reset').click(function () {
					demo1.destroySlider();
					return false;
				});
				$('.reload').click(function () {
					demo1.reloadSlider();
					return false;
				});
				$('.init').click(function () {
					demo1 = $("#demo1").slippry();
					return false;
				});
			});
		</script>
