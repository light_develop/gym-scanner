function initWyswigs(blocks) {
    $(blocks).hide();
    $(blocks).each(function(index, element) {
        $(element).wysiwyg({
            classes: 'wysiwyg-edit',
            // 'selection'|'top'|'top-selection'|'bottom'|'bottom-selection'
            toolbar: 'top-selection',
            buttons: {
                // Dummy-HTML-Plugin
                dummybutton1:  {
                    html: $('<input id="submit" type="button" value="bold" />').click(function () {
                        // We simply make 'bold'
                        if ($(element).wysiwyg('shell').getSelectedHTML())
                            $(element).wysiwyg('shell').bold();
                        else
                            alert('Please selection some text');
                    }),
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                smilies: {
                    title: 'Smilies',
                    image: '\uf118', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        var list_smilies = [
                            '<img src="/themes/wysiwig/smiley/afraid.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/amorous.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/angel.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/angry.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/bored.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/cold.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/confused.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/cross.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/crying.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/devil.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/disappointed.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/dont-know.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/drool.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/embarrassed.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/excited.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/excruciating.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/eyeroll.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/happy.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/hot.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/hug-left.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/hug-right.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/hungry.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/invincible.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/kiss.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/lying.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/meeting.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/nerdy.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/neutral.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/party.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/pirate.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/pissed-off.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/question.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/sad.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/shame.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/shocked.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/shut-mouth.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/sick.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/silent.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/sleeping.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/sleepy.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/stressed.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/thinking.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/tongue.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/uhm-yeah.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/wink.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/working.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/bathing.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/beer.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/boy.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/camera.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/chilli.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/cigarette.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/cinema.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/coffee.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/girl.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/console.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/grumpy.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/in_love.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/internet.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/lamp.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/mobile.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/mrgreen.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/musical-note.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/music.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/phone.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/plate.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/restroom.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/rose.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/search.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/shopping.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/star.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/studying.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/suit.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/surfing.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/thunder.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/tv.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/typing.png" width="16" height="16" alt="" />',
                            '<img src="/themes/wysiwig/smiley/writing.png" width="16" height="16" alt="" />'
                        ];
                        var $smilies = $('<div/>').addClass('wysiwyg-plugin-smilies')
                            .attr('unselectable','on');
                        $.each( list_smilies, function(index,smiley) {
                            if( index != 0 )
                                $smilies.append(' ');
                            var $image = $(smiley).attr('unselectable','on');
                            // Append smiley
                            var imagehtml = ' '+$('<div/>').append($image.clone()).html()+' ';
                            $image
                                .css({ cursor: 'pointer' })
                                .click(function(event) {
                                    $(element).wysiwyg('shell').insertHTML(imagehtml); // .closePopup(); - do not close the popup
                                })
                                .appendTo( $smilies );
                        });
                        var $container = $(element).wysiwyg('container');
                        $smilies.css({ maxWidth: parseInt($container.width()*0.95)+'px' });
                        $popup.append( $smilies );
                        // Smilies do not close on click, so force the popup-position to cover the toolbar
                        var $toolbar = $button.parents( '.wysiwyg-toolbar' );
                        if( ! $toolbar.length ) // selection toolbar?
                            return ;
                        return { // this prevents applying default position
                            left: parseInt( ($toolbar.outerWidth() - $popup.outerWidth()) / 2 ),
                            top: $toolbar.hasClass('wysiwyg-plugin-bottom') ? ($container.outerHeight() - parseInt($button.outerHeight()/4)) : (parseInt($button.outerHeight()/4) - $popup.height())
                        };
                    },
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },

                insertimage: {
                    title: 'Insert image',
                    image: '\uf030', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: index == 2 ? true : false    // wanted on selection
                },

                insertvideo: {
                    title: 'Insert video',
                    image: '\uf03d', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: index == 2 ? true : false    // wanted on selection
                },
                insertlink: {
                    title: 'Insert link',
                    image: '\uf08e' // <img src="path/to/image.png" width="16" height="16" alt="" />
                },
                // Fontname plugin
                fontname: index == 1 ? false : {
                    title: 'Font',
                    image: '\uf031', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        var list_fontnames = {
                            // Name : Font
                            'Arial, Helvetica' : 'Arial,Helvetica',
                            'Verdana'          : 'Verdana,Geneva',
                            'Georgia'          : 'Georgia',
                            'Courier New'      : 'Courier New,Courier',
                            'Times New Roman'  : 'Times New Roman,Times'
                        };
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_fontnames, function( name, font ) {
                            var $link = $('<a/>').attr('href','#')
                                .css( 'font-family', font )
                                .html( name )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').fontName(font).closePopup();
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    },
                    //showstatic: true,    // wanted on the toolbar
                    showselection: index == 0 ? true : false    // wanted on selection
                },
                // Fontsize plugin
                fontsize: index != 1 ? false : {
                    title: 'Size',
                    image: '\uf034', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        // Hack: http://stackoverflow.com/questions/5868295/document-execcommand-fontsize-in-pixels/5870603#5870603
                        var list_fontsizes = [];
                        for( var i=8; i <= 11; ++i )
                            list_fontsizes.push(i+'px');
                        for( var i=12; i <= 28; i+=2 )
                            list_fontsizes.push(i+'px');
                        list_fontsizes.push('36px');
                        list_fontsizes.push('48px');
                        list_fontsizes.push('72px');
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_fontsizes, function( index, size ) {
                            var $link = $('<a/>').attr('href','#')
                                .html( size )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').fontSize(7).closePopup();
                                    $(element).wysiwyg('container')
                                        .find('font[size=7]')
                                        .removeAttr("size")
                                        .css("font-size", size);
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    }
                    //showstatic: true,    // wanted on the toolbar
                    //showselection: true    // wanted on selection
                },
                header: index != 1 ? false : {
                    title: 'Header',
                    image: '\uf1dc', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        var list_headers = {
                            // Name : Font
                            'Header 1' : '<h1>',
                            'Header 2' : '<h2>',
                            'Header 3' : '<h3>',
                            'Header 4' : '<h4>',
                            'Header 5' : '<h5>',
                            'Header 6' : '<h6>',
                            'Code'     : '<pre>'
                        };
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_headers, function( name, format ) {
                            var $link = $('<a/>').attr('href','#')
                                .css( 'font-family', format )
                                .html( name )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').format(format).closePopup();
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    }
                    //showstatic: true,    // wanted on the toolbar
                    //showselection: false    // wanted on selection
                },
                fontname: index == 1 ? false : {
                    title: 'Font',
                    image: '\uf031', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        var list_fontnames = {
                            // Name : Font
                            'Arial, Helvetica' : 'Arial,Helvetica',
                            'Verdana'          : 'Verdana,Geneva',
                            'Georgia'          : 'Georgia',
                            'Courier New'      : 'Courier New,Courier',
                            'Times New Roman'  : 'Times New Roman,Times'
                        };
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_fontnames, function( name, font ) {
                            var $link = $('<a/>').attr('href','#')
                                .css( 'font-family', font )
                                .html( name )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').fontName(font).closePopup();
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    },
                    //showstatic: true,    // wanted on the toolbar
                    showselection: index == 0 ? true : false    // wanted on selection
                },
                // Fontsize plugin
                fontsize: index != 1 ? false : {
                    title: 'Size',
                    image: '\uf034', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        // Hack: http://stackoverflow.com/questions/5868295/document-execcommand-fontsize-in-pixels/5870603#5870603
                        var list_fontsizes = [];
                        for( var i=8; i <= 11; ++i )
                            list_fontsizes.push(i+'px');
                        for( var i=12; i <= 28; i+=2 )
                            list_fontsizes.push(i+'px');
                        list_fontsizes.push('36px');
                        list_fontsizes.push('48px');
                        list_fontsizes.push('72px');
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_fontsizes, function( index, size ) {
                            var $link = $('<a/>').attr('href','#')
                                .html( size )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').fontSize(7).closePopup();
                                    $(element).wysiwyg('container')
                                        .find('font[size=7]')
                                        .removeAttr("size")
                                        .css("font-size", size);
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    }
                    //showstatic: true,    // wanted on the toolbar
                    //showselection: true    // wanted on selection
                },
                // Header plugin
                header: index != 1 ? false : {
                    title: 'Header',
                    image: '\uf1dc', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    popup: function( $popup, $button ) {
                        var list_headers = {
                            // Name : Font
                            'Header 1' : '<h1>',
                            'Header 2' : '<h2>',
                            'Header 3' : '<h3>',
                            'Header 4' : '<h4>',
                            'Header 5' : '<h5>',
                            'Header 6' : '<h6>',
                            'Code'     : '<pre>'
                        };
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable','on');
                        $.each( list_headers, function( name, format ) {
                            var $link = $('<a/>').attr('href','#')
                                .css( 'font-family', format )
                                .html( name )
                                .click(function(event) {
                                    $(element).wysiwyg('shell').format(format).closePopup();
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append( $link );
                        });
                        $popup.append( $list );
                    }
                    //showstatic: true,    // wanted on the toolbar
                    //showselection: false    // wanted on selection
                },
                bold: {
                    title: 'Bold (Ctrl+B)',
                    image: '\uf032', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    hotkey: 'b'
                },
                italic: {
                    title: 'Italic (Ctrl+I)',
                    image: '\uf033', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    hotkey: 'i'
                },
                underline: {
                    title: 'Underline (Ctrl+U)',
                    image: '\uf0cd', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    hotkey: 'u'
                },
                strikethrough: {
                    title: 'Strikethrough (Ctrl+S)',
                    image: '\uf0cc', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    hotkey: 's'
                },
                forecolor: {
                    title: 'Text color',
                    image: '\uf1fc' // <img src="path/to/image.png" width="16" height="16" alt="" />
                },
                highlight: {
                    title: 'Background color',
                    image: '\uf043' // <img src="path/to/image.png" width="16" height="16" alt="" />
                },
                alignleft: index != 0 ? false : {
                    title: 'Left',
                    image: '\uf036', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                aligncenter: index != 0 ? false : {
                    title: 'Center',
                    image: '\uf037', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignright: index != 0 ? false : {
                    title: 'Right',
                    image: '\uf038', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignjustify: index != 0 ? false : {
                    title: 'Justify',
                    image: '\uf039', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                subscript: index == 1 ? false : {
                    title: 'Subscript',
                    image: '\uf12c', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: true    // wanted on selection
                },
                superscript: index == 1 ? false : {
                    title: 'Superscript',
                    image: '\uf12b', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: true    // wanted on selection
                },
                indent: index != 0 ? false : {
                    title: 'Indent',
                    image: '\uf03c', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                outdent: index != 0 ? false : {
                    title: 'Outdent',
                    image: '\uf03b', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                orderedList: index != 0 ? false : {
                    title: 'Ordered list',
                    image: '\uf0cb', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                unorderedList: index != 0 ? false : {
                    title: 'Unordered list',
                    image: '\uf0ca', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                removeformat: {
                    title: 'Remove format',
                    image: '\uf12d' // <img src="path/to/image.png" width="16" height="16" alt="" />
                }
            },
            selectImage: 'Click or drop image',
            submit : {
                title : "Submit",
                image: ''
            }
        })

    });
}

