function $1(id) {
    return document.getElementById(id);
}

function $$1(query) {
    return document.querySelectorAll(query);
}

function _createElement(name, attribs, parent) {
    var el = document.createElement(name);

    for(var key in attribs) {
        if(key == "inner") {
            el.innerHTML = attribs[key];
            continue;
        }

        el.setAttribute(key, attribs[key]);
    }

    parent.appendChild(el);

    return el;
}

function _undefined(value) {
    return typeof(value) == "undefined" || value === null || !value;
}

function notempty(field) {
    if(field.tagName == "INPUT" || field.tagName == "TEXTAREA") {
        return field.value.replace(/\s+/, "") != "";
    } else {
        return field.innerHTML.replace(/\s+/, "") != "";
    }
}

function validate(fields) {
    if(fields.length == 0) return false;

    for(i=0; i< fields.length; i++) {
        var type = fields[i].getAttribute("validation");
        if(type != "undefined" && window[type] != "undefined" && typeof(window[type]) == "function") {
            if(!window[type](fields[i])) return false;
        }
    }
    return true;
}

function changeLanguage(option) {
    document.cookie = "lang="+option.value;
}

function getCookie(name) {
    _allCookie = document.cookie.split(";");

    var cookiesArray = {};

    for(i=0; i< _allCookie.length; i++) {
        cookiesArray[_allCookie[i].split("=")[0].replace(/\s/, "")] = _allCookie[i].split("=")[1];
    }

    return cookiesArray[name];
}

function httpget(url) {
    var params = url.match(/^.*\?(.*)$/);
    if(params && typeof(params[1]) != "undefined") {
        return _toObject("=", params[1].split("&"));
    }
    return false;
}

function _toObject(delimiter, toSplit) {
    var splitedObject=  {};
    if(toSplit.length > 1) {
        for(i=0; i< toSplit.lenght; i++) {
            _row = toSplit[i].split(delimiter);
            if(_row[0] && _row[1]) {
                splitedObject[_row[0]] = _row[1];
            }
        }
    } else {
        _row = toSplit[0].split(delimiter);
        splitedObject[_row[0]] = _row[1];
    }

    console.log(splitedObject);
    return splitedObject;
}

NodeList.prototype.showAll = function() {
    if(this.length > 1) {
        for(i=0; i<this.length; i++) {
            this[i].style.display = "block";
        }
    } else {
        if(this.length == 1) {
            this[0].style.display = "block";
        }
    }
};

NodeList.prototype.hideAll = function() {
    if(this.length > 1) {
        for(i=0; i<this.length; i++) {
            this[i].style.display = "none";
        }
    } else {
        if(this.length == 1) {
            this[0].style.display = "none";
        }
    }
};

HTMLCollection.prototype.hideAll = function() {
    if(this.length > 1) {
        for(i=0; i<this.length; i++) {
            this[i].style.display = "none";
        }
    } else {
        if(this.length == 1) {
            this[0].style.display = "none";
        }
    }
};

HTMLCollection.prototype.showAll = function() {
    if(this.length > 1) {
        for(i=0; i<this.length; i++) {
            this[i].style.display = "block";
        }
    } else {
        if(this.length == 1) {
            this[0].style.display = "block";
        }
    }
};

function loadImage(file, callback) {
    if(!file) return false;
    if(file.type.indexOf("image") == -1) return false;

    var reader = new FileReader();

    reader.onloadend  = function(e) {
        callback(e);
    };

    reader.readAsDataURL(file);
}
