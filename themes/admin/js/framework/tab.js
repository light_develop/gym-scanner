var TabGrid = function(containerId, postfix, dataPostFix, dataClass) {
    this.container = $1(containerId);
    this.postfix = postfix;
    this.dataClass = dataClass;
    this.dataPostfix = dataPostFix;
    this.additionalCallback = function() {};

    if(typeof(this.container) == "undefined") {
        alert("Specify correct block for tabs");
    }
};

TabGrid.prototype.init = function() {
    this._addListeners();
    this.hideAllContainers();

    return this;
};

TabGrid.prototype.hideAllContainers = function() {
    var allContainer = $$1('.' + this.dataClass);

    if(typeof(allContainer) != "undefined") {
        for(i=0; i< allContainer.length; i++) {
            allContainer[i].style.display = "none";
        }
    }
};

TabGrid.prototype.hideAllLi = function() {
    this.allLi().hideAll();
    this.addBackButton();
};

TabGrid.prototype.allLi = function() {
    return this.container.getElementsByTagName("LI");
};

TabGrid.prototype._addListeners = function() {
    var allLi = this.allLi();
    _self = this;
    for(i=0; i<allLi.length; i++) {
        allLi[i].addEventListener("click", this.listener.bind(_self));
    }
};

TabGrid.prototype.addBackButton = function() {
    if(!_undefined($1('go-back-button'))) {
        $1('go-back-button').style.display = "block";
    } else {
        _createElement("a", {"href": "javascript:void(null)", "id": "go-back-button", "inner": "Back"}, this.container).addEventListener("click", this.goBack.bind(this));
    }
};

TabGrid.prototype.goBack = function(event) {
    this.allLi().showAll();
    this.hideAllContainers();
    this.additionalCallback("close");
    event.target.style.display = "none";

};

TabGrid.prototype.addAdditionalCallback = function(callback) {
    this.additionalCallback = callback;
}

TabGrid.prototype.listener = function(event) {
    _el = event.target;

    this.additionalCallback("open");

    _dataEl = $1(_el.id.replace(this.postfix, this.dataPostfix));

    if(!_undefined(_dataEl)) {
        _dataEl.style.display = "block";
        this.hideAllLi();
    }
};