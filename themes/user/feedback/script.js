function viewChat(e) {
    newwindow = window.open(e, "name", "height=400,width=800,toolbar=no,menubar=no,resizable=no,scrollbars=no,location=no,directories=no,status=no");
    if (window.focus) {
        newwindow.focus()
    }
}

$(function() {
    function e() {
        var e = $(".accountdtl-left").height() - 12;
        $(".accountdtl-right").css("min-height", e)
    }

    function t() {
        var e = {
            speed: 300,
            containerWidth: $(".objects-panel").outerWidth(),
            tabWidth: $(".showobjects-tab").outerWidth(),
            tabWidth: $(".hideobjects-tab").outerWidth(),
            init: function() {
                $("a.showobjects-tab").click(function(t) {
                    if (!$(".objects-panel").hasClass("open")) $(".objects-panel").animate({
                        left: "0"
                    }, e.speed).addClass("open");
                    $("a.showobjects-tab").hide();
                    $("a.hideobjects-tab").show();
                    $("#objectsForm").show();
                    t.preventDefault()
                });
                $("a.hideobjects-tab").click(function(t) {
                    if ($(".objects-panel").hasClass("open")) $(".objects-panel").animate({
                        left: "-" + e.containerWidth
                    }, e.speed).removeClass("open");
                    setTimeout(function() {
                        $("a.showobjects-tab").show();
                        $("a.hideobjects-tab").hide();
                        $("#objectsForm").show()
                    }, 400);
                    t.preventDefault()
                })
            }
        };
        e.init()
    }
    $("#btnSendFeedBack").click(function() {
        var e = $("#txtFeedBackEmail").val();
        var t = $("#txtFeedBackName").val();
        var n = $("#txtFeedBackMessage").val();
        var r = $("[id$=hdnFeedBackURL]").val();
        var i = $("[id$=hdnApplicationPath]").val();
        var s = "";
        if ($("#chkFeedBackType1").is(":checked")) {
            if (s.length == 0) s = "Bug in system";
            else s += ", Bug in system"
        }
        if ($("#chkFeedBackType2").is(":checked")) {
            if (s.length == 0) s = "Bug in a Form field";
            else s += ", Bug in a Form field"
        }
        if ($("#chkFeedBackType3").is(":checked")) {
            if (s.length == 0) s = "Confusion";
            else s += ", Confusion"
        }
        if ($("#chkFeedBackType4").is(":checked")) {
            if (s.length == 0) s = "Visual issues";
            else s += ", Visual issues"
        }
        if (e == null || e == "") {
            if ($(".appriseOverlay").is(":visible")) {} else {
                if ($(".feedback-panel").hasClass("open")) {
                    apprise("<h4>Your Email address is required</h4>", {
                        animate: true
                    });
                    return false
                }
            }
        } else {
            $("#response").show();
            $("#feedBackForm").hide();
            $("a.hidefeedback-tab").hide();
            $.ajax({
                type: 'get',
                url: SitePath + "/handlers/UserAjax.ashx",
                cache: false,
                data: {
                    Name: t,
                    Email: e,
                    Feedback: n,
                    Problem: s,
                    URL: r,
                    job: 'SendFeedBack'
                },
                success: function(result) {},
                error: function(jqXHR, textStatus, errorThrown) {
                    apprise('<h4>' + errorThrown + '</h4>');
                }
            });
        }
        return false
    });
    $("body").delay(500).queue(function(e) {
        $(this).css("padding-right", "1px");
        $(this).css("padding-right", "")
    });
 	//$.fn.placeholder ? $('input, textarea').placeholder() : null;
    if ($(".htmlContent *").width() > 670) {
        $(".htmlContent table").width("670")
    }
 
    var n = {
        speed: 300,
        containerWidth: $(".feedback-panel").outerWidth(),
        tabWidth: $(".showfeedback-tab").outerWidth(),
        tabWidth: $(".hidefeedback-tab").outerWidth(),
        init: function() {
            $("a.showfeedback-tab").click(function(e) {
                if (!$(".feedback-panel").hasClass("open")) {
                    $(".feedback-panel").animate({
                        left: "0"
                    }, n.speed).addClass("open")
                }
                $("a.showfeedback-tab").hide();
                $("a.hidefeedback-tab").show();
                $("#feedBackForm").show();
                $("#response").hide();
                $("#txtFeedBackEmail").val("");
                $("#txtFeedBackName").val("");
                $("#txtFeedBackMessage").val("");
                $("#chkFeedBackType1").checked = false;
                $("#chkFeedBackType2").checked = false;
                $("#chkFeedBackType3").checked = false;
                $("#chkFeedBackType4").checked = false;
                e.preventDefault()
            });
            $("#btnCancelFeedBack, #btnCancelFeedBack1, a.hidefeedback-tab").click(function(e) {
                if ($(".feedback-panel").hasClass("open")) {
                    $(".feedback-panel").animate({
                        left: "-" + n.containerWidth
                    }, n.speed).removeClass("open")
                }
                setTimeout(function() {
                    $("a.showfeedback-tab").show();
                    $("a.hidefeedback-tab").hide();
                    $("#feedBackForm").show();
                    $("#response").hide()
                }, 400);
                e.preventDefault()
            })
        }
    };
    n.init();
    e();
    t()
});
/*$.blockUI.defaults.message = "<h1>loading ...</h1>";
$.blockUI.defaults.css.border = "none";
$.blockUI.defaults.css.padding = "15px";
$.blockUI.defaults.css.backgroundColor = "#34aade";
$.blockUI.defaults.css.backgroundColor = "#34aade";
$.blockUI.defaults.css.opacity = ".9";
$.blockUI.defaults.css.color = "#fff";
$.blockUI.defaults.overlayCSS.opacity = ".3"*/