<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{


    public function __construct()
    {
        header("Content-type: text/html; charset = utf-8");
        parent::__construct();

        $this->load->model("user_model", 'user');
        $this->load->model("gym_model", 'gym');
        $this->user->loadLanguage($this->lang);
        $this->load->database();
        $this->_install();
    }

    public function _install()
    {
        $this->db->query("DROP TABLE IF EXISTS `user`;

       ");
        $this->db->query("CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `reffered_by` int(11) NOT NULL DEFAULT '-1',
  `gender` enum('1','2') NOT NULL COMMENT '1->male,2->female',
  `status` enum('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '0 -Pending, 1 - Activate, 2 - Rejected, 3 - Hold',
  `createdon` datetime NOT NULL,
  `updatedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `country_list` int(255) DEFAULT NULL,
  `state_select` varchar(255) DEFAULT NULL,
  `city_select` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

       ");
    }

    public function index()
    {
        $this->data['cat'] = $this->user->get_cats();
        $this->data['cntry'] = $this->user->get_cntrs();
        $this->data['latest'] = $this->user->get_latest();

        $this->data['recently_viewed'] = array();
        $usr_id = $this->session->userdata('user_id');
        if ($usr_id) {
            $this->data['recently_viewed'] = $this->gym->get_recent_viewed_gym($usr_id);

        }
        //$this->data['page'] = "main";
        $this->data['countdown_time'] = $this->user->get_countdown_time();
        //print_r($this->data['recently_viewed']);
        //exit;
        //var_dump($_GET);

        $this->load->view('main', $this->data);
    }

    public function get_gymname()
    {
        if (isset($_GET['term'])) {
            $gymname = strtolower($_GET['term']);
            $this->user->get_gymname($gymname);
        }
    }

    public function get_states($id)
    {
        extract($_POST);
        $data = $this->user->get_states($id);
        $attributes = 'id="state"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" onchange="get_cities(this.value)" required';
        $data['0'] = 'State';
        echo form_dropdown('state', array_map('strtoupper', $data), '0', $attributes);
    }

    public function get_states_for_register_form($id)
    {
        extract($_POST);
        $data = $this->user->get_states($id);
        $attributes = 'id="state_select" style="width: 30%" onchange="get_cities_form_registration(this.value)" required';
        $data['0'] = 'State';
        echo form_dropdown('state_select', array_map('strtoupper', $data), '0', $attributes);
    }

    public function get_cities_for_register_form($x)
    {
        $data = $this->user->get_cities($x);
        $attributes = 'id="city_select" style="width: 20%" ';
        $city[''] = '';
        echo form_dropdown('city_select', array_map('strtoupper', $data), '', $attributes);
    }

    public function get_cities($x)
    {
        $data = $this->user->get_cities($x);
        $attributes = 'id="city"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "';
        $city[''] = '';
        echo form_dropdown('city', array_map('strtoupper', $data), '', $attributes);
    }

    public function get_latest()
    {
        $res = $this->user->get_latest();
        print_r($res);
    }

    public function signup_conf($x = '')
    {

        if ($x != '' && $x != 0) {
            $this->db->where('id', $x)->update('user', array('status' => '1'));
            redirect('user');
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function signup($x = '')
    {
        if ($x != '' && $x != 0) {
            $this->db->where('id', $x)->update('user', array('status' => '1'));
            $data['id'] = $x;
            $this->load->view('signup', $data);
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function exist_user()
    {
        extract($_POST);
        $res = $this->user->user_exist();
        echo $res;
    }

    public function exist_pass()
    {
        extract($_POST);
        $res = $this->user->pass_exist();
        echo $res;
    }

    public function exist_email()
    {
        extract($_POST);
        //echo $email;
        $res = $this->user->email_exist();
        echo $res;
    }

    public function register()
    {

        extract($_POST);

        if (isset($user_name)) {
            $str = trim($user_name);
            $cnt = strlen($user_name);
            if ($cnt >= 6) {
                $unique = $this->user->user_check();
            }
        }


        if (@$unique) {
            $res = $this->user->user_register();

            if ($res) {
                /****** email config settings ******/
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                // mail sending process //

                $this->email->from('admin@gymscanner.com', 'Gym Scanner');
                $this->email->to($email);
                $this->email->subject('Confirmation Message');

                $val = "<html><body>";
                $val .= "<h4>Dear " . ucfirst($first_name) . ",</h4>";
                $val .= "<p>Thank you registering with GymScanner.com</p>";
                $val .= "<p>To confirm your registration, please <a href='" . USER_URL . "signup/" . $res . "'>click here</a></p>";
                $val .= "<p>Your Credentials are:</p>";
                $val .= "<div><br/>";
                $val .= "<p><b>Username :</b>" . $user_name . "</p>";
                $val .= "<p><b>Password :</b>" . $password . "</p>";
                $val .= "</div>";
                $val .= "<p>From the Gymscanner.com team, we thank you for getting involved. Stay fit.</p>";
                $val .= "<div ><br/>";
                $val .= "With Regards,<br/>";
                $val .= "Team Gymscanner<br/>";
                $val .= "Gymscanner.com<br/>";
                $val .= "1005 West Champlin Drive<br/>";
                $val .= "Minnesota 55030, USA<br/>";
                $val .= "</div>";
                $val .= "</div><br/><hr style='color:#800000;' />";
                $val .= "<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
                $val .= "</div></body></html>";

                $this->email->message($val);
                $this->email->send();
                // end mail sending process //
                echo '1';
            } else
                echo 'error';
        } else
            echo '' /*'User already exist.'*/
            ;
    }

    public function login()
    {
        extract($_POST);
        $res = $this->user->login();
//		echo $res;
        if ($res == 1)
            echo $cur_url;
        else
            echo 'The username and password you entered did not match our records. Please check and try again.';
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(USER_URL);
    }

    public function chng_pwd()
    {
        //$uid = $this->session->userdata['user_id'];
        //$table = $this->session->userdata['level']=='1'? 'supplier': 'user';
        $data['page'] = 'profile';
        $data['mode'] = 'change_pwd';
        //$res = $this->db->where('id',$uid)->get($table)->row();
        $this->load->view('template', $data);
    }

    public function change_pwd()
    {
        extract($_POST);
        $uid = $this->session->userdata['user_id'];
        $table = $this->session->userdata['level'] == '1' ? 'supplier' : 'user';
        //$this->form_validation->set_rules('old_password', 'Current Password', 'required');
        //$this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
        //$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        //if ($this->form_validation->run() == FALSE) {
        //$data['page'] = 'profile';
        //$data['mode'] = 'change_pwd';
        //$res = $this->db->where('id',$uid)->get($table)->row();
        //$this->load->view('template',$data);
        //}else{
        $res = $this->db->where(array('id' => $uid, 'password' => $old_password))->get($table)->num_rows();
        echo $res;
        if ($res > 0) {
            if ($this->db->where('id', $uid)->update($table, array('password' => $password))) {
                //$this->session->set_flashdata('error', 'Password changed Successfully...');
                redirect(USER_URL . 'chng_pwd');
            }
        } else {
            //$this->session->set_flashdata('error', 'Invalid current password');
            $data['page'] = 'profile';
            $data['mode'] = 'change_pwd';
            $res = $this->db->where('id', $uid)->get($table)->row();
            $this->load->view('template', $data);
        }

        //}
    }

    public function forgetpwd()
    {
        $data['page'] = 'forgotpwd';
        $this->load->view('template', $data);
    }

    public function send_pwd()
    {
        extract($_POST);
        //print_r($_POST); //exit;
        if (isset($level))
            $table = 'supplier';
        else
            $table = 'user';
        //echo $table; //exit;
        $res = $this->db->where(array('user_name' => $user_name, 'email' => $email))->get($table)->num_rows();
        //echo $this->db->last_query();
        //echo $res;exit();
        if ($res > 0) {
            $result = $this->db->where(array('user_name' => $user_name, 'email' => $email))->get($table)->row();
            $lng = 10;
            $pwd = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $lng);
            $this->db->where('id', $result->id)->update($table, array('password' => $pwd));

            /****** email config settings ******/
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['smtp_port'] = 25;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            // mail sending process //

            $this->email->from('admin@gymscanner.com', 'Gym Scanner');
            $this->email->to($result->email);
            $this->email->subject('Confirmation Message');

            $val = "<html><body>";
            $val .= "<h4>Dear " . ucfirst($result->first_name) . ",</h4>";
            $val .= "<p>You recently requested your password to be reset.</p>";
            $val .= "<p>Your new credentials are:</p>";
            $val .= "<div>";
            $val .= "<p><b>Username :</b>" . $user_name . "</p>";
            $val .= "<p><b>Password :</b>" . $pwd . "</p>";
            $val .= "<p>You may now login and change your password to your liking.</p>";
            $val .= "</div>";
            $val .= "<p>From the Gymscanner.com team, we thank you for getting involved. Stay fit.</p>";
            $val .= "<div ><br/>";
            $val .= "With Regards,<br/>";
            $val .= "Team Gymscanner<br/>";
            $val .= "Gymscanner.com<br/>";
            $val .= "1005 West Champlin Drive<br/>";
            $val .= "Minnesota 55030, USA<br/>";
            $val .= "</div>";
            $val .= "</div><br/><hr style='color:#800000;' />";
            $val .= "<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
            $val .= "</div></body></html>";

            $this->email->message($val);
            $this->email->send();
            // end mail sending process //

            $this->session->set_flashdata('success', 'Password sent to your email...');
            $data['page'] = 'forgotpwd';
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('error', 'Please provide valid Credentials...');
            $data['page'] = 'forgotpwd';
            $this->load->view('template', $data);
        }
    }

    public function profile_view($x = '')
    {
        if ($x != '' && $x != 0) {
            if (isset($this->session->userdata['type']))
                $table = 'supplier';
            else
                $table = 'user';

            $res = $this->db->where(array('id' => $x, 'status' => '1'))->get($table)->num_rows();
            //echo $this->db->last_query(); exit;
            if ($res > 0) {
                $data['res'] = $this->db->where(array('id' => $x, 'status' => '1'))->get($table)->row();
                $data['page'] = 'profile';
                $data['mode'] = 'view';
                $this->load->view('template', $data);
            } else {
                $data['page'] = 'error';
                $this->load->view('template', $data);
            }
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function edit_profile($x = '')
    {
        $x = $this->session->userdata['user_id'];
        if (isset($this->session->userdata['type']))
            $table = 'supplier';
        else
            $table = 'user';
        $res = $this->db->where(array('id' => $x, 'status' => '1'))->get($table)->num_rows();
        if ($res > 0) {
            $data['res'] = $this->db->where(array('id' => $x, 'status' => '1'))->get($table)->row();
            $data['page'] = 'profile';
            $data['mode'] = 'edit';
            $this->load->view('template', $data);
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function profile()
    {
        extract($_POST);
        //print_r($_POST);
        if (isset($this->session->userdata['level']))
            $table = 'supplier';
        else
            $table = 'user';
        $data = array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'password' => $password);
        $this->db->where('id', $uid)->update($table, $data);
        redirect(site_url('profile/' . $uid));
    }

    public function get_username()
    {
        extract($_POST);
        if ($_POST['level'] == 1) {
            $res = $this->db->where('user_name', $user_name)->get('supplier')->num_rows();
            if ($res > 0) {
                echo '<font color="red">Username already exist</font>';
            } else {
                echo '';
            }
        } else {
            $res = $this->db->where('user_name', $user_name)->get('user')->num_rows();
            if ($res > 0) {
                echo '<font color="red">Username already exist</font>';
            } else {
                echo '';
            }
        }
    }

    public function search()
    {
        if (isset($_POST['cat_id'])) {
            extract($_POST);
            //print_r($_POST);exit;


            if (isset($adv)) {
                $output = array();
                foreach ($adv as $key => $value) {
                    $output[] = $key;
                }
                $output = implode(',', $output);
            } else {
                $output = '';
            }
            $newdata = array(
                'cat_id' => $cat_id,
                'cntry' => $cntry,
                'state' => $state,
                'city' => $city,
                'zipcode' => $zipcode,
                'search_name' => $search_name,
                'adv' => $output
            );
            $this->session->set_userdata($newdata);
            $this->data['cat'] = $this->user->get_cats();
            $this->data['cntry'] = $this->user->get_cntrs();
            $this->data['states'] = $this->user->get_states($this->session->userdata('cntry'));
            $this->data['cities'] = $this->user->get_cities($this->session->userdata('state'));
            //print_r($this->data['cities']);exit;
            $resultset = $this->user->getgyms();

            $this->data['cnt_det'] = $this->user->get_cnt();
            $this->data['gyms'] = $resultset;
            //$this->dev($resultset);
            //$this->dev($this->data['gyms']);

            $this->data['containt'] = $this->load->view('datatable', $this->data, true);
            $this->data['page'] = 'gyms';
            $this->load->view('template', $this->data);
        } else {
            redirect('user');
        }
    }

    public function gym_imgs($x = '')
    {

        $gym_id = $x;
        $res = $this->db->where('id', $gym_id)->get('gym')->num_rows();

        if ($res > 0) {
            if (isset($gym_id)) {
                $usr_id = $this->session->userdata('user_id');
                if ($usr_id) {
                    //updated viewed details
                    $this->gym->update_recently_viewed($gym_id, $usr_id);
                }
                $this->data['cat'] = $this->user->get_cats();
                $this->data['cntry'] = $this->user->get_cntrs();

                if (null !== ($this->session->userdata('cntry')))
                    $this->data['states'] = $this->user->get_states($this->session->userdata('cntry'));

                if (null !== ($this->session->userdata('state')))
                    $this->data['cities'] = $this->user->get_cities($this->session->userdata('state'));

                $this->data['gym_det'] = $this->user->get_images($gym_id);

                $this->data['gym_reviews'] = $this->gym->get_reviews($gym_id);


                $this->data['gym_images'] = $this->db->where('gym_id', $gym_id)->get('gym_images')->result();

                $this->data['page'] = 'gym_det';
                $this->load->view('template', $this->data);
            } else {
                redirect('user');
            }
        } else {
            redirect('user');
        }
    }

    public function view_gym($gym_id)
    {
        $res = $this->db->where('id', $gym_id)->get('gym')->num_rows();
        if ($res > 0) {

            $usr_id = $this->session->userdata('user_id');
            if ($usr_id) {
                //updated viewed details
                $this->gym->update_recently_viewed($gym_id, $usr_id);
            }
            $this->data['cat'] = $this->user->get_cats();
            $this->data['cntry'] = $this->user->get_cntrs();
            $this->data['states'] = $this->user->get_states($this->session->userdata('cntry'));
            $this->data['cities'] = $this->user->get_cities($this->session->userdata('state'));
            $this->data['gym_det'] = $this->user->get_images($gym_id);
            $this->data['gym_reviews'] = $this->gym->get_reviews($gym_id);

            $this->data['gym_images'] = $this->db->where('gym_id', $gym_id)->get('gym_images')->result();

            $this->data['page'] = 'gym_det';
            $this->load->view('template', $this->data);

        } else {
            redirect('user');
        }
    }

    public function filter_search()
    {
        extract($_REQUEST);
        parse_str($_POST['ser_data'], $searcharray);
        $this->session->unset_userdata('speciality');
        $this->session->unset_userdata('byname');
        $this->session->unset_userdata('rating');
        $this->session->unset_userdata('price');
        $this->session->set_userdata($searcharray);

        $resultset = $this->user->getgyms_filter($searcharray);
        $this->data['cnt_det'] = $this->user->getcnt_filter();
        //print_r($this->data['cnt_det']); die();
        $this->data['gyms'] = $resultset;
        echo $this->data['containt'] = $this->load->view('datatable', $this->data, true);
    }

    public function filter_types($value)
    {
        $resultset = $this->user->getgyms_filter($value);
        $this->data['cnt_det'] = $this->user->getcnt_filter();
        $this->data['gyms'] = $resultset;
        echo $this->data['containt'] = $this->load->view('datatable', $this->data, true);
    }

    public function get_mapdet($id)
    {
        $this->data['gym_det'] = $this->user->get_mapdet($id);
    }

    public function regst()
    {
        extract($_POST);
        /****** email config settings ******/
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $data = array('company_name' => $cname,
            'cat_id' => $cat_id,
            'company_email' => $cemail,
            'company_url' => $url,
            'telephone' => $tel_ph,
            'cnt_id' => $cntry,
            'city_id' => $city,
            'contact_name' => $uname,
            'ph_no' => $phno,
            'email' => $email,
            'created_on' => mktime());
        $this->db->insert('contact_details', $data);
        $cat_name = $this->db->get_where('category', array('id' => $cat_id))->row();
        $cnt_name = $this->db->get_where('country', array('id' => $cntry))->row();
        $city_name = $this->db->get_where('city', array('id' => $city))->row();

        // mail sending process //

        $this->email->from('admin@provab.com', 'Gym Scanner');
        $this->email->to($email);
        $this->email->subject('Your contact form has been submitted');

        $val = "<html><body>";
        $val .= "Dear " . $uname . ",";
        $val .= "<h3>Thanks for Showing your Interest</h3>";
        $val .= "<h4>Your details are:</h4>";
        $val .= "<table  cellspacing='1' width='100%'>";
        $val .= "<tr style='color:blue;background:lightblue;'>
			        <th>Comapny Name</th>
			        <th>Category Name</th>
			        <th>Company email</th>
			        <th>Telephone</th>
			        <th>City</th>
			        <th>Country</th>
					<th>Contact Name</th>
					<th>Contact Mobile</th>
					<th>Contact email</th>
      			</tr>";
        $val .= "<tr style='background:lightgreen;' valign='top'>
		          <td>" . $cname . "</td>
		          <td>" . $cat_name->categoryname . "</td>
				  <td>" . $cemail . "</td>
				  <td>" . $tel_ph . "</td>
			 	  <td>" . $city_name->city_name . "</td>
				  <td>" . $cnt_name->country_name . "</td>			        
				  <td>" . $uname . "</td>
				  <td>" . $phno . "</td>
				  <td>" . $email . "</td>";
        $val .= "</tr></table>";
        $val .= "<div ><br/>";
        $val .= "With Regards,<br/>";
        $val .= "Gym Scanner Management<br/>";
        $val .= "</div></body></html>";

        $this->email->message($val);
        $this->email->send();
        // end mail sending process //
        $this->data['page'] = 'user/regst';
        $this->load->view('template', $this->data);
    }

    public function sendlocation()
    {
        extract($_POST);

        $gym_det = $this->user->get_images($gym_id);
        $lat = $lon = $cntryname = $cityname = $gymname = $gstreet = "";

        $lat = $gym_det->gmap_lat;
        $lon = $gym_det->gmap_long;

        $cntryname = $gym_det->state;
        $cityname = $gym_det->city;
        $gymname = ucfirst($gym_det->gymname);
        $gstreet = ucfirst($gym_det->gstreet);

        $map_img = "http://maps.googleapis.com/maps/api/staticmap?center=" . $lat . "," . $lon . "&zoom=13&size=600x300&maptype=roadmap&markers=color:blue|label:S|" . $lat . "," . $lon;
        $html_msg = '
		<div style="width:600px; height:400px;border:2px #ccc solid;">
			  <img src="' . $map_img . '"><br/>
				<div style="width:350px; height:10px; ">
<span style="float:left;padding-left:5px;"><b>Gym Name :</b> ' . $gymname . '</span><br/>
<span style="float:left;padding-left:5px;"><b>Gym Address:</b> ' . $gstreet . ', ' . $cityname . ', ' . $cntryname . '</span><br/>
<span style="float:left;padding-left:5px;"><a href="' . USER_URL . 'gym_imgs/' . $gym_det->id . '" target="_blank">View full details</a></span>
				</div>
			</div>';

        /****** email config settings ******/
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('admin@provab.com', 'Gym Scanner');
        $this->email->to($mail_id);
        $this->email->subject('Find gym location on map');

        $this->email->message($html_msg);
        $this->email->send();

        redirect(site_url('fac_det/' . $gym_id), 'refresh');
    }

    public function renew()
    {
        if (isset($_POST['renew']) && $this->session->userdata('cntry') != '') {
            extract($_POST);
            $this->data['cities'] = $this->user->get_cities($this->session->userdata('cntry'));
            $gym_id = $this->input->post('renew');
            $this->load->model('admin/cat_model', 'cat');
            $this->data['cat'] = $this->cat->dropdown('id', 'categoryname');
            $this->load->model('admin/country_model', 'cntry');
            $this->data['cntry'] = $this->cntry->dropdown('id', 'country_name');
            $this->data['offrs'] = $this->user->get_offers();
            $gimages = $this->user->get_images($gym_id);
            $this->data['gym_det'] = $gimages;
            $this->data['page'] = 'user/gym_offers';
            $this->load->view('template', $this->data);
        } else {
            redirect('user');
        }
    }

    public function gymreview()
    {

        extract($_POST);
        if (count($_POST) > 0) {
            if ($user_level == '') {
                $user_level = "user";
            } else {
                $user_level = "supplier";
            }
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'comment' => $comment,
                'gym_rating' => $rating,
                'gym_id' => $gym_id,
                'user_level' => $user_level,
                'createdon' => date('Y-m-d H:i:s')
            );
            $this->db->insert('reviews', $data);

            redirect(site_url('fac_det/' . $gym_id));
        }
    }

    public function get_new_map()
    {
        $id = $this->input->post('id_val');
        $result = $this->user->get_mapdet($id);

        $this->data['gym_det'] = $result[0];

        $this->load->view('user/gym_det_map', $this->data);
    }

    public function get_popup()
    {
        $gym_id = $this->uri->segment(4);
        $this->data['gym_det'] = $this->user->get_mapdet($gym_id);
        echo "<pre>";
        print_r($this->data['gym_det']);
    }

    public function user_details()
    {
        if (isset($_POST['gym_id'])) {
            extract($_POST);
            $off = $this->user->get_offer_details();
            $due = $off->name; //die();
            $ofprice = $off->price - ($off->price * $off->offer) / 100;
            $date = date("Y-m-d", strtotime($datepicker));
            $end_date = date("Y-m-d", strtotime("$date +$due month"));
            $this->session->set_userdata(array('uname' => $uname, 'phno' => $phno, 'email' => $email, 'joining_date' => $date, 'end_date' => $end_date,
                'mem_id' => $mem_id, 'price' => $off->price, 'offer' => $off->offer, 'offer_price' => $ofprice, 'gym_id' => $gym_id));

            $this->data['page'] = 'user/payment_method';
            $this->load->view('template', $this->data);
        } else {
            redirect('user');
        }

    }

    public function authorized_form()
    {
        $this->data['page'] = 'user/payment_method';
        $this->load->view('template', $this->data);
    }

    public function payment_final()
    {
        //print_r($_POST);
        include('AuthorizeNet.php');

        if ($_SERVER['HTTP_HOST'] == 'localhost') {

            define("AUTHORIZENET_API_LOGIN_ID", "6pS7c2cbrdFq");
            define("AUTHORIZENET_TRANSACTION_KEY", "5u8955zE3PK4z534");
            define("AUTHORIZENET_SANDBOX", true);
        } else {

            define("AUTHORIZENET_API_LOGIN_ID", "9AM6dm4p5c");
            define("AUTHORIZENET_TRANSACTION_KEY", "64ALPz2dJ7k62kkb");
            define("AUTHORIZENET_SANDBOX", false);
        }
        $sale = new AuthorizeNetAIM;

        $sale->amount = $_POST['amount'];
        $sale->card_num = $_POST['card_number'];
        $sale->exp_date = $_POST['exp_date'];
        $response = $sale->authorizeAndCapture();

        $this->user->storeUserDetails(); //die();
        if ($response->response_code != 1) {

            $trans_id = $response->transaction_id;
            $this->data['error'] = $response->response_reason_text;
            $this->data['page'] = 'user/authorize_payment_error_page';
            $this->load->view('template', $this->data);
        } else {

            //$this->webservices->storeUserDetails();
            //need to check here weather the response is accepted or declined
            // $this->user->storeUserDetails(); //die();
            $trans_ids = $response->transaction_id;
            redirect('user/paypal_payment_success');
        }

    }

    public function paypal_payment_success()
    {


        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('admin@provab.com', 'Gym Scanner');
        $this->email->to($this->session->userdata('email'));
        $this->email->subject('Your renewal subscription has been activated');

        $val = "<html><body>";
        $val .= "Dear " . $this->session->userdata('uname') . ",";
        $val .= "<h3>Your renewal subscription details are :</h3>";
        $val .= "<table  cellspacing='1' width='100%'>";
        $val .= "<tr style='color:blue;background:lightblue;'>
			        <th>Name</th>
			        <th>Phone Number</th>
			        <th>Price</th>
			        <th>Offer</th>
			        <th>Offer price</th>
			        <th>Suscription of</th>
					<th>From date</th>
					<th>To date</th>
      			</tr>";
        $val .= "<tr style='background:lightgreen;' valign='top'>
		          <td>" . $this->session->userdata('uname') . "</td>
		          <td>" . $this->session->userdata('phno') . "</td>
				  <td>" . $this->session->userdata('price') . "</td>
				  <td>" . $this->session->userdata('offer') . "</td>
			 	  <td>" . $this->session->userdata('offer_price') . "</td>
				  <td>" . $this->session->userdata('offer_name') . "</td>			        
				  <td>" . $this->session->userdata('joining_date') . "</td>
				  <td>" . $this->session->userdata('end_date') . "</td>";
        $val .= "</tr></table>";
        $val .= "<div ><br/>";
        $val .= "With Regards,<br/>";
        $val .= "Gym Scanner Management<br/>";
        $val .= "</div></body></html>";

        $this->email->message($val);
        $this->email->send();

        $this->data['page'] = 'user/payment_success';
        $this->load->view('template', $this->data);
    }

    public function addChoosenOffers()
    {
        extract($_GET);
        //$this->dev($_GET);

        if ($choose == 1) {
            $data = array(
                'offer_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'offered_on' => date('Y-m-d h:i:s')
            );
            $this->db->insert('offered_gyms', $data);
            echo json_encode('ok');
            exit;
        }
        if ($choose == 2) {
            $data = array(
                'gym_id' => $id,
                'name' => 'Requesting for offer',
                'user_id' => $this->session->userdata('user_id'),
                'type' => '2',
                'start_date' => date('Y-m-d', strtotime($start_date)),
                'end_date' => date('Y-m-d', strtotime($end_date)),
                'createdon' => date('Y-m-d h:i:s')
            );
            $this->db->insert('offers', $data);
            echo json_encode('ok');
            exit;
        }

    }

}
