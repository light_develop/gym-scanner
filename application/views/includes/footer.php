</div>
	<div id="footer">
		2013 &copy; Provab Dashboard.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/js/bootstrap-timepicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/additional-methods.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-slimscroll/jquery.slimscroll.min.js"	type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.blockui.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.cookie.js" type="text/javascript"></script>
	<!--[if lt IE 9]><script src="<?php echo base_url()?>themes/admin/js/excanvas.js"></script><script src="<?php echo base_url()?>themes/admin/js/respond.js"></script><![endif]-->
	
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/data-tables/DT_bootstrap.js"></script>
	<script src="<?php echo base_url()?>themes/admin/js/scripts.js" type="text/javascript"></script>
	<script type="text/javascript">jQuery(document).ready(function(){App.setMainPage(true);App.init()});</script>
</body>
</html>