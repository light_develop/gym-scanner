<?php $this->load->view ('includes/header'); ?>
<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
<div class="dash_bord">
<div class="top_menu_wrapp">
  <ul>
    <li><a href="#"><?php echo $this->lang->line("Confirmation Page"); ?></a></li>
  </ul>
  
  <div class="clear"></div>
</div>
<div class="wi_wrapp">
  
  <div class="dimound_details">
    <h3 align="center"><?php echo $this->lang->line("Thank you for confirming your account"); ?> <a
          href="<?= base_url() ?>"> <?php echo $this->lang->line("Please proceed to login"); ?> </a></h3>

  </div>
</div>

</div>
</div>
<?php $this->load->view ('includes/footer'); ?>