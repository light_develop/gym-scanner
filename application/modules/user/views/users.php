<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
<div class="dash_bord" id="getuser">
<div class="top_menu_wrapp">
  <ul>
      <li><a style="padding:0px 10px 0px 0px;"><?php echo $this->lang->line("Users List"); ?></a></li>
      <li><a href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a></li>
  </ul>
  
    <div class="user_le">
     <div class="my_right">
         <a href="<?php echo site_url('add_users') ?>" class="viw_usr"
            style="float:right"><?php echo $this->lang->line("+add"); ?></a>
      <div class="clear"></div>
      </div> 
    </div>
  
  <div class="clear"></div>
</div>
<div class="wi_wrapp">
  <div class="name_wrapp">
      <div class="first_gy" style="width:7%"><h2><?php echo $this->lang->line("Sl No."); ?></h2></div>
      <div class="second_gy" style="width:20%"><h2><?php echo $this->lang->line("Name"); ?></h2></div>
      <div class="third_gy" style="width:20%"><h2><?php echo $this->lang->line("User Name"); ?></h2></div>
      <div class="user_gy" style="width:30%"><h2><?php echo $this->lang->line("Email"); ?></h2></div>
    <div class="act_gy" style="width:20%"><h2> </h2></div>

    <div class="clear"></div>
  </div>
  <?php if(isset($res) && is_array($res) && count($res)){ $i = 1;?>
    <?php foreach ($res as $key => $value) { ?>
  <div class="dimound_details" id="div_<?php echo $value->id;?>">
    <div class="first_gy" style="width:7%"><p><?php echo $i;?></p></div>
    <div class="second_gy hei_n" style="min-height:38px; width:20%"><p style="margin-top:9%;"><?php echo ucfirst($value->first_name).' '.ucfirst($value->last_name);?></p></div>
    <div class="third_gy hei_n" style="min-height:38px; width:20%" ><p style="margin-top:9%;"><?php echo $value->user_name; ?></p></div>
    <div class="user_gy hei_n" style="min-height:38px;width:30%" ><p style="margin-top:9%;"><?php echo $value->email; ?></p></div>
    <div class="act_gy hei_n" style="margin-top:16px; min-height:36px">
      <a href="<?php echo site_url('view_user/'.$value->id);?>"><button class="view_p"></button></a>
      <a href="<?php echo site_url('edit_user/'.$value->id);?>"><button class="edit_p"></button></a>
      <button onclick="del_usr(<?php echo $value->id;?>)" class="delete_p"></button>

      <?php if($value->status==0){?>
        <a class="pending_p"></a>
      <?php } else if($value->status == 1){ ?>
        <a id="<?php echo $value->id; ?>" onclick="status_change_user(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>')" class="active_p"></a>
      <?php } else if($value->status == 2){?>
        <a class="dislike_red_p"></a>
      <?php } else if($value->status == 3){?>
        <a id="<?php echo $value->id; ?>" onclick="status_change_user(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>')" class="sas_p"></a>
      <?php } ?>

    </div>
    <div class="clear "></div>
  </div>
  <?php $i++; } } ?>
</div>

</div>
</div>

<script>
  function del_usr(selected){
      $.ajax({
        type : "POST",
        url : "<?php echo SUPPLIER_URL?>del_usr",
        data : {
          'id':selected
        },
        success : function(data){
          if(data == 1){
            alert('sorry, User allocated to gym');
          }else{
            $('#div_'+selected).hide();
          }
        },
        error : function(){
          alert("We are unable to do your request. Please contact webadmin");
        }
      });
  }
</script>
<script type="text/javascript">
  function status_change_user(id,status,aid)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo SUPPLIER_URL;?>change_status_user",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '1'){
                  $('#'+aid).removeClass('active_p').addClass('sas_p');
                  $('#'+aid).attr('onclick','status_change_user('+id+',3,"'+aid+'")');
                }else if(status == '3'){
                  $('#'+aid).removeClass('sas_p').addClass('active_p');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>
<style>
#getuser .act_gy {
    float: right;
    margin-left: 0;
    padding-left: 0;
    width: 20.2719%;
}
@media screen and (max-width: 750px){
	#getuser .act_gy {
		float: right;
		margin-left: 0;
		padding-left: 0;
		width: 20.2719%;
	}
}
@media screen and (max-width: 550px){
	#getuser .my_right {
		float: right;
		width: 20%;
	}
	#getuser .top_menu_wrapp ul li a {
		border-right: 1px solid #5b0708;
		color: #fff;
		display: block;
		font-family: gothic;
		font-size: 15px;
		font-weight: bold;
		padding: 0 10px;
		text-transform: uppercase;
	}
	#getuser .viw_usr {
		color: #fff;
		display: block;
		font-family: gothic;
		font-size: 15px;
		font-weight: bold;
		margin-left: 3px;
		text-transform: uppercase;
	}
}
@media screen and (max-width: 350px){
	#getuser .my_right {
		float: right;
		width: 10%;
	}
	#getuser .top_menu_wrapp ul li a {
		border-right: 1px solid #5b0708;
		color: #fff;
		display: block;
		font-family: gothic;
		font-size: 13px;
		font-weight: bold;
		padding: 0 10px;
		text-transform: uppercase;
	}
	#getuser .viw_usr {
		color: #fff;
		display: block;
		font-family: gothic;
		font-size: 13px;
		font-weight: bold;
		margin-left: 3px;
		text-transform: uppercase;
	}
}

</style>