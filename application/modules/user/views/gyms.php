<div class="clearfix"></div>
<section style="width: 100%;"> <img src="<?php echo base_url()?>themes/user/images/inner-img.png" class="img-responsive" width="100%" id="blur"/> </section>
<div class="clearfix"></div>
<div class="container serach_result">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
   <aside class="col-lg-3 col-md-3 col-sm-12 hidden-xs" >
   <div class="serach_inner col-lg-12 col-md-12 col-sm-12 ">
       <h1 class="heading_gym"><?php echo $this->lang->line("I AM LOOKING FOR"); ?></h1>
    <form action="<?php echo site_url('search');?>" method="post" id="search_form" >
    <div class="col-lg-12 col-md-12 col-sm-12 ">
      <div class="search_bg col-md-12 col-sm-12  ">
      	
        <div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
      		<?php $attributes= 'id="cat_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
    			echo form_dropdown('cat_id', $cat,$this->session->userdata('cat_id'),$attributes);?>
    	  </div>
    	    	
        <div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
		
        	<?php  $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " ';
      			    	
      if(null !== ($this->session->userdata('cntry')) )
			{
				$country = @$this->session->userdata('cntry');
				$cntry_z = array('0' => 'Country');	
				$states_z = array('0' => 'States');			
				$cities_z = array('0' => 'All');

				$cntry = $cntry_z + $cntry;        
				$states = $states_z + $states;
       	$cities = $cities_z + $cities;
			}
   
		    	echo form_dropdown('cntry', $cntry, @$this->session->userdata('cntry'),$attributes);
		    ?><font color="red"><span id="error_cntry"></span></font>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-6 padding_art ">
        	<?php $attributes= 'id="state"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" onchange="get_cities(this.value)"';
		    	echo form_dropdown('state', $states, @$this->session->userdata('state'),$attributes);                     
		    ?><font color="red"><span id="error_state"></span></font> 
        </div>
        <div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
        	<?php $attributes= 'id="city"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ';
		    	echo form_dropdown('city', $cities, $this->session->userdata('city'),$attributes);                     
		    ?> <font color="red"><span id="error_city"></span></font>
        </div>
		<div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
        	<input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" placeholder="SEARCH BY NAME" id="search_name" name="search_name" value="<?php echo @$this->session->userdata('search_name').'' ;?>" />
        </div>
        <div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
        	<input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" placeholder="ZIPCODE" name="zipcode" value="<?php echo @$this->session->userdata('zipcode').'' ;?>" />
        </div>

    </div>
    </div>
    <?php $adv = explode(",",str_replace(" ","",$this->session->userdata['adv']));?>
    <div class="clearfix"></div>
        <div class="col-lg-12 search_bg_1"><?php echo $this->lang->line("ADVANCE SEARCH"); ?></div>
    <div class="col-lg-12">
    <span class="adv_search">
    <input type="checkbox" name="adv[pilates]" value="1" <?php if(in_array('pilates',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("PILATES / YOGA"); ?></label>
    </span>
    <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[mma]" value="1" <?php if(in_array('mma',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("MMA"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[boxing]" value="1" <?php if(in_array('boxing',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("BOXING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[cross]" value="1" <?php if(in_array('cross',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("CROSS TRAINING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[spinning]" value="1" <?php if(in_array('spinning',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("SPINNING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[gymnastics]" value="1" <?php if(in_array('gymnastics',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("GYMNASTICS"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[martial]" value="1" <?php if(in_array('martial',$adv)) echo "checked";?>/>
    <label><?php echo $this->lang->line("MARTIAL ARTS"); ?></label>
    </span>
    </div>
        <div class="col-lg-12">
            <button class="search_gym_button"><?php echo $this->lang->line("Search"); ?></button>
        </div>
  
  </form>
  </div>
   </aside>
   <aside class="col-lg-9 col-md-9 col-sm-12 col-xs-12 righ_pnl" >

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fillter_bg">
  <form name="filter_search" method="post" id="filter_search">
      <span class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><p
              class="organize"><?php echo $this->lang->line("Organize results by"); ?></p></span>
    <span class="col-lg-9 col-md-9 col-sm-12 hidden-xs">
      <span class="fillter" style="border:none">
        <label><?php echo $this->lang->line("Gender"); ?></label>
          <select name="speciality" id="speciality">
              <option value="all" selected><?php echo $this->lang->line("All"); ?></option>
              <option value="3"><?php echo $this->lang->line("Both"); ?></option>
              <option value="1"><?php echo $this->lang->line("Male"); ?></option>
              <option value="2"><?php echo $this->lang->line("Female"); ?></option>
          </select>   
      </span>     
      <span class="fillter">
        <input id="checkbox1" type="checkbox" name="price" value="price">
        <label for="checkbox1"><?php echo $this->lang->line("Price"); ?></label>
      </span>   
      <span class="fillter">
        <input id="checkbox2" type="checkbox" name="rating" value="star">
        <label for="checkbox2"><?php echo $this->lang->line("Star Rating"); ?></label>
      </span>
      <span class="fillter">
        <input id="checkbox3" type="checkbox" name="byname" value="gymname">  
        <label for="checkbox3"><?php echo $this->lang->line("By Name"); ?></label>
      </span>
      <span class="fillter">
        <input id="checkbox4" type="checkbox" name="nearest" value="4" >     
        <label for="checkbox4"><?php echo $this->lang->line("Closest to me"); ?></label>
      </span>   
    </span> 
  </form>
  </div>
  
  <div id="right_kuwait">
    <?php echo $containt;?>
  </div> 

  </aside>     
  </div>
  <div class="clearfix"></div>
</div>

<script> 
   $( "#search_form" ).submit(function( event ) {
   	  var search_name  = document.getElementById('search_name').value;
      var cat = document.getElementById('cat_id').value;
      var cntry = document.getElementById('cntry').value;
      var state = document.getElementById('state').value;
      var city = document.getElementById('city').value;
      $('#error1').html('');
      $('#error_cntry').html('');
      $('#error_state').html('');
      $('#error_city').html('');
	  
	   if(search_name != ""){
			return true;  
		}
		if(cat == ''){
		  document.getElementById('error1').innerHTML = "Select any category";
		  return false;  
		}
		else if(cntry == "" || cntry == 0)
		{
		  document.getElementById('error_cntry').innerHTML = "Select any country";
		  return false;             
		  }
		else if(state == '' || state == 0)
		{
		  document.getElementById('error_state').innerHTML = "Select any state";
		  return false;             
		}
		else if(city == '')
		{
		  document.getElementById('error_city').style.display = "Select any city";
		  return false;              
		} 
		else
		{
		  return true;              
		}
  });


   function load_dropdown_content(selected_value){ 

       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_states/' + selected_value,
                   'async' : false
             }).responseText;
         $("#state").replaceWith(result);
      } 
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function showValues() {
    //$(".right_kuwait").html('');
    var str = $( "#filter_search" ).serialize();
    var result = $.ajax({
			'type' : 'post',
        	         'url' : '<?php echo USER_URL;?>filter_search',
        	         'data' : {'ser_data':str},
        	         'async' : false
    	       }).responseText;
    	   $("#right_kuwait").html(result);
  }
  $( "input[type='checkbox']").on( "click", showValues );
  showValues();
 
  $( "#speciality" ).on( "change", function(){
      showValues();
  });
   
</script>
<script>
$(function(){
  $("#search_name").autocomplete({
    source: "user/get_gymname" // path to the get_birds method
  });
});
</script>
<style>
.ui-corner-all .ui-state-focus{
	background-color:#8c2526;
	color:#ffffff;
}
#ui-id-1{
	max-height:500px;
	overflow-x:hidden;
}
</style>
