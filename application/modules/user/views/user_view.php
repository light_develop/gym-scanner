<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container">
	<div class="add_gym_wra">
		<h1><?php echo $this->lang->line("User Details"); ?> |<a
				href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a>|<a
				onclick="goBack()"><?php echo $this->lang->line("Back"); ?></a></h1>

		<div class="col-lg-12 ad_form_wr">
			<h2><?php echo $this->lang->line("First Name"); ?></h2>
		   		<p><?php echo $details->first_name;?></p>
		  	<div class="clear"></div>

			<h2><?php echo $this->lang->line("Last Name"); ?></h2>
	        	<p><?php echo $details->last_name;?></p>

			<div class="clear"></div>

			<h2><?php echo $this->lang->line("User Name"); ?></h2>
				<p><?php echo $details->user_name;?></p>

			<div class="clear"></div>

			<h2><?php echo $this->lang->line("Email"); ?></h2>
		  		<p><?php echo $details->email;?></p>
		 	<div class="clear"></div>

		</div>
		<div class="clear"></div>
	</div>
</div>
