<?php $this->load->view('includes/header'); ?>
<script src="<?=base_url();?>themes/user/js/jquery.bxslider.min.js"> </script>
<link href="<?=base_url();?>themes/user/css/jquery.bxslider.css" rel="stylesheet" type="text/css" media="all" />
<script>

$(document).ready(function(){
  $('.slider4').bxSlider({
    slideWidth: 300,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10,
	pager:0
  });
});
</script>

<div style="clear:both"></div>

<div class="content_wrap">

<div class="right_kuwait">
  <div class="gym_centers">
    
  <div class="gym_details" >
      <div><b class="cor"><?php echo $this->lang->line("Qortuba Fitness Center"); ?></b>
<div class="star"><img src="<?php echo base_url();?>themes/user/images/star-on.png"/>
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" /></div>
</div>
<div class="height5"></div>
<div>
    <p><?php echo $this->lang->line("L-RIGGAE, AL FARWANIYAH,"); ?></p>

    <p><?php echo $this->lang->line("GOVERNORATE,"); ?></p>

    <p><?php echo $this->lang->line("KUWAIT"); ?></p>
<div class="height5"></div>
<div class="height5"></div>
 </div>
 <div>

     <div class="map_location">
         <div class="text"><?php echo $this->lang->line("Map"); ?></div>
     </div>


     <div class="black_icon"></div>
  <div class="pink_icon"></div>
 </div>
</div>
<div class="gym_image"><img src="<?php echo base_url();?>themes/user/images/image.png" width="117" height="117"  class="b_radius"  /></div>

</div>


<div class="map">
<img src="image/000.jpg"/> </div>
<div class="slider">
    <div class="rit_heading"><?php echo $this->lang->line("gallery"); ?></div>
<div style="height:20px">&nbsp;</div>
<div class="slider4" style="float:left;">

  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/1.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/2.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/3.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/4.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/5.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/6.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/7.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/1.jpg"></div>
  <div class="slide"><img src="<?php echo base_url();?>themes/user/images/2.jpg"></div>
</div>
<div style="height:20px">&nbsp;</div>
    <button class="search_button"><?php echo $this->lang->line("Renew Subscription"); ?></button>
<div style="height:20px">&nbsp;</div>
</div>

</div>


<div class="left_search">
    <h2><?php echo $this->lang->line("Search"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
    <div class="fnt"><?php echo $this->lang->line("I am Looking"); ?></div>
<div><select class="ddl_select">
        <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>

<div class="height5"></div>
    <div class="fnt"><?php echo $this->lang->line("Country"); ?></div>
<div><select class="ddl_select">
        <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>

<div class="height5"></div>
    <div class="fnt"><?php echo $this->lang->line("City"); ?></div>
<div><select class="ddl_select">
        <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>


<br/>
<div><input name="name" type="button" value="Search"  class="search_button"/></div>
<div class="height5"></div>
</div>

<br/>

<div class="left_search">
    <h2><?php echo $this->lang->line("Filter"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
<div class="fnt"><input name="name" type="checkbox" value="value"/>&nbsp;&nbsp;Price</div>
<div class="fnt"><input name="name" type="checkbox" value="value" />&nbsp;&nbsp;By Name</div>
<div class="fnt"><input name="name" type="checkbox" value="value" />&nbsp;&nbsp;Start Rating</div>
</div>



<div style="clear:both;"></div>
</div>

</div>
<div class="clr"></div>
<?php $this->load->view('includes/footer'); ?>

</body>
</html>