<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container" id="add_offer_div">
<div class="add_gym_wra">
	<h1><? echo $this->lang->line('Create Offer') ?>|<a href="<?php echo site_url('facility'); ?>"
														style="color:#fff;"><? echo $this->lang->line('Dashboard') ?></a>
	</h1>
	  	<form action="<?php echo site_url('add_offer');?>" class="form-horizontal" method="post" id="add_offr"/>
      <div class="col-lg-12 ad_form_wr">	
        <?php if($this->session->flashdata('error') !='') { ?>
          <div class="error"> <?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>                 
        
		
		<div class="clear"></div>
		  <div style="float: left;">
			  <h2 class="textwidth"><? echo $this->lang->line('APPLY THIS OFFER TO ALL MY FACILITIES') ?></h2>
		  <?php  $res =  array_keys($gyms); while($element = current($gyms)) {
    //echo key($gyms)."\n";
     ?> <input type="hidden" value="<?php echo key($gyms);?>"  name="result[]"/> <?php next($gyms);
} ?>
		  <input type="checkbox" name="all_facility" id="all_facility" style="float: left; margin-top: 25px;" value="<?php print_r(array_keys($gyms)); ?>"/>
		</div>
		<div class="clear"></div>
		<div style="float: left;">
			<h2 class="textwidth"><? echo $this->lang->line('APPLY THIS OFFER TO SELECT FACILITIES') ?></h2>
		  <input type="checkbox" name="select_facility" id="select_facility" style="float: left; margin-top: 25px;"/>
		</div> 
		<div class="clear"></div>
		  <div id="facilities" style="display:none;">
			  <h2><? echo $this->lang->line('Select Facility') ?></h2>
			<?php 
			  $attributes = 'id="gym_id" class="form_big required" onchange="chk_gym(this.value)" '; 
			  $gyms[''] = 'Choose Facility';
			  //print_r($gyms);
			  echo form_dropdown('gym',$gyms,'',$attributes);?>
				<div class="clear"></div>
             <div id="error_mes"> <span class="error_gym"></span></div>
		</div>
		<div class="offerdeils">
			<h2><? echo $this->lang->line('OFFER DETAILS') ?></h2>
			  <div class="table_right" id="offer">            
				<div class="ofr_wraper">
				<table width="99%" class="new_tab table_right">
				  <tr class="row_cal">
					<td> 
					  <div class="top_clr_he boxs_mn top_clr_he">
						  <h2><? echo $this->lang->line('Duration') ?></h2>

						  <p><? echo $this->lang->line('Enter the duartion period of your gym/fitness center membership') ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he1">
						  <h2><? echo $this->lang->line('Membership Rates') ?></h2>

						  <p><? echo $this->lang->line('Your current membership rate before discount or perks') ?></p>
					  </div>
					</td>
					<td>    
					  <div class="boxs_mn top_clr_he2">
						  <h2><? echo $this->lang->line('Cash Discount') ?></h2>

						  <p><? echo $this->lang->line('The level cash discount you would like to offer?') ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he3">
						  <h2><? echo $this->lang->line('Special Offer') ?></h2>

						  <p><? echo $this->lang->line('Any special perks you would like to throw in?') ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he4">
						  <h2><? echo $this->lang->line('Customer gets') ?></h2>

						  <p><? echo $this->lang->line('This is what your customer gets today') ?></p>
					  </div>
					</td>
					<td></td>             
				  </tr>
				  <tr class="row_cal">
					  <td><input name="duration[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 1 Month') ?>" value="" type="text"
								 required/></td>
					  <td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: US$ 30') ?>" value="" type="text"
								 required/></td>
					  <td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: 10% off') ?>" value="" type="text"/>
					  </td>
					  <td><input name="offer[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 1 Week Free') ?>" value="" type="text"
								 required/></td>
					<td></td>
					<td style="width:14%;"><input name="plus" type="button" class="plus_btp add_more"></td>
				  </tr>
				  <tr class="row_cal">
					  <td><input name="duration[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 3 Months') ?>" value="" type="text">
					  </td>
					  <td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: US$ 75') ?>" value="" type="text">
					  </td>
					  <td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: 10% off') ?>" value="" type="text">
					  </td>
					  <td><input name="offer[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 10 Free Ses-') ?>" value=""
								 type="text"></td>
					<td></td>
					<td style="width:14%;">
					  <input name="plus" type="button" class="min_btp">
						<input name="minus" type="button" class="plus_btp add_more"></td>
				  </tr>
				  <tr class="row_cal">
					  <td><input name="duration[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 6 Months') ?>" value="" type="text">
					  </td>
					  <td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: US$ 120') ?>" value="" type="text">
					  </td>
					  <td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: 10% off') ?>" value="" type="text">
					  </td>
					  <td><input name="offer[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 3 Weeks Free') ?>" value=""
								 type="text"></td>
					<td></td>
					<td style="width:14%;">
					  <input name="plus" type="button" class="min_btp">
						<input name="minus" type="button" class="plus_btp add_more"></td>
				  </tr>
				  <tr class="row_cal">
					  <td><input name="duration[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 8 Months') ?>" value="" type="text">
					  </td>
					  <td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: US$ 200') ?>" value="" type="text">
					  </td>
					  <td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
								 placeholder="<? echo $this->lang->line('Example: 20% off') ?>" value="" type="text">
					  </td>
					  <td><input name="offer[]" class="te_name"
								 placeholder="<? echo $this->lang->line('Example: 1 Month Free') ?>" value=""
								 type="text"></td>
					<td></td>
					<td style="width:14%;">
					  <input name="plus" type="button" class="min_btp">
						<input name="minus" type="button" class="plus_btp add_more"></td>
				  </tr>           
				</table>
				<div class="clear"></div>
				</div>
			  </div> 
	  </div>
		  <div class="clear"></div>
		  <input name="submit" class="addd_offer" id="sub_new" value="<? echo $this->lang->line('Create') ?>"
				 type="submit"/>
      </div>
      </form>             
	  	<div class="clear"></div>
	  </div>


</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery-1.7.2.min.js"> </script>
<script type="text/javascript">
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      else
      return true;
}
</script>
<script type="text/javascript">
	var isanyerror=false;
  $(document).on('click','#sub_new',function(){
    var gym = $('#gym_id').val();
    //console.log(gym);
	if($("#all_facility").is(':checked')){
		return true;
	}
    if($.trim(gym) == ''){
      var result1 = 'Please Choose facility';
      $(".error_gym").text(result1).css('color','red');
      return false;
    }else{
	if(!isanyerror)
      return true;
      else
      return false;
    }
  });
</script>
<script type="text/javascript">
  var cnt = 0;
  $(document).on('click','.add_more',function(){
    cnt = cnt + 1; 
    $('.new_tab').append('<tr><td><input name="" class="te_name" placeholder="Example: 1 Month" type="text"></td><td><input name="" class="te_name" onkeypress="return isNumberKey(event)" placeholder="Example: US$ 30" type="text"></td><td><input name="" class="te_name" onkeypress="return isNumberKey(event)" placeholder="Example: 10% off" type="text"></td><td><input name="" class="te_name" placeholder="Example: 1 Week Free" type="text"></td><td></td><td style="width:14%;"><input type="button" class="min_btp"><input type="button" class="plus_btp add_more"></td></tr>');
  });
  $(document).on('click','.min_btp',function (){
    $(this).closest('tr').remove();
  });
</script>

<script type="text/javascript">

  $(document).on('focusout', '.row_cal input', function(event){  
    $('.row_cal').each(function(){
      var first=$(this).find('td:first-child input').val();
      var second=$(this).find('td:nth-child(2) input').val();
      var third=$(this).find('td:nth-child(3) input').val();
      var fourth=$(this).find('td:nth-child(4) input').val();
      if($.trim(first)!='')
      var total = first+' Membership ';
      if($.trim(second)!='')
      total = first+' Membership for US$ '+(second-(second*third/100));
      if($.trim(fourth) !='')
        total=first+' Membership + '+$.trim(fourth)+' for US$ '+(second-(second*third/100));

      //$(this).find('td:nth-child(5) td').val(total);
      $(this).find('td:nth-child(5)').text(total);
    });
    });
</script>
<script>
  function chk_gym(gym){
    $('.error_gym').text('');
    var gym = $('#gym_id').val();
    if($.trim(gym) == ''){
      var result1 = 'Please Choose facility';
      $(".error_gym").text(result1).css('color','red');
      return false;
    }else{
      var result1 = $.ajax({
                   'url' : '<?php echo USER_URL;?>asoffers/check_gym/' + gym,
                   'async' : false
             }).responseText;
         $(".error_gym").text(result1).css({
             "color":'#87191a',
             "font-size":'15px'
         });
         if($.trim(result1)!='')
         isanyerror=true;
         else
         isanyerror=false;
    }
  }
</script>
<script>
$(document).ready(function(){
	$("#select_facility").click(function(){
		if($(this).is(':checked')){
			$( "#all_facility" ).prop( "checked", false );
			$("#facilities").css("display","block");
		}else{
			$("#facilities").css("display","none");
		}
	});
	$("#all_facility").click(function(){
		if($(this).is(':checked')){
			$( "#select_facility" ).prop( "checked", false );
			$("#facilities").css("display","none");
		}else{
			$("#facilities").css("display","none");
		}
	});
});
</script>

<style>
.textwidth{
	width:370px !important;
}
.offerdeils{
	float: left; 
	margin-top: 25px;
}
@media screen and (max-width: 1200px){
	#add_offr .ad_form_wr {
		background: none repeat scroll 0 0 #f5f6f6;
		border: 1px solid #c8c8c8;
		float: left;
		margin-bottom: 5px;
		padding-top: 20px;
	}
	#add_offr .boxs_mn {
		height: 100px;
		padding-top: 7px;
		text-align: center !important;
		width: 124px;
	}
	#add_offr .table_right {
		float: left;
	}
	.table_right {
		float: left;
		width: 100%;
	}
}
@media screen and (max-width: 995px){
	#add_offr .ad_form_wr {
		background: none repeat scroll 0 0 #f5f6f6;
		border: 1px solid #c8c8c8;
		float: left;
		margin-bottom: 5px;
		padding-top: 20px;
		width: 100%;
	}
}
@media screen and (max-width: 750px){
	#add_offr .boxs_mn {
		height: 100px;
		padding-top: 8px;
		text-align: center !important;
		width: 110px;
	}
}
@media screen and (max-width: 675px){
	#add_offr .boxs_mn {
		height: 100px;
		padding-top: 8px;
		text-align: center !important;
		width: 100%;
	}
	#offer{
		float: left !important;
		overflow-x: auto;
		overflow-y: hidden;
		width: 650px;
	}
	.offerdeils{
		float: left;
		margin-top: 25px;
		overflow: scroll;
		width: 100%;
	}
}
@media screen and (max-width: 475px){
	.add_gym_wra h2 {
		color: #605f5f;
		display: block;
		float: left;
		font-size: 12px !important;
	}
	.textwidth {
		width: 250px !important;
	}
}
@media screen and (max-width: 450px){
	#add_offr .boxs_mn {
		height: auto;
		padding-top: 8px;
		text-align: center !important;
		width: 100%;
		float:left;
	}
}
@media screen and (max-width: 450px){
	#add_offer_div .add_gym_wra h2 {
		color: #605f5f;
		display: block;
		float: left;
		font-size: 12px !important;
		width: 90% !important;
	}
	.table_right h2{
		color:#FFFFFF !important;
	}
}
div#error_mes {
    left: 19%;
    position: relative;
}
</style>