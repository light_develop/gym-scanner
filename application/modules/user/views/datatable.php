<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>themes/user/css/www-pkg.css">
<link rel="stylesheet" type="text/css" media="all" href="http://s3-media4.fl.yelpcdn.com/assets/2/www/css/0e6aa74bccf9/baz-pkg.css">
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h1 class="show_all_rslt">
      <?php if(count($cnt_det) > 0){if(isset($cnt_det)){ ?>
        Showing all <?php echo @$this->session->userdata('search_name').'' ;?> gym for <?php echo ucfirst($cnt_det->categoryname); ?> in <?php echo ucfirst($cnt_det->city) .','. ucfirst($cnt_det->state) .','. ucfirst($cnt_det->country_name);?>
      <?php } }else{ echo 'Showing '. @$this->session->userdata('search_name').' gym' ; } ?>
    </h1>
  </div> 
  <?php  if(isset($gyms) && is_array($gyms) && count($gyms)){ $i = 1; 
      foreach($gyms as $key => $value):
        if($i%2 == 0) $cls ='col-lg-12 col-md-12 col-sm-12 col-xs-12 search_result2';
        else $cls = 'col-lg-12 col-md-12 col-sm-12 col-xs-12 search_result'; ?>  
  <div class="<?php echo $cls;?>">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 search_result_img">
        <?php $img = $value->gym_logo;?>
        <a href="<?php echo site_url('fac_det/'.$value->id);?>"><img src="<?php echo base_url();?>uploads/gym/logos/<?php echo $img;?>"  class="img-responsive" /> </a>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">   
        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-8  search_result_head"><?php echo ucfirst($value->gymname);?> &nbsp; 
          <?php $splty =  $value->speciality;
          if($splty == 1){ ?> 
            <span><img src="<?php echo base_url()?>themes/user/images/male.png" /></span>
          <?php } else if ($splty == 2) { ?>
            <span><img src="<?php echo base_url()?>themes/user/images/female.png" /></span>
          <?php } else { ?>
            <span><img src="<?php echo base_url()?>themes/user/images/female.png" /></span>&nbsp;
            <span><img src="<?php echo base_url()?>themes/user/images/male.png" /></span>
          <?php }?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 search_result_star">         
          <?php $rating = $value->star; ?>
            <?php // for($s = 0; $s < $rating; $s++){ ?>
              <!--<span class="glyphicon glyphicon-star"></span>-->
            <?php  //} ?>
            <div class="rating" style="float: right;">
                <li class="star-img stars_<?php if (isset($rating)) echo $rating; else echo 0;?>"
                    title="<?php echo $this->lang->line("5.0 star rating"); ?>"><img alt="5.0 star rating"
                                                                                     class="offscreen"
                                                                                     height="303"
                                                                                     src="<?php echo base_url()?>themes/user/images/stars_map.png"
                                                                                     width="84"></li>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_result_add" ><?php echo ucfirst($value->gstreet).',';?><br>
          <?php echo ucfirst($value->city).',';?><br>
          <?php echo ucfirst($value->state).',';?><br>
          <?php echo ucfirst($value->country_name);?>,<?php echo $value->gzip;?>
        </div>
          <div class="col-lg-9  col-md-9 col-sm-9 col-xs-8 search_result_map"><span><img
                      src="<?php echo base_url()?>themes/user/images/map.png"/></span>&nbsp;<a
                  href="<?php echo site_url('fac_det/' . $value->id);?>"><?php echo $this->lang->line("Details"); ?></a>
          </div>
        <!--<div class="col-lg-3  col-md-3 col-sm-3 col-xs-4 search_result_doller" >
          <?php $rating = $value->price_range; ?>
          <?php for($s = 0; $s < $rating; $s++){?>
            <span><img src="<?php echo base_url()?>themes/user/images/doller.png"/></span>
          <?php } ?>
        </div>-->
      </div>
  </div>
  <?php $i++; endforeach;?>
  <?php } else { ?>
      <div align="center"
           style="font-size:16px; padding-top:5px;"><?php echo $this->lang->line("No records"); ?></div> <?php } ?>
<script>
function get_map(latd,longt){ 
$(".black-window,.map_new").hide().fadeIn(1000);
var data='<iframe class="ifrm" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='+latd+','+longt+'&amp;aq=&amp;sll='+latd+','+longt+'&amp;sspn=0.006514,0.009806&amp;ie=UTF8&amp;t=m&amp;z=14&amp;ll='+latd+','+longt+'&amp;output=embed"></iframe>';    
  $(".map_new").html(data);
}
$(document).ready(function(){
  $(".black-window").click(function(){$(".black-window,.map_new").hide()})
});
</script>

