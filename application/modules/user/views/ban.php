
<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
<div class="dash_bord">
<div class="top_menu_wrapp">
  <ul>
    <li><a style="border:none"><?php echo $this->lang->line("Ban page"); ?></a></li>
</ul>

<div class="clear"></div>
</div>


<div class="dimound_details error">
    <h1><?php echo $this->lang->line("You have been banned by the administrator."); ?></h1>
    <h1><?php echo $this->lang->line("Check your mail"); ?></h1>
    <h3><a style="text-decoration: none; position: relative;  left: 45%" href="<?php echo base_url() ?> ">Back to Main Page</a></h3>
</div>

</div>
</div>
