<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="clearfix"></div>
<section  style="width: 100%;"> <img src="<?php echo base_url()?>themes/user/images/inner-img.png" class="img-responsive blur" width="100%" id="blur"/> </section>
<div class="clearfix"></div>
<div class="container serach_result">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " >

  <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12 righ_pnl" >

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fillter_bg">
    <span class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="width: 75%;"><p class="organize gymheading"><?php echo strtoupper($gym_det->gymname);?></p></span>
    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="width: 25%;">
    <span class="fillter" style="border:none">
      <label for="checkbox1"><a style="color:#fff;" class="gymheading" href="<?php echo USER_URL;?>gym_imgs/<?=$gym_det->id;?>">BACK</a></label>   
    </span></span>
  </div>


  <div class="clearfix"></div><br/>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="viewoffers">
  <form name="offr_form" method="post" action="<?php echo site_url('choose_ofr');?>">
  <div class="add_gym_wra" style="margin-bottom:15px;margin-top:0px !important;">
    <?php if($this ->session -> flashdata('success')!=''){?>
        <div><h4 style="color:green;"><?php echo $this ->session -> flashdata('success');?></h4></div>
      <?php } ?>

       <?php if($this->session -> flashdata('delete')!=''){?>
        <div><h4 style="color:red;"><?php echo $this ->session -> flashdata('delete');?></h4></div>
    <?php } ?>
    <input type="hidden" name="gym_id" value="<?php echo $gym_det->id;?>">
    <div class="prof_frm">
		<div class="prof_fst_div"><h2><?php echo $this->lang->line("Join date"); ?></h2></div>
      <div class="prof_sec_div"><!--<input class="form_sm" type="date" name="join_date" id="joindate"/>--><input class="form_sm" type="text" name="join_date" id="joindate"/></div>
    </div>
    <?php if(isset($gym_offers)  && count($gym_offers)){ $i = 1; ?>
    <div class="clear"></div>
	  <h2><?php echo $this->lang->line("Choose Offer"); ?></h2>
      <div class="widget-body">

        <div class="clear"></div>
        <!-- <div class="table_right"> -->
		<div class="chooseofr">
			  <div class="ofr_wraper">

				<table style="width:100%">
				  <tr class="row_cal" align="center">
					<td>
					  <div class="top_clr_he boxs_mn top_clr_he">
						  <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Duration"); ?></h2>

						  <p><?php echo $this->lang->line("Select the length of your membership"); ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he1">
						  <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Rate"); ?></h2>

						  <p><?php echo $this->lang->line("Membership fees"); ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he2">
						  <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Cash Discount"); ?></h2>

						  <p><?php echo $this->lang->line("You get the following discount today"); ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he3">
						  <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Exclusive Perk"); ?></h2>

						  <p><?php echo $this->lang->line("Enjoy these perks only with Gymscanner.com"); ?></p>
					  </div>
					</td>
					<td>
					  <div class="boxs_mn top_clr_he4">
						  <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("You get"); ?></h2>

						  <p><?php echo $this->lang->line("So today you get:"); ?></p>
					  </div>
					</td>
					<td></td>
				  </tr>
				  <?php $i = 0; //echo count($details);
					foreach ($gym_offers as $key => $value) { ?>
					  <tr class="row_cal" style="text-align:center;">
						<td class="usr_ofr_td"><?php echo $value->duration;?></td>
						<td class="usr_ofr_td"><?php echo '$'.$value->membership;?></td>
						<td class="usr_ofr_td"><?php echo $value->discount.'%';?></td>
						<td class="usr_ofr_td"><?php echo $value->offer;?></td>
						<td><?php echo $value->description;?><br/></td>
						<td style="width:4%;">
						  <input type="radio" name="offer" value="<?php echo $value->id;?>"/>
						</td>
					  </tr>
				  <?php $i++; } ?>

				</table>
				<div class="clear"></div>
				</div>
			</div>

          <div class="clear"></div>
          <button type="submit" id="ofr_list_btn" style="margin-top:5px;" class="btn btn-danger "><?php echo $this->lang->line("PROCEED TO PAYMENT"); ?>&emsp;<span class="glyphicon glyphicon-usd"></span> <span class="glyphicon glyphicon-usd"></span> <span class="glyphicon glyphicon-usd"></span> </span></button>
		  <!--<input type="submit" id="ofr_list_btn" class="search_gym_button"
				 value="<?php /*echo $this->lang->line("PROCEED TO PAYMENT"); */?>"><img
			  src="<?php /*echo base_url(); */?>themes/user/images/payment-con.jpg" alt="payment-icon"
			  style="margin-top: -4px;"/>-->
	  <div id="share_view_offer">
	  	<span class='st_sharethis_large'></span>
		<span class='st_facebook_large'></span>
		<span class='st_twitter_large'></span>
		<span class='st_instagram_large'></span>
		  <span class='st_youtube_large' displayText='<?php echo $this->lang->line("Youtube Subscribe"); ?>'></span>
	  	<!--<img src="<?php//echo base_url(); ?>themes/user/images/shar-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"  class='st_sharethis' />
		<img src="<?php// echo base_url(); ?>themes/user/images/fb-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;" class='st_facebook'/>
		<img src="<?php// echo base_url(); ?>themes/user/images/twitter-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;" class='st_twitter'/>
		<img src="<?php// echo base_url(); ?>themes/user/images/ph-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;" class='st_instagram'/>
		<img src="<?php// echo base_url(); ?>themes/user/images/youtube-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;" class='st_youtube' />-->
	  </div>
    <?php } else {  ?>
      <div class="prof_frm">
		  <div class="prof_fst_div"><h2><?php echo $this->lang->line("Price(per month)"); ?></h2></div>
        <div class="prof_sec_div" style="margin-top:22px;"><?php echo $gym_det->price;?></div>
      </div>
      <!-- <span>
        <a href="javascript:void(0);" data-toggle="modal" data-target=".bs-example-modal-smjoin"><img src="<?php echo base_url();?>themes/user/images/authorize_net.jpg" width="200" height="35"></a>
      </span><br/><br/> -->
		<input type="submit" id="ofr_btn" class="search_gym_button"
			   value="<?php echo $this->lang->line("PROCEED TO PAYMENT"); ?>"><img
			src="<?php echo base_url(); ?>themes/user/images/payment-con.jpg" alt="payment-icon"
			style="margin-top: -3px;"/>
	  <div style="float: right; margin-top: 10px;">
	  	<img src="<?php echo base_url(); ?>themes/user/images/shar-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"/>
		<img src="<?php echo base_url(); ?>themes/user/images/fb-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"/>
		<img src="<?php echo base_url(); ?>themes/user/images/twitter-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"/>
		<img src="<?php echo base_url(); ?>themes/user/images/ph-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"/>
		<img src="<?php echo base_url(); ?>themes/user/images/youtube-icon.png" alt="shar-icon" style="float:left;margin-right: 10px;"/>
	  </div>
     <?php } ?>

        </div>
	  <p style="font-size: 13px;margin-top: 10px;"><?php echo $this->lang->line("This facility may or may not charge an initiation / start-up fee for new customers. Please contact the facility to find out if this applies to you."); ?></p>
        </div>
    </form>
   </div>
  </div>
  <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

  <div class="add_gym_wra">
        <h3><?php echo $this->lang->line("Make payment"); ?></h3>

  <span>
   <a href="javascript:void(0);"><img src="<?php echo base_url();?>themes/user/images/authorize_net.jpg" width="200" height="35"></a>
  </span><br/><br/>
    </div>
  </div> -->



  </aside>
</div>

  <div class="clearfix"></div>

</div>
<script>
    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("join_date")[0].setAttribute('min', today);
</script>
<script type="text/javascript">
  $(function(){
      $(document).on('click','#ofr_list_btn',function(){
        if(!$("input:radio[name='offer']").is(":checked")) {
          alert('Please choose any offer');
          return false;
        }
        var date = $('#joindate').val();
        if(date == ''){
          alert('Please mention Join date');
          return false;
        }
    });
  });
  $(document).on('click','#ofr_btn',function(){
    var date = $('#joindate').val();
    if(date == ''){
          alert('Please mention Join date');
          return false;
    }
  });
  var $j = jQuery.noConflict();
  $j(document).on('focus','#joindate',function(){
        $(this).datepicker({
          //changeMonth: true,
          //changeYear: true,
          minDate: 0, maxDate: "+15D",
          dateFormat: 'yy-mm-dd',
          yearRange: "-50:+0",
        });
    });

</script>
<style>
#viewoffers #share_view_offer{
	margin-top: 10px;
	float: right;
}
@media screen and (max-width: 995px){
	.chooseofr .boxs_mn {
		height: 100px;
		padding-top: 7px;
		text-align: center !important;
		width: 100%;
	}
}
@media screen and (max-width: 650px){
	.chooseofr{
		float: left;
		overflow: scroll;
		width: 100%;
	}
	.chooseofr .ofr_wraper{
		float: left;
		width: 530px;
	}
}
@media screen and (max-width: 530px){
	#viewoffers .prof_fst_div {
		float: none;
		margin-left: 5px;
		width: 20%;
	}
	#viewoffers  .form_sm {
		background: none repeat scroll 0 0 #fff;
		border: 1px solid #c0c0c0;
		border-radius: 3px;
		box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
		float: left;
		height: 34px;
		margin-bottom: 15px;
		margin-top: 22px;
		padding: 0 5px;
		width: 100%;
	}
	#viewoffers #share_view_offer{
		margin-top: 10px;
		float: none;
	}
}

</style>

