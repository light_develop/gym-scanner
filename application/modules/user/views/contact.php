<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
	<div class="add_gym_wra new_bg">
        <h1><?php echo $this->lang->line("Contact Us"); ?></h1>
	  	<?php if($this->session->flashdata('success')!=""){?>
	        <div class="alert-success">
	            <button type="button" class="close" data-dismiss="alert">×</button>
                <font
                    color="green"><strong><?php echo $this->lang->line("Success !"); ?></strong> <?php echo $this->session->flashdata('success'); ?>
                </font>
	        </div>
	    <?php }?> 
	    <?php if ($this->session->flashdata('error') != "") { ?>
	        <div class="alert">
	            <button type="button" class="close" data-dismiss="alert">×</button>
                <font
                    color="red"><strong><?php echo $this->lang->line("Error !"); ?></strong> <?php echo $this->session->flashdata('error'); ?>
                </font>
	        </div>
	    <?php } ?> 
	  	
		<div class="col-lg-3 col-sm-3 col-xs-12 stylep">
            <?php echo $this->getStaticBlock("contact_us"); ?>
		</div>
    <div class="col-lg-4 col-sm-4 col-xs-12">
      <div id="map-canvas" style="width: 200; height: 225px;border:2px solid #000"></div>
    </div>



	<div class="clear"></div>
</div>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

  <!-- <div id="map-canvas" style="width: 300px; height: 200px;"></div> -->

  <script type="text/javascript">
    var locations = [
      ['1005 West River Rd., Champlin, Minnesota 55030', 45.178648, -93.380994, 2],
    ];

    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      zoom: 2,
      center: new google.maps.LatLng(45.178648, -93.380994),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
      
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    
    }
  </script>
<script>

</script>