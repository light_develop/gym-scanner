<div class="left_search">
<form action="<?php echo USER_URL;?>user/search" class="form-horizontal" method="post" id="search_form" >
	<h2><?php echo $this->lang->line("Search"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
	<div class="fnt"><?php echo $this->lang->line("I am  Looking"); ?></div>
<div><?php $attributes= 'id="cat_id" class="ddl_select"';
    	echo form_dropdown('cat_id', $cat,$this->session->userdata('cat_id'),$attributes);
    ?>
</div>

<div class="height5"></div>
	<div class="fnt"><?php echo $this->lang->line("I AM LOOKING FOR"); ?></div>
	<div class="fnt"><?php echo $this->lang->line("I AM LOOKING FOR"); ?></div>
<div><?php $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="ddl_select"';
    	echo form_dropdown('cntry', $cntry, $this->session->userdata('cntry'),$attributes);
    ?>  <font color="red"><span id="error"></span></font>
    </div>

<div class="height5"></div>
	<div class="fnt"><?php echo $this->lang->line("States"); ?></div>
<div>
	
	<?php $attributes= 'id="state"  class="ddl_select" onchange="get_cities(this.value)"';
    	echo form_dropdown('state', $states, $this->session->userdata('state'),$attributes);                     
    ?>
</div>

	<div class="fnt"><?php echo $this->lang->line("Cities"); ?></div>
<div>
	
	<?php $attributes= 'id="city"  class="ddl_select" ';
    	echo form_dropdown('city', $cities, $this->session->userdata('city'),$attributes);                     
    ?>
</div>

<br/>

	<div>
		<button value="Search" name="search" class="search_button"><?php echo $this->lang->line("Search"); ?></button>
	</div>
<div class="height5"></div>
</form>
</div>

<br/>


<div class="left_search">
<form name="filter_search" method="post" id="filter_search">
	<h2><?php echo $this->lang->line("Filter"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
	<div class="fnt"><input name="byname" type="checkbox"
							value="gymname"/>&nbsp;&nbsp;<?php echo $this->lang->line("By Name"); ?></div>
	<div class="fnt"><input name="rating" type="checkbox"
							value="star"/>&nbsp;&nbsp;<?php echo $this->lang->line("Star Rating"); ?></div>

	<div class="fnt"><?php echo $this->lang->line("Gender"); ?></div>

<div style="margin-left:10px;">
	<div class="fnt"><input name="speciality" type="radio"
							value="1"/>&nbsp;&nbsp;<?php echo $this->lang->line("Male"); ?></div>
	<div class="fnt"><input name="speciality" type="radio"
							value="2"/>&nbsp;&nbsp;<?php echo $this->lang->line("Female"); ?></div>
	<div class="fnt"><input name="speciality" type="radio"
							value="3"/>&nbsp;&nbsp;<?php echo $this->lang->line("Both"); ?></div>
	<div class="fnt"><input name="speciality" type="radio" value="all"
							checked="checked"/>&nbsp;&nbsp;<?php echo $this->lang->line("All"); ?></div>
</div>

</form>
</div>
<script> 
   $( "#search_form" ).submit(function( event ) {
  		var cntry = document.getElementById('cntry').value;
  		var city = document.getElementById('city').value;
		if(cntry == "")
		{
			document.getElementById('error').innerHTML = "Select any country";
			return false;							
			}
		else if(isNaN(cntry))
		{
			document.getElementById('error').innerHTML = "Select any country";
			return false;							
			}
		else if(city == '')
		{
			document.getElementById('error').innerHTML = "Select another country";
			return false;							
			}
		else
		{
			document.getElementById('error').style.display = "none";
			return true;							
		}
	});

   function load_dropdown_content(selected_value){ 

       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_states/' + selected_value,
                   'async' : false
             }).responseText;
         $("#state").replaceWith(result);
      } 
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function showValues() {
    var str = $( "#filter_search" ).serialize();
    var result = $.ajax({
			'type' : 'post',
        	         'url' : '<?php echo USER_URL;?>user/filter_search',
        	         'data' : {'ser_data':str},
        	         'async' : false
    	       }).responseText;
    	   $(".right_kuwait").html(result);
  }
  $( "input[type='checkbox'], input[type='radio']" ).on( "click", showValues );
  showValues();
 
  $( "input[type='checkbox'], input[type='radio']" ).on( "click", showValues );
  showValues();
   
</script>
