<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container">
	<div class="add_gym_wra" id="profile_page">
	  	<?php if (validation_errors() != "") { ?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
					<strong><?php echo $this->lang->line("Errors Found!"); ?></strong> <?php echo validation_errors(); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error') != "") { ?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
					<strong><?php echo $this->lang->line("Success!"); ?></strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>	  
	  	<?php if($mode == 'view'){?>
			<h1><?php echo $this->lang->line("Profile View"); ?>
				|<?php if (isset($this->session->userdata['type'])) { ?><a
					href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a>|<?php } ?>
				<a onclick="goBack()"><?php echo $this->lang->line("Back"); ?></a>
				<a href="<?= site_url('edit_profile'); ?>"
				   style=" float:right;margin:0px 5px 0 0; color:#ffffff;"><?php echo $this->lang->line("Edit"); ?></a>
				<a href="<?= site_url('chng_pwd'); ?>"
				   style=" float:right;margin:0px 5px 0 0; color:#ffffff;"><?php echo $this->lang->line("Change Password"); ?>
					|</a>
	  		</h1>
			<div class="col-lg-12 ad_form_wr" id="profile_view_nagure">
				<h2><?php echo $this->lang->line("First Name"); ?> <span style="float:right">:</span></h2>
			  	<p class="profile_res"><?php if(isset($res)) echo $res->first_name;?></p>
			  	<div class="clear"></div>

				<h2><?php echo $this->lang->line("Last Name"); ?> <span style="float:right">:</span></h2>
			  	<p class="profile_res"><?php if(isset($res)) echo $res->last_name;?></p>
			  	<div class="clear"></div>

				<h2><?php echo $this->lang->line("User Name"); ?> <span style="float:right">:</span></h2>
			  	<p class="profile_res"><?php if(isset($res)) echo $res->user_name;?></p>
			  	<div class="clear"></div>

			  	<h2>Email <span style="float:right">:</span></h2>
			  	<p class="profile_res"><?php if(isset($res)) echo $res->email;?></p>
			  	<div class="clear"></div>
			</div>
		<?php } else if($mode == 'edit'){?>
			<h1><?php echo $this->lang->line("Edit Profile"); ?>
				|<?php if (isset($this->session->userdata['type'])) { ?><a
					href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a>|<?php } ?>
				<a onclick="goBack()"><?php echo $this->lang->line("Back"); ?></a></h1>
			<form action="<?php if(isset($res)) echo USER_URL.'profile';?>" class="form-horizontal" method="post" id="profile_form">
				<div class="col-lg-12 ad_form_wr">
					<h2><?php echo $this->lang->line("First Name:"); ?></h2>
			  	<input type="hidden" name="uid" value="<?php if(isset($res)) echo $res->id;?>"/>
			  	<p class="profile_res" style=" margin-top: 20px !important;"><input type="text" name="first_name" value="<?php if(isset($res)) echo $res->first_name;?>"/></p>
			  	<div class="clear"></div>

					<h2><?php echo $this->lang->line("Last Name:"); ?></h2>
			  	<p class="profile_res" style=" margin-top: 20px !important;"><input type="text" name="last_name" value="<?php if(isset($res)) echo $res->last_name;?>" /></p>
			  	<div class="clear"></div>

					<h2><?php echo $this->lang->line("User Name:"); ?></h2>
			  	<p class="profile_res" style=" margin-top: 20px !important;"><?php if(isset($res)) echo $res->user_name;?></p>
			  	<div class="clear"></div>

					<!-- <h2><?php echo $this->lang->line("Password:"); ?></h2>
			  	<p><input type="password" name="password" value="<?php if(isset($res)) echo $res->password;?>" /></p>
			  	<div class="clear"></div> -->

					<h2><?php echo $this->lang->line("Email:"); ?></h2>
			  	<p class="profile_res" style=" margin-top: 20px !important;"><input type="text" name="email" value="<?php if(isset($res)) echo $res->email;?>" /></p>
			  	<div class="clear"></div>
			  	<div class="clear"></div>
			  	<h2>&nbsp;</h2>
					<input name="submit" class="sub_green" value="<?php echo $this->lang->line("SUBMIT"); ?>"
						   type="submit">
			  	<div class="clear"></div>	
			</div>
			</form>
		<?php } else if($mode == 'change_pwd'){?>
			<h1><?php echo $this->lang->line("Change Password"); ?>
				|<?php if (isset($this->session->userdata['type'])) { ?><a
					href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a>|<?php } ?>
				<a onclick="goBack()"><?php echo $this->lang->line("Back"); ?></a></h1>
			<form action="<?php //echo USER_URL.'change_pwd';?>" class="form-horizontal" method="post" id="profile_form">
			<div class="col-lg-12 ad_form_wr">	
				<div class="form-group">
					<div class="wrapper">
						<div class="outerdiv" style="float: left; width: 360px;">
							<input type="password" name="old_password" id="old_password" class="form-control"
								   placeholder="<?php echo $this->lang->line("Current Password"); ?>"
								   value="<?php echo $this->input->post('old_password'); ?>">
						</div>
						<div class="Validation"></div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="form-group">
					<div class="wrapper">
						<div class="outerdiv" style="float: left; width: 360px;">
							<input type="password" name="password" id="passwordnew" class="form-control"
								   placeholder="<?php echo $this->lang->line("New Password"); ?>"
								   value="<?php echo $this->input->post('password'); ?>"><img class="subimg" alt=""
																							  style="float: right; margin-top: -30px;"/>
							<div id="progressbar1"></div>
						</div>
						<div class="Validation"></div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="form-group">
					<div class="wrapper">
						<div class="outerdiv" style="float: left; width: 360px;">
							<input type="password" name="passconf" id="passconf" class="form-control"
								   placeholder="<?php echo $this->lang->line("Confirm Password"); ?>"
								   value="<?php echo $this->input->post('passconf'); ?>"><img class="subimg" alt=""
																							  style="float: right; margin-top: -30px;"/>
						</div>
						<div class="Validation"></div>
					</div>
				</div>
				  
			  	
			  	<div class="clear"></div>
				<input name="submit" class="sub_green" value="<?php echo $this->lang->line("SUBMIT"); ?>" type="button"
					   style="margin-left:1%;border-radius: 20px;" id="changpass">
			  	<div class="clear"></div>	
			</div>
			</form>
		<?php } ?>
		<div class="clear"></div>
	</div>
</div>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css"
        type="text/css" media="all" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="http://www.codecomplete4u.com/wp-content/MyPlugins/maskinput.js" type="text/javascript"></script>

<!--<link rel="stylesheet" href="external/jquery-ui.css"
        type="text/css" media="all" />
<script type="text/javascript" src="external/jquery.min.js"></script>
<script type="text/javascript" src="external/jquery-ui.min.js"></script>
<script src="external/maskinput.js" type="text/javascript"></script>
--><script>
$(document).ready(function(){
$("#progressbar1").progressbar({
	value: 1
});
$('#old_password').keyup(function () {
var password = $("#old_password").val();
	if(password != ""){
		$.ajax({
			type : "POST",
			url : "<?php echo USER_URL?>exist_pass",
			data : {
						'password' : password
				   },
			success : function(data) { 
				//alert(data);
				if(data != 1){
					//alert(data);
					$("#old_password").parent().siblings("div").text("* Invalid current password");
					ValidateAnimation($("#old_password"));
				}
				else{
					$("#old_password").parent().siblings("div").text("");
					ValidateAnimation($("#old_password"));
				}
			}
		});
	} 
});

$('#passconf').blur(function () {
	repwd = $(this).val();
	pwd = $('#passwordnew').val();
	if (repwd == pwd) {
		loadRight($('#passconf'));
		ValidationRemove($('#passconf'));
	}
	else if ($(this).val() == "") { $(this).siblings('img').remove(); ValidationRemove($('#passconf')); }
	else {
		loadWrong($('#passconf'));
		$("#passconf").parent().siblings("div").text("* Password is not same");
		ValidateAnimation($('#passconf'));
	}

});



$('#passwordnew').keyup(function () {
	pwd = $(this).val();
	var strpwd = /[^\w\*]/;
	$("#progressbar1").removeClass("beginning middle end");
	var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
	if(!regularExpression.test(pwd)) {
        loadWrong($('#passwordnew'));
    }
	else{
		loadRight($('#passwordnew'));
	}
	if ($(this).val() == "") { $(this).siblings('img').remove(); }
	if (pwd.length == 0) {
		$("#progressbar1").addClass("beginning");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "0%"
		}, 200);
		return true;
	}
	if (pwd.length < 5) {
		$("#progressbar1").addClass("beginning");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "20%"
		}, 200);
		return true;
	}
	else if (pwd.length >= 5 && pwd.length <= 10) {
		$("#progressbar1").addClass("middle");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "40%"
		}, 200);
		return true;
	}
	else if (pwd.length >= 10 && pwd.length <= 12) {
		$("#progressbar1").addClass("middle");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "60%"
		}, 200);
		return true;
	}
	else if (pwd.length >= 12 && pwd.length <= 15) {
		$("#progressbar1").addClass("middle");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "80%"
		}, 200);
		return true;
	}
	else if (pwd.length >= 15) {
		$("#progressbar1").addClass("end");
		$("#progressbar1 > .ui-progressbar-value").animate({
			width: "100%"
		}, 200).addClass("end");
		return true;
	}
});

function Empty_validation() {
	if ($("#passconf").val() == "") {
		$("#passconf").parent().siblings("div").text("* This field is required");
		ValidateAnimation($("#passconf"));
	}
	if ($("#passwordnew").val() == "") {
		$("#passwordnew").parent().siblings("div").text("* This field is required");
		ValidateAnimation($("#passwordnew"));   
	}
	if ($("#old_password").val() == "") {
		$("#old_password").parent().siblings("div").text("* This field is required");
		ValidateAnimation($("#old_password"));   
	}
}
function cross_validation() {
	if ($("#passwordnew").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
		$("#passwordnew").parent().siblings("div").text("* Enter atleast 1 number and 1 special character");
		ValidateAnimation($("#passwordnew"));  
	}
}	
function RigthValidation() {
	if ($("#passconf").val() != "") {
		ValidationRemove($("#passconf"));
	}
	if ($("#passwordnew").val() != "") {
		ValidationRemove($("#passwordnew"));
	}
	if ($("#old_password").val() != "") {
		ValidationRemove($("#old_password"));
	}
}
function ValidateAnimation(thisis) {
	thisis.parent().siblings("div").addClass("Validation").effect("bounce", { times: 4 }, 1000).fadeIn("slow", function () {
	//$("#sub_btn").removeAttr('disabled');
	});
}
function ValidationRemove(thisis) {
	thisis.parent().siblings("div").fadeOut("slow");
}
function loadRight(thisis) {
	thisis.siblings('img').remove();
	$('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px;"/>').insertAfter(thisis).delay(100).fadeOut(1000, function () {
		thisis.siblings('img').remove();
		$('<img src="<?php echo base_url()?>themes/user/images/right.png" class="subimg" style="float: right; margin-top: -30px;"/>').insertAfter(thisis);
	});
}
function loadWrong(thisis) {

	thisis.siblings('img').remove();
	$('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px;"/>').insertAfter(thisis).delay(100).fadeOut(100, function () {
		thisis.siblings('img').remove();
		$('<img src="<?php echo base_url()?>themes/user/images/cross.png" class="subimg" style="float: right; margin-top: -30px;"/>').insertAfter(thisis);
	});
}
$('#changpass').click(function(){ 
	RigthValidation();
	Empty_validation();
	cross_validation();
	var old_password = $('#old_password').val(); 
	var passconf     = $('#passconf').val(); 
	var password     = $('#passwordnew').val();
	if((passconf == password) && old_password != ''){
		$.ajax({
			type : "POST",
			url : "<?php echo USER_URL?>change_pwd",
			data : {
						'password' : passconf,
						'old_password': old_password
				   },
			success : function(data123) { 
				
				alert('Password changed Successfully...');
				location.reload();
				//window.location.href;
			}
		});
	}

      return false; 
});



});
</script>
<style>
#progressbar1
{
	background: white;
    float: right;
    height: 8px;
    margin-right: 35px;
    margin-top: -25px;
    width: 23%;
	
}
.ui-progressbar.beginning .ui-progressbar-value
{
	background: green;
}
.ui-progressbar.middle .ui-progressbar-value
{
	background: green;
}
.ui-progressbar.end .ui-progressbar-value
{
	background: green;
}
.Validation
        {
            width: 330px;
            height: 35px;
            display: none; /* IE10 Consumer Preview */
            font-size: middle;
            font-weight: bold;
            color: #AF414A;
            float: left;
            padding-top: 10px;
			margin-left:30px;
        }
		.ui-progressbar .ui-progressbar-value {
    height: 100%;
    margin: 0px !important;
}
#profile_page h1 {
    background-color: #87191a;
    color: #fff;
    font-family: gothic;
    font-size: 15px;
    font-weight: bold;
    margin-bottom: 10px;
    margin-top: 0;
    padding-bottom: 10px;
    padding-left: 10px !important;
    padding-right: 10px;
    padding-top: 10px;
    text-transform: uppercase;
}
#profile_page h2 {
    color: #605f5f;
    display: block;
    float: left;
    font-family: gothic;
    font-size: 16px;
    width: 208px;
}
#profile_page .profile_res{
    font-family: gothic !important;
    margin-top: 0 !important;
    padding: 0 10px;
}
#profile_page a:hover, a:focus {
    cursor: pointer;
    text-decoration: underline;
}
#profile_page .sub_green {
    background: none repeat scroll 0 0 #87bb33;
    border: medium none;
    color: #fff;
    display: block;
    font-family: gothic;
    font-size: 16px;
    font-weight: bold;
    margin-bottom: 30px;
    margin-left: 20%;
    padding: 5px 20px;
}
</style>
