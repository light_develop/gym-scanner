<div class="clearfix"></div>
<div class="container">
	<div class=" cnt_h1"><h1><?php echo $this->lang->line("Raise a ticket"); ?></h1></div>
	<div class="page_text_p">	
		<?php 
			if($this->session->userdata('user_id'))
			{
			?>
				<h3><strong><?php echo $this->lang->line("Ask new question"); ?></strong></h3>
			<div>
				<form action="<?php echo USER_URL;?>customersupport/addnew" method="post">
				<textarea name="message" required></textarea><br/>
					<input type="submit" value="Send Question"><?php echo $this->lang->line("			"); ?></form>
				<?php if($this -> session -> flashdata('success')!=''){?>
				<div><h4 style="color: red;"><?php echo $this -> session -> flashdata('success');?></h4></div>
				<?php } ?>
			</div>
			<br/>
			<?php foreach($all_faq as $key=>$val):?>
				<p>
					<?php echo '<b>Ticke Id: TIC00' . $val->id; ?></b><br/> '.$val->message;?>
					<a href="javascript:void(0);" onclick="replyFunc('<?= $val->id; ?>');"
					   class="reply<?= $val->id; ?>"><?php echo $this->lang->line("reply"); ?></a>
				</p><br/>		
				<span id="reply_re<?=$val->id;?>"></span>
				<?php $reply = $this->customersupport->getFaq($val->id);
				if(count($reply)>0)
				{?>
				
				<ul style="padding-left:40px;">
				<?php
					foreach($reply as $rval):
					if(($rval->visible_flag == 2 && $val->user_id == $this->session->userdata('user_id')) || $rval->visible_flag == 1)
					{
					?>
					<li><?=$rval->message;?></li>
					<?php }	
					endforeach;?>
				</ul>
					
				<?php }?>
				<hr/>
				<?php
				endforeach;}?>
	</div>		
</div>
<script type="text/javascript">
function replyFunc(id)
{
	$('[id^="reply_re"]').hide();
	var cnt = '<form action="<?php echo USER_URL;?>customersupport/addnew/'+id+'" method="post">\
		<textarea name="message"></textarea><br/>\
		<input type="submit" value="reply"></form>';
	$('#reply_re'+id).show('slow').prepend(cnt);
}	
</script>
