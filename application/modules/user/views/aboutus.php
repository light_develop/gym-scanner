<header>
    <meta name="title" content="About Gymscanner.com">
    <meta name="keywords"
          content="Gyms online , Gyms in America , Gyms in the US , Fitness Trainers Online , Personal Trainers , Fitness Center ">
    <meta name="description"
          content="<p><?php echo $this->lang->line("Gymscanner.com website and the mobile application allows users to search, locate, view and renew their subscription to gyms, fitness centers and personal trainers worldwide."); ?></p>">
    </div>
    <div class="clearfix"></div>
    <?php echo $this->getStaticBlock("about_us"); ?>
    </div>
</header>
<script>


</script>