<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container">
	<div class="add_gym_wra">
		<div class="view_page"><h1><?php echo $this->lang->line("My Profile"); ?> |<a
					href="<?php echo site_url('facility') ?>"><?php echo $this->lang->line("Dashboard"); ?></a>|<a
					onclick="goBack()"><?php echo $this->lang->line("Back"); ?></a></h1></div>

		<div class="col-lg-12 ad_form_wr">
			<h2><?php echo $this->lang->line("Gym Name"); ?></h2>
		   		<p><?php if(isset($details)) echo $details->gymname;?></p>
		  	<div class="clear"></div>

			<h2><?php echo $this->lang->line("Logo"); ?></h2>
		 	<p>
		  	<?php if(isset($details)){
		 		if($details->gym_logo != '')
		 			$url = base_url().'uploads/gym/logos/'.$details->gym_logo;
		 		else
		 			$url = base_url().'no-image.png';?>
		 		<div class="map_gym logo_img"><img src="<?php echo $url;?>" style="height: 85px;"/></div>
		 		<?php }?>
		 	</p>

			<h2><?php echo $this->lang->line("Images"); ?></h2>
			<p>
			<?php if(isset($images)){
				foreach ($images as $key => $value) { 
              $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
                <img src="<?php echo $image;?>" width="100px" height="85px"> &nbsp;&nbsp;
            <?php } }?> 
            </p>
			<div class="clear"></div>   
		   
		   	<div id="add_more"></div>
		   	<div class="clear"></div>

			<h2><?php echo $this->lang->line("Video"); ?></h2>
		   	<p>	   
		    <?php 
		      if($details->gym_video != '') {
		        $videos = $details->gym_video;
		        $res = explode('~',$details->gym_video);
		        foreach($res as $value){ 
		          $var = explode('v=',$value);   
		          $new = explode('/',$value);
		          if($new[3] ||$var[1]) {
		            if(strpos($value,'vimeo') !== false) 
		            {?>
		                <iframe src="http://player.vimeo.com/video/<?=$new[3]?>" width="220" height="145" frameborder="1"></iframe>
		            <?php }    
		                    
		            if (strpos($value,'youtube') !== false) 
		            { ?>
		                <iframe width="220" height="145" src="http://www.youtube.com/embed/<?=$var[1]?>" frameborder="1"> </iframe>
		            <?php }
		          } else {?>
		      
		      <iframe width="220" height="145" src="<?=$product->video_url?>" frameborder="1"> </iframe>
		          
		  	<?php } } }?>
		  	</p>
		   	<div class="clear"></div>
			<? echo '<br>'?>
			<h2><?php echo $this->lang->line("Availability"); ?></h2>
			    <?php if($details->open_time != '' && $details->close_time != ''){
				     $open  = explode(",",$details->open_time);
				     $close = explode(",",$details->close_time);?>	
				<div class="alighment_sup_gyms" style="float: left;">
					<?php
				      if(isset($open[0]) && $open[0] != ''){echo '<p style="float: left; margin: 0px; width: 85px;">Saturday</p>'. $open[0].$open[1].' - '.$close[0].$close[1].'<br>';}
					if (isset($open[2]) && $open[2] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;"> Sunday </p>' . $open[2] . $open[3] . ' - ' . $close[2] . $close[3] . '<br>';
					}
					if (isset($open[4]) && $open[4] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;">Monday</p>' . $open[4] . $open[5] . ' - ' . $close[4] . $close[5] . '<br>';
					}
					if (isset($open[6]) && $open[6] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;">Tuesday</p>' . $open[6] . $open[7] . ' - ' . $close[6] . $close[7] . '<br>';
					}
					if (isset($open[8]) && $open[8] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;">Wednesday</p>' . $open[8] . $open[9] . ' - ' . $close[8] . $close[9] . '<br>';
					}
					if (isset($open[10]) && $open[10] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;">Thursday</p>' . $open[10] . $open[11] . ' - ' . $close[10] . $close[11] . '<br>';
					}
					if (isset($open[11]) && $open[11] != '') {
						echo '<p style="float: left; margin: 0px; width: 85px;">Friday</p>' . $open[12] . $open[13] . ' - ' . $close[12] . $close[13] . '<br>';
					} ?></div><?php } ?>
			<?php if ($details->open_time == '' && $details->close_time == '') { ?>
				<p><?php echo $this->lang->line("24 Hours"); ?></p><?php } ?>
	            <!--<p><?php// echo $details->open_time .'&nbsp; To &nbsp;'. $details->close_time;?></p>-->
			<div class="clear"></div>
			<? echo '<br>'?>
			<? echo '<br>'?>
			<? echo '<br>'?>
			<h2><?php echo $this->lang->line("Caters to"); ?></h2>
				<p><?php if(isset($details)){ if($details->speciality == 1) echo 'Men only'; else if($details->speciality == 2) echo 'Women only'; else echo 'Mixed';} ?></p>

			<div class="clear"></div>
			<h2><?php echo $this->lang->line("Country"); ?></h2>
		  		<p><?php if(isset($details)) echo $details->country_name;?></p>
		 	<div class="clear"></div>
			<h2><?php echo $this->lang->line("State"); ?></h2>
		  		<p><?php if(isset($details)) echo $details->state;?></p>
		 	<div class="clear"></div>
			<h2><?php echo $this->lang->line("City"); ?></h2>
		  		<p><?php if(isset($details)) echo $details->city;?></p>
		 	<div class="clear"></div>
			<h2><?php echo $this->lang->line("Street"); ?></h2>
		  		<p><?php if(isset($details)) echo $details->gstreet;?></p>
		 	<div class="clear"></div>
			<h2><?php echo $this->lang->line("Zip code "); ?></h2>
				<p><?php if(isset($details)) echo $details->gzip;?></p>

			<div class="clear"></div>
			<h2><?php echo $this->lang->line("Category"); ?></h2>
		 		<p><?php if($this->session->userdata['type'] == 1) echo 'Gym'; else echo 'Personal Trainer';?> </p>
		 	<div class="clear"></div> 
		 	<?php if(isset($details)){ $adv = explode(',',$details->advanced);}?>
			<h2><?php echo $this->lang->line("Advanced"); ?></h2>
		  				<p>
		  				<?php echo $details->advanced;?></p>

			<div class="clear"></div>
			<h2><?php echo $this->lang->line("Contact Person"); ?></h2>
				<p><?php if(isset($details)) echo $details->owner;?></p>
		  	<div class="clear"></div>
			<h2><?php echo $this->lang->line("Phone No"); ?></h2>
				<p><?php if(isset($details)) echo $details->ow_phone;?></p>
		  	<div class="clear"></div>
			<h2><?php echo $this->lang->line("Email"); ?></h2>
				<p><?php if(isset($details)) echo $details->ow_email;?></p>
		  	<div class="clear"></div>
			<h2><?php echo $this->lang->line("Site Url"); ?></h2>
				<p><?php if(isset($details)) echo $details->site_url;?></p>
		  	<div class="clear"></div>
		  	<?php if($this->session->userdata['type'] == 2){?>
				<h2><?php echo $this->lang->line("Certifications"); ?></h2>
			  	<p> 
			 	<?php $res = explode('~',$details->certifications);
			 		echo implode(',', $res);?> </p>
			 	<div class="clear"></div> 
		  	<?php } ?>
			<h2><?php echo $this->lang->line("Description"); ?></h2>
				<p style="width:80% !important;"><?php if(isset($details)) echo $details->description;?></p>
		  	<div class="clear"></div>		   
		</div>
		<div class="clear"></div>
	</div>
</div>