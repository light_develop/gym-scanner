<html>
	<head>
<meta charset="utf-8" />
        <title><?php echo $this->lang->line("Admin Dashboard"); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_responsive.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_default.css" rel="stylesheet" id="style_color" />
<link href="<?php echo base_url()?>themes/admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/uniform/css/uniform.default.css" />
<link href="<?php echo base_url()?>themes/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>themes/admin/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

	<body>
		
<div id="main-content" style="margin-left: 0px; ">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
  
         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4><?php echo $this->lang->line("User Registration"); ?></h4>
                <!-- <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo User_URL; ?>users" class="icon-arrow-left"> Back </a> </span> -->
              </div>
              <div class="widget-body form">

              <form action="<?php echo USER_URL;?>user/add" class="form-horizontal" method="post" id="user_profile" enctype = "multipart/form-data"/>
                <div class="error"> </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("First Name"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="firstname" value=""/>
                  </div>
                </div>


                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Last Name"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="lastname" value=""/>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Date of Birth"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="dob" value="" id="datepicker" />
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Gender"); ?></label>
                  <div class="controls">
                    <label class="radio">
                        <input type="radio" name="gender" value="1" checked/=""
                        /><?php echo $this->lang->line("Male"); ?></label>
                    <label class="radio">
                        <input type="radio" name="gender" value="2"/><?php echo $this->lang->line("Female"); ?></label>
                    </label>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Height"); ?></label>
                  <div class="controls">
                   <input class="span6 required number" type="text" name="height" value=""/>
                      <span><?php echo $this->lang->line("In cms"); ?></span>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Weight"); ?></label>
                  <div class="controls">
                   <input class="span6 required number" type="text" name="weight" value=""/>
                      <span><?php echo $this->lang->line("In kgs"); ?></span>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Mobile No."); ?></label>
                  <div class="controls">
                      <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile"
                             value=""></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Email"); ?></label>
                  <div class="controls">
                      <input class="span6 required email" type="text" name="email" value=""></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Street"); ?></label>
                  <div class="controls">
                   <input class="span6" type="text" name="street" value=""/>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("City"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="city" value=""/>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("State"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="state" value=""/>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Country"); ?></label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="country" value=""/>
                  </div>
                </div>

                 <div class="control-group">
                     <label class="control-label"><?php echo $this->lang->line("Zip Code"); ?></label>
                  <div class="controls">
                    <input class="span6 required" type="text" name="zip" value=""/>
                  </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Occupation"); ?></label>
                  <div class="controls">
                      <input class="span6 required" type="text" name="occupation" value=""></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Comments"); ?></label>
                    <div class="controls">
                        <input class="span6 " type="text" name="comments" value=""></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("membership"); ?></label>
                    <div class="controls">
                     <?php $cat['select'] = 'Select Category'; $attributes = 'id="cat_id" class="chosen span6 required"  tabindex="6" ';
                       echo form_dropdown('cat_id', $cat, 'select',$attributes);
                     ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Status"); ?></label>
                  <div class="controls">
                      <select name="status" id="status" class="chosen span6 required">
                          <option value="1" selected="selected"><?php echo $this->lang->line("Active"); ?></option>
                          <option value="0"><?php echo $this->lang->line("Inactive"); ?></option>
                   </select>
                </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Referred By"); ?></label>
                    <div class="controls">
                        <input class="span6 required" type="text" name="refferedby" value=""></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Gym Name"); ?></label>
                    <div class="controls">
                     <?php $gyms['select'] = 'Select Gym'; $attributes= 'id="gym_id" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('gym_id', $gyms, 'select',$attributes);
                    ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo $this->lang->line("Points"); ?></label>
                    <div class="controls">
                        <input class="span6 number" type="text" name="points" value=""></div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"
                            id="update_profile"><?php echo $this->lang->line("Submit"); ?></button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>    
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script>
  jQuery(document).ready(function(){

 $("#user_profile").validate({

  rules: {
    // simple rule, converted to {required:true}
    name: "required",
    // compound rule
    email: {
      required: true,
      email: true
    },
    
    firstname:"required"
  }
});

 $(function() {
    $( "#datepicker" ).datepicker({
         changeMonth: true,
         changeYear: true,
         yearRange: '1900:+0'
         //yearrange: '1900:'+ new Date().getFullYear()
    });
   
 }); 
 
  });
   
</script>
</div>
	<div id="footer">
		2013 &copy; Provab Dashboard.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"
		type="text/javascript"></script>
<script src="<?php echo base_url()?>themes/admin/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>themes/admin/js/additional-methods.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.blockui.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.cookie.js" type="text/javascript"></script>
	<!--[if lt IE 9]><script src="<?php echo base_url()?>themes/admin/js/excanvas.js"></script><script src="<?php echo base_url()?>themes/admin/js/respond.js"></script><![endif]-->
	
	<script src="<?php echo base_url()?>themes/admin/assets/jquery-knob/js/jquery.knob.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>themes/admin/assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>themes/admin/assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript"
		src="<?php echo base_url()?>themes/admin/assets/data-tables/DT_bootstrap.js"></script>
	<script src="<?php echo base_url()?>themes/admin/js/scripts.js" type="text/javascript"></script>
	<script type="text/javascript">jQuery(document).ready(function(){App.setMainPage(true);App.init()});</script>
</body>
</html>
