<?php $this->load->view('includes/home_header'); ?>

<script src="<?php echo base_url() ?>themes/user/js/slippry.min.js"></script>
<script src="//use.edgefonts.net/cabin;source-sans-pro:n2,i2,n3,n4,n6,n7,n9.js"></script>


<meta name="viewport" content="width=device-width">
<!-- <link rel="stylesheet" href="<?php echo base_url() ?>themes/user/css/demo.css"> -->
<link rel="stylesheet" href="<?php echo base_url() ?>themes/user/css/slippry.css">
<style>

    .heading_gym {
        /* font-family: 'display_light'; */
        font-family: sans-serif;
        text-align: left;
    }
    .ui-corner-all .ui-state-focus {
        background-color: #8c2526;
        color: #ffffff;
    }

    #ui-id-1 {
        max-height: 500px;
        overflow-x: hidden;
    }

    #category {
        background: url("themes/user/images/down-arrow.png") no-repeat scroll right center rgb(255, 255, 255);
        overflow: hidden;
        float: left;
        padding: 0px;
        width: 100%;
    }

    #category select {
        background: none repeat scroll 0 0 transparent;
        border: 0 none;
        width: 285px;
        padding: 4px 4px 0px 4px;
        height: 23px;
        margin-bottom: 1px;
    }

    .search_bg input[placeholder] {
        color: rgb(175, 174, 174) ! important;
    }

    #mainpage select option {
        color: #8c2526 !important;
    }

    #mainpage input {
        color: #8c2526 !important;
    }

    #mainpage .main_cntry {
        width: 31%;
        margin-left: 24px;
    }

    #mainpage .start_div {
        width: 31%;
    }

    .phone_icons {
        margin: 25px 0px;
        text-align: center;
    }
    #category select {
        text-align:center;
        padding-top: 5px;
        font-family: inherit;
    }
    .heading_gym {
        /* font-family: 'display_light'; */
        font-family: sans-serif;
        text-align: left;
    }
    @media screen and (max-width: 1100px) {
        #category select {
            background: none repeat scroll 0 0 transparent;
            border: 0 none;
            width: 200px;
            padding: 4px 4px 0px 4px;
            height: 23px;
            margin-bottom: 1px;
        }

        #mainpage .main_cntry {
            width: 31%;
            margin-left: 20px;
        }
    }

    @media screen and (max-width: 995px) {
        .sideimg {
            float: left;
            width: 100% !important;
        }

        .rightimg {
            margin: 0 auto;
        }

        .textright {
            text-align: right;
        }

        .textleft {
            text-align: left;
        }

        .textcenter {
            text-align: center;
        }

    }

    html * {max-height:1000000px;}

    @media screen and (max-width: 670px) {
        #category select {
            background: none repeat scroll 0 0 transparent;
            border: 0 none;
            width: 155px;
            padding: 4px 4px 0px 4px;
            height: 23px;
            margin-bottom: 1px;
        }

        #mainpage .main_cntry {
            width: 31%;
            margin-left: 15px;
        }
    }

    @media screen and (max-width: 645px) {
        .gymimage {
            width: 50%;
        }
    }

    @media screen and (max-width: 530px) {
        #mainpage .main_cntry {
            width: 100%;
            margin-left: 0px;
        }

        #mainpage .start_div {
            width: 100%;
        }

        #category select {
            background: none repeat scroll 0 0 transparent;
            border: 0 none;
            width: 100%;
            padding: 4px 4px 0px 4px;
            height: 23px;
            margin-bottom: 1px;
        }

        .gymimage {
            width: 100%;
        }
    }

    @media screen and (max-width: 360px) {
        .serach_gym {
            margin-top: 0px !important;
        }
    }
    body {
        position: static;
    }

</style>

<div class="clearfix"></div>
<?php echo $this->getStaticBlock("slider_image"); ?>
<div class="clearfix"></div>
<div class="container na_pd_l">

    <div class="clearfix"></div>
    <section class="mar_top5">
        <aside class="col-lg-9 col-md-9 col-sm-12 col-xs-12 na_pd_l">
            <div class="serach_gym col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <h1 class="heading_gym"><b><? echo $this->lang->line('I AM LOOKING FOR') ?></b></h1>

                <div class="col-lg-12 " style="color: rgb(175, 174, 174) ! important;">
                    <div class="search_bg  col-md-12 col-sm-12 col-xs-12 "
                         style="font-family:gothic !important;color:#8c2526 !important;" id="mainpage">
                        <form action="<?php echo site_url('search') ?>" class="form-horizontal" method="post"
                              id="search_form"/>
                        <div style="float: left; width: 100%;">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 dropdown padding_art start_div">
                                <div
                                    id="category"><?php $attributes = 'id="cat_id" required class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
                                    $cat = array_map('strtoupper', $cat);
                                    echo form_dropdown('cat_id', $cat, '', $attributes);
                                    ?> </div>
                                <font color="red"><span id="error1"></span></font>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding_art main_cntry">
                                <div id="category"><?php $cntry[''] = $this->lang->line('SELECT COUNTRY');
                                    $attributes = 'id="cntry" onchange="load_dropdown_content(this.value)" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
                                    echo form_dropdown('cntry', $cntry, '', $attributes);
                                    ?></div>
                                <font color="red"><span id="error_cntry"></span></font>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 padding_art main_cntry">
                                <div
                                    id="category"><?php $attributes = 'id="state"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required"  ';
                                    $state = array();
                                    $state[''] = $this->lang->line('SELECT STATE');
                                    echo form_dropdown('state', $state, '', $attributes);
                                    ?></div>
                                <font color="red"><span id="error_state"></span></font>
                            </div>
                        </div>
                        <div style="float: left; width: 100%;">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 padding_art start_div">
                                <div
                                    id="category"><?php $attributes = 'id="city" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" ';
                                    $city = array();
                                    $city[''] = $this->lang->line('SELECT CITY');
                                    echo form_dropdown('city', $city, '', $attributes);
                                    ?></div>
                                <font color="red"><span id="error_city"></span></font>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 padding_art main_cntry">
                                <input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required"
                                       placeholder="<? echo $this->lang->line('ZIP CODE') ?>" name="zipcode"
                                       style="height:28px;" id="zipcode"/>
                                <font color="red"><span id="error_zip"></span></font>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 padding_art main_cntry">
                                <input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required"
                                       placeholder="<? echo $this->lang->line('SEARCH BY NAME') ?>" name="search_name"
                                       style="height:28px;" id="search_name"/>
                                <font color="red"><span id="error_search_name"></span></font>
                                <font color="red"><span id=""></span></font>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_bg_1"><? echo $this->lang->line('ADVANCE SEARCH') ?></div>
                <div class="col-lg-10 check_box_bg">
    <span class="adv_search">
    <input type="checkbox" name="adv[pilates]" value="1"/>
    <label><? echo $this->lang->line('PILATES/YOGA') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[mma]" value="1"/>
    <label><? echo $this->lang->line('MMA') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[boxing]" value="1"/>
    <label><? echo $this->lang->line('BOXING') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[cross]" value="1"/>
    <label><? echo $this->lang->line('CROSS TRAINING') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[spinning]" value="1"/>
    <label><? echo $this->lang->line('SPINNING') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[gymnastics]" value="1"/>
    <label><? echo $this->lang->line('GYMNASTICS') ?></label>
    </span>
    <span class="adv_search">
       <input type="checkbox" name="adv[martial]" value="1"/>
    <label><? echo $this->lang->line('MARTIAL ARTS') ?></label>
    </span>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <button class="search_gym_button"><? echo $this->lang->line('Search') ?></button>
                </div>
                </form>
            </div>
            <div style="clear:both">&nbsp;</div>
            <h3><?php echo $this->lang->line("Recently Viewed Gyms"); ?></h3>
            <? foreach ($latest as $key => $value) { ?>
                <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_art na_pd_le main_page_tabs">

                    <div class="gym_cnt">
                        <h2><?= $value->gymname; ?></h2>
                        <figure class="img_fg gymimage"><img
                                src="<?php echo base_url() ?>uploads/gym/logos/<?= $value->gym_logo; ?>" width="100%"
                                height="127" class="img-responsive"/></figure>
                        <article class="art_min_cnt"><?php $data = substr($value->description, 0, 130);
                            echo $data; ?> <span style="float:right;color:blue;"><a
                                    href="<?php echo USER_URL; ?>gym_imgs/<?php echo $value->id; ?>">more...</a></span>
                        </article>
                        <a href="<?php echo site_url('fac_det/' . $value->id); ?>">
                            <button class="button_more_1"><?php echo $this->lang->line("Join Today"); ?></button>
                            <img src="<?php echo base_url(); ?>themes/user/images/arrow.png"/></a>
                    </div>
                </aside>
            <?php }
            if (isset($recently_viewed) && count($recently_viewed)) { ?>

                <?php foreach ($recently_viewed as $key => $value) { ?>
                    <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding_art na_pd_le main_page_tabs">
                        <div class="gym_cnt">
                            <h2><?= $value->gymname; ?></h2>
                            <figure class="img_fg gymimage"><img
                                    src="<?php echo base_url() ?>uploads/gym/logos/<?= $value->gym_logo; ?>"
                                    width="100%" height="127" class="img-responsive"/></figure>

                            <article class="art_min_cnt"><?php $data = substr($value->description, 0, 130);
                                echo $data; ?>
                                <span style="float:right;color:blue;">
			  <a href="<?php echo USER_URL; ?>view_gym/<?php echo $value->id; ?>">more...</a>
			</span>
                            </article>

                            <a href="<?php echo site_url('fac_det/' . $value->id); ?>">
                                <button class="button_more_1"><?php echo $this->lang->line("Join Today"); ?></button>
                                <img src="<?php echo base_url(); ?>themes/user/images/arrow.png"/></a>
                        </div>
                    </aside>
                <?php }
            } ?>

            <div id="timer_div_sh">
                <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padding_art na_pd_l ">
                    <!-- <div align="center" style="margin:25px 0px"><img src="<?php echo base_url() ?>themes/user/images/time_counter.png" class="img-responsive"/></div> -->
                    <div align="center" style="margin:25px 0px;float:left;width:100%;" class="count_time">
                        <h1 style=" font-family: Alpaca Scarlett Demo;"><? echo $this->lang->line('Gymscanner.com goes live in Minnesota:') ?></h1>

                        <div id="countdown" class="countvalues"></div>
                    </div>
                </aside>
            </div>
        </aside>
        <aside class="col-lg-3 col-md-3 col-sm-9 col-xs-12 sideimg">
            <section><img src="<?php echo base_url() ?>themes/user/images/mobil.png" class="img-responsive rightimg"/>
            </section>
            <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 phone_icons">
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4 textright"><img
                        src="<?php echo base_url() ?>themes/user/images/ios.png"/></div>
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4 textcenter"><img
                        src="<?php echo base_url() ?>themes/user/images/android.png"/></div>
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4 textleft"><img
                        src="<?php echo base_url() ?>themes/user/images/windows.png"/></div>
            </section>
        </aside>
    </section>
</div>

<script>
    $(function () {
        $("#search_name").autocomplete({
            source: "user/get_gymname" // path to the get gyms
        });
    });
</script>
<script>

    $("#search_form").submit(function (event) {
        var search_name = document.getElementById('search_name').value;
        var cat = document.getElementById('cat_id').value;
        var cntry = document.getElementById('cntry').value;
        var state = document.getElementById('state').value;
        var city = document.getElementById('city').value;
        $('#error1').html('');
        $('#error_cntry').html('');
        $('#error_state').html('');
        $('#error_city').html('');
        if (search_name != "") {
            return true;
        }
        if (cat == '') {
            document.getElementById('error1').innerHTML = "Select any category";
            return false;
        }
        else if (cntry == "") {
            document.getElementById('error_cntry').innerHTML = "Select any country";
            return false;
        }
        else if (state == '' || state == 0) {
            document.getElementById('error_state').innerHTML = "Select any state";
            return false;
        }
        else if (city == '') {
            document.getElementById('error_city').style.display = "Select any city";
            return true;
        }
        else {
            return true;
        }

    });
    //add states to register-user form
    function load_dropdown_contents(selected_value) {

        if (selected_value != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_states_for_register_form/' + selected_value,
                'async': false
            }).responseText;
            console.log(result);
            $("#state_select").replaceWith(result);
        }
    }
    function load_dropdown_content(selected_value) {

        if (selected_value != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_states/' + selected_value,
                'async': false
            }).responseText;
            $("#state").replaceWith(result);
        }
    }
    //add cities into register user form
    function get_cities_form_registration(state) {
        if (state != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_cities_for_register_form/' + state,
                'async': false
            }).responseText;
            $("#city_select").replaceWith(result);
        }
    }
    function get_cities(state) {
        if (state != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_cities/' + state,
                'async': false
            }).responseText;
            $("#city").replaceWith(result);
        }
    }

</script>
<script>
    $(function () {
        var demo1 = $("#demo1").slippry({
            transition: 'horizontal',
            useCSS: true,
            speed: 1000,
            pause: 3000,
            auto: true,
            preload: 'visible'
        });

        $('.stop').click(function () {
            demo1.stopAuto();
        });

        $('.start').click(function () {
            demo1.startAuto();
        });

        $('.prev').click(function () {
            demo1.goToPrevSlide();
            return false;
        });
        $('.next').click(function () {
            demo1.goToNextSlide();
            return false;
        });
        $('.reset').click(function () {
            demo1.destroySlider();
            return false;
        });
        $('.reload').click(function () {
            demo1.reloadSlider();
            return false;
        });
        $('.init').click(function () {
            demo1 = $("#demo1").slippry();
            return false;
        });
    });

</script>

<?php $this->load->view('includes/footer'); ?>

