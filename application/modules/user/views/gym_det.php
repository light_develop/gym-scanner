<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>themes/user/css/www-pkg.css">
<link rel="stylesheet" type="text/css" media="all" href="http://s3-media4.fl.yelpcdn.com/assets/2/www/css/0e6aa74bccf9/baz-pkg.css">



<script src="<?=base_url();?>themes/user/js/jquery.bxslider.min.js"> </script>
<link href="<?=base_url();?>themes/user/css/jquery.bxslider.css" rel="stylesheet" type="text/css" media="all" />

<script>

$(document).ready(function(){
  $('.slider4').bxSlider({
    slideWidth: 200,
    infiniteLoop: false,
    slideMargin: 10,
  	pager:0
  });
});
</script>

<div class="clearfix"></div>
<section style="width: 100%;"> <img src="<?php echo base_url()?>themes/user/images/inner-img.png" class="img-responsive" width="100%" id="blur"/> </section>
<div class="clearfix"></div>
<div class="container serach_result">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
   <aside class="col-lg-3 hidden-xs" >
   <div class="serach_inner col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
       <h1 class="heading_gym"><?php echo $this->lang->line("I AM LOOKING FOR"); ?></h1>
    <form action="<?php echo site_url('search');?>" method="post" id="search_form" >
    <div class="col-lg-12 ">
      <div class="search_bg col-md-12 col-sm-12 col-xs-12 ">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_art ">
      		<?php $attributes= 'id="cat_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
    			echo form_dropdown('cat_id', $cat,$this->session->userdata('cat_id'),$attributes);?>
    	  </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_art ">
        	<?php $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" required';

        	if(null !== ($this->session->userdata('cntry')) )
			{
				$cntry_z = array('0' => 'Country');
				$states_z = array('0' => 'States');
				$cities_z = array('0' => 'Cities');

				$cntry = $cntry_z + $cntry;
				$states = $states_z + $states;
       	$cities = $cities_z + $cities;

			}
		    	echo form_dropdown('cntry', $cntry, $this->session->userdata('cntry'),$attributes);
		    ?>
		    <font color="red"><span id="error_cntry"></span></font>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_art ">
        	<?php $attributes= 'id="state"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" onchange="get_cities(this.value)"';
		    	echo form_dropdown('state', $states, $this->session->userdata('state'),$attributes);
		    ?>
		    <font color="red"><span id="error_state"></span></font>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_art ">
        	<?php $attributes= 'id="city"  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ';
		    	echo form_dropdown('city', $cities, $this->session->userdata('city'),$attributes);
		    ?> <font color="red"><span id="error_city"></span></font>
        </div>
		<div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
        	<input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" placeholder="SEARCH BY NAME" id="search_name" name="search_name" value="<?php echo @$this->session->userdata('search_name').'' ;?>" />
        </div>
        <div class="col-lg-12 col-md-12 col-sm-6  padding_art ">
        	<input type="text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 required" placeholder="ZIPCODE" name="zipcode" value="<?php echo @$this->session->userdata('zipcode').'' ;?>" />
        </div>
    </div>
    </div>
        <div class="col-lg-12 search_bg_1"><?php echo $this->lang->line("ADVANCE SEARCH"); ?></div>
    <div class="col-lg-12">
    <span class="adv_search">
    <input type="checkbox" name="adv[pilates]" value="1" />
    <label><?php echo $this->lang->line("PILATES / YOGA"); ?></label>
    </span>
    <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[pilates]" value="1" />
    <label><?php echo $this->lang->line("MMA"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[pilates]" value="1" />
    <label><?php echo $this->lang->line("BOXING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[cross]" value="1" />
    <label><?php echo $this->lang->line("CROSS TRAINING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[spinning]" value="1" />
    <label><?php echo $this->lang->line("SPINNING"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[gymnastics]" value="1" />
    <label><?php echo $this->lang->line("GYMNASTICS"); ?></label>
    </span>
     <span class="clearfix"></span>
    <span class="adv_search">
       <input type="checkbox" name="adv[martial]" value="1" />
    <label><?php echo $this->lang->line("MARTIAL ARTS"); ?></label>
    </span>
    </div>
        <div class="col-lg-12">
            <button class="search_gym_button"><?php echo $this->lang->line("Search"); ?></button>
        </div>

  </form>
  </div>
   </aside>
  <aside class="col-lg-9 col-md-9 col-sm-12 col-xs-12 righ_pnl" id="gymdetails">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fillter_bg">
    <span class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="width:50%;"><p class="organize gymheading" style="padding:15px 0px 0px 0px;"><?php echo ucfirst($gym_det->gymname);?></p></span>
    <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="width:50%;">
    <span class="fillter" style="border:none">
      <label><a onclick="goBack()" class="gymheading"><?php echo $this->lang->line("Back"); ?></a></label>
    </span></span>
  </div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 search_result_img" style="width: 25%;">
    	<?php $img = $gym_det->gym_logo;?>
    	<img src="<?php echo base_url();?>uploads/gym/logos/<?php echo $img;?>"  class="img-responsive" style="height: 200px; width: 200px;"/>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 " style="width: 75%;">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12  search_result_head"><?php echo ucfirst($gym_det->gymname);?> &nbsp;
  		  <?php $splty =  $gym_det->speciality;
  		  if($splty == 1){ ?>
  			 <span><img src="<?php echo base_url()?>themes/user/images/male.png" /></span>
  		  <?php } else if ($splty == 2) { ?>
  			 <span><img src="<?php echo base_url()?>themes/user/images/female.png" /></span>
  		  <?php } else { ?>
  			 <span><img src="<?php echo base_url()?>themes/user/images/female.png" /></span>&nbsp;
  			 <span><img src="<?php echo base_url()?>themes/user/images/male.png" /></span>
  		  <?php }?>
   	  </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 search_result_star">
        <?php $rating = $gym_det->star; ?>
    	  <?php // for($s = 0; $s < $rating; $s++){ ?>
    	    <!--<span class="glyphicon glyphicon-star"></span>-->
    	 <?php // } ?>
		 <div class="rating" style="float: right;"><li class="star-img stars_<?php if(isset($rating)) echo $rating; else echo 0;?>" title="5.0 star rating"><img alt="5.0 star rating" class="offscreen" height="303" src="<?php echo base_url()?>themes/user/images/stars_map.png" width="84"></li></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_result_add gym_address">
	  <?php if(isset($gym_det->gstreet)) echo '<b>Address</b> <br>'.ucfirst($gym_det->gstreet).',<br>'.ucfirst($gym_det->city).','.ucfirst($gym_det->state).',<br>'.$gym_det->gzip.', '.ucfirst($gym_det->country_name).'<br>'; ?>
        <?php if(isset($gym_det->site_url) && $gym_det->site_url != '') echo '<p class="profile_url">Website</p> '. $gym_det->site_url.'<br>';?>
        <?php if(isset($gym_det->ow_phone) && $gym_det->ow_phone != '') echo '<p class="profile_url">Telephone</p> +1 '. $gym_det->ow_phone;?>
      </div>
        <div id="working_hours"><b style="float:left;"><?php echo $this->lang->line("Hours Open"); ?></b>
	  <?php if($gym_det->close_time != ''){ ?>
	  <?php if(isset($gym_det->open_time)) $open  = explode(",",$gym_det->open_time);?>
	  <?php if(isset($gym_det->close_time)) $close = explode(",",$gym_det->close_time);?>
		<div style="float: left; width: 100%;">
		  <?php
			if(isset($open[2]) && $open[2] != ''){echo '<p style="float: left; margin: 0px; width: 85px;">Monday</p>'. $open[4].$open[5].' - '.$close[4].$close[5].'<br>';}
          if (isset($open[3]) && $open[3] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Tuesday</p>' . $open[6] . $open[7] . ' - ' . $close[6] . $close[7] . '<br>';
          }
          if (isset($open[4]) && $open[4] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Wednesday</p>' . $open[8] . $open[9] . ' - ' . $close[8] . $close[9] . '<br>';
          }
          if (isset($open[5]) && $open[5] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Thursday</p>' . $open[10] . $open[11] . ' - ' . $close[10] . $close[11] . '<br>';
          }
          if (isset($open[6]) && $open[6] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Friday</p>' . $open[12] . $open[13] . ' - ' . $close[12] . $close[13] . '<br>';
          }
          if (isset($open[0]) && $open[0] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Saturday</p>' . $open[0] . $open[1] . ' - ' . $close[0] . $close[1] . '<br>';
          }
          if (isset($open[1]) && $open[1] != '') {
              echo '<p style="float: left; margin: 0px; width: 85px;">Sunday</p>' . $open[2] . $open[3] . ' - ' . $close[2] . $close[3] . '<br>';
          }
		  ?>
		</div>
	  <?php }else{ ?>
          <div style="width: 100%; float: left;"><?php echo $this->lang->line("24 Hours"); ?></div><?php } ?>
	  </div>
      <div class="col-lg-9  col-md-9 col-sm-9 col-xs-12 search_result_map"></div>
      <!-- <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 search_result_doller" >
       	<?php $rating = $gym_det->price_range; ?>
    		<?php for($s = 0; $s < $rating; $s++){?>
    			<span><img src="<?php echo base_url()?>themes/user/images/doller.png"/></span>
    		<?php } ?>
      </div> -->
    </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="map" style="width:100%;height:250px;">
    <?php $this->load->view('gymmap'); ?>
    </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div style="float:right; margin-top: 10px;">
  <form action="<?php echo USER_URL;?>sendlocation" method="post">
      <p style="float: left; margin-left: 10px;"><?php echo $this->lang->line("Share facility location to email"); ?></p>

    <input type="email" name="mail_id" placeholder="Email id" style="float: left; margin-left: 10px;" required>
    <input type="hidden" name="gym_id" value="<?php echo $gym_det->id;?>" style="float: left; margin-left: 10px;">

      <div style="float: left; margin-left: 10px;"><!--<input type="submit" class="btn btn-danger "
                                                          value="<?php /*echo $this->lang->line("EMAIL ME"); */?>"
                                                          style="float: left;"><img
              src="<?php /*echo base_url(); */?>themes/user/images/mail-icon.jpg" alt="mail-icon"/>-->
          <button type="submit" class="btn btn-danger "><?php echo $this->lang->line("EMAIL ME"); ?>&emsp;<span class="glyphicon glyphicon-envelope"></span></button>
      </div>

  </form>
  </div>
  </div>

  <div style="height:20px">&nbsp;</div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div style="float: left; width: 100%;"><!--<input type="button" style="float:left;" class="search_gym_button"  value="PROFILE"/><img src="<?php /*echo base_url(); */?>themes/user/images/list-icon.jpg" alt="list-icon" />--><button type="button" class="btn btn-danger">PROFILE&emsp;<span class="glyphicon glyphicon-list-alt"></span></button></div>
    <article><?php echo $gym_det->description;?></article>
  </div>
  <div style="height:20px">&nbsp;</div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	<div style="float: left; width: 100%;margin-top: 20px;">
        <button type="button" class="btn btn-danger">PICTURES&emsp;<span class="glyphicon glyphicon-picture"></span></button>
		<!--<div style="margin-top: 20px;">
		  <span class='st_facebook' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		  <span class='st_googleplus' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		  <span class='st_twitter' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		  <span class='st_linkedin' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		  <span class='st_pinterest' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		  <span class='st_email' st_url="<?php // echo site_url('fac_det/'.$gym_det->id); ?>/image"></span>
		 </div>-->
	</div>
	<div style="float:left; width:800px;">

    <div class="slider4" style="float:left;">
      <?php

	  foreach($gym_images as $imgs): ?>
      <div class="slide" style="width:200px !important;">
        <div class="popup-gallery">
		<input type="hidden" value="<?php echo $imgs->gym_images;?>"  id="img_name" />
          <a href="<?php echo base_url();?>uploads/gym/images/<?php echo $imgs->gym_images;?>" title="Gym Images"><img src="<?php echo base_url();?>uploads/gym/images/<?php echo $imgs->gym_images;?>" width="200px" height="170px" alt="<?php echo $imgs->gym_images;?>" /> </a>
          <!-- <a href="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_b.jpg" title="The Cleaner"><img src="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_s.jpg" width="300px" height="170px"/></a> -->
        </div>
      </div>
      <?php endforeach;?>
    </div>
	</div>
	<div style="float: left;" id="video_share">
	  <span class='st_facebook' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/image"  style="margin-right:15px;"></span>
	  <span class='st_twitter' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/image"  style="margin-right:15px;"></span>
	  <span class='st_instagram' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/image"  style="margin-right:15px;"></span>
	  <span class='st_youtube_large' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/image"  style="margin-right:15px;"></span>
	</div>

  </div>
  <div class="clearfix"></div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div style="float: left; width: 100%;">
		<div style="width:100%;float: left;margin-top:15px;"><!--<input type="button"  class="search_gym_button"  value="VIDEOS" style="float:left;"/><img src="<?php /*echo base_url(); */?>themes/user/images/play-icon.jpg" alt="play-icon" />-->
            <button type="button" class="btn btn-danger">VIDEOS&emsp;<span class="glyphicon glyphicon-film"></span></button>
        </div>
	 </div>
	 <div>
		<?php
		if($gym_det->gym_video != '') {
			$videos = $gym_det->gym_video;
			$res = explode('~',$gym_det->gym_video);
			foreach($res as $value){
			  $var = explode('v=',$value);
			  $new = explode('/',$value);
			  if($new[3] ||$var[1]) {
			  if(strpos($value,'vimeo') !== false)
				{?>
					<iframe src="http://player.vimeo.com/video/<?=$new[3]?>" width="31%" height="224" frameborder="1"  class="gymvideo"></iframe>
				<?php }

			   if (strpos($value,'youtube') !== false)
			   { ?>
					<iframe width="29%" height="224" style="margin-left:0px;" src="http://www.youtube.com/embed/<?=$var[1]?>" frameborder="1" class="gymvideo"> </iframe>
				<?php }
		  } else {?>

					<iframe width="29%" height="224" style="margin-left:34px;" src="<?=$product->video_url?>" frameborder="1" class="gymvideo"> </iframe>

	  <?php } } }?>
    </div>
	<div style="float: left;" id="video_share">
	  <span class='st_facebook' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/video"  style="margin-right:15px;"></span>
	  <span class='st_twitter' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/video"  style="margin-right:15px;"></span>
	  <span class='st_instagram' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/video"  style="margin-right:15px;"></span>
	  <span class='st_youtube' st_url="<?php echo site_url('fac_det/'.$gym_det->id); ?>/video"  style="margin-right:15px;"></span>
	</div>
  </div>
  <?php if(@$this->session->userdata('user_id')){ ?>
  <div class="clearfix"></div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top:10px;padding-bottom:10px;">
    <div class="exclusive"><a href="<?php echo site_url('view_offers/'.$gym_det->id)?>" ><input type="button" style="margin:auto;" class="search_bg_1"  value="View Exclusive Offers"/></a></div>

  <br/>
  <br/>

  <form action="<?php echo USER_URL;?>gymreview" method="post">
	<div style="float:left;width:100%;"><!--<input type="button" style="float:left;" class="search_gym_button"  value="Reviews"/><img src="<?php /*echo base_url(); */?>themes/user/images/profile-icon.jpg" alt="right-icon" />--><button type="button" class="btn btn-danger">REVIEWS &emsp;<span class="glyphicon glyphicon-user"></span></button></div>
	<input type="hidden" value="" id="ratingval" name="ratingval" />
	<input type="hidden" value="<?php echo $gym_det->is_supplier;?>" name="user_type" />
	<div style="float: left; width: 100%; margin-top: 10px;box-shadow: 0 0 8px #d0d0d0;margin-bottom: 20px;">
        <div style="float: left; width: 100%;padding: 10px;">
            <div
                style="float: left; width: 20%;font-size: 14px; font-weight: bold;"><?php echo $this->lang->line("Rating"); ?></div>

			<div style="float: right; width: 75%;"><div id="field-star-rating" class="js"><div class="star-rating-widget" id="rating_ul">
				<ul class="stars-0">
				<?php for($k=1;$k<=5;$k++):?>
				<li><input id="rating_<?=$k;?>" type="radio" name="rating" value="<?=$k;?>" <?php if($k == 1) { ?>checked <?php } ?> ></li>
				<?php endfor;?><br/>
				</ul>
			</div></div></div><br/>
		</div>
        <div style="float: left; width: 100%;padding: 10px;">
            <div
                style="float: left; width: 20%;font-size: 14px; font-weight: bold;"><?php echo $this->lang->line("Your review"); ?></div>
			<div style="float: right; width: 75%;"><textarea style="width:100%;min-height:60px;" name="comment" placeholder="Review message" required></textarea><br/>
				<input type="hidden" name="gym_id" value="<?php echo $gym_det->id;?>">
				<input type="hidden" name="user_type" value="<?php echo $this->session->userdata('level');?>">
				<input type="hidden" name="user_level" value="<?php echo $this->session->userdata('type');?>">
			</div>
		</div>
		<div style="float: left; width: 100%;padding: 10px;margin-top: -20px;">
			<div style="float: left; width: 20%;font-size: 14px; font-weight: bold;"></div>
			<div style="float: right; width: 75%; margin-top: 10px;"><!--<input type="submit" style="float:left;" class="search_gym_button"  value="Submit"><img src="<?php /*echo base_url(); */?>themes/user/images/right-icon.jpg" alt="right-icon" />--><button type="submit" class="btn btn-danger"> SUBMIT&emsp;<span class="glyphicon glyphicon-ok"></span></button></div>
		</div>
	</div>
  </form>
  <br/>
  <?php  if(isset($gym_reviews)  && count($gym_reviews)){  ?>
          <b style="font-size: 16px;"><?php echo $this->lang->line("View All Reviews "); ?></b>


          <?php $over_all_rating = 0; $i = 1;foreach ($gym_reviews as $row)  {  ?>

          <div style="float: left; width: 100%; margin-top: 10px;border-bottom: 1px solid #e5e5e1;">
			<span>&nbsp;&nbsp;
				<?php

				 $rating = $row->gym_rating;
				 $over_all_rating +=$rating;
				 //for($s = 0; $s < $rating; $s++){ ?>
				<!--<span class="glyphicon glyphicon-star" style="float:right;"></span>-->
			 <div style="float: left; width: 20%;font-size: 14px; font-weight: bold;"><?php //}

			 if($row->user_level == "supplier"){
			 	$qry = "SELECT first_name, last_name FROM supplier
									WHERE id = $row->user_id ";
			 }else{
			 	$qry = "SELECT first_name, last_name FROM user
									WHERE id = $row->user_id ";

			 }
			 $result = $this->db->query($qry)->result();
			 echo ucfirst(@$result[0]->first_name).' '.ucfirst(@$result[0]->last_name).' :';  ?></div>
			 <div style="float: right; width: 75%;">
			     <div style="float: left; width: 100%;margin-bottom: 10px;"><div class="rating" style="float: left;"><li class="star-img stars_<?php if(isset($rating)) echo $rating; else echo 0;?>" title="5.0 star rating"><img alt="5.0 star rating" class="offscreen" height="303" src="<?php echo base_url()?>themes/user/images/stars_map.png" width="84"></li></div><span style="float: left;margin-left: 10px; font-size: 14px;"><?php echo date('m/d/Y', strtotime($row->createdon)); ?></span></div>
				 </span><br/>
				 <div style="float: left; margin-bottom: 10px; text-align: justify; font-size: 14px; line-height: 20px;"><?php echo $row->comment; ?></div><br/>

          	 </div>
          </div>
          <hr/>

  <?php $i++; }
          ?>
          <div style="float: left; width: 100%; margin-top: 10px;">
              <div
                  style="float: left; width: 20%;font-size: 14px; font-weight: bold;"><?php echo $this->lang->line("Over all rating:"); ?></div><?php
   //echo "<hr/>Over all rating : </b>";
			$no_of_usr = $i-1;
			$over_all_rating = $over_all_rating/$i;
		//for($s = 0; $s < $over_all_rating; $s++){ ?>
				<!--<span class="glyphicon glyphicon-star" style="float:right;"></span>-->
				<div style="float: right; width: 75%;"><div class="rating" style="float: left;"><li class="star-img stars_<?php if(isset($over_all_rating)) echo round($over_all_rating); else echo 0;?>" title="5.0 star rating"><img alt="5.0 star rating" class="offscreen" height="303" src="<?php echo base_url()?>themes/user/images/stars_map.png" width="84"></li></div></div></div>
			 <?php //}

		}
 }
		else { ?>
  <div style="float: left; margin-left: 15px; margin-top: 10px;"><a href="#openModal"  data-toggle="modal" data-target=".bs-example-modal-sm" style="text-decoration: none;">
          <input type="button" style="width:180px !important;;" class="search_gym_button"
                 value="<?php echo $this->lang->line("Sign-in to see offers"); ?>"/></a></div>
  <?php  } ?>

  </div>

  <div class="clearfix"></div>
  </aside>
</div>

  <div class="clearfix"></div>
	<!--<a target="_blank" title="Share this page" href="http://www.sharethis.com/share?url=[INSERT URL]&title=[INSERT TITLE]&summary=[INSERT SUMMARY]&img=http://www.gymscanner.com/uploads/gym/images/package_387402.png&&img=http://www.gymscanner.com/uploads/gym/images/package_287954.png&pageInfo=%7B%22hostname%22%3A%22[INSERT DOMAIN NAME]%22%2C%22publisher%22%3A%22[INSERT PUBLISHERID]%22%7D"><img width="86" height="25" alt="Share this page" src="http://w.sharethis.com/images/share-classic.gif"></a>-->

</div>
<link rel="stylesheet" href="<?php echo base_url();?>themes/user/js/magnific-popup/magnific-popup.css">

<script src="<?php echo base_url();?>themes/user/js/magnific-popup/jquery.magnific-popup.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $( "input[type='checkbox']" ).on( "click", showValues );

  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
	  cursor: 'mfp-zoom-out-cur',
      titleSrc: function(item) {
        return item.el.attr('title') + '<small>by Gym Scanner</small>';
      }
    },
	callbacks: {

         buildControls: function() {
		 //var $index = $.magnificPopup.instance.currItem.index,

            // re-appends controls inside the main container
			//var img = delegate: 'a';
            //this.contentContainer.append('<a style="position: sticky; float: left; height: 30px;" target="_blank" title="Share this page" href="http://www.sharethis.com/share?url=[INSERT URL]&title=[INSERT TITLE]&summary=[INSERT SUMMARY]&img='+$index+'&pageInfo=%7B%22hostname%22%3A%22[INSERT DOMAIN NAME]%22%2C%22publisher%22%3A%22[INSERT PUBLISHERID]%22%7D"><img width="86" height="25" alt="Share this page" src="http://w.sharethis.com/images/share-classic.gif"></a>');

         }

      }


  });

  $( "#search_form" ).submit(function( event ) {
  	  var search_name  = document.getElementById('search_name').value;
      var cat = document.getElementById('cat_id').value;
      var cntry = document.getElementById('cntry').value;
      var state = document.getElementById('state').value;
      var city = document.getElementById('city').value;
      $('#error1').html('');
      $('#error_cntry').html('');
      $('#error_state').html('');
      $('#error_city').html('');

	  	if(search_name != ""){
			return true;
		}
		if(cat == ''){
		  document.getElementById('error1').innerHTML = "Select any category";
		  return false;
		}
		else if(cntry == "" || cntry == 0)
		{
		  document.getElementById('error_cntry').innerHTML = "Select any country";
		  return false;
		  }
		else if(state == '' || state == 0)
		{
		  document.getElementById('error_state').innerHTML = "Select any state";
		  return false;
		}
		else if(city == '' || city == 0)
		{
		  document.getElementById('error_city').style.display = "Select any city";
		  return false;
		}
		else
		{
		  return true;
		}
  });

/*$('.slider4 img').map(function(){
	//alert($(this).attr("src").last());
	var img_src =  $(this).attr("src");
	$("#light").append("<meta property='og:image' content='"+img_src+"' />");
	$("head").append("<meta property='og:image' content='"+img_src+"' /><meta property='og:image:height' content='300' /><meta property='og:image:width' content='300' />");
});*/

});




   function load_dropdown_content(selected_value){

       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_states/' + selected_value,
                   'async' : false
             }).responseText;
         $("#state").replaceWith(result);
      }
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function showValues() {
    var str = $( "#filter_search" ).serialize();
    var result = $.ajax({
			'type' : 'post',
        	         'url' : '<?php echo USER_URL;?>filter_search',
        	         'data' : {'ser_data':str},
        	         'async' : false
    	       }).responseText;
    	   $(".right_kuwait").html(result);
  }


   function saveOfferFunc(id)
   {
	    $('span[id^=errMsg]').html('');
		var choose = $('input[name="choose_offer'+id+'"]:checked').val();

		if(choose == 1)
		{
			$.getJSON('<?php echo USER_URL;?>addChoosenOffers', {id:id, choose:choose}, function(data){
				$('#errMsg'+id).css('color', 'green').html('Offers details added!');
			});
		}
		else if(choose == 2)
		{
			var s_date = $('#start_date'+id).val();
			var e_date = $('#end_date'+id).val();
			if(!s_date || !e_date)
			{
				$('#errMsg'+id).css('color', 'red').html('Please select start date & end date!');
			}
			else
			{
				$.getJSON('<?php echo USER_URL;?>addChoosenOffers', {id:'<?=$gym_det->id;?>', choose:choose, start_date:s_date, end_date:e_date}, function(data){
						$('#errMsg'+id).css('color', 'green').html('Offers details added!');
				});
			}
		}
		else
		{
			$('#errMsg'+id).css('color', 'red').html('Please choose any option!');
		}

   }
   function showDatePick(id)
   {
	   $('input[id^=start_date], input[id^=end_date]').val('').hide();
	   $('#start_date'+id+', #end_date'+id).show();

	   	$('#start_date'+id).datepicker({
			dateFormat: 'dd-mm-yy',
			minDate:0,
			onClose: function( selectedDate ) {
				$("#end_date"+id).datepicker( "option", "minDate", selectedDate );
			}
		});

		$('#end_date'+id).datepicker({
			dateFormat: 'dd-mm-yy',
			minDate:0,
			onClose: function( selectedDate ) {
				$("#start_date"+id).datepicker( "option", "maxDate", selectedDate );
			}
		});
   }
   $( "#rating_1" ).hover(function() {
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-1");
	});
	$( "#rating_2" ).hover(function() {
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-2");
	});
	$( "#rating_3" ).hover(function() {
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-3");
	});
	$( "#rating_4" ).hover(function() {
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-4");
	});
	$( "#rating_5" ).hover(function() {
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-5");
	});
	$( "#rating_ul ul" ).mouseout(function() {
		var star_val = $("#ratingval").val();
		if(star_val == ''){
			$("#rating_ul ul").removeClass();
			$("#rating_ul ul").addClass("stars-0");
		}
	});
	$( "#rating_ul li input" ).click(function() {
    	//alert($(this).val());
		var star_val = $(this).val();
		$("#ratingval").val(star_val);
		$("#rating_ul ul").removeClass();
		$("#rating_ul ul").addClass("stars-"+star_val);
	});
</script>

<style>
.slide{
	width:200px !important;
}
.js .star-rating-widget li {
    float: left;
    height: 30px;
    margin-left: 3px;
    overflow: hidden;
    padding-right: 0;
    width: 30px;
}
.js .star-rating-widget li input {
	border: medium none;
    height: 30px;
    margin: 0;
    opacity: 0;
    padding: 0;
    width: 30px;
}
.star-rating-widget {
    background-color: #fdffcd;
    border: 1px solid #c41200;
    float: left;
    margin-right: 20px;
    padding: 10px;
}
.bx-wrapper{
	float: left;
    margin: 0 auto;
    max-width: 920px;
}
.ui-corner-all .ui-state-focus{
	background-color:#8c2526;
	color:#ffffff;
}
#ui-id-1{
	max-height:500px;
	overflow-x:hidden;
}
#working_hours{
	box-shadow: 0 0 5px #d0d0d0;
    float: right;
    padding: 10px;
    width: 40%;
	line-height: 30px;
}
.profile_url{
	font-weight: bold;
    margin: 15px 0 0;
}
.bx-wrapper .bx-viewport {
    background: none repeat scroll 0 0 #fff;
    border: 5px solid #fff;
    box-shadow: 0 0 5px #ccc;
    left: 0;
}
.gym_address{
	float: left;
	width: 60%;
	line-height: 25px;
}
@media screen and (max-width: 1080px){
	#working_hours {
		box-shadow: 0 0 5px #d0d0d0;
		float: right;
		line-height: 30px;
		padding: 10px;
		width: 220px;
	}
	.gym_address{
		float: left;
		width: 50%;
		line-height: 25px;
	}
}
@media screen and (max-width: 720px){
	.gym_address{
		float: left;
		width: 40%;
		line-height: 25px;
	}
}
@media screen and (max-width: 625px){
	#working_hours {
		box-shadow: 0 0 5px #d0d0d0;
		line-height: 30px;
		margin-top: 10px;
		padding: 10px;
		width: 100%;
	}
	.gym_address {
		float: left;
		line-height: 25px;
		width: 100%;
	}
}
@media screen and (max-width: 550px){
	.gymvideo{
		width:100%;
	}
}
</style>
<script>
$(function(){
  $("#search_name").autocomplete({
    source: "user/get_gymname" // path to the get_birds method
  });
  var val = $("#img_name").val();
  if(val == undefined){
	$(".bx-wrapper").css("display","none");
  }

});
</script>