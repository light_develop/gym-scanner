<style>
    div#footer_info_panel {
        left: 14%;
    }

    form#subsc_form {
        width: 87%;
        margin: 10px 8px;
        position: relative;
        padding: 0px;
    }

    .col-lg-10.check_box_bg input {
        width: 16px;
        height: 16px;
    }

    div#footer_info_panel {
        width: 111%;
        position: relative;
        margin-left: -15%;
    }

    @media (min-width: 968px) {
        div#footer_info_panel {
            width: 111%;
            position: relative;
            margin-left: -15%;
        }
    }

    @media (min-width: 992px) {
        div#footer_info_panel {
            width: 111%;
            position: relative;
            margin-left: -6%;
        }
    }

    @media (min-width: 1200px) {
        div#footer_info_panel {
            width: 111%;
            position: relative;
            margin-left: -6%;
        }
    }

    @media (min-width: 300px) {
       /* ul#navbar li {
            padding: 9px;
        }*/

        .dd-select {
            width: 164px;
            top: 12px;
            left: 10px;
            background: rgb(238, 238, 238);
        }

       /* div#icons_footer {
            left: -51%;
            *//* margin-left: -35%; *//*
            top: -7px;
        }*/

    }

    @media (min-width: 768px) {
        /*div#icons_footer {
            left: -35%;
            *//* margin-left: -35%; *//*
            top: 3px;
        }*/
    }

    }
    @media (min-width: 768px) {
       /* ul#navbar li {
            padding: 10px;
        }*/

    }

    @media (min-width: 768px) {
       /* ul#navbar li {
            padding: 10px;
        }*/

    }

    /*  @media (min-width: 1200px) {
          div#icons_footer {
              left: -13%;
              *//* margin-left: -35%; *//*
              top: 3px;
          }

      }*/
    @media (min-width: 968px) {
       /* div#icons_footer {
            left: -3%;
            *//* margin-left: -35%; *//*
            top: -39px;
        }*/

    }
   
    div#footer_text_gym {
        text-align: center;
    }
</style>
<footer class="footer">
    <div class="footer">
        <div class="container na_pd_le">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
                <img src="<?php echo base_url() ?>themes/user/images/footer_logo.png" class="img-responsive"/>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12"></div>
            <div class="col-lg-5 col-md-5 col-sm-7 col-xs-12 text-center">

            </div>
            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 na_pd_le">
                <div id="footer_info_panel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <nav class="footer_nav">
                        <ul>
                            <li><a href="<?php echo site_url('about'); ?>"><?php echo $this->lang->line("About"); ?></a>
                            </li>
                            <?php //if(isset($this->session->userdata['user_id'])){?>
                            <!--<li><a href="<?php // echo site_url('support')?>"> Customer Support</a></li>-->
                            <?php //} ?>
                            <li><a href="<?php echo site_url('faq'); ?>"><?php echo $this->lang->line("FAQs"); ?></a>
                            </li>
                            <li><a href="#openModal" data-toggle="modal"
                                   data-target=".bs-example-modal-sm"><?php echo $this->lang->line("Gym Login"); ?></a>
                            </li>
                            <!--<li>
                                <a href="<?php /*echo site_url('pers_trns'); */?>"><?php /*echo $this->lang->line("Personal Trainers"); */?></a>
                            </li>-->
                            <li>
                                <a href="<?php echo site_url('refunds'); ?>"><?php echo $this->lang->line("Refunds"); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('privacy_policy'); ?>"><?php echo $this->lang->line("Privacy Policy"); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('terms_cond'); ?>"><?php echo $this->lang->line("Terms and Conditions"); ?></a>
                            </li>
                            <li><a href="<?php echo site_url('contact'); ?>"
                                   style="border:none;"><?php echo $this->lang->line("Contact Us"); ?></a></li>
                        </ul>
                    </nav>
                    <div id="icons_footer" class="col-lg-2 col-md-2 col-sm-12 col-xs-12 social_icons">
                        <ul>
                            <li><a href="https://www.facebook.com/gymscanner" title="Facebook share button"
                                   target="_blank"><img
                                        src="<?php echo base_url() ?>themes/user/images/face_icon.png"/></a></li>
                            <li><a href="https://twitter.com/gymscanner" title="Twitter share button"
                                   target="_blank"><img
                                        src="<?php echo base_url() ?>themes/user/images/twtr_iocn.png"/></a></li>
                            <li><a href="http://instagram.com/gymscanner" title="Instagram share button"
                                   target="_blank"><img src="<?php echo base_url() ?>themes/user/images/pin_icon.png"/></a>
                            </li>
                            <li><a href="http://www.youtube.com/user/gymscanner" title="Youtube share button"
                                   target="_blank"><img
                                        src="<?php echo base_url() ?>themes/user/images/you_t_icon.png"/></a></li>
                        </ul>
                    </div>
                </div>
                <script>
                    function MM_openBrWindow(theURL, winName, features) { //v2.0
                        window.open(theURL, winName, features);
                    }
                </script>
                <?php
                $current_url = base_url();
                $current_title = "GymScanner";
                ?>
                <!-- <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>-->

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 na_pd_le">
                <form action="<?php echo USER_URL . 'asoffers/order_det' ?>" id="subsc_form"
                      class="navbar-form navbar-left" style="margin:10px 8px; padding:0px" role="search" method="post">
                    <div class="form-group"><?php echo $this->lang->line("Manage an existing reservation:"); ?>
                        <input style="margin:0px;border-radius:4px !important;" type="text" class="form-control"
                               placeholder="<?php echo $this->lang->line("Booking Number"); ?>" name="boooking_number">
                    </div>
                    <div class="form-group">
                        <input style="margin:0px;border-radius:4px !important;" type="text" class="form-control"
                               placeholder="<?php echo $this->lang->line("Pin Code"); ?>" name="pin_code"></div>

                    <button style="border-radius:5px;" type="submit"
                            class="btn btn-default"><?php echo $this->lang->line("Go"); ?></button>

                </form>
            </div>
        </div>
        <div class="col-lg-2"></div>

    </div>

    <div class="footer_redbg">

        <div id="footer_text_gym"
            class="container na_pd_le"><?php echo $this->lang->line("GymScanner.com is the first website in the world where you can subscribe to gyms, fitness centers."); ?></div>
    </div>
    <div style="clear:both"></div>
    <div class="container na_pd_le bottom_footer">
        <?php echo $this->getStaticBlock("footer_app_images"); ?>
        

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="right_div">
            <div class="navbar-form navbar-left send_input" role="search">
                <div class="form-group form-group_1" id="email_frm">
                    <p id="text_msg"></p>

                    <div style="float: left; width: 130%;text-align:left;">
                        <input style="background-color:#fff;border-radius:4px !important;" type="text"
                               class="form-control"
                               id="mail_id" placeholder="<?php echo $this->lang->line("Your email id"); ?>">
                        <button style="border-radius:5px;" type="submit" id="subsc_mail"
                                class="btn btn-default"><?php echo $this->lang->line("Send email"); ?></button>
                    </div>
                </div>
            </div>
            <div class="payment_part">
                <img src="<?php echo base_url() ?>themes/user/images/powerstrip.png" class="stripe_img"/>

                <div class="card_type">
                    <img src="<?php echo base_url() ?>themes/user/images/visa.jpg" style="margin-left:5px;"/>
                    <img src="<?php echo base_url() ?>themes/user/images/mastercard.jpg" style="margin-left:5px;"/>
                    <img src="<?php echo base_url() ?>themes/user/images/american-exp.jpg" style="margin-left:5px;"/>
                    <img src="<?php echo base_url() ?>themes/user/images/jcb.png" style="margin-left:5px;"/>
                </div>
            </div>

        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <article class="text-center "
                     style="color:#6b8584"><?php echo $this->lang->line("Use of this website constitutes the acceptance of the Gymscanner.com In corporated Terms and Conditions and Privacy Policy."); ?></article>
            <article class="text-center "
                     style="color:#6b8584"><?php echo $this->lang->line("@ 2015 Gymscanner.com. All Rights Reserved"); ?></article>
        </div>
    </div>
    </div>

    <?php
    if (isset($_POST['formid']) && $_POST['formid'] == 'gotcontact') {


        $name = $_POST['name'];
        $email = $_POST['email'];
        $category = $_POST['request_form'];
        $detailmessage = $_POST['message'];

        $email_to = "admin@gymscanner.com";

        $email_subject = "Main Feedback Form";
        /*	$headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: WEBSITE LEAD <jganesh1485@gmail.com>' . "\r\n";		*/

        $email_message = 'Subject: ' . $email_subject . '<br />Name: ' . $name . '<br />Email: ' . $email . '<br />Category: ' . $category . '<br />Message: ' . $detailmessage . '';
        $success = '';
        $messege = "";

        //$success = mail($email_to,$email_subject,$email_message,$headers);

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        $this->email->from('info@gymscanner.com', 'Gymscanner');
        $list = array($email_to);
        $this->email->to($list);
        $this->email->subject('Main Feedback Form');
        // $val = 'Subject: '.$email_subject.'<br />Name: '.$name.'<br />Email: '.$email.'<br />Category: '.$category.'<br />Message: '.$detailmessage.'';
        $this->email->message($email_message);


        if ($this->email->send()) {
            $messege = "<p style='color: red; margin-top: 21px; text-align: center; font-size: 12px;'><b>YOUR QUESTION HAS BEEN SENT TO GYMSCANNER.COM ADMIN</b></p>";
            ?>
            <script language="javascript">$(document).ready(function () {
                    $('a.showfeedback-tab').trigger('click');
                });  </script>
        <?php
        }
        else{
        $messege = "<p style='color: red; margin-top: 21px; text-align: center; font-size: 12px;'><b>FAILED!. PLEASE TRY AGAIN LATER.</b></p>";
        ?>
            <script language="javascript">$(document).ready(function () {
                    $('a.showfeedback-tab').trigger('click');
                });  </script>
        <?php }

    }
    ?>

    <form id="feedback_formrequest" name="feedback_formrequest" method="post" enctype="multipart/form-data"
          onsubmit="return validateForm()">
        <input type="hidden" name="formid" id="formid" value="gotcontact"/>

        <div class="feedback-panel">

            <a href="javascript:;" class="hidefeedback-tab"><?php echo $this->lang->line("Feedback"); ?></a> <a
                href="javascript:;"
                class="showfeedback-tab"><?php echo $this->lang->line("Feedback"); ?></a>

            <div id="feedback">
                <div id="feedBackForm">
                    <h3><?php echo $this->lang->line("CONTACT US"); ?></h3>

                    <p style="font-size: 11px;">
                        <?php echo $this->lang->line("Have a query about your order? Need more information? Feel free to ask anything."); ?>
                    </p>
                    <ul>
                        <li>
                            <label><?php echo $this->lang->line("NAME"); ?></label>
                            <input id="txtFeedBackName" type="text" class="txtbox" name="name"/>
                        </li>
                        <li>
                            <label><?php echo $this->lang->line("EMAIL "); ?></label>
                            <input id="txtFeedBackEmail" type="text" class="txtbox" name="email"/>
                        </li>
                        <li>
                            <label style="position: relative; top: 8px;  width: 239px; font-size:15px;"><?php echo $this->lang->line("WHAT CAN WE HELP YOU WITH?"); ?>
                                </label>
                        </li>
                        <li>
                            <ul class="fd-type" style=" margin-top: 4px;">
                                <li>
                                    <div style="float: left; padding: 0px 0px 5px;">
                                        <input type="checkbox" id="chkFeedBackType1" value="My order"
                                               name="request_form" style="float:left;margin:0;"/>

                                        <p style="color: rgb(0, 0, 0); font-size: 11px;margin:2px 0 0 17px;"><?php echo $this->lang->line("My order"); ?></p>
                                    </div>
                                    <div style="float: left; padding: 0px 0px 5px;">
                                        <input type="checkbox" id="chkFeedBackType2" value="My account"
                                               name="request_form" style="float:left;margin:0;"/>

                                        <p style="color: rgb(0, 0, 0); font-size: 11px;margin:2px 0 0 17px;"><?php echo $this->lang->line("My account"); ?></p>
                                    </div>
                                    <div style="float: left; padding: 0px 0px 5px;">
                                        <input type="checkbox" id="chkFeedBackType3" value="Selling through Gymscanner"
                                               name="request_form" style="float:left;margin:0;"/>

                                        <p style="color: rgb(0, 0, 0); font-size: 11px;margin:2px 0 0 17px;"><?php echo $this->lang->line("Selling through Gymscanner"); ?></p>
                                    </div>
                                    <div style="float: left; padding: 0px 0px 5px;">
                                        <input type="checkbox" id="chkFeedBackType4" value="Broken link / error"
                                               name="request_form" style="float:left;margin:0;"/>

                                        <p style="color: rgb(0, 0, 0); font-size: 11px;margin:2px 0 0 17px;"><?php echo $this->lang->line("Broken link / error"); ?></p>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <label><?php echo $this->lang->line("DETAILS"); ?></label>

                            <p style="font-size: 12px; margin-top: 9px;"><?php echo $this->lang->line("Please include your order pin code if you have a question regarding a purchase on Gymscanner"); ?></p>
                            <textarea id="txtFeedBackMessage" class="txtbox" name="message"
                                      style="height: 85px;"></textarea>
                        </li>
                    </ul>
                    <input type="submit" value="<?php echo $this->lang->line("Send"); ?>" name="gotcontact" class="removebut"/>
                    <input type="submit" value="<?php echo $this->lang->line("Cancel"); ?>" class="removebut" onClick="return show()"/>
                    <?php
                    echo @$messege;
                    ?>

                </div>
            </div>

        </div>

    </form>
</footer>

<script type="text/javascript">
    function show() {
        $("#feedback").css("display", "none");
        $(".showfeedback-tab").css("display", "block");
        $(".hidefeedback-tab").css("display", "none");
        $(".feedback-panel").css("border", "none");
        $(".feedback-panel").css("left", "-215px");
        // document.location.href ="<?php //echo base_url()?>";
        return false;
    }
    $(".showfeedback-tab").click(function () {
        $("#feedback").css("display", "block");
        $(".feedback-panel").css("border", "1px solid #ccc");
        $(".feedback-panel").css("left", "0");
    });

    function is_email_valid(email) {
        //for validating email id
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(email);

        if (!valid) {
            return false;
        } else {
            return true;
        }
    }
</script>

<script>

    $('#subsc_mail').click(function () {
        var mail = $('#mail_id').val();
        var result = is_email_valid(mail);
        if (mail == "" || result == false) {
            alert('Please Give valid email id');
            $('#mail_id').val('');
            return false;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo USER_URL;?>pages/subscribe",
            data: {
                'email': mail
            },
            success: function (data) {
                if (data == 1) {
                    //alert('"Thank you. The mobile application is under development and we are tweaking it for optimal performance. As soon as it is ready, you will be the among the first ones to receive it."');
                    $("#email_frm p").addClass("success_msg");
                    $("#email_frm p").removeClass("error_msg");
                    $("#text_msg").html("Thank you. The mobile application is under development and we are tweaking it for optimal performance. As soon as it is ready, you will be the among the first ones to receive it.");
                    $('#mail_id').val('');
                    $("#email_frm p").fadeIn();
                    $("#email_frm p").fadeOut(10000);
                }
                else {
                    //alert('Please give an alternative email')
                    $("#email_frm p").addClass("error_msg");
                    $("#email_frm p").removeClass("success_msg");
                    $("#text_msg").html("Please give an alternative email");
                    $("#email_frm p").fadeIn();
                    $("#email_frm p").fadeOut(10000);

                }
            },
            error: function () {
                alert("We are unable to do your request. Please contact webadmin");
            }
        });
    });
</script>

<script>
    function validateForm() {
        var name = document.forms["feedback_formrequest"]["name"].value;
        if (name == null || name == "") {
            alert("Name must be filled out");
            return false;
        }
        var email = document.forms["feedback_formrequest"]["email"].value;
        if (email == null || email == "") {
            alert("Email must be filled out");
            return false;
        }

        // var category = document.forms["feedback_formrequest"]["request_form"].value;
//    if (category==null || category=="") {
//        alert("category must be filled out");
//        return false;
//    }
//	
        var messagetext = document.forms["feedback_formrequest"]["message"].value;
        if (messagetext == null || messagetext == "") {
            alert("Message must be filled out");
            return false;
        }
    }

</script>
<style>
    #email_frm .success_msg {
        border-radius: 10px;
        box-shadow: 0 0 5px #a0a0a0;
        color: green;
        padding: 5px 10px;
        width: 100%;
    }

    #right_div #email_frm {
        margin-left: -6px;
    }

    #right_div #email_frm input {
        float: left;
        height: 36px;
        padding: 5px 9px;
        width: 57%;
    }

    #email_frm .error_msg {
        border-radius: 10px;
        box-shadow: 0 0 5px #a0a0a0;
        color: red;
        padding: 5px 10px;
        width: 100%;
    }

    .payment_part .stripe_img {
        float: left;
    }

    .payment_part .card_type {
        margin-top: 10px;
        width: 100%;
    }

    @media screen and (max-width: 1200px) {
        #right_div #email_frm input {
            float: left;
            height: 36px;
            padding: 5px 9px;
            width: 100%;
            margin-bottom: 10px;
            margin-left: 15px;
        }

        #right_div .btn {
            -moz-user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 15px;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857;
            margin-bottom: 0;
            margin-left: 15px;
            margin-top: 0px;
            padding: 6px 21px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }

        .social_icons {
            float: right;
            padding: 0;
            width: 100%;
        }
    }

    @media screen and (max-width: 1025px) {
        .payment_part .card_type {
            float: left;
            margin-left: 10px;
            margin-top: 10px;
            width: 100%;
        }
    }

    form#subsc_form {
        width: 105%;
        margin: 10px 8px;
        padding: 0px;
    }

    @media screen and (max-width: 991px) {
        .bottom_footer {
            margin: 0 auto;
            text-align: center;
        }

        .payment_part .stripe_img {
            float: none;
        }

        .payment_part .card_type {
            float: left;
            margin-left: 0;
            margin-top: 10px;
            width: 100%;
        }

        #email_frm {
            width: 80%;
        }

        #left_div .col-sm-2 {
            width: 31.667%;
        }

        .send_input {
            float: left;
            margin-top: 10px;
            width: 50%;
        }

        .payment_part {
            float: left;
            margin-left: 20px;
            margin-top: 10px;
            width: 47%;
            padding-top: 10px;
        }

        #right_div #email_frm {
            margin-left: 35px;
        }
    }

    @media screen and (max-width: 1200px) {
        #right_div #email_frm input {
            float: left;
            height: 36px;
            padding: 5px 9px;
            width: 40%;
            margin-bottom: 10px;
            margin-left: 15px;
        }
    }

    @media screen and (max-width: 550px) {

        .social_icons ul {
            float: left;
            margin: 0;
            padding: 0;
        }

        #right_div .btn {
            -moz-user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 15px;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857;
            margin-bottom: 0;
            margin-left: 1px;
            margin-top: 1px;
            padding: 6px 21px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }

        
        .payment_part {
            float: left;
            /* margin-left: 50px; */
            margin-top: 10px;
            padding-top: 10px;
            text-align: left;
            width: 100%;
        }
        .send_input {
            float: left;
            margin-top: 10px;
            width: 100%;
        }
    }

    @media screen and (max-width: 450px) {
        #feedback {
            float: left;
            height: 300px;
            overflow-x: hidden;
            overflow-y: auto;
            width: 121%;
            margin-left: -15px;
        }

        #feedBackForm {
            bottom: 26px;
            padding-left: 0;
            position: static;
            right: 11px;
            width: 221px;
        }

        .feedback-panel {
            left: -250px;
        }

        @media screen and (max-width: 350px) {
            .social_icons ul {
                float: left;
                margin: 0;
                padding: 0;
            }
        }
    }
    @media (min-width: 1200px){
    div#footer_info_panel {
        width: 111%;
        position: relative;
        margin-left: 5%;
    }
    }
    a {
        user-drag: none;
        -moz-user-select: none;
        -webkit-user-drag: none;
    }
    img{
        user-drag: none;
        -moz-user-select: none;
        -webkit-user-drag: none;
    }
</style>
<script>



</script>
</div>
</body>
</html>
