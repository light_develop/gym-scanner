<!DOCTYPE html>
<html lang="EN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?php echo $this->lang->line("GYM"); ?></title>
<link href="<?php echo base_url()?>themes/user/css/bootstrap.css" rel="stylesheet" />

<link href="<?php echo base_url()?>themes/user/css/custom.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery-1.7.2.min.js"> </script>

</head>

<body>
<header>
  <div class="header-bg">
    <div class="container" style="padding:0px;">
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12"><a href="<?php echo base_url();?>"><img src="<?php echo base_url()?>themes/user/images/logo.png" class="img-responsive" /></a> </div>
      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
      
      <button class="navbar-toggle " type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only"><?php echo $this->lang->line("Toggle navigation"); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <!-- <nav class="collapse navbar-collapse bs-navbar-collapse">
          <ul class="nav navbar-nav navbar-right" id="navbar">
			  
            <?php if(isset($this->session->userdata['user_id'])){
              $user_id = $this->session->userdata['user_id'];
              $first_name = isset($this->session->userdata['first_name'])?$this->session->userdata['first_name']:"";
              $last_name = isset($this->session->userdata['last_name'])?$this->session->userdata['last_name']:"";       
            ?> 
            <li><a href="#"> Signed in as  <?=ucfirst($first_name).' '.ucfirst($last_name);?></a></li>
            <li><a href="<?= USER_URL.'profile_view/'.$user_id;?>">View Profile</a></li>  
            <li><a href="<?php echo USER_URL;?>logout" style="border:none;"> Logout </a></li>
            <?php } else{?>
            <li><a href="#openModal" data-toggle="modal" data-target=".bs-example-modal-sm123"> Register </a></li>   
            <li><a style="border:none;" href="#openModal"  data-toggle="modal" data-target=".bs-example-modal-sm">Sign In</a></li>
            <?php } ?>
            <?php if(isset($this->session->userdata['level'])){?>
              <li><a style="border:none;" href="<?php echo SUPPLIER_URL?>login_redirect"><button class="add_fc">ADD YOUR FACILITY</button></a></li>
            <?php } else{?>
            <li><a style="border:none;" href="#openModal" data-toggle="modal" data-target=".bs-example-modal-sm-supplier"><button class="add_fc">ADD YOUR FACILITY</button></a></li>
            
            <?php } ?>
            
          </ul>
        </nav> -->
      </div>
    </div>
  </div>
</header>

<!-- Languages  -->
<div class="modal fade bs-example-modal-smlang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
              class="sr-only"><?php echo $this->lang->line("Close"); ?></span></button>
        <h4 class="modal-title" id="mySmallModalLabel"><?php echo $this->lang->line("Coming Soon...."); ?></h4>
      </div>    
    </div>
  </div>
</div>

