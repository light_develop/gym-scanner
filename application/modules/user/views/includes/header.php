<!DOCTYPE html>
<html lang="EN">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<?php echo base_url() ?>uploads/site_logo.ico" rel="icon">
    <!--<script type="text/javascript" src="<?php /*echo base_url() */?>themes/user/js/jquery-1.7.2.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url() ?>themes/user/js/jquery-ui.js"></script>
  <!--  <script src="<?php /*echo base_url() */?>themes/user/js/feedback/jquery-1.7.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url() ?>themes/user/js/feedback/script.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="<?php echo base_url() ?>themes/user/js/bootstrap.js"></script>
    <link href="<?php echo base_url() ?>gothic/font.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>themes/user/fonts/displaypro/pfdindisplaypro.css" rel="stylesheet"/>
    <script src="<?php echo base_url(); ?>themes/admin/js/framework/main.js"></script>
    <link href="<?php echo base_url() ?>themes/user/css/bootstrap.css" rel="stylesheet"/>
    <link href="<?=base_url();?>themes/user/new_buttons_style/new_style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url() ?>themes/user/css/select.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>themes/user/countdown/someCss.css" rel="stylesheet"/>
    <?php

    if (current_url() == base_url()) { ?>
        <meta name="title"
              content="GymScanner | Gyms on discount | Gyms in Minnesota | Gyms in USA | Fitness trainers | Personal Trainers | Worldwide Gyms">
        <meta name="keywords"
              content="Gyms online , Gyms in America , Gyms in the US , Fitness Trainers Online , Personal Trainers ">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Gymscanner.com allows you to search, locate and book a gym or personal trainer anywhere in the world."); ?></p>">
    <?php } else if (current_url() == site_url('fac_det/' . @$gym_det->id . '/image') || current_url() == site_url('fac_det/' . @$gym_det->id . '/video')) { ?>

        <?php
        @$pageType = $this->uri->segment(3);
        if (@$pageType == 'image') {
            foreach ($gym_images as $imgs):?>
                <meta property="og:url" content="<?php echo site_url('fac_det/' . $gym_det->id); ?>/image"/>
                <meta property="og:title" content="GYMSCANNER-<?= @$gym_det->gymname; ?>"/>
                <meta property="og:type" content="Sharing Widgets"/>
                <meta property="og:image"
                      content="<?php echo base_url();?>uploads/gym/images/<?php echo $imgs->gym_images;?>"/>
                <meta property="og:description" content="<?= @$gym_det->description; ?>"/>
            <?php
            endforeach;
        }
        ?>
        <?php
        if (@$pageType == 'video') {
            ?>

            <?php if ($gym_det->gym_video != '') {
                $videos = $gym_det->gym_video;

                $res = explode('~', $gym_det->gym_video);
                ?>
                <meta property="fb:app_id" content="259859127379080">
                <meta property="og:title" content="<?= @$gym_det->gymname; ?>">
                <meta property="og:description" content="<?= @$gym_det->description; ?>">
                <meta property="og:type" content="video">
                <?php
                foreach ($res as $value) { ?>

                    <?php $var = explode('v=', $value);
                    $new = explode('/', $value);
                    if ($new[3] || $var[1]) {
                        if (strpos($value, 'vimeo') !== false) {
                            ?>
                            <meta property="og:url" content="http://player.vimeo.com/video/<?= $new[3] ?>">
                            <meta property="og:video" content="http://player.vimeo.com/video/<?= $new[3] ?>"> <?php }
                        if (strpos($value, 'youtube') !== false) { ?>
                            <meta property="og:url" content="http://www.youtube.com/embed/<?= $var[1] ?>">
                            <meta property="og:video"
                                  content="http://www.youtube.com/embed/<?= $var[1] ?>">  <?php } else {
                            ?>
                            <meta property="og:url" content="">
                            <meta property="og:video" content=""> <?php } ?>
                        <meta property="og:video:type" content="application/x-shockwave-flash">
                        <meta property="og:video:width" content="398">
                        <meta property="og:video:height" content="224">


                    <?php }
                }
            }
        }
        ?>
    <?php } else if (current_url() == site_url('about')) { ?>
        <meta name="title" content="About Gymscanner.com">
        <meta name="keywords"
              content="Gyms online,Gyms in America,Gyms in the US,Fitness Trainers Online,Personal Trainers,Fitness Center">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Gymscanner.com website and the mobile application allows users to search, locate, view and renew their subscription to gyms, fitness centers and personal trainers worldwide."); ?></p>">
    <?php } else if (current_url() == site_url('faq')) { ?>
        <meta name="title" content="Gymscanner.com FAQs">
        <meta name="keywords"
              content="Frequently asked questions about Gymscanner.com | Gyms online | Gyms in America | Gyms in the US | Fitness Trainers Online | Personal Trainers | Fitness Center |">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Frequently asked questions about Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('pers_trns')) { ?>
        <meta name="title" content="Personal trainers | Fitness trainers | Hire personal trainers">
        <meta name="keywords" content="Personal trainers | Fitness trainers">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Personal fitness trainers worldwide."); ?></p>">
    <?php } else if (current_url() == site_url('refunds')) { ?>
        <meta name="title" content="Refunds">
        <meta name="keywords" content="Refunds">
        <meta name="description" content="<p><?php echo $this->lang->line("Refunds at Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('privacy_policy')) { ?>
        <meta name="title" content="Privacy policy">
        <meta name="keywords" content="Privacy policy">
        <meta name="description" content="<p><?php echo $this->lang->line("Privacy policy at Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('terms_cond')) { ?>
        <meta name="title" content="Gymscanner.com terms and conditions">
        <meta name="keywords" content="Gymscanner.com terms and conditions">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Gymscanner.com terms and conditions."); ?></p>">
    <?php } else if (current_url() == site_url('contact')) { ?>
        <meta name="title" content="Contact Gymscanner.com">
        <meta name="keywords" content="Contact Gymscanner.com">
        <meta name="description" content="<p><?php echo $this->lang->line("Contact Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('search')) { ?>
        <meta name="title" content="Your Gymscanner.com serach results">
        <meta name="keywords" content="Your Gymscanner.com serach results">
        <meta name="description"
              content="<p><?php echo $this->lang->line("Your Gymscanner.com serach results"); ?></p>">
    <?php } else if (current_url() == site_url('facility')) { ?>
        <meta name="title" content="Gymscanner.com facility dashboard">
        <meta name="keywords" content="Gymscanner.com facility dashboard">
        <meta name="description" content="<p><?php echo $this->lang->line("Gymscanner.com facility dashboard"); ?></p>">
    <?php } else if (current_url() == site_url('new_offer')) { ?>
        <meta name="title" content="Add new offers at Gymscanner.com">
        <meta name="keywords" content="Add new offers at Gymscanner.com">
        <meta name="description" content="<p><?php echo $this->lang->line("Add new offers at Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('facility_offers')) { ?>
        <meta name="title" content="View current offers at Gymscanner.com">
        <meta name="keywords" content="View current offers at Gymscanner.com">
        <meta name="description"
              content="<p><?php echo $this->lang->line("View current offers at Gymscanner.com"); ?></p>">
    <?php } else if (current_url() == site_url('edit_offer/(:num)')) { ?>
        <meta name="title" content="Edit offers at Gymscanner.com">
        <meta name="keywords" content="Edit offers at Gymscanner.com">
        <meta name="description" content="<p><?php echo $this->lang->line("Edit offers at Gymscanner.com"); ?></p>">
    <?php } ?>
    <title><?php echo $this->lang->line("Gymscanner"); ?></title>
  <!--  <link href="<?php /*echo base_url() */?>themes/user/css/bootstrap.css" rel="stylesheet"/>-->

    <link href="<?php echo base_url() ?>themes/user/css/custom.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>themes/user/css/feedback/customfeed.css" rel="stylesheet" type="text/css"/>


    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({
            publisher: "3be5b06c-8e7d-4651-8a9b-ac9bacdcf0c6",
            doNotHash: false,
            doNotCopy: false,
            hashAddressBar: false
        });</script>

    <style>
        #progress {
            background: white;
            float: right;
            height: 8px;
            margin-right: 35px;
            margin-top: -25px;
            width: 23%;

        }

        .ui-progressbar.beginning .ui-progressbar-value {
            background: green;
        }

        .ui-progressbar.middle .ui-progressbar-value {
            background: green;
        }

        .ui-progressbar.end .ui-progressbar-value {
            background: green;
        }

        .Validation {
            width: 330px;
            height: 35px;
            display: none; /* IE10 Consumer Preview */
            font-size: middle;
            font-weight: bold;
            color: #AF414A;
            padding-top: 10px;
            margin-left: 30px;
        }

        .ui-progressbar .ui-progressbar-value {
            height: 100%;
            margin: 0px !important;
        }

        #undefined > div > a > img {
            margin-left: 9px;
            margin-top: 3px;
            height: 20px;
        }

        #undefined > ul > li > a > img {
            height: 20px;
        }
        .padding_my_li{
            padding-bottom: 19px;
        }
       /* ul#navbar li {
            padding: 12px;
        }
*/
        @media (min-width: 300px) {
           /* ul#navbar li {
                padding: 9px;
            }*/

            /*.dd-select {
                width: 164px;
                top: 12px;
                left: 10px;
                background: rgb(238, 238, 238);
            }*/

            /* div#icons_footer {
                 left: -51%;
                 *//* margin-left: -35%; *//*
                 top: -7px;
             }*/

        }

        @media (min-width: 768px) {
            /*div#icons_footer {
                left: -35%;
                *//* margin-left: -35%; *//*
                top: 3px;
            }*/
        }

        }
        @media (min-width: 768px) {
            /*ul#navbar li {
                padding: 10px;
            }*/

        }

        @media (min-width: 768px) {
           /* ul#navbar li {
                padding: 10px;
            }*/

        }

        .confirmation {
            top: 3px;
            left: 8px;
        }

        body {
            overflow-x: hidden;
        }
        .padding_my_li{
            padding-bottom: 19px;
        }
        @media screen and (max-width: 1200px) {
            ul#navbar {

                overflow-x: hidden;

            }
        }
        div {
            font-family: sans-serif;
        }
        #navbar > li:nth-child(3) > div.mobile > a > img {
            max-width: 20%;
        }
        #undefined > ul {
            width: 183px;
            text-align: center;
        }
    </style>
    <link href="<?php echo base_url() ?>themes/user/fonts/displaypro/pfdindisplaypro.css" rel="stylesheet"/>
   <!-- <link rel="stylesheet" href="<?php /*echo base_url() */?>themes/user/countdown/jquery.countdown.css"/>-->
    <link rel="stylesheet" href="<?php echo base_url() ?>themes/user/countdown/someCss.css"/>

   <!-- <script type="text/javascript">
        var countdown_time = '<?php /*echo $countdown_time*1000;*/?>';
    </script>
-->

   <!-- <link href="<?php /*echo base_url() */?>themes/user/css/feedback/customfeed.css" rel="stylesheet" type="text/css"/>-->

</head>

<body>
<header>
    <div class="header-bg">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding:0px;"><a
                    href="<?php echo base_url(); ?>"><img
                        src="<?php echo base_url() ?>themes/user/images/logo.png" class="img-responsive"/></a></div>
            <div class=" pull-right">

                <button class="navbar-toggle " type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only"><?php echo $this->lang->line("Toggle navigation"); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                   <!-- <img
                        src="http://www.iptrackeronline.com/ip2countryflag.php" width="33" height="25"
                        alt="http://www.iptrackeronline.com/ip2country.php?lang=1" border="0">-->
                </button>
                <nav class="collapse navbar-collapse bs-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right" id="navbar">

                        <?php if (isset($this->session->userdata['user_id'])) {
                            $user_id = $this->session->userdata['user_id'];
                            $first_name = isset($this->session->userdata['first_name']) ? $this->session->userdata['first_name'] : "";
                            $last_name = isset($this->session->userdata['last_name']) ? $this->session->userdata['last_name'] : "";
                            ?>
                            <li>
                                <a href="#"><?php echo $this->lang->line("Signed in as"); ?> <?= ucfirst($first_name) . '  ' . ucfirst($last_name); ?></a>
                            </li>
                            <li>
                                <a href="<?= site_url('profile/' . $user_id); ?>"><?php echo $this->lang->line("View Profile"); ?></a>
                            </li>
                            <li><a href="<?php echo USER_URL; ?>logout"><?php echo $this->lang->line("Logout"); ?></a>
                            </li>
                        <?php } else { ?>
                            <li class="ip_detect"><img src="http://www.iptrackeronline.com/ip2countryflag.php"></li>
                            <li class="padding_my_li"><a href="#openModal" data-toggle="modal"
                                   data-target=".bs-example-modal-sm123"> <?php echo $this->lang->line("Register"); ?> </a>
                            </li>
                            <li class="padding_my_li"><a href="#openModal" data-toggle="modal"
                                   data-target=".bs-example-modal-sm"><?php echo $this->lang->line("Sign In"); ?></a>
                            </li>

                            <li class="padding_my_li">

                                <div class="desktop">
                                    <?php echo $this->getLanguagesSelect("onchange='changeLanguage(this); location.reload();' class='ssclick' ", null, 1); ?>
                                </div>
                                <div class="mobile">
                                    <?php foreach($this->getLanguages() as $lang): ?>
                                        <a style="cursor: pointer"
                                           onclick="(function() {changeLanguage({value: '<?php echo $lang["lang_id"]; ?>'});
                                               window.location.reload();
                                               })(); "
                                            >
                                            <img src="<?php echo $lang["image"]; ?>"/>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                            </li>
                        <?php } ?>

                        <?php if (isset($this->session->userdata['level']) && isset($this->session->userdata['type'])) { ?>
                            <li class="padding_my_li" ><a style="border:none;" href="<?php echo site_url('facility') ?>">
                                    <button
                                        class="add_fc"><?php echo $this->lang->line("ADD YOUR FACILITY"); ?></button>
                                </a></li>
                        <?php } else { ?>
                            <li ><a style="border:none;" href="#openModal" data-toggle="modal"
                                   data-target=".bs-example-modal-sm-supplier">
                                    <button
                                        class="add_fc"><?php echo $this->lang->line("ADD YOUR FACILITY"); ?></button>
                                </a></li>
                            <!-- <li class="flgimg"><a style="border:none;"  href="<?php echo base_url(); ?>" title="IP Location"><img src="http://www.iptrackeronline.com/ip2countryflag.php" alt="http://www.iptrackeronline.com/ip2country.php?lang=1" border="0"></a></li> -->
                        <?php } ?>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<script>

    function goBack() {
        window.history.back()
    }
</script>
<script>
    $(document).ready(function () {
        $('.flag a').removeAttr('href');

    });
</script>

<script type="text/javascript">
    function is_valid_email(email) {
        //for validating email id
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(email);

        if (!valid) {
            return false;
        } else {
            return true;
        }
    }
</script>

<!-- sign in form -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"><? echo $this->lang->line('Close') ?></span></button>
                <h4 class="modal-title" id="mySmallModalLabel"><? echo $this->lang->line('Login') ?></h4>
            </div>
            <div>
                <form role="form">
                    <div class="form-group">
                        <div id="msg1" style="margin-left:35px;"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="user_name" class="form-control" id="user_name"
                               placeholder="<? echo $this->lang->line('Enter Username') ?>"
                               required></div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password"
                               placeholder="<? echo $this->lang->line('Password') ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="login_forgotpassword">
                            <label for="forgotpwd"><a
                                    href="<?php echo USER_URL; ?>forgetpwd"><? echo $this->lang->line('Forgot Password?') ?>
                                    &nbsp;
                                    &nbsp; </a></label>
                            <button type="button" id="log_btn"
                                    class="btn btn-default"><?php echo $this->lang->line("Login"); ?></button>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="checkbox">
                        <label class="confirmation">
                            <input type="checkbox" id="level" name="level"
                                   value="1"><? echo $this->lang->line('I am a gym / personal trainer') ?>

                        </label>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- sign up form for suppliers-->
<div class="modal fade bs-example-modal-sm-supplier" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"><?php echo $this->lang->line("Close"); ?></span></button>
                <h4 style="color:#6a8387 !important;" class="modal-title"
                    id="mySmallModalLabel"><?php echo $this->lang->line("Add Your Facility"); ?></h4>
            </div>
            <div>

                <div class="form-group">
                    <div style="margin-left:50px;" id="sup_msg"></div>
                </div>
                <div class="form-group">
                    <div class="wrapper">
                        <div class="outerdiv" style="min-height: 60px;">
                            <input type="text" name="first_name" id="sup_fname" class="form-control"
                                   placeholder="<?php echo $this->lang->line("Enter First Name"); ?>"><img
                                class="subimg"
                                style="float: right; margin-top: -30px; margin-right: 8px;"/>
                        </div>
                        <div class="Validation"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="wrapper">
                        <div class="outerdiv" style="min-height: 60px;">
                            <input type="text" name="last_name" id="sup_lname" class="form-control"
                                   placeholder="<?php echo $this->lang->line("Enter Last Name"); ?>"><img class="subimg"
                                                                                                          style="float: right; margin-top: -30px; margin-right: 8px;"/>
                        </div>
                        <div class="Validation"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="wrapper">
                        <div class="outerdiv">
                            <input type="hidden" name="name_result" id="name_result" value="">
                            <input type="text" name="user_name" id="sup_uname" class="form-control"
                                   placeholder="<?php echo $this->lang->line("Enter Username"); ?>"><img class="subimg"
                                                                                                         style="float: right; margin-top: -30px; margin-right: 8px;"/>
                        </div>
                        <span id="error_username"></span>

                        <div class="Validation" style="margin-left: 6%"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="wrapper">
                        <div class="outerdiv">
                            <input type="password" name="password" id="sup_pwd" class="form-control"
                                   placeholder="<?php echo $this->lang->line("Password"); ?>"><img class="subimg" alt=""
                                                                                                   style="float: right; margin-top: -30px; margin-right: 8px;"/>

                            <div id="progressbar"></div>
                        </div>
                        <span id="error_password"></span>

                        <div class="Validation"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="wrapper">
                        <div class="outerdiv">
                            <input type="hidden" value="" name="email_result" id="email_result">
                            <input type="text" name="email" id="sup_email" class="form-control"
                                   placeholder="<?php echo $this->lang->line("Enter Email"); ?>"><img class="subimg"
                                                                                                      alt=""
                                                                                                      style="float: right; margin-top: -30px; margin-right: 8px;"/>
                        </div>
                        <div class="Validation"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="login_forgotpassword">
                        <button style="background-color:#6a8387 !important;" type="button" id="supplier_btn"
                                class="btn btn-default"><?php echo $this->lang->line("Get Started"); ?></button>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div class="checkbox" style="background-color:#6a8387 !important;">
                    <input type="checkbox" id="level1" name="type_gym" value="1"
                           style="margin-top:3px;"><?php echo $this->lang->line("I am a gym"); ?>
                    <div style="clear:both"></div>

                    <div style="clear:both"></div>
                    <input type="checkbox" name="sup_terms" value="1"
                           style="margin-top:3px;"><?php echo $this->lang->line("I agree with Gymscanner.com terms and conditions and privacy policy"); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- sign up form for users -->

<div class="modal fade bs-example-modal-sm123" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"><?php echo $this->lang->line("Close"); ?></span></button>
                <h4 class="modal-title" id="mySmallModalLabel"><?php echo $this->lang->line("Register"); ?></h4>
            </div>
            <div>
                <form role="form" action="<?php echo USER_URL ?>register" method="post" name="reg_form">
                    <div class="form-group">
                        <div id="msg" style="margin-left:35px;"></div>
                    </div>
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv" style="min-height: 60px;">
                                <input type="text" name="first_name" id="first_name" class="form-control"
                                       placeholder="<?php echo $this->lang->line("Enter First Name"); ?>"><img
                                    class="subimg"
                                    style="float: right; margin-top: -30px; margin-right: 8px;"/>
                            </div>
                            <div class="Validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv" style="min-height: 60px;">
                                <input type="text" name="last_name" id="last_name" class="form-control"
                                       placeholder="<?php echo $this->lang->line("Enter Last Name"); ?>"><img
                                    class="subimg"
                                    style="float: right; margin-top: -30px; margin-right: 8px;"/>
                            </div>
                            <div class="Validation"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv">
                                <input type="hidden" name="name_res" id="name_res" value="">
                                <input type="text" name="user_name" id="uname" class="form-control"
                                       placeholder="<?php echo $this->lang->line("Enter Username"); ?>"><img
                                    class="subimg"
                                    style="float: right; margin-top: -30px; margin-right: 8px;"/>
                            </div>
                            <span id="error_username"></span>

                            <div class="Validation spesial_for_alrede_exist"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv">
                                <input type="password" name="password" id="pwd" class="form-control"
                                       placeholder="<?php echo $this->lang->line("Password"); ?>"><img class="subimg"
                                                                                                       alt=""
                                                                                                       style="float: right; margin-top: -30px; margin-right: 8px;"/>

                                <div id="progressbar1"></div>
                            </div>
                            <span id="error_password_customer_form"></span>

                            <div class="Validation"></div>
                        </div>
                    </div>
                    <!--EMAIL Start-->
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv">

                                <input type="hidden" name="email" id="email_res_u" value="">
                                <input type="email" name="email" id="user_email" class="form-control"
                                       placeholder="<?php echo $this->lang->line("Enter Email"); ?>"><img class="subimg"
                                                                                                          style="float: right; margin-top: -30px; margin-right: 8px;"/>
                            </div>

                            <div class="Validation spesial_for_alrede_exist"></div>
                        </div>
                    </div>
                    <!--EMAIL END-->

                    <div class="login_forgotpassword" style="width:83%">
                        <div class="form-group">
                            <div class="wrapper">
                                <div class="outerdiv">
                                    <!--Gender SELECT START-->
                                    <label for="R1">
                                        <input id="R1" type="radio" name="gender"
                                               value="1"><?php echo $this->lang->line("Male"); ?>
                                    </label>
                                    &nbsp;<label for="R2">
                                        <input id="R2" type="radio" name="gender"
                                               value="2"><?php echo $this->lang->line("Female"); ?>
                                    </label>
                                </div>
                            </div>
                            <!--Gender SELECT END-->

                            <div class="form-group">
                                <div class="wrapper">
                                    <div class="outerdiv" style="float: left">
                                        <!-- START Country Select-->
                                        <div
                                            style="position: fixed; margin-top: 2%;   margin-left: -3%; display: inline-flex;   width: 87%;">
                                            <?php $cntry[''] = $this->lang->line('COUNTRY');
                                            $attribute = 'id="country_list_value" onchange="load_dropdown_contents(this.value)" class="form-control" ';
                                            echo form_dropdown('country_list', $cntry, '', $attribute);
                                            ?>

                                            <!-- END Country Select-->
                                            <!-- START City Select-->

                                            <?php $attribute = 'id="state_select" class="form-control"  ';
                                            $states = array();
                                            $states[''] = $this->lang->line('STATE');
                                            echo form_dropdown('state_select', $states, '', $attribute);
                                            ?>
                                            <!-- END City Select-->
                                            <!--CITY START Select-->
                                            <?php $attributes = 'id="city_select"  class="form-control" ';
                                            $city = array();
                                            $city[''] = $this->lang->line('CITY');
                                            echo form_dropdown('city_select', $city, '', $attributes);
                                            ?></div>
                                        <!--CITY END SELECT-->
                                    </div>
                                </div>

                            </div>
                        </div>

                        <button type="button" id="sub_btn" class="btn btn-default"
                                style="float:right; margin-left: 80%;
  margin-top: -17%;"><?php echo $this->lang->line("Register"); ?></button>
                    </div>
                    <div style="clear:both"></div>
                    <div class="form-group">
                        <div class="wrapper">
                            <div class="outerdiv" style="float: left">

                            </div>
                        </div>
                    </div>

                    <span id="error_username"></span>

                    <div class="checkbox">
                        <div class="wrapper">
                            <div class="Validation"
                                 style="display: block; float: left; margin-top: -40px; margin-left: 0px;"></div>
                            <div class="outerdiv" style="float: left; float: left; position: relative; left: 8px;">
                                <input type="checkbox" name="usr_terms" id="cb1"
                                       style="margin-top: 3px;"><span
                                    style="left: 11px; position: relative;"><?php echo $this->lang->line("I agree with Gymscanner.com terms and conditions and privacy policy."); ?></span>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Languages  -->
<div class="modal fade bs-example-modal-smlang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only"><?php echo $this->lang->line("Close"); ?></span></button>
                <h4 class="modal-title" id="mySmallModalLabel"><?php echo $this->lang->line("Coming Soon...."); ?></h4>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $('#log_btn').click(function () {
        var user_name = $('#user_name').val();
        var password = $('#password').val();
        var chk = $('#level').is(':checked');
        if (chk == true) {
            var level = 1;
            var log_url = "<?php echo SUPPLIER_URL?>login";
            var cur_url = "<?php echo current_url()?>";
        } else {
            var level = 0;
            var log_url = "<?php echo USER_URL?>login";
            var cur_url = "<?php echo current_url()?>";
        }

        $.ajax({
            type: "POST",
            url: log_url,
            data: {
                'user_name': user_name,
                'password': password,
                'level': level,
                'cur_url': cur_url
            },
            success: function (data) {
                //alert(data);
                var url = data.indexOf('http');
                //alert(url);
                if (url != -1)
                    window.location = data;
                else
                    $('#msg1').text(data).css('display', 'block').delay(5000).fadeOut();
            },
            error: function () {
                $('#msg1').text("We are unable to do your request. Please contact webadmin").css('color', 'red');
            }
        });
    });
</script>

<link href="<?php echo base_url() ?>themes/user/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<!--<script src="http://www.codecomplete4u.com/wp-content/MyPlugins/maskinput.js" type="text/javascript"></script>-->

<!--<link rel="stylesheet" href="external/jquery-ui.css" type="text/css" media="all" />
<script type="text/javascript" src="external/jquery.min.js"></script>
<script type="text/javascript" src="external/jquery-ui.min.js"></script>
<script src="external/maskinput.js" type="text/javascript"></script>
-->

<script>
    jQuery(function ($) {
        $('#sup_fname').keyup(function (e) {
            return false;
            var numchar = $("#sup_fname").val().length;
            if (numchar >= 2) {
                loadRight($('#sup_fname'));
            }
            else {
                loadWrong($('#sup_fname'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });

        $('#sup_lname').keyup(function (e) {
            var numchar = $("#sup_lname").val().length;
            if (numchar >= 2) {
                loadRight($('#sup_lname'));
            }
            else {
                loadWrong($('#sup_lname'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });

        $('#sup_uname').keyup(function (e) {
            var numchar = $("#sup_uname").val().length;
            var user_name = $("#sup_uname").val();
            if (numchar < 6) {

                $("#error_username").html("* Enter at least 6 characters");
            }
            else {
                $("#error_username").html(' ')
            }

            if (numchar > 5) {
                loadRight($('#sup_uname'));
                if (user_name != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo SUPPLIER_URL?>exist_user",
                        data: {
                            'user_name': user_name
                        },
                        success: function (data) {
                            //alert(data);
                            if (data == 1) {
                                //alert(data);
                                $("#name_result").val($.trim(data));
                                $("#sup_uname").parent().siblings("div").text("* Already user name exist");
                                ValidateAnimation($("#sup_uname"));
                                loadWrong($('#sup_uname'));

                                return false;
                            }
                            else {
                                //alert(data);
                                $("#name_result").val(' ');
                                $("#sup_uname").parent().siblings("div").text("");
                                ValidationRemove($("#sup_uname"));
                                loadRight($('#sup_uname'));

                                return false;
                            }
                        }
                    });
                }
            }
            else {
                loadWrong($('#sup_uname'));

            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });

        $("#progressbar").progressbar({
            value: 1
        });
        $('#sup_pwd, #pwd').keyup(function () {
            sup_pwd = $(this).val();
            var strpwd = /[^\w\*]/;
            $("#progressbar").removeClass("beginning middle end");
            var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

            if (sup_pwd.length < 6 && !regularExpression.test(sup_pwd)) {
                errorPasswordsReload("* Enter at least 6 charaters");
                $("#progressbar").addClass("beginning");
                $("#progressbar > .ui-progressbar-value").animate({
                    width: "40%"
                }, 200);
            }
            else if (!regularExpression.test(sup_pwd)) {
                errorPasswordsReload("* Enter atleast 1 number and 1 special character");
                $("#progressbar").addClass("beginning");

            }
            else {
                errorPasswordsReload(' ');
                $("#progressbar").addClass("beginning");
                $("#progressbar > .ui-progressbar-value").animate({
                    width: "100%"
                }, 200);
                loadRight($('#sup_pwd'));
            }

            if (!regularExpression.test(sup_pwd)) {
                loadWrong($('#sup_pwd'));
            }
            else {
                loadRight($('#sup_pwd'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
            if (pwd.length == 0) {
                $("#progressbar").addClass("beginning");
                $("#progressbar > .ui-progressbar-value").animate({
                    width: "0%"
                }, 200);
                return true;
            }
        });

        $('#sup_email').keyup(function () {
            var email = $(this).val();
            var sup_email = $("#sup_email").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (filter.test(email)) {


                loadRight($('#sup_email'));
                if (email != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo SUPPLIER_URL;?>exist_email",
                        data: {

                            'email': sup_email
                        },
                        success: function (data) {
                            //alert(data);
                            if (data == 1) {
                                //alert(data);
                                $("#email_result").val($.trim(data));
                                $("#sup_email").parent().siblings("div").text("* Already email exist");
                                ValidateAnimation($("#sup_email"));
                                loadWrong($('#sup_email'));
                            } else {
                                //alert(data);
                                $("#email_result").val(' ');
                                $("#sup_email").parent().siblings("div").text("");
                                /*ValidateAnimation($("#sup_email"));*/
                            }
                        }
                    });
                }
                ValidationRemove($('#sup_email'));
            }
            else {
                if ($(this).val() == "") {
                    $(this).siblings('img').remove();
                }
                else {
                    loadWrong($('#sup_email'));
                    return false;
                }
            }
        });

        function Emptysetvalidation() {
            if ($("#sup_fname").val() == "") {
                $("#sup_fname").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#sup_fname"));
            }
            if ($("#sup_lname").val() == "") {
                $("#sup_lname").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#sup_lname"));
            }
            if ($("#sup_uname").val() == "") {
                $("#sup_uname").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#sup_uname"));
            }
            if ($("#sup_pwd").val() == "") {
                $("#sup_pwd").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#sup_pwd"));
            }
            if ($("#sup_email").val() == "") {
                $("#sup_email").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#sup_email"));
            }
        }

        function crosssetvalidation() {
            if ($("#sup_fname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#sup_fname").parent().siblings("div").text("* Enter at least 3 charaters");
                ValidateAnimation($("#sup_fname"));
            }
            if ($("#sup_lname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#sup_lname").parent().siblings("div").text("* Enter at least 3 charaters");
                ValidateAnimation($("#sup_lname"));
            }
            /*if ($("#uname").siblings('img').attr('src') == '
            <?php // echo base_url()?>themes/user/images/cross.png') {
             $("#uname").parent().siblings("div").text("* Enter at least 6 charaters");
             ValidateAnimation($("#uname"));
             }
             if ($("#pwd").siblings('img').attr('src') == '
            <?php //echo base_url()?>themes/user/images/cross.png') {
             $("#pwd").parent().siblings("div").text("* Enter atleast 1 number and 1 special character");
             ValidateAnimation($("#pwd"));
             }*/
            if ($("#sup_email").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#sup_email").parent().siblings("div").text("* Enter Correct Email Id");
                ValidateAnimation($("#sup_email"));
            }
        }

        function RigthsetValidation() {
            if ($("#sup_fname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#sup_fname"));
            }
            if ($("#sup_lname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#sup_lname"));
            }
            if ($("#sup_uname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#sup_uname"));
            }
            if ($("#sup_pwd").val() != "") {
                ValidationRemove($("#sup_pwd"));
            }
            if ($("#sup_email").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#sup_email"));
            }

        }

        function ValidateAnimation(thisis) {
            thisis.parent().siblings("div").addClass("Validation").effect("bounce", {times: 4}, 1000).fadeIn("slow", function () {
                //$("#sub_btn").removeAttr('disabled');
            });
        }

        function ValidationRemove(thisis) {
            thisis.parent().siblings("div").fadeOut("slow");
        }

        function loadRight(thisis) {
            thisis.siblings('img').remove();
            $('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px; margin-right: 10px;"/>').insertAfter(thisis).delay(100).fadeOut(1000, function () {
                thisis.siblings('img').remove();
                $('<img src="<?php echo base_url()?>themes/user/images/right.png" class="subimg" style="float: right; margin-top: -30px; margin-right: 8px;"/>').insertAfter(thisis);
            });
        }

        function loadWrong(thisis) {
            thisis.siblings('img').remove();
            $('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px; margin-right: 10px;"/>').insertAfter(thisis).delay(100).fadeOut(100, function () {
                thisis.siblings('img').remove();
                $('<img src="<?php echo base_url()?>themes/user/images/cross.png" class="subimg" style="float: right; margin-top: -30px; margin-right: 8px;"/>').insertAfter(thisis);
            });
        }

        $('#supplier_btn').click(function () {
            RigthsetValidation();
            Emptysetvalidation();
            crosssetvalidation();
            var first_name = $('#sup_fname').val();
            var last_name = $('#sup_lname').val();
            var user_name = $('#sup_uname').val();
            var password = $('#sup_pwd').val();
            var email = $('#sup_email').val();
            var result = is_valid_email(email);
            var stype = '';

            $('#fname_msg').html('');
            $('#lname_msg').html('');
            $('#uname_msg').html('');
            $('#pwd_msg').html('');
            $('#email_msg').html('');

            if (first_name == "") {
                $('#fname_msg').html('First Name required');
                return false;
            } else if (last_name == "") {
                $('#lname_msg').html('Last Name required' + first_name.length);
                return false;
            } else if (user_name == "") {
                $('#uname_msg').html('Username required');
                return false;
            } else if (password == "") {
                $('#pwd_msg').html('Password required');
                return false;
            }
            else if (email == "" || result == false) {
                $('#email_msg').html('Please Give valid email id');
                $('#email').val('');
                return false;
            }
            else if (!$('input[name="type_gym"]').is(':checked') && !$('input[name="type_trainer"]').is(':checked')) {
                $('#sup_msg').text('Please check your facility!').css('color', 'red');
                return false;
            }
            else if (!$('input[name="sup_terms"]').is(':checked')) {
                $('#sup_msg').text('Please check terms and conditions!').css('color', 'red');
                return false;
            }
            else {
                if ($('input[name="type_gym"]').is(':checked')) {
                    var level = 1;
                    stype = '1';
                } else if ($('input[name="type_trainer"]').is(':checked')) {
                    var level = 1;
                    stype = '2';
                }

                $.ajax({
                    type: "POST",
                    url: "<?php echo SUPPLIER_URL;?>register",
                    data: {
                        'first_name': first_name,
                        'email': email,
                        'last_name': last_name,
                        'user_name': user_name,
                        'password': password,
                        'level': level,
                        'type': stype
                    },
                    success: function (datasup) {
                        //var url = datasup.indexOf('http');

                        if (datasup == 1) {
                            alert('Thank You. Please check your email');
                            $('.close').trigger('click');
                        }
                        else
                            $('#sup_msg').text(datasup).css('color', 'red');
                    },
                    error: function () {
                        $('#sup_msg').text("We are unable to do your request. Please contact webadmin").css('color', 'red');
                        return false;
                    }
                });
            }
        });

    });

</script>


<script>
    jQuery(function ($) {
        $('#first_name').keyup(function (e) {
            var numchar = $("#first_name").val().length;
            if (numchar >= 3) {
                loadRight($('#first_name'));
            }
            else {
                loadWrong($('#first_name'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });

        $('#last_name').keyup(function (e) {
            var numchar = $("#last_name").val().length;
            if (numchar >= 3) {
                loadRight($('#last_name'));
            }
            else {
                loadWrong($('#last_name'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });
        /* $('#user_email').keyup(function () {
         var email = $(this).val();
         var user_name = $("#sup_uname").val();
         var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         if (filter.test(email)) {


         loadRight($('#user_email'));
         if (email != "") {
         $.ajax({
         type: "POST",
         url: "
        <?php echo SUPPLIER_URL;?>exist_email",
         data: {
         'first_name': first_name,
         'email': email
         },
         success: function (data) {
         //alert(data);
         if (data == 1) {
         //alert(data);
         $("#email_result").val($.trim(data));
         $("#user_email").parent().siblings("div").text("* Already email exist");
         ValidateAnimation($("#user_email"));
         } else {
         //alert(data);
         $("#email_result").val(' ');
         $("#user_email").parent().siblings("div").text("");
         ValidateAnimation($("#user_email"));
         }
         }
         });
         }
         ValidationRemove($('#user_email'));
         }
         else {
         if ($(this).val() == "") {
         $(this).siblings('img').remove();
         }
         else {
         loadWrong($('#user_email'));
         return false;
         }
         }
         });*/

        $('#uname').keyup(function (e) {
            var numchar = $("#uname").val().length;
            var user_name = $("#uname").val();
            if (numchar < 6) {
                $("#error_username").html("* Enter at least 6 charaters");
            }
            else {
                $("#error_username").html(' ')
            }

            if (numchar > 5) {
                loadRight($('#uname'));
                if (user_name != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo USER_URL?>exist_user",
                        data: {
                            'user_name': user_name
                        },
                        success: function (data) {
                            //alert(data);
                            if (data == 1) {
                                //alert(data);
                                $("#name_res").val($.trim(data));
                                $("#uname").parent().siblings("div").text("* Already user name exist");
                                ValidateAnimation($("#uname"));

                                loadWrong($('#uname'));
                                return false;
                            }
                            else {
                                //alert(data);

                                $("#uname").parent().siblings("div").text("");
                                ValidationRemove($("#uname"));
                                return false;
                            }
                        }
                    });
                }
            }
            else {
                loadWrong($('#uname'));


            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
        });
        $("#progressbar1").progressbar({
            value: 1
        });

        $('#pwd').keyup(function () {
            pwd = $(this).val();
            var strpwd = /[^\w\*]/;
            $("#progressbar1").removeClass("beginning middle end");
            var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

            if (pwd.length < 6 && !regularExpression.test(pwd)) {
                $("#error_password").html("* Enter at least 6 charaters");
                $("#progressbar1").addClass("beginning");
                $("#progressbar1 > .ui-progressbar-value").animate({
                    width: "40%"
                }, 200);
            }
            else if (!regularExpression.test(pwd)) {
                $("#error_password").html("* Enter atleast 1 number and 1 special character");
                $("#progressbar1").addClass("beginning");

            }
            else {
                $("#error_password").html(' ');
                $("#progressbar1").addClass("beginning");
                $("#progressbar1 > .ui-progressbar-value").animate({
                    width: "100%"
                }, 200);
                loadRight($('#pwd'));
            }

            if (!regularExpression.test(pwd)) {
                loadWrong($('#pwd'));
            }
            else {
                loadRight($('#pwd'));
            }
            if ($(this).val() == "") {
                $(this).siblings('img').remove();
            }
            if (pwd.length == 0) {
                $("#progressbar1").addClass("beginning");
                $("#progressbar1 > .ui-progressbar-value").animate({
                    width: "0%"
                }, 200);
                return true;
            }
            /*if (pwd.length < 5) {
             $("#progressbar").addClass("beginning");
             $("#progressbar > .ui-progressbar-value").animate({
             width: "20%"
             }, 200);
             return true;
             }
             else if (pwd.length >= 5 && pwd.length <= 10) {
             $("#progressbar").addClass("middle");
             $("#progressbar > .ui-progressbar-value").animate({
             width: "40%"
             }, 200);
             return true;
             }
             else if (pwd.length >= 10 && pwd.length <= 12) {
             $("#progressbar").addClass("middle");
             $("#progressbar > .ui-progressbar-value").animate({
             width: "60%"
             }, 200);
             return true;
             }
             else if (pwd.length >= 12 && pwd.length <= 15) {
             $("#progressbar").addClass("middle");
             $("#progressbar > .ui-progressbar-value").animate({
             width: "80%"
             }, 200);
             return true;
             }
             else if (pwd.length >= 15) {
             $("#progressbar").addClass("end");
             $("#progressbar > .ui-progressbar-value").animate({
             width: "100%"
             }, 200).addClass("end");
             return true;
             }*/
        });

        $('#user_email').keyup(function () {
            var email = $(this).val();
            var user_name = $("#uname").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (filter.test(email)) {

                if (email != "") {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo USER_URL?>exist_email",
                        data: {
                            'user_name': user_name,
                            'email': email
                        },
                        success: function (data) {
                            //alert(data);
                            if (data == 1) {

                                //alert(data);
                                $("#email_res_u").val(1);
                                $("#user_email").parent().siblings("div").text("* Already email exist");

                                loadWrong($('#user_email'));
                                return false;
                                /**/
                            } else {
                                if (data == 0)
                                    $("#email_res_u").val(0);
                                loadRight($('#user_email'));
                                return false;
                            }
                        }
                    });
                } else {
                    loadWrong($('#user_email'));
                    return false;
                }

            }
            else {
                loadWrong($('#user_email'));
                return false;
            }
        });
        function Empty_validation() {
            if ($("#first_name").val() == "") {
                $("#first_name").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#first_name"));
            }
            if ($("#last_name").val() == "") {
                $("#last_name").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#last_name"));
            }
            if ($("#uname").val() == "") {
                $("#uname").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#uname"));
            }
            if ($("#pwd").val() == "") {
                $("#pwd").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#pwd"));
            }
            if ($("#email").val() == "") {
                $("#email").parent().siblings("div").text("* This field is required");
                ValidateAnimation($("#email"));
            }
        }

        function cross_validation() {
            if ($("#first_name").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#first_name").parent().siblings("div").text("* Enter at least 3 charaters");
                ValidateAnimation($("#first_name"));
            }
            if ($("#last_name").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#last_name").parent().siblings("div").text("* Enter at least 3 charaters");
                ValidateAnimation($("#last_name"));
            }
            /*if ($("#uname").siblings('img').attr('src') == '
            <?php // echo base_url()?>themes/user/images/cross.png') {
             $("#uname").parent().siblings("div").text("* Enter at least 6 charaters");
             ValidateAnimation($("#uname"));
             }
             if ($("#pwd").siblings('img').attr('src') == '
            <?php echo base_url()?>themes/user/images/cross.png') {
             $("#pwd").parent().siblings("div").text("* Enter atleast 1 number and 1 special character");
             ValidateAnimation($("#pwd"));
             }*/
            if ($("#email").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/cross.png') {
                $("#email").parent().siblings("div").text("* Enter Correct Email Id");
                ValidateAnimation($("#email"));
            }
        }

        function RigthValidation() {
            if ($("#first_name").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#first_name"));
            }
            if ($("#last_name").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#last_name"));
            }
            if ($("#uname").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#uname"));
            }
            if ($("#pwd").val() != "") {
                ValidationRemove($("#pwd"));
            }
            if ($("#email").siblings('img').attr('src') == '<?php echo base_url()?>themes/user/images/right.png') {
                ValidationRemove($("#email"));
            }

        }

        function ValidateAnimation(thisis) {
            thisis.parent().siblings("div").addClass("Validation").effect("bounce", {times: 4}, 1000).fadeIn("slow", function () {
                //$("#sub_btn").removeAttr('disabled');
            });
        }

        function ValidationRemove(thisis) {
            thisis.parent().siblings("div").fadeOut("slow");
        }

        function loadRight(thisis) {
            thisis.siblings('img').remove();
            $('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px; margin-right: 10px;"/>').insertAfter(thisis).delay(100).fadeOut(1000, function () {
                thisis.siblings('img').remove();
                $('<img src="<?php echo base_url()?>themes/user/images/right.png" class="subimg" style="float: right; margin-top: -30px; margin-right: 8px;"/>').insertAfter(thisis);
            });
        }

        function loadWrong(thisis) {
            thisis.siblings('img').remove();
            $('<img src="<?php echo base_url()?>themes/user/images/ajax-loader.gif" class="subimg" style="float: right; margin-top: -30px; margin-right: 10px;"/>').insertAfter(thisis).delay(100).fadeOut(100, function () {
                thisis.siblings('img').remove();
                $('<img src="<?php echo base_url()?>themes/user/images/cross.png" class="subimg" style="float: right; margin-top: -30px; margin-right: 8px;"/>').insertAfter(thisis);
            });
        }

        $('#sub_btn').click(function () {

            //$(this).attr("disabled", "disabled");
            RigthValidation();
            Empty_validation();
            cross_validation();

            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var user_name = $('#uname').val();
            var password = $('#pwd').val();
            var email = $('#user_email').val();
            var email_res_u = $("#email_res_u").val();
            var name_res = $("#name_res").val();
            var city_select = $("#city_select").val();
            var state_select = $("#state_select").val();
            var country_list = $("#country_list_value").val();
            var cur_url = "<?php echo current_url()?>";

            if (!$("#city_select").val()) {
                confirm('Do not forget to enter your city');
                return false;
            }


            if (!$('input[name="usr_terms"]').is(':checked')) {
                $("#cb1").parent().siblings("div").text("* Please Select an terms and conditions");
                ValidateAnimation($('#cb1'));
                return false;
            }
            else {
                ValidationRemove($("#cb1"));
            }
            if (!$('input:radio[name="gender"]').is(':checked')) {
                $("#R1").parent().siblings("div").text("* Please Select an option");
                ValidateAnimation($('#R1'));
                return false;
            }
            else {
                var gender = $('input:radio[name=gender]:checked').val();
                ValidationRemove($("#R1"));
                //console.log(gender);
            }
            if (last_name == "") {
                return false;
            }
            if (user_name == "") {
                return false;
            }
            if (password == "") {
                return false;
            }
            if (!email) {
                confirm('Do not forget to enter your email');
                return false;
            }

            if (email_res_u == 1) {
                alert("Email already exist");
                return false;
            }
            if (user_name == 1) {
                alert("User name already exist");
                return false;
            }

            var cur_url = "<?php echo current_url()?>";
            $.ajax({
                type: "POST",
                url: "<?php echo USER_URL?>register",
                data: {
                    'first_name': first_name,
                    'email': email,
                    'last_name': last_name,
                    'user_name': user_name,
                    'password': password,
                    'cur_url': cur_url,
                    'gender': gender,
                    'city_select': city_select,
                    'state_select': state_select,
                    'country_list': country_list
                },
                success: function (data123) {
                    if (data123 == 1) {
                        alert('Thank You. Please check your email');
                        $('.close').trigger('click');
                    }
                    else
                        $('#msg').text(data123).css('color', 'red');
                },
                error: function () {
                    $('#msg').text("We are unable to do your request. Please contact webadmin").css('color', 'red');
                }
            });

            return false;
        });
    });
</script>
<script src="<?php echo base_url(); ?>themes/admin/js/framework/select.js"></script>
<script>
    $(function () {
        $("#search_name").autocomplete({
            source: "user/get_gymname" // path to the get_birds method
        });
    });

    var _sIndex;
    $('.ssclick').ddslick({
        onSelected: function (sslick) {
            var selectValue = sslick.original[0][sslick.selectedIndex].value;
            if (!_sIndex) {
                _sIndex = (getCookie("lang")) ? getCookie("lang") : " ";
                return false;
            }
            if (_sIndex != sslick.original[0][sslick.selectedIndex].value) {
                changeLanguage({value: selectValue});
                location.reload();
            }
        }
    });
    /*var a = document.getElementsByClassName('dd-select');
    a[0].style.width = "164px";
    a[0].style.background = "white";
    a[0].style.color = "black";
    var b = document.getElementsByClassName('dd-container');
    b[0].style.width = "auto";
    var c = document.getElementsByClassName("dd-options dd-click-off-close")[0];
    c.style.width = '164px';*/
</script>
<!--<script type="text/javascript" src="<?php /*echo base_url() */?>themes/user/js/jquery-1.7.2.min.js"></script>-->
<!--
<script type="text/javascript" src="<?php /*echo base_url() */?>themes/user/js/jquery-ui.js"></script>-->

    <!--<script src="<?php /*echo base_url() */?>themes/user/js/feedback/script.js?ver=1" type="text/javascript"></script>-->
<!--
<script src="<?php /*echo base_url() */?>themes/user/countdown/jquery.countdown.js"></script>-->
<!--<script src="<?php /*echo base_url() */?>themes/user/js/script.js?ver=1"></script>-->
<!--<script src="<?php /*echo base_url() */?>themes/user/js/bootstrap.js"></script>-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    //add states to register-user form
    function load_dropdown_contents(selected_value) {

        if (selected_value != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_states_for_register_form/' + selected_value,
                'async': false
            }).responseText;
            console.log(result);
            $("#state_select").replaceWith(result);
        }
    }
    function load_dropdown_content(selected_value) {

        if (selected_value != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_states/' + selected_value,
                'async': false
            }).responseText;
            $("#state").replaceWith(result);
        }
    }
    //add cities into register user form
    function get_cities_form_registration(state) {
        if (state != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_cities_for_register_form/' + state,
                'async': false
            }).responseText;
            $("#city_select").replaceWith(result);
        }
    }
    function get_cities(state) {
        if (state != '') {
            var result = $.ajax({
                'url': '<?php echo USER_URL;?>get_cities/' + state,
                'async': false
            }).responseText;
            $("#city").replaceWith(result);
        }
    }

    var a = document.getElementsByClassName('dd-select');
    a[0].style.width = "100px";
    a[0].style.background = "white";
    a[0].style.color = "black";
    var b = document.getElementsByClassName('dd-container');
    b[0].style.width = "auto";
    var c = document.getElementsByClassName("dd-options dd-click-off-close")[0];
    c.style.width = '185px';
</script>