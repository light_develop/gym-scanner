<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
<div class="dash_bord">
<div class="top_menu_wrapp">
  <ul>
    <li><a style="border:none"><?php echo $this->lang->line("Error page"); ?></a></li>
  </ul>
  
  <div class="clear"></div>
</div>
  
  
  <div class="dimound_details error">
    <h1><?php echo $this->lang->line("404 error"); ?></h1>
  </div>

</div>
</div>
