<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
	<div class="add_gym_wra">
		<h1><?php echo $this->lang->line("Forgot Password"); ?></h1>
	  	<?php if($this->session->flashdata('success')!=""){?>
	        <div class="alert-success">
	            <button type="button" class="close" data-dismiss="alert">×</button>
				<font
					color="green"><strong><?php echo $this->lang->line("Success!"); ?></strong> <?php echo $this->session->flashdata('success'); ?>
				</font>
	        </div>
	    <?php }?> 
	    <?php if ($this->session->flashdata('error') != "") { ?>
	        <div class="alert">
	            <button type="button" class="close" data-dismiss="alert">×</button>
				<font
					color="red"><strong><?php echo $this->lang->line("Error !"); ?></strong> <?php echo $this->session->flashdata('error'); ?>
				</font>
	        </div>
	    <?php } ?> 
	  	<form action="<?php echo USER_URL.'send_pwd'?>" class="form-horizontal" method="post" id="gym_form" enctype = "multipart/form-data">
			<div class="col-lg-12 ad_form_wr">
				<h2><?php echo $this->lang->line("Username"); ?></h2>
				<input type="text" class="form_big" name="user_name"
					   placeholder="<?php echo $this->lang->line("Username"); ?>" value="" required/>
		  	<div class="clear"></div>
				<h2><?php echo $this->lang->line("Email"); ?></h2>
		  	<input type="text" class="form_big" name="email" placeholder="Email" value="" required/>
		  	<div class="clear"></div>
		  	<h2>&nbsp;</h2>
				<input type="checkbox" id="level" name="level" value="1"><?php echo $this->lang->line("Supplier"); ?>
				<font
					color="#87191a"><?php echo $this->lang->line("(Check if you are a Gym or Personal Trainer)"); ?></font>
		  	<div class="clear"></div>
				<input name="submit" class="sub_green" value="<?php echo $this->lang->line("Send"); ?>" type="submit">
			</div>
		</form>
	<div class="clear"></div>
</div>
</div>
