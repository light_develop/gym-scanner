<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container">
<div class="add_gym_wra editoffer" id="add_offer_div">
      <h1> Edit Offer |<a href="<?php echo site_url('facility');?>" style="color:#fff;">Dashboard</a></h1>
      <form action="<?php echo site_url('update_offer');?>" class="form-horizontal" method="post" id="add_offr"/>
      <div class="col-lg-12 ad_form_wr">  
        <?php if($this->session->flashdata('error') !='') { ?>
          <div class="error"> <?php echo $this->session->flashdata('error'); ?></div>
		<?php } ?>

		  <h2><?php echo $this->lang->line("Select Facility"); ?></h2>
        <?php if(isset($details)){ ?>
          <?php 
          $attributes1 = 'class="form_big" onchange="chk_edit_gym(this.value)" '; 
          echo form_dropdown('gym',$gyms,$details[0]->gym_id,$attributes1); ?>
          <span class="error_gym"></span>

        <input type="hidden" id="old_gym" name="old_gym" value="<?php echo $details[0]->gym_id;?>"/>
        <?php } ?>
        <div class="clear"></div>
		<div id="editofr">
			<h2><?php echo $this->lang->line("Offer Details"); ?></h2>
			  <div class="table_right">
				<div class="ofr_wraper">
				  <table width="99%" class="new_tab table_right">
					<tr class="row_cal">
					  <td> 
						<div class="top_clr_he boxs_mn top_clr_he">
							<h2><?php echo $this->lang->line("Duration"); ?></h2>

							<p><?php echo $this->lang->line("Enter the duartion period of your gym/fitness center membership"); ?></p>
						</div>
					  </td>
					  <td>
						<div class="boxs_mn top_clr_he1">
							<h2><?php echo $this->lang->line("Membership Rates"); ?></h2>

							<p><?php echo $this->lang->line("Your current membership rate before discount or perks"); ?></p>
						</div>
					  </td>
					  <td>    
						<div class="boxs_mn top_clr_he2">
							<h2><?php echo $this->lang->line("Cash Discount"); ?></h2>
						  <p>The level cash discount you would like to offer?</p>
						</div>
					  </td>
					  <td>
						<div class="boxs_mn top_clr_he3">
							<h2><?php echo $this->lang->line("Special Offer"); ?></h2>
						  <p>Any special perks you would like to throw in?</p>
						</div>
					  </td>
					  <td>
						<div class="boxs_mn top_clr_he4">
							<h2><?php echo $this->lang->line("Customer gets"); ?></h2>

							<p><?php echo $this->lang->line("This is what your customer gets today"); ?></p>
						</div>
					  </td>
					  <td></td>             
					</tr>
					<?php $i = 0; //echo count($details);
					  foreach ($details as $key => $value) { ?>
						<input type="hidden" name="offr_id[]" value="<?php echo $value->id?>"/>
						<input type="hidden" name="relation_id" value="<?php echo $details[0]->offer_relation_id?>"/>
						<tr class="row_cal">
							<td><input name="duration[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 1 Month"); ?>" type="text"
									   value="<?php echo $value->duration; ?>" required/></td>
							<td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: US$ 30"); ?>"
									   value="<?php echo $value->membership; ?>" type="text" required/></td>
							<td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: 10% off"); ?>"
									   value="<?php echo $value->discount; ?>" type="text"/></td>
							<td><input name="offer[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 1 Week Free"); ?>"
									   value="<?php echo $value->offer; ?>" type="text" required/></td>
						  <td><?php echo $value->description;?><br/></td>
						  <td style="width:14%;">
							<?php if($i == 3){?>
							<input type="button" class="plus_btp add_more">
							<?php } ?>
						  </td>
						</tr>
					<?php $i++; } ?> 
					 
					<?php while($i < 4){
					  if($i == 0){ ?>
						<tr class="row_cal">
							<td><input name="duration[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 1 Month"); ?>" value=""
									   type="text" required/></td>
							<td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: US$ 30"); ?>" value=""
									   type="text" required/></td>
							<td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: 10% off"); ?>" value=""
									   type="text"/></td>
							<td><input name="offer[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 1 Week Free"); ?>" value=""
									   type="text" required/></td>
						  <td></td>
						  <td></td>
						</tr>
					  <?php } else if($i == 1){?>
						<tr class="row_cal">
							<td><input name="duration[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 3 Months"); ?>" value=""
									   type="text"></td>
							<td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: US$ 75"); ?>" value=""
									   type="text"></td>
							<td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: 10% off"); ?>" value=""
									   type="text"></td>
							<td><input name="offer[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 10 Free Ses-"); ?>" value=""
									   type="text"></td>
						  <td></td>
						  <td></td>
						</tr>
					  <?php } else if($i == 2){?>
						<tr class="row_cal">
							<td><input name="duration[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 6 Months"); ?>" value=""
									   type="text"></td>
							<td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: US$ 120"); ?>" value=""
									   type="text"></td>
							<td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: 10% off"); ?>" value=""
									   type="text"></td>
							<td><input name="offer[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 3 Weeks Free"); ?>" value=""
									   type="text"></td>
						  <td></td>
						  <td></td>
						</tr>
					  <?php } else if($i == 3){?>
						<tr class="row_cal">
							<td><input name="duration[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 8 Months"); ?>" value=""
									   type="text"></td>
							<td><input name="membership[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: US$ 200"); ?>" value=""
									   type="text"></td>
							<td><input name="discount[]" class="te_name" onkeypress="return isNumberKey(event)"
									   placeholder="<?php echo $this->lang->line("Example: 20% off"); ?>" value=""
									   type="text"></td>
							<td><input name="offer[]" class="te_name"
									   placeholder="<?php echo $this->lang->line("Example: 1 Month Free"); ?>" value=""
									   type="text"></td>
						  <td></td>
						  <td style="width:14%;">
							  <input name="minus" type="button" class="plus_btp add_more"></td>
						</tr> 
					  <?php } ?>
					<?php $i++;} ?>           
				  </table>
				  <div class="clear"></div>
				  </div>  
			  </div> 
		  </div>
		  <div class="clear"></div>
		  <input name="submit" class="addd_offer" id="sub_new" value="<?php echo $this->lang->line("Commit"); ?>"
				 type="submit"/>
      </div>
      </form>             
      <div class="clear"></div>
    </div>


</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery-1.7.2.min.js"> </script>
<script type="text/javascript">
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      else
      return true;
}
</script>
<script type="text/javascript">
  var isanyerror=false;
  $(document).on('click','#sub_new',function(){
    //console.log(gym);
  if(!isanyerror)
      return true;
      else
      return false;
  });
</script>
<script type="text/javascript">
  var cnt = 0;
  $(document).on('click','.add_more',function(){
    cnt = cnt + 1; 
    $('.new_tab').append('<tr><td><input name="" class="te_name" placeholder="Example: 1 Month" type="text"></td><td><input name="" class="te_name" onkeypress="return isNumberKey(event)" placeholder="Example: US$ 30" type="text"></td><td><input name="" class="te_name" onkeypress="return isNumberKey(event)" placeholder="Example: 10% off" type="text"></td><td><input name="" class="te_name" placeholder="Example: 1 Week Free" type="text"></td><td></td><td style="width:14%;"><input type="button" class="min_btp"><input type="button" class="plus_btp add_more"></td></tr>');
  });
  $(document).on('click','.min_btp',function (){
    $(this).closest('tr').remove();
  });
</script>

<script type="text/javascript">

  $(document).on('focusout', '.row_cal input', function(event){  
    $('.row_cal').each(function(){
      var first=$(this).find('td:first-child input').val();
      var second=$(this).find('td:nth-child(2) input').val();
      var third=$(this).find('td:nth-child(3) input').val();
      var fourth=$(this).find('td:nth-child(4) input').val();
      if($.trim(first)!='')
      var total = first+' Membership ';
      if($.trim(second)!='')
      total = first+' Membership for US$ '+(second-(second*third/100));
      if($.trim(fourth) !='')
        total=first+' Membership + '+$.trim(fourth)+' for US$ '+(second-(second*third/100));
      //$(this).find('td:nth-child(5) td').val(total);
      $(this).find('td:nth-child(5)').text(total);
    });
    });
</script>
<script>
  function chk_edit_gym(gym){
    var old_gym = $('#old_gym').val();
    $('.error_gym').text('');
    if(gym != old_gym){
      var result = $.ajax({
                   'url' : '<?php echo USER_URL;?>asoffers/check_gym/' + gym,
                   'async' : false
             }).responseText;
         $(".error_gym").text(result).css('color','red');
         if($.trim(result)!='')
         isanyerror=true;
         else
         isanyerror=false;
    }else{
      isanyerror=false;
    }
  } 
</script>
<style>
.editoffer h1 {
    background-color: #87191a;
    color: #fff;
    font-family: gothic !important;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 10px;
    margin-top: 0;
    padding-bottom: 10px;
    padding-left: 10px !important;
    padding-right: 10px;
    padding-top: 10px;
    text-transform: uppercase;
}
.editoffer h2 {
    color: #605f5f;
    display: block;
    float: left;
    font-size: 16px;
    width: 208px;
}
@media screen and (max-width: 980px){
	#editofr .table_right {
		float: left;
		width: 100%;
	}
	#editofr .boxs_mn {
		height: 100px;
		padding-top: 7px;
		text-align: center !important;
		width: 100%;
	}
}
@media screen and (max-width: 730px){
	#editofr .table_right {
		float: left;
		width: 650px;
	}
	#editofr{
		float: left;
		overflow: scroll;
		width: 100%;
	}
}
</style>