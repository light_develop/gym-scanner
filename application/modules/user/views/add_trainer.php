<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/jquery-gmaps-latlon-picker.css"/>
<script src="<?php echo base_url();?>themes/admin/js/jquery-gmaps-latlon-picker.js"></script>
<div class="container">
	<div class="add_gym_wra" id="addgym">
		<h1> <?php if (isset($details)) echo "Edit Facility"; else echo "Add Facility"; ?> |<a
				href="<?php echo site_url('facility') ?>"><? echo $this->lang->line('Dashboard') ?></a>|<a
				onclick="goBack()"><? echo $this->lang->line('Back') ?></a></h1>
<?php 	$edit = 0;
	if(isset($details)) {
		$edit = 1;
	}?>
		<span id="ytlInfo"></span>
	  	<form action="<?php if(isset($details)) echo site_url('edit_gym'); else echo site_url('edit_gym');?>" class="form-horizontal" method="post" id="gym_det_form" enctype = "multipart/form-data">
		<div class="col-lg-12 ad_form_wr">	
			<div class="prof_frm">
				<div class="flt"><h2 class="flt"><? echo $this->lang->line('Facility Name') ?></h2></div>
				<div class="prof_sec_div">
		  			<input type="hidden" name="id" value="<?php if(isset($details)) echo $details->id;?>"/>
		   			<input name="gymname" class="form_big" type="text" required value="<?php if(isset($details)) echo $details->gymname;?>" />
				</div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Face Picture') ?></h2></div>
                <div class="prof_sec_div">
                <input name="logo" class="form_sm" type="file" <?php if(!$edit) { ?> <?php } ?>>
                <div class="clear"></div> 
                <?php if(isset($details)){
                    if($details->gym_logo != '')
                        $url = base_url().'uploads/gym/logos/'.$details->gym_logo;
                    else
                        $url = base_url().'no-image.png';?>
                    <div class="map_gym logo_img"><img src="<?php echo $url;?>"/></div>
                    <input type="hidden" name="old_logo" value="<?php echo $details->gym_logo;?>">
                    <?php }?>
                </div>
			</div>
			<!-- <div class="prof_frm">
		 		<div class="prof_fst_div"><h2><?php echo $this->lang->line("Face Picture"); ?></h2></div>
		  		<div class="prof_sec_div"><input name="logo" class="form_sm" type="file" <?php if(!$edit) { ?>required <?php } ?>></div>
		  	</div> 
		  	<?php if(isset($details)){
		 		if($details->gym_logo != '')
		 			$url = base_url().'uploads/gym/logos/'.$details->gym_logo;
		 		else
		 			$url = base_url().'no-image.png';?>
		 		<div class="map_gym logo_img"><img src="<?php echo $url;?>"/></div>
		 		<input type="hidden" name="old_logo" value="<?php echo $details->gym_logo;?>">
		 		<?php }?>
 -->
 			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Images') ?></h2></div>
				<div class="prof_sec_div">
					<?php if(isset($images)){
						foreach ($images as $key => $value) { 
			              $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
			            	<span style="position:relative" id="img_<?php echo $value->id;?>">
			                <img src="<?php echo $image;?>" width="100px" height="50"> &nbsp;&nbsp;
			                <i class="icon-remove" name="<?php echo $value->id?>" id="<?php echo $value->gym_images?>" style="position:absolute;margin-top:-10px;margin-left:-10px;color:red;">X</i></span>                      
			            <?php } }else{?> 
						<img id="uploadPreview" style="display:none;"/>
						<input name="upload_file[]" class="form_sm" type="file" id="uploadImage" required>
						
						<p style="padding-left:20px;color:#87191a;margin-top: 0;">
							<? echo $this->lang->line('All images will be cropped to maximum size of 750px X 500px') ?>
						</p>
							  
					<?php } ?>
					<div style="clear:both"></div>   
					<!--<h2>&nbsp;</h2> -->
					<div style="margin-left: 24%; margin-top: 10px;"><input id="add_images" class="r_addmore"
																			value="<? echo $this->lang->line('ADD MORE') ?>"
																			type="button">
					<img src="<?php echo base_url();?>themes/user/images/plus.png"  /></div>
					<div class="clear"></div>
				   
				   	<div id="add_more"></div>
				   	<div class="clear"></div>
				</div>
		   	</div>

		   	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Video') ?></h2></div>
			   	<div class="prof_sec_div">
			   		<?php if(isset($details)){
			   			$videos = $details->gym_video;
			   			$res = explode('~',$details->gym_video);
				  		foreach($res as $value){ ?>		                        
	                        <input name="gym_video[]" class="form_sm youtube" type="text" value="<?php echo $value;?>" />
	                        <div style="clear:both"></div>
						<?php }
					} else { ?>
						<input name="gym_video[]" id="youtube" class="form_sm youtube" type="text"
							   placeholder="<? echo $this->lang->line('ENTER YOUTUBE / VIMEO LINK') ?>"/> &nbsp;&nbsp;
			    	<!-- <span id="ytlInfo"></span> -->
			    	<?php } ?>
				   	<div class="clear"></div>

				   	<!--<h2>&nbsp;</h2> -->
					<div style="margin-left: 24%; margin-top: 10px;"><input id="add_urls" class="r_addmore"
																			value="<? echo $this->lang->line('ADD MORE') ?>"
																			type="button">
					<img src="<?php echo base_url();?>themes/user/images/plus.png"  /></div>
					<div class="clear"></div>
				</div>
			</div>

		    <div id="add_videos"></div>

		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Availability') ?></h2></div>
		        <div class="prof_sec_div">
					<div><input type="checkbox" name="availablity" id="check_availablity"
								value="1">&nbsp;<? echo $this->lang->line('We are available 24 x 7') ?></div>
					<br/>

					<div id="open_close_time"><? echo $this->lang->line('OR') ?><br/> <br/>
						<? echo $this->lang->line('We are open from') ?> &nbsp;
				    <div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp;<? echo $this->lang->line('to') ?>  &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('SATURDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time1"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time1" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('SUNDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time2"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time2" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('MONDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time3"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time3" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('TUESDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time4"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time4" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('WEDNESDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time5"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time5" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('THURSDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					<div style="width: 80%; float: right;">
						<div class="time"><input type="text" class="time start required" name="open_time6"
												 value="<?php if (isset($details)) echo $details->open_time; ?>"
												 style="width:70px;float:left;height: 29px;"/></div>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end required" name="close_time6" value="<?php if(isset($details)) echo $details->close_time;?>" style="width:70px;float:left; height: 29px;margin-right: 10px;"/></div>
						<input id="add_urls" class="r_addmore" value="<? echo $this->lang->line('FRIDAY') ?>"
							   type="text"
							   style="float: left; height: 29px; padding: 0px 5px; width: 85px;color: #000;">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
		            </div>
					
				  </div>
			  </div>
			</div>

			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Caters to') ?></h2></div>
				<div class="prof_sec_div" id="select_arrow">
					<?php if(isset($details)){?>
						<select name="speciality" class="form_sm form_clr">
							<option
								value="1" <?php if ($details->speciality == 1) echo 'selected'; ?>><? echo $this->lang->line('Men only') ?></option>
							<option
								value="2" <?php if ($details->speciality == 2) echo 'selected'; ?>><? echo $this->lang->line('Women only') ?></option>
							<option
								value="3" <?php if ($details->speciality == 3) echo 'selected'; ?>><? echo $this->lang->line('Both') ?></option>
						</select>
					<?php } else{?>
					<select name="speciality" class="form_sm form_clr">
						<option value="1" selected><? echo $this->lang->line('Men only') ?></option>
						<option value="2"><? echo $this->lang->line('Women only') ?></option>
						<option value="3"><? echo $this->lang->line('Both') ?></option>
					</select>
					<?php } ?>
				</div>
			</div>

		  	<!-- <div class="member_wrapp">
			  	<h2><?php echo $this->lang->line("Certifications"); ?></h2>
			  	<div class="clear"></div>
			  	<div class="price_bx">
			  		<?php if(isset($details)){
			  			$res = explode('~',$details->certifications);
			  			foreach($res as $value){ ?>
			  			<input type="text" name="cert[]" value="<?php echo $value;?>"/> &nbsp;<br/><br/>
			  		<?php } } else{ ?>	
			  		<input type="text" name="cert[]"> &nbsp;<br/><br/>
			  		<?php } ?>
					<input id="add_cert" class="r_addmore" value="ADD MORE" type="button">
					<div class="clear"></div>
			  		<div id="add_certs"></div>
		   			<div class="clear"></div>
			  	</div>
		  	</div> -->	
		  	<div class="prof_frm">
				<div class="flt"><h2><?php echo $this->lang->line("Certifications"); ?></h2></div>
			  	<div class="prof_sec_div">
			  	<div class="price_bx">
			  		<?php if(isset($details)){
			  			$res = explode('~',$details->certifications);
			  			foreach($res as $value){ ?>
			  			<input type="text" name="cert[]" value="<?php echo $value;?>"/> &nbsp;<br/><br/>
			  		<?php } } else{ ?>	
			  		<input type="text" name="cert[]"> &nbsp;<br/><br/>
			  		<?php } ?>
					<div style="margin-top: 10px;"><input id="add_cert" class="r_addmore"
														  value="<? echo $this->lang->line('ADD MORE') ?>"
														  type="button">
					<img src="<?php echo base_url();?>themes/user/images/plus.png"  /></div>
					
					<div class="clear"></div>
			  		<div id="add_certs"></div>
		   			<div class="clear"></div>
			  	</div>
			  	</div>
		  	</div>	
			<div id="location_content">
				<div class="prof_frm">
					<div class="flt"><h2><? echo $this->lang->line('Location') ?></h2></div>
					<div style="float:left;width: 35%;">
					  <div id="select_arrow1">
						<div style="height: 29px; background-color: rgb(255, 255, 255); width: 89px;">
						<?php $cntry[''] = 'Country';$attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="form_sm" required';
							if(isset($details))
								echo form_dropdown('cntry', $cntry, $details->gcountry,$attributes);
							else
								echo form_dropdown('cntry', $cntry, '',$attributes);
						?> 
						</div>
					  </div>
					  <div id="select_arrow1" style="margin-left: 10px;">
						<div style="height: 29px; background-color: rgb(255, 255, 255); width: 89px;">
						<?php $attributes= 'id="state"  class="form_sm" required onchange="get_cities(this.value)"';
							if(isset($details))
								echo form_dropdown('state', $states, $details->gstate,$attributes); 
							else
								echo form_dropdown('state', array(), NULL,$attributes);     
						?>
						</div>
					  </div>
					  <div id="select_arrow1" style="margin-left: 10px;">
						<div style="height: 29px; background-color: rgb(255, 255, 255); width: 89px;">
						<?php $attributes= 'id="city"  class="form_sm" required';   
							if(isset($details))                  
								echo form_dropdown('city', $cities, $details->gcity,$attributes);
							else
								echo form_dropdown('city', array(), NULL,$attributes);  
						?> 
						</div>
					  </div>
						<div class="prof_sec_div"><input type="text" name="gstreet" onblur="get_map()" id="gstreet"
														 required class="form_sm form_clr"
														 value="<?php if (isset($details)) echo $details->gstreet; ?>"
														 style="width: 100%; margin-top: 20px;"
														 placeholder="<? echo $this->lang->line('STREET ADDRESS') ?>"/>
						</div>
						<div class="prof_sec_div"><input name="gzip" id="zip_code" onblur="get_map();"
														 class="form_sm form_clr" required type="text"
														 value="<?php if (isset($details)) echo $details->gzip; ?>"
														 style="width: 100%; margin-top: 10px;"
														 placeholder="<? echo $this->lang->line('ZIP CODE') ?>"/></div>
					  
					</div>
					
					<div style="float: right;width: 55%;">
						<div class="map_gym">
							<fieldset class="gllpLatlonPicker" id="custom_id" >
								<input type="hidden" name="search_location" id="search_location" class="gllpSearchField" value="<?php if(isset($details)) echo $details->location;?>" required/>
								<input type="button" name="search" class="gllpSearchButton" value="search" style="display:none;"/>
								<br/><br/>

								<div class="gllpMap"><? echo $this->lang->line('Google Maps') ?></div>
								<input type="hidden" class="gllpLatitude" name="latitude" value="60"/>
								<input type="hidden" class="gllpLongitude" name="longitude" value="30"/>
								<input type="hidden" class="gllpZoom" name="zoom" value="12"/>                                                       
							</fieldset>
						</div>   
					</div> 
				</div>
			</div>
				  
            <div class="prof_frm">
		 	<input name="cat_id" type="hidden" value="2" readonly>
		 	<?php if(isset($details)){ $adv = explode(',',$details->advanced);}?>
				<div class="flt"><h2><? echo $this->lang->line('Tags') ?></h2></div>
		  		<div class="prof_sec_div" style="width: 100%;">
		  			<div class="addv_r_wrapp">		  
					 	<div class="first_rd">
					 		<input name="adv[pilates]" type="checkbox" value="1" <?php if(isset($details) && in_array('pilates', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('PILATES/YOGA') ?></h6>
					 		<div class="clear"></div>
						</div>

					  	<div class="second_rd">
						 	<input name="adv[mma]" type="checkbox" value="1" <?php if(isset($details) && in_array('mma', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('MMA') ?></h6>
						 	<div class="clear"></div>
						</div>  
				 
				  		<div class="third_rd">
				 			<input name="adv[boxing]" type="checkbox" value="1" <?php if (isset($details) && in_array('boxing', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('BOXING') ?></h6>
				 			<div class="clear"></div>
						</div>  
				 
						<div class="fourth_rd">
						 	<input name="adv[cross]" type="checkbox" value="1" <?php if (isset($details) && in_array('cross', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('CROSS TRAINING') ?></h6>
						 	<div class="clear"></div>
						</div>    

						<div class="fifth_rd">
						 	<input name="adv[spinning]" type="checkbox" value="1" <?php if (isset($details) && in_array('spinning', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('SPINNING') ?></h6>
						 	<div class="clear"></div>
						</div>

						<div class="fifth_rd">
						 	<input name="adv[gymnastics]" type="checkbox" value="1" <?php if (isset($details) && in_array('gymnastics', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('GYMNASTICS') ?></h6>
						 	<div class="clear"></div>
						</div>
				    	
					 	<div class="fifth_rd">
						 	<input name="adv[martial]" type="checkbox" value="1" <?php if (isset($details) && in_array('martial', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('MARTIAL ARTS') ?></h6>
						 	<div class="clear"></div>
						</div>
						<div class="first_rd">
						 	<input name="adv[weight]" type="checkbox" value="1" <?php if (isset($details) && in_array('weight', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('WEIGHT LOSS') ?></h6>
						 	<div class="clear"></div>
						</div>
						<div class="first_rd">
						 	<input name="adv[cond]" type="checkbox" value="1" <?php if (isset($details) && in_array('cond', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('CONDITIONING') ?></h6>
						 	<div class="clear"></div>
						</div>
						<div class="sixth_rd">
						 	<input name="adv[building]" type="checkbox" value="1" <?php if (isset($details) && in_array('building', $adv)) echo "checked"; ?>/>
							<h6><? echo $this->lang->line('BODY BUILDING') ?></h6>
						 	<div class="clear"></div>
						</div> 
					</div>
				</div>  
		  	</div>
		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Contact Person') ?></h2></div>
				<div class="prof_sec_div"><input name="owner" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->owner;?>"/></div>  
		  	</div>
		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Phone No') ?></h2></div>
				<div class="prof_sec_div"><input name="ow_phone" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->ow_phone;?>"/></div>
		  	</div>
		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Email') ?></h2></div>
				<div class="prof_sec_div"><input name="ow_email" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->ow_email;?>"/></div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Site Url') ?></h2></div>
				<div class="prof_sec_div"><input name="site_url" class="form_sm" type="text" value="<?php if(isset($details)) echo $details->site_url;?>"/></div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Trainer Profile') ?></h2></div>
				<div class="prof_sec_div"><textarea name="description" rows="8" cols="59" required type="text" ><?php if(isset($details)) echo $details->description;?></textarea></div>
		  	</div>	
		  	<div class="clear"></div>	   
		    <div class="prof_frm">
		    	<div class="flt">&nbsp;</div>
		   		<div class="prof_sec_div"><input name="submit" class="sub_red" value="Save and Next Page" type="submit" style="margin-left: 0%;"><img src="<?php echo base_url();?>themes/user/images/Right-arrow.png" /></div>
		   	</div>			   
		</div>
		</form>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery-1.7.2.min.js"> </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="http://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" />
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#gym_det_form").validate({
	  rules: {
	    // simple rule, converted to {required:true}
	    cntry: "required",
	    state: "required",
	    state: "required",
	    // compound rule
	    email: {
	      required: true,
	      email: true
	    }
	  }
	});
</script>
<script type="text/javascript">
	function ytVidId(url) {
		var contain1=url.indexOf('youtube');
		var contain2=url.indexOf('vimeo');
		if(contain1>-1)
    	var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
    	else if(contain2 > -1)
    	var p = /http:\/\/(?:www\.|player\.)?(vimeo|youtube)\.com\/(?:embed\/|video\/)?(.*?)(?:\z|$|\?)/;
    	else
    		return false;
    	return (url.match(p)) ? RegExp.$1 : false;
	}

	$(document).on("change keyup input",'.youtube', function() {
		//console.log('hi');
		//$('.ytlInfo').html('');
	    var url = $(this).val();
	    if (ytVidId(url) !== false) {
	        $('#ytlInfo').html('');
	    } else {
	        $('#ytlInfo').html('give valid url').css('color','red');
	    }
	});
</script>

<script> 
   
  function load_dropdown_content(selected_value){ 
  		get_map();
       if(selected_value!=''){
    	       var result = $.ajax({
        	         'url' : '<?php echo SUPPLIER_URL;?>get_states/' + selected_value,
        	         'async' : false
    	       }).responseText;
    	   $("#state").replaceWith(result);
      }
  }
  function get_cities(state){
  	get_map();
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo SUPPLIER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function deleteImage(id, imgname)
  {
      if(confirm('Are you sure want to delete this image?'))
      {
        $('#delimg'+id).remove();
        $('#removeimgdiv').append('<input type="hidden" name="remove_gym_image[]" value="'+imgname+'">');
      }
  }
  
</script>
<script>
    $(function (){
        var url_count = 0;
        $('#add_urls').on("click",function(){
            url_count = url_count+1;
			$("#add_videos").append('<div class="prof_frm" id="divs' + url_count + '"><div class="prof_fst_div">&nbsp;</div><div class="prof_sec_div"><input type="text" required class="form_sm youtube" name="gym_video[]" />&nbsp;&nbsp;<button type="button" class="btn btn-warning " onclick="remove_url(' + url_count + ')" ><?php echo $this->lang->line("Remove"); ?></button></div></div>');
            $("#divs"+url_count).hide();

            $( "#divs"+url_count ).slideDown( "slow", function() {
            // Animation complete.
            });
        });
    });
    function remove_url(id){

    $( "#divs"+id).slideUp( "slow", function() {
        $("#divs"+id).remove();
      });
    }
</script>
<script type="text/javascript">
	function get_map(){
		var sea_cntry = $( "#cntry option:selected" ).text();
		var sea_state = $( "#state option:selected" ).text();
		var sea_city = $( "#city option:selected" ).text();
		var sea_street = $( "#gstreet" ).val();
		var sea_zip = $( "#zip_code" ).val();
		$('#search_location').val($.trim(sea_street+", "+sea_city+', '+sea_state+', '+sea_cntry+', '+sea_zip));
		//$('#search_location').css('display', 'none');
		//$('#custom_id .gllpSearchButton').css('display', 'none');
		$('#custom_id .gllpSearchButton').trigger('click');
	}
</script>
<script>
    $(function (){
        var img_count = 0;
        $('#add_images').on("click",function(){
            img_count = img_count+1;
			$("#add_more").append('<div class="map_gym" id="divname' + img_count + '"><input type="file" name="upload_file[]" class="form_sm"/><button type="button" class="btn btn-warning " onclick="remove_btn(' + img_count + ')" ><?php echo $this->lang->line("Remove"); ?></button></div>');
            $("#divname"+img_count).hide();
            //$("#jcrop_target"+img_count).hide();

            $( "#divname"+img_count ).slideDown( "slow", function() {
            // Animation complete.
            });
        });
    });
</script>
<script type="text/javascript">
    function remove_btn(id){

    $( "#divname"+id).slideUp( "slow", function() {
        $("#divname"+id).remove();
      });
    }
</script>
<script>
    $(function (){
        var cont = 0;
        $('#add_cert').on("click",function(){
            cont = cont+1;
			$("#add_certs").append('<div id="div' + cont + '"><input type="text" name="cert[]" />&nbsp;&nbsp;<button type="button" class="btn btn-warning " onclick="remv_btn(' + cont + ')" ><?php echo $this->lang->line("Remove"); ?></button><br></div>');
            $("#div"+cont).hide();
            //$("#jcrop_target"+img_count).hide();

            $( "#div"+cont ).slideDown( "slow", function() {
            // Animation complete.
            });
        });
    });
</script>
<script type="text/javascript">
    function remv_btn(id){

    $( "#div"+id).slideUp( "slow", function() {
        $("#div"+id).remove();
      });
    }
</script>
<script type="text/javascript">
    $('.icon-remove').click (function () {

     var id = $(this).attr('name');
     var image = $(this).attr('id');
         var result = $.ajax({
            'url' : '<?php echo SUPPLIER_URL?>del_img/' + id  + '/' + image,
            'async' : false
        }).responseText;
           $('#img_'+id).hide();
     return false;
     });

</script>
<script>
	$(document).ready(function(){
		
		$('#check_availablity').click(function(){
			if($(this).is(':checked'))
			{
				$('.start').val('12:00am').attr('readonly', true);
				$('.end').val('11:30pm').attr('readonly', true);
			}
			else
			{
				$('.start').val('').attr('readonly', false);
				$('.end').val('').attr('readonly', false);
			}
			
		});
	});


	
  	$('#timeOnlyExample .time').timepicker({
    	'showDuration': true,
    	'timeFormat': 'g:ia'
	});

</script>
<script>
	$(document).ready(function(){
		
		$('#check_availablity').click(function(){
			if($(this).is(':checked'))
			{
				$('.start').val('12:00am').attr('readonly', true);
				$('.end').val('11:30pm').attr('readonly', true);
			}
			else
			{
				$('.start').val('').attr('readonly', false);
				$('.end').val('').attr('readonly', false);
			}
			
		});
	});


	
  	$('#timeOnlyExample .time').timepicker({
    	'showDuration': true,
    	'timeFormat': 'g:ia'
	});

</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://odyniec.net/projects/imgareaselect/css/imgareaselect-default.css" />
<script type="text/javascript" src="http://odyniec.net/projects/imgareaselect/jquery.imgareaselect.pack.js"></script>
<script>
function setInfo(i, e) {
  $('#x').val(e.x1);
  $('#y').val(e.y1);
  $('#w').val(e.width);
  $('#h').val(e.height);
}

$(document).ready(function() {
  var p = $("#uploadPreview");

  // prepare instant preview
  $("#uploadImage").change(function(){
    // fadeOut or hide preview
    p.fadeOut();

    // prepare HTML5 FileReader
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
         p.attr('src', oFREvent.target.result).fadeIn();
    };
  });

  // implement imgAreaSelect plug in (http://odyniec.net/projects/imgareaselect/)
  $('img#uploadPreview').imgAreaSelect({
    // set crop ratio (optional)
    aspectRatio: '1:1',
    onSelectEnd: setInfo
  });
});
</script>
<style>
#addgym .flt{
	float:none !important;
}
#addgym .form_sm {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #c0c0c0;
    box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
    float: left;
    height: 34px;
    padding: 0 5px;
    width: 40%;
	margin-bottom:0px;
	margin-top:0px;
}
#addgym .form_big {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #c0c0c0;
    border-radius: 3px;
    box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
    float: left;
    height: 38px;
    margin-bottom: 0px;
    margin-top: 0px;
    width: 40%;
}
#addgym  h2 {
	font-size: 18px;
	color: #605f5f;
	display: block;
	float:none;
	text-transform:uppercase;
}
#addgym .prof_sec_div {
    float: none;
    width: 75%;
	margin-top: -5px;
	
}
#addgym{
	font-family:Gothic !important;
}
#addgym .r_addmore {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #87191a;
    color: #87191a;
    display: block;
    font-size: 18px;
    margin-bottom: 18px;
    padding: 2px 22px;
	float: left;
    height: 29px;
}
#addgym .form_clr{
    border: 1px solid #87191a;
    color: #87191a;
	border-radius: 0;
}
#addgym .form_aln{
    border: 1px solid #87191a;
    color: #87191a;
	margin-top: 20px;
    width: 80%;
}
#addgym #select_arrow{
	background: url("themes/user/images/DownArrow.png") no-repeat scroll right center rgb(255, 255, 255);
    float: left;
    overflow: hidden;
    padding: 0;
    width: 24%;

}
#addgym #select_arrow1{
	background: url("themes/user/images/DownArrow.png") no-repeat scroll right top rgba(0, 0, 0, 0);
    float: left;
    overflow: hidden;
    padding: 0;
    width: 30%;
}
#select_arrow select {
    background: none repeat scroll 0 0 transparent !important;
    border: 1px solid #87191a !important;
    height: 29px !important;
    padding: 2px 4px 1px !important;
    width: 276px !important;
	color:#87191a;
	border-radius: 0;
}
#select_arrow1 select {
    background: none repeat scroll 0 0 transparent !important;
    border: 1px solid #87191a !important;
    height: 29px !important;
    padding: 2px 4px 1px !important;
    width: 150px !important;
	color:#87191a;
	border-radius: 0;
}
#addgym #location_content{
    border-bottom: 1px solid #d0d0d0;
    border-top: 1px solid #d0d0d0;
    float: left;
    width: 100%;
	margin-top: 10px;
}
#addgym .sub_red {
    background: none repeat scroll 0 0 #ffffff;
    border: 1px solid #87191a;
    color: #87191a;
    display: block;
    float: left;
    font-size: 20px;
    height: 29px;
    margin-bottom: 30px;
    padding: 0 10px;
}
#addgym .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:20px;
}
#addgym #location_content .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:0px;
}
#addgym .time{
	float: left;
    width: 12%;
}
#addgym .time .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:0px;
}
</style>

