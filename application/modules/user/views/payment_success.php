<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="clearfix"></div>
<section> <img src="<?php echo base_url()?>themes/user/images/inner-img.png" class="img-responsive" width="100%" /> </section>
<div class="clearfix"></div>
<div class="container serach_result">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
   
  <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12 righ_pnl" >

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fillter_bg">
    <span style="float: left; width: 28%; margin-left: 5px;"><p
          class="organize"><?php echo $this->lang->line("ORDER SUCCESSFUL"); ?></p></span>
    <span >
    <span class="fillter" style="border:none">
    <label for="checkbox1"><a style="color:#fff;font-family:MyriadPro-Regular;font-size:18px;"
                              href="<?php echo base_url(); ?>"><?php echo $this->lang->line("BACK TO MAIN PAGE"); ?></a></label>
    </span></span>
  </div>
   
  
  <div class="clearfix"></div><br/>
  
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="add_gym_wra" style="margin-bottom: 15px; margin-top: 0px ! important; border: 1px solid; float: left; overflow: scroll; width: 100%;">

    <div class="clear"></div>
			<div class="widget-body">
			<?php // print_r($det); echo "sdfdsfsd".$det->first_name;?>
        <?php $map_img = 'http://maps.googleapis.com/maps/api/staticmap?center='.$det->gmap_lat.','.$det->gmap_long.'&zoom=13&size=600x300&maptype=roadmap&markers=color:blue|label:S|'.$det->gmap_lat.','.$det->gmap_long;?>
<table class="ecxmaincontent" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="720">
  <tr>
    <td colspan="3" style="text-align: center; color: #003580; font-weight: bold; font-size: 14px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 18px">
    <font color="#333333">Thanks, <?php echo ucfirst($det->first_name);?>! Your membership is now confirmed.
    </font></td>
  </tr>
  <tr>
    <td width="356">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="356">&nbsp;</td>
  </tr>
  <tr>
    <td style="width: 360px; padding-bottom: 20px" valign="top">
    <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-top: 10px" valign="top">
              <font color="#333333"><b><?php echo $this->lang->line("Booking number:"); ?></b></font>
            </td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-right: 12px; padding-top: 10px" align="right" valign="top">
            <font color="#333333"><?php echo substr($det->transaction_id,3);?></font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
              <font color="#333333"><b><?php echo $this->lang->line("PIN Code:"); ?></b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333"><?php echo $det->ofr_pin;?></font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
              <font color="#333333"><b><?php echo $this->lang->line("Email:"); ?></b></font></td>
            <td id="ecxemailAddress" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333"><?php echo $det->email;?></font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px; padding-bottom: 10px" valign="top">
              <font color="#333333"><b><?php echo $this->lang->line("Booked by:"); ?></b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px; padding-bottom: 10px" align="right" valign="top">
            <font color="#333333"><?php echo ucfirst($det->first_name).' '.ucfirst($det->last_name);?> </font></td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-top: 10px" valign="top">
            <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
                  <font color="#333333"><b><?php echo $this->lang->line("Your reservation:"); ?></b></font></td>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333"><?php echo $duration.' membership subscription at '.ucfirst($det->gymname);?></font>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            <?php echo $spe_data;?>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            <?php echo $spe_ofr;?>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-bottom: 10px" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="color: #003580; font-weight: bold; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; border-left: 1px solid #003580; padding-left: 12px; background-color: #E3E3E1" valign="top">
              <font color="#333333"><?php echo $this->lang->line("Total Price"); ?></font></td>
            <td style="color: #003580; font-weight: normal; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; white-space: nowrap; border-right: 1px solid #003580; padding-right: 12px; background-color: #E3E3E1" align="right" valign="top">
            <font color="#333333">US$ <?php echo $det->amount;?>/- </font></td>
          </tr>
          <!-- <tr>
            <td colspan="2" style="color: #000; font-weight: normal; font-size: 11px; font-family: Helvetica, Arial, Sans-serif; line-height: 14px; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; background-color: #E3E3E1" valign="top">&nbsp;</td>
          </tr> -->
          <tr>
            <td colspan="2" style="color: #333; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-bottom: 1px solid #003580; padding-left: 6px; padding-right: 12px; padding-bottom: 10px; background-color: #E3E3E1">
              <p class="ecxcurrency_disclaimer_message"
                 style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0"><?php echo $this->lang->line("Details:"); ?></p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Start date: <?php echo $det->joining_date;?><br>
            End Date: <?php echo $end_date;?></p>

              <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td style>&nbsp;</td>
    <td style valign="top">
    <table style="color: #003580; padding: 10px" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" style="color: #003580; font-weight: bold; font-size: 16px; padding-bottom: 10px">
        <font color="#333333"><?php echo ucfirst($det->gymname).', '.ucfirst($det->city);?></font><br>
&nbsp;</td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
          <font color="#333333"><b><?php echo $this->lang->line("Address:"); ?></b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333"><?php echo ucfirst($det->gstreet).', '.ucfirst($det->city);?><br>
        <?php echo ucfirst($det->state).', '.$det->gzip;?></font></td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
          <font color="#333333"><b><?php echo $this->lang->line("Phone:"); ?></b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">+1 <?php echo $det->ow_phone;?></font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold" valign="top">
          <font color="#333333"><?php echo $this->lang->line("Email:"); ?></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333"><?php echo $det->ow_email;?></font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; padding-bottom: 10px" valign="top">
          <font color="#333333"><?php echo $this->lang->line("Getting there:"); ?></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; padding-bottom: 10px" valign="top">
          <font color="#333333"><?php echo $this->lang->line("Show directions"); ?></font></td>
      </tr>
      <tr>
        <td class="ecxgooglemap" colspan="2" style="height: 255px" align="center">
        <img src="<?php echo $map_img;?>" style="display: block" border="0" height="255" width="354"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td class="ecxmybooking_bar" colspan="3">
    <table style="color: #FFF; font-family: Helvetica, Arial, Sans-serif; font-size: 15px; line-height: 15px; font-weight: normal; text-align: center; background: #333333" cellpadding="5" width="100%">
      <tr>
        <td style="color: #FFFFFF">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 20px; font-size: 11px; line-height: 20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; ">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
          <span style="font-size: 8pt; font-weight: 700"><?php echo $this->lang->line("Terms"); ?>
            &amp;<?php echo $this->lang->line("Conditions"); ?></span><span style="font-weight: 400"><br>
        <span style="font-size: 8pt"><?php echo $this->lang->line("This is a non-transferable membership
        subscription. You confirm that you are at least 18 years of age.
        Please consult your local physician before joining any fitness
        regime. Neither Gymscanner nor any fitness facility will be held
        responsible for any medical complications that may arise due to
        your prior or existing health conditions."); ?><br>
        <br><?php echo $this->lang->line("Please see our terms and conditions here."); ?></span></span></font></p>
        </td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td valign="top">
    <table style="padding-left: 10px; padding-right: 10px; padding-top: 0; padding-bottom: 0" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 0px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
          <span
              style="font-size: 8pt; font-weight: 700"><?php echo $this->lang->line("Special Requests"); ?></span><span
              style="font-weight: 400"><br>
        <span style="font-size: 8pt"><?php echo $this->lang->line("None mentioned"); ?></span></span></font></p>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 0px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
          <span
              style="font-size: 8pt; font-weight: 700"><?php echo $this->lang->line("Important Information"); ?></span><span
              style="font-weight: 400"><br>
        <span
            style="font-size: 8pt"><?php echo $this->lang->line("If you have any questions, you may contact"); ?><?php echo ucfirst($det->gymname); ?><?php echo $this->lang->line("directly at"); ?>
          <br>+1 <?php echo $det->ow_phone; ?></span></span></font></p>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 0px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
          <span style="font-size: 8pt; font-weight: 700"><?php echo $this->lang->line("Need help?"); ?></span><span
              style="font-weight: 400"><br>
        <span
            style="font-size: 8pt"><?php echo $this->lang->line("Our Customer Service team is also here to help. Send us a message"); ?></span></span></font>
        </p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 10px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

</table>
      
        <div class="clear"></div>
        
        </div>
        </div>
   </div>
  </aside>    
</div>
 
  <div class="clearfix"></div>
  
</div>



