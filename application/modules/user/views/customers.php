<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">

<div class="container">
<div class="dash_bord">
<div class="top_menu_wrapp">
  <ul>
    <li><a style="padding:0px 10px 0px 0px;"><?php echo $this->lang->line("Customers List"); ?></a></li>
    <li><a href="<?php echo site_url('facility')?>">Dashboard</a></li>
  </ul>
  
    <div class="user_le">
     <div class="my_right">
       <a onclick="goBack()" class="viw_usr" style="float:right"><?php echo $this->lang->line("Back"); ?></a>
      <div class="clear"></div>
      </div> 
    </div>
  
  <div class="clear"></div>
</div>
<div class="wi_wrapp">
  <div class="name_wrapp">
    <div class="first_gy" style="width:7%"><h2><?php echo $this->lang->line("Sl No."); ?></h2></div>
    <div class="second_gy" style="width:20%"><h2><?php echo $this->lang->line("Name"); ?></h2></div>
    <div class="third_gy" style="width:20%"><h2><?php echo $this->lang->line("User Name"); ?></h2></div>
    <div class="act_gy" style="width:20%"><h2><?php echo $this->lang->line("Email"); ?></h2></div>

    <div class="clear"></div>
  </div>
  <?php if(isset($res) && is_array($res) && count($res)){ $i = 1;?>
    <?php foreach ($res as $key => $value) { ?>
  <div class="dimound_details" id="div_<?php echo $value->id;?>">
    <div class="first_gy" style="width:7%"><?php echo $i;?></p></div>
    <div class="second_gy hei_n" style="min-height:38px; width:20%"><p style="margin-top:9%;"><?php echo ucfirst($value->first_name).' '.ucfirst($value->last_name);?></p></div>
    <div class="third_gy hei_n" style="min-height:38px; width:20%" ><p style="margin-top:9%;"><?php echo $value->user_name; ?></p></div>
    <div class="user_gy hei_n" style="min-height:38px;width:20%" ><p style="margin-top:9%;"><?php echo $value->email; ?></p></div>
    
    <div class="clear "></div>
  </div>
  <?php $i++; } } ?>
</div>

</div>
</div>
