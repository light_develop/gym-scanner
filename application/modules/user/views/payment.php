<?php
error_reporting(0);
?>
<div class="clearfix"></div>
<section style="width: 100%;"> <img src="<?php echo base_url()?>themes/user/images/inner-img.png" class="img-responsive" width="100%" id="blur"/> </section>
<div class="clearfix"></div>
<div class="container serach_result">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " >   
    <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12 righ_pnl" >

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fillter_bg">
          <span class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><p
                  class="organize gymheading"><?php echo $this->lang->line("PAYMENT"); ?></p></span>
        <span class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <span class="fillter" style="border:none">
        <!-- <label for="checkbox1"><a style="color:#fff;" onclick="goback()"><?php echo $this->lang->line("Back"); ?></a></label>    -->
        </span></span>
      </div>  
      <div class="clearfix"></div><br/>
  
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">

        <?php if(count($ofr_det)>0){?>

        <div class="pay_amount"><span style="font-size: 15px; text-align: left; margin-left: 7px;"><?php echo 'You are paying for '.$ofr_det->description .' at '.$gym_name->gymname.' starting on '.$this->session->userdata['join_date'].'.</br>
        Tax is : US$ '. round($tax,2).'</br> Total: US$ '.round($total_price,2);?></span></div>
       <?php } else{?>
        <div class="pay_amount"><span style="font-size: 15px; text-align: left; margin-left: 7px;"><?php echo 'You are paying US $'.$gym_name->price .' at '.$gym_name->gymname.' starting on '.$this->session->userdata['join_date'];?></span></div>
       <?php } ?>
        <?php if($tax): ?>
           <!-- <div class="tax_amount"><span><?php /*echo $this->lang->line("Tax is : $") . $tax; */?></span></div>-->
        <?php endif; ?>
       <div class="clear"></div>
       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="payment">
       
        <div class="add_gym_wra">
        <?php if ($this->session->flashdata('error') != "") { ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">X</button>
                <font
                    color="red"><strong><?php echo $this->lang->line("Error!"); ?></strong> <?php echo $this->session->flashdata('error'); ?>
                </font>
            </div>
        <?php } ?>
          <form action="<?php echo USER_URL.'asoffers/payment/'.$total_price?>" method="POST" id="example-form">
            <span class="payment-errors"></span>

            <!-- <form action="/" method="post" id="example-form" style="display: none;"> -->
            
            <div class="form-row">
                <label for="name" class="stripeLabel"><?php echo $this->lang->line("Name on Card"); ?></label>
                <input type="text" name="name" class="required" />
            </div>            
    
            <div class="form-row">
                <label for="email"><?php echo $this->lang->line("Email Address"); ?></label>
                <input type="text" name="email" class="required" />
            </div>            
    
            <div class="form-row">
                <label><?php echo $this->lang->line("Card Number"); ?></label>
                <input type="text" maxlength="20" autocomplete="off" class="card-number stripe-sensitive required" />
            </div>
            
            <div class="form-row">
                <label><?php echo $this->lang->line("CVC"); ?></label>
                <input type="text" maxlength="4" autocomplete="off" class="card-cvc stripe-sensitive required" />
            </div>
            
            <div class="form-row"  >
                <label style="float:left"><?php echo $this->lang->line("Expiration"); ?></label>
                <div class="expiry-wrapper"  style="float:left; margin-left: 5px;">
                    <select class="card-expiry-month stripe-sensitive required"></select>
                    <script type="text/javascript">
                        var select = $(".card-expiry-month"),
                            month = new Date().getMonth() + 1;
                        for (var i = 1; i <= 12; i++) {
                            select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
                        }
                    </script>
                    <span><?php echo $this->lang->line("/"); ?></span>
                    <select class="card-expiry-year stripe-sensitive required"></select>
                    <script type="text/javascript">
                        var select = $(".card-expiry-year"),
                            year = new Date().getFullYear();
 
                        for (var i = 0; i < 12; i++) {
                            select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
                        }
                    </script>
                </div>
            </div>
            <div style="clear:both"></div>
            <label>&nbsp;</label>
			<div class="paybtns">
                <div style="float:left;">
                    <!--<button type="submit" class="btn btn-default" style="border-radius:5px;float:left;"
                            name="submit-button"><?php /*echo $this->lang->line("CHECKOUT"); */?></button>
                    <img src="<?php /*echo base_url(); */?>themes/user/images/cart-icon.jpg" align="cart-icon"/>-->
                    <button type="submit" name="submit-button" class="btn btn-danger"><?php echo $this->lang->line("CHECKOUT"); ?>&emsp;<span class="glyphicon glyphicon-shopping-cart"></button>
                </div>
                <div><a style="color:inherit;text-decoration:none;"
                        href="<?php echo site_url('view_offers/' . $this->session->userdata['gym_id']) ?>">
                        <!--<button type="button" style="border-radius:5px; margin: 0px 0 10px 5px;float:left;"
                                class="btn btn-default"><?php /*echo $this->lang->line("GO BACK"); */?></button>-->
                        <button type="button" class="btn btn-danger"><?php echo $this->lang->line("GO BACK"); ?>&emsp;<span class="glyphicon glyphicon-circle-arrow-left"></span></button>
                       <!-- <img src="<?php /*echo base_url(); */?>themes/user/images/back-btn-icon.jpg" align="goback-icon"/>-->
				</a></div>
			</div>
          </form>
        </div>
      </div>
      
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 card_div"  >
      <div  style="width: 100%; float: left;">
	  	<img src="<?php echo base_url();?>themes/user/images/Powered-btn.png" alt="Powered-btn" style="width: 140px;"/>
		<img src="<?php echo base_url();?>themes/user/images/master-card.jpg" alt="master-card" />
		<img src="<?php echo base_url();?>themes/user/images/visa-card.jpg" alt="visa-card" />
		<img src="<?php echo base_url();?>themes/user/images/AE-card.jpg" alt="AE-card" />
		<img src="<?php echo base_url();?>themes/user/images/discover-card.jpg" alt="discover-card" />
		<img src="<?php echo base_url();?>themes/user/images/Jcb-card.jpg" alt="Jcb-card" />
		<img src="<?php echo base_url();?>themes/user/images/secure-lock-icon.jpg" alt="secure-lock-icon" style="width: 70px;"/>
		
	  </div>
      
      </div>
    </aside>
  </div>  
</div>
 
<div class="clearfix"></div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript">
  Stripe.setPublishableKey('pk_test_nGGZ7SLPUISwLsJw9gWjd2UJ');
    $(document).ready(function() {
        function addInputNames() {
            // Not ideal, but jQuery's validate plugin requires fields to have names
            // so we add them at the last possible minute, in case any javascript 
            // exceptions have caused other parts of the script to fail.
            $(".card-number").attr("name", "card-number")
            $(".card-cvc").attr("name", "card-cvc")
            $(".card-expiry-year").attr("name", "card-expiry-year")
        }

        function removeInputNames() {
            $(".card-number").removeAttr("name")
            $(".card-cvc").removeAttr("name")
            $(".card-expiry-year").removeAttr("name")
        }

        function submit(form) {
            // remove the input field names for security
            // we do this *before* anything else which might throw an exception
            removeInputNames(); // THIS IS IMPORTANT!

            // given a valid form, submit the payment details to stripe
            $(form['submit-button']).attr("disabled", "disabled")

            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(), 
                exp_year: $('.card-expiry-year').val()
            }, function(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    $(form['submit-button']).removeAttr("disabled")

                    // show the error
                    $(".payment-errors").html(response.error.message);

                    // we add these names back in so we can revalidate properly
                    addInputNames();
                } else {
                    // token contains id, last4, and card type
                    var token = response['id'];

                    // insert the stripe token
                    var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' />");
                    form.appendChild(input[0])

                    // and submit
                    form.submit();
                }
            });
            
            return false;
        }
        
        // add custom rules for credit card validating
        jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
        jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
        jQuery.validator.addMethod("cardExpiry", function() {
            return Stripe.validateExpiry($(".card-expiry-month").val(), 
                                         $(".card-expiry-year").val())
        }, "Please enter a valid expiration");

        // We use the jQuery validate plugin to validate required params on submit
        $("#example-form").validate({
            submitHandler: submit,
            rules: {
                "card-cvc" : {
                    cardCVC: true,
                    required: true
                },
                "card-number" : {
                    cardNumber: true,
                    required: true
                },
                "card-expiry-year" : "cardExpiry" // we don't validate month separately
            }
        });

        // adding the input field names is the last step, in case an earlier step errors                
        addInputNames();
    });
</script>
<style>
.paybtns{
	float: right;
    margin-top: 10px;
    width: 62%;
}
.pay_amount{
	background-color: #ededeb; 
	width: 57%; 
	height: 115px; 
	border-radius: 5px; 
	float: right;
	font-weight: bold;
}
#payment .btn {
display: inline-block;
padding: 0px 10px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
}

@media screen and (max-width: 1200px){
	.paybtns{
		float: left;
		margin-top: 10px;
		width: 80%;
	}
}
@media screen and (max-width: 995px){
	.paybtns {
		float: left;
		margin-top: 10px;
		width: 100%;
	}
}
@media screen and (max-width: 750px){
	.card_div {
		margin-top: 10px;
		padding: 0;
		width: 100% !important;
	}
	.pay_amount{
		background-color: #ededeb;
		border-radius: 5px;
		float: left;
		font-weight: bold;
		height: 115px;
		margin-left: 10px;
		width: 100%;
	}
}
@media screen and (max-width: 675px){
	#payment {
		font-family: gothic !important;
		margin-top: 10px;
		width: 75%;
	}
}
@media screen and (max-width: 425px){
	#payment {
		font-family: gothic !important;
		margin-top: 10px;
		width: 100%;
	}
}
#payment .alert {
    border: 1px solid #d0d0d0;
    border-radius: 4px;
    margin-bottom: 20px;
    padding: 15px;
}
</style>


