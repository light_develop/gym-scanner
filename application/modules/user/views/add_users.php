<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<div class="container">
	<div class="add_gym_wra" id="adduser">
		<h1><? echo $this->lang->line('Add User Details') ?><a onclick="goBack()"
															   style=" float:right;margin:5px 5px 0 0; color:#ffffff;"><? echo $this->lang->line('Back') ?></a>
		</h1>

	  	<form action="<?php if(isset($details)) echo site_url('edit_users'); else echo site_url('add_users');?>" class="form-horizontal" method="post" id="user_form" >
		<div class="col-lg-12 ad_form_wr">	
			<?php if (validation_errors() != "") { ?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
					<strong><? echo $this->lang->line('Errors Found!') ?></strong> <?php echo validation_errors(); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error') != "") { ?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
					<strong><? echo $this->lang->line('Success!') ?></strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
			<?php } ?>
			<h2><? echo $this->lang->line('FIRST NAME') ?></h2>
		  	<input type="hidden" name="uid" value="<?php if(isset($details)) echo $details->id;?>"/>
		   	<input name="first_name" class="form_big" type="text" required value="<?php if(isset($details)) echo $details->first_name; else echo $this->input->post('first_name');?>" />
		  	<div class="clear"></div>

			<h2><? echo $this->lang->line('LAST NAME') ?></h2>
		  	<input name="last_name" class="form_big" type="text" required value="<?php if(isset($details)) echo $details->last_name; else echo $this->input->post('last_name');?>" />
		   	<div class="clear"></div>

			<h2><? echo $this->lang->line('USER NAME') ?></h2>
		  	<input name="user_name" id="uname" class="form_big" type="text" required value="<?php if(isset($details)) echo $details->user_name; else echo $this->input->post('user_name');?>" />
		   	<div class="clear"></div>

			<h2><? echo $this->lang->line('PASSWORD') ?></h2>
		  	<input name="password" class="form_big" type="password" required value="<?php if(isset($details)) echo $details->password; else echo $this->input->post('password');?>" />
		   	<div class="clear"></div>

			<h2><? echo $this->lang->line('EMAIL') ?></h2>
		  	<input name="email" id="email" class="form_big" type="text" required value="<?php if(isset($details)) echo $details->email; else echo $this->input->post('email');?>" />
		   	<span id="email_error"></span>
		   	<div class="clear"></div>
		   	<input type="hidden" name="old_name" value="<?php if(isset($details)) echo $details->user_name;?>">

			<div class="submitdiv"><input name="submit" class="sub_green" value="<? echo $this->lang->line('SUBMIT') ?>"
										  type="submit"><img alt="right-icon"
															 src="<?php echo base_url(); ?>themes/user/images/right-icon.jpg"
															 style="width: 30px; height: 30px;"></div>
		</div>
		</form>
		<div class="clear"></div>
	</div>
</div>

<style>
@media screen and (max-width: 1200px){
	#adduser .submitdiv{
		 margin-bottom: 20px;
	}
}
@media screen and (max-width: 550px){
	#adduser .form_big {
		background: none repeat scroll 0 0 #fff;
		border: 1px solid #c0c0c0;
		border-radius: 3px;
		box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
		float: left;
		height: 38px;
		margin-bottom: 15px;
		margin-top: 5px;
		width: 100%;
	}
}

</style>
