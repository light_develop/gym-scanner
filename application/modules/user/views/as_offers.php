<link href="<?php echo base_url() ?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<style>
    .initiation_fees {
        border: 1px solid #d0d0d0;
        box-shadow: 2px 2px 6px 2px #d0d0d0;
        color: #87191a;
        font-size: 17px;
        margin-bottom: 10px;
        margin-top: 10px;
        padding: 10px;
        text-align: center;
    }

    #view-ofr .viewoffer {
        overflow: scroll;
        padding: 3px 15px 15px;
    }

    #st_btn > img {
        margin-top: 10px;
        cursor: pointer;
        width: 26px;
        /* background-color: #87191a; */
        /* background: #87191a; */
        border: 1px solid #87191a;
    }
    button#close_button_img {
        position: absolute;
        left: 97%;
        top: -15%;
        background-color: transparent;
    }
    .close:hover, .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
        filter: alpha(opacity=50);
        opacity: .8;
    }
    .close {
        float: right;
        font-size: 21px;
        font-weight: bold;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        filter: alpha(opacity=20);
        opacity: .5;
    }

    .btn.btn-danger {
        color: #ffffff;
        background-color: #D9534F;
        background-image: linear-gradient(to bottom, #D9534F, #C12E2A);
        border-color: #B92C28 #B92C28 #B92C28;
    }
    .btn.btn-danger:hover {
        color: #ffffff;
        background-color: #C12E2A;
        background-image: linear-gradient(to bottom, #C12E2A, #C12E2A);
        border-color: #B92C28 #B92C28 #B92C28;
    }
</style>
<div class="container">

    <?php if (isset($mode) && $mode == 'all'): ?>
        <br/><br/>
        <div class="add_gym_wra" id="asofr">
            <h1> All Offers |<a href="<?php echo site_url('facility'); ?>" style="color:#fff;">Dashboard</a></h1>

            <div class="widget-body scrool_mo">
                <?php if ($this->session->flashdata('success') != '') { ?>
                    <div><h4 style="color:green;"><?php echo $this->session->flashdata('success'); ?></h4></div>
                <?php } ?>

                <?php if ($this->session->flashdata('delete') != '') { ?>
                    <div><h4 style="color:red;"><?php echo $this->session->flashdata('delete'); ?></h4></div>
                <?php } ?>
                <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                    <tr>
                        <th><? echo $this->lang->line('Facility Name') ?></th>
                        <th><? echo $this->lang->line('Details') ?></th>
                        <th class="hidden-phone"><? echo $this->lang->line('Actions') ?> </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if (isset($offers) && count($offers)) {
                        $i = 1; ?>

                        <?php foreach ($offers as $row) { ?>
                            <tr class="odd gradeX" id="ofr_<?php echo $row->id; ?>">
                                <td><?php echo $row->gymname; ?></td>
                                <?php $res = $this->db->where('offer_relation_id', $row->id)->get('offer_description')->result(); ?>
                                <td>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th><? echo $this->lang->line('Duration') ?> </th>
                                            <th><? echo $this->lang->line('Membership') ?> </th>
                                            <th><? echo $this->lang->line('Discount') ?> </th>
                                            <th><? echo $this->lang->line('Offer') ?> </th>
                                            <th><? echo $this->lang->line('Description') ?> </th>
                                            <th class="hidden-phone"><? echo $this->lang->line('Status') ?> </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($res as $key => $value) { ?>
                                            <tr id="div_<?php echo $value->id; ?>">
                                                <input type="hidden" id="ofr_id" name="ofr_id"
                                                       value="<?= $value->id; ?>">
                                                <input type="hidden" id="gym_id" name="gym_id"
                                                       value="<?= $row->gym_id; ?>">
                                                <input type="hidden" id="reference" name="reference"
                                                       value="<?= $value->reference; ?>">
                                                <td class="usr_ofr_td"><?php echo $value->duration; ?></td>
                                                <td class="usr_ofr_td"><?php echo $value->membership; ?></td>
                                                <td class="usr_ofr_td"><?php echo $value->discount; ?></td>
                                                <td class="usr_ofr_td"><?php echo $value->offer; ?></td>
                                                <td><?= $value->description ?></td>
                                                <td row-status="<?= $value->status; ?>">

                                                    <!--button view start-->
                                                    <?php if ($value->status == 0) { ?>
                                                        <a title="View facility offer" style="border:none; pointer-events: none;"
                                                           href="#openModal" data-toggle="modal"
                                                           data-target=".bs-example-modal-sm-offer">
                                                            <button class="view_p_rocket" ></button>
                                                        </a>
                                                    <?php } else if ($value->status == 1) { ?>
                                                        <a title="View facility offer" style="border:none; pointer-events: none;"
                                                           href="#openModal" data-toggle="modal"
                                                           data-target=".bs-example-modal-sm-offer">
                                                            <button class="view_p_rocket" ></button>
                                                        </a>
                                                    <?php } else if ($value->status == 2) { ?>
                                                        <a title="View facility offer" style="border:none; pointer-events: none;"
                                                           href="#openModal" data-toggle="modal"
                                                           data-target=".bs-example-modal-sm-offer">
                                                            <button class="view_p_rocket" ></button>
                                                        </a>
                                                    <?php } else if ($value->status == 3) { ?>
                                                        <a title="View facility offer" style="border:none;"
                                                           href="#openModal" data-toggle="modal"
                                                           data-target=".bs-example-modal-sm-offer">
                                                            <button class="view_p_rocket" ></button>
                                                        </a>
                                                    <?php } ?>

                                                    <!--button view-->
                                                    <!-- <a href="<?php echo site_url('view_offer/' . $value->id) ?>"></a> -->
                                                    <?php if ($value->status == 0) { ?>
                                                        <a title="<? echo $this->lang->line('The offer needs to be reviewed by Gymscanner.com and is pending approval.') ?>"
                                                           id="<?php echo $value->id; ?>"

                                                           class="pending_p" ></a>
                                                    <?php } else if ($value->status == 1) { ?>
                                                        <a title="<? echo $this->lang->line('Offer is active and can accept customers.') ?>"
                                                           id="<?php echo $value->id; ?>"
                                                           onclick="status_change(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>')"
                                                           class="active_p"></a>
                                                    <?php } else if ($value->status == 2) { ?>
                                                        <a title="<? echo $this->lang->line('The offer was rejected as it did not fall under Gymscannner.com terms and conditions. Please contact Gymscanner.com to resolve this.') ?>"
                                                           class="dislike_red_p"></a>
                                                    <?php } else if ($value->status == 3) { ?>
                                                        <a title="<? echo $this->lang->line('The offer is still in edit mode. Once you are ready, make it live. To make it live, click on the icon on the left.') ?>"
                                                           class="sas_p"></a>
                                                    <?php } ?>
                                                    <button title="<? echo $this->lang->line('Delete Offer') ?>"
                                                            class="delete_p"
                                                            onclick="del_row(<?php echo $value->id; ?>)"></button>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="hidden-phone">
                                    <a title="<? echo $this->lang->line('Edit facility offer') ?>"
                                       href="<?php echo site_url('edit_offer/' . $row->id); ?>">
                                        <button class="edit_p"></button>
                                    </a>
                                    <button title="<? echo $this->lang->line('Delete offer') ?>" class="delete_p"
                                            onclick="del_offr(<?php echo $row->id; ?>)"></button>
                                </td>
                            </tr>
                            <?php $i++;
                        }
                    } ?>
                    </tbody>
                </table>

            </div>
        </div>
    <?php endif; ?>

</div>

<!-- sign in form -->
<div class="modal fade bs-example-modal-sm-offer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:80%;font-family:gothic;">
        <div id="viewofr">
            <div class="modal-content" id="view-ofr">

                <div class="ofr_wraper viewoffer">
                    <form role="form">
                        <div class="form-group">
                            <div id="ofr_msg"></div>
                        </div>
                        <table class="table table-striped table-bordered" style="display:none;">
                            <tr id="rowdatadynamic"></tr>
                        </table>
                        <table style="width:100%">
                            <tr class="row_cal" align="center">
                                <td>
                                    <div class="top_clr_he boxs_mn top_clr_he">
                                        <h2 style="color:#FFFFFF;"><? echo $this->lang->line('Duration') ?></h2>

                                        <p><? echo $this->lang->line('The length of the membership of your customers') ?></p>
                                    </div>
                                </td>
                                <td>
                                    <div class="boxs_mn top_clr_he1">
                                        <h2 style="color:#FFFFFF;"><? echo $this->lang->line('Rate') ?></h2>

                                        <p><? echo $this->lang->line('Membership fees') ?></p>
                                    </div>
                                </td>
                                <td>
                                    <div class="boxs_mn top_clr_he2">
                                        <h2 style="color:#FFFFFF;"><? echo $this->lang->line('Cash Discount') ?></h2>

                                        <p><? echo $this->lang->line('Offer a cash discount') ?></p>
                                    </div>
                                </td>
                                <td>
                                    <div class="boxs_mn top_clr_he3">
                                        <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Exclusive Perk"); ?></h2>

                                        <p><?php echo $this->lang->line("Any freebies you want to offer"); ?></p>
                                    </div>
                                </td>
                                <td>
                                    <div class="boxs_mn top_clr_he4">
                                        <h2 style="color:#FFFFFF;"><?php echo $this->lang->line("Customers Get"); ?></h2>

                                        <p><?php echo $this->lang->line("This is what your customers get today"); ?></p>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr id="userdatadynamic" class="row_cal" style="font-weight: bold;"></tr>
                        </table>
                        <div id="st_btn"></div>
                        <div>
                            <button id="close_button_img" type="button" class="close"  data-dismiss="modal" ><img src="<?php echo base_url(); ?>themes/user/images/close_button.png"></button>
                        </div>
                        <div class="clear"></div>

                    </form>
                </div>


                <!--      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="mySmallModalLabel">Review Offer</h4>
                      </div>
                      <div>
                        <form role="form">
                          <div class="form-group">
                            <div id="ofr_msg"></div>
                          </div>
                          <table class="table table-striped table-bordered">
                          <tr id="rowdatadynamic"></tr>
                          </table>
                          <table class="table table-striped table-bordered">
                          <h2>User View</h2>
                          <thead>
                            <th>Duaration</th>
                            <th>Membership</th>
                            <th>Discount</th>
                            <th>Offer</th>
                            <th>User get</th>
                            <th></th>
                          </thead>
                          <tr id="userdatadynamic"></tr>
                          </table>
                          <div id="st_btn">

                          </div>
                        </form>
                      </div>-->

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>themes/user/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
    $('.view_p_rocket').click(function () {
        var row = $(this).closest('tr').html();
        var art = $(this).closest('tr').find('td:nth-child(9)').attr('row-status');
        //console.log(row);
        if (art == '3')
            $('#st_btn').html('<button type="button" id="review_ofr" class="btn btn-danger btn-lg">Make my offer live <span class="glyphicon glyphicon-send"></span></button>');
        $('#rowdatadynamic').html(row);
        $('#rowdatadynamic').find('td:last-child').remove();
        $('#userdatadynamic').html(row);
        $('#userdatadynamic').find('td:last-child').html('<input type="radio" style="margin-left: 8px;"/>');
        $('#userdatadynamic').find('td:last-child').css("width", "3%");
    });
    $(document).on('click', '#review_ofr', function () {
        var ofr_id = $('#rowdatadynamic #ofr_id').val();
        var reference = $('#rowdatadynamic #reference').val();
        var gym_id = $('#rowdatadynamic #gym_id').val();
        console.log(ofr_id);
        //console.log(reference);
        //console.log(gym_id);
        $.ajax({
            type: "POST",
            url: "<?php echo USER_URL;?>asoffers/review_ofr",
            data: {
                'ofr_id': ofr_id,
                'reference': reference,
                'gym_id': gym_id,
                'hide_param': 1
            },
            success: function (data) {
                //console.log(data);
                //var url = data123.indexOf('http');
                if (data == 1) {
                    alert('Offer Pending Approval');
                    $('.close').trigger('click');
                    $(".close").click();
                    location.reload();
                }
                else
                    $('#ofr_msg').text(data).css('color', 'red');
            },
            error: function () {
                $('#ofr_msg').text("We are unable to do your request. Please contact webadmin").css('color', 'red');
            }
        });

        return false;
    });
    function status_change(id, status, aid) {
        $.ajax({
            type: "POST",
            url: "<?php echo USER_URL;?>asoffers/change_status",
            data: {
                'id': id,
                'status': status
            },
            success: function (data) {
                if (status == '1') {
                    $('#' + aid).removeClass('active_p').addClass('pending_p');
                    $('#' + aid).attr('onclick', 'status_change(' + id + ',0,"' + aid + '")');
                    $('#' + aid).attr('title', 'Pending');
                } else if (status == '0') {
                    $('#' + aid).removeClass('pending_p').addClass('active_p');
                    $('#' + aid).attr('onclick', 'status_change(' + id + ',1,"' + aid + '")');
                    $('#' + aid).attr('title', 'Active');
                }
            },
            error: function () {
                alert("We are unable to do your request. Please contact webadmin");
            }
        });
    }
</script>
<script>
    function del_row(selected) {
        $.ajax({
            type: "POST",
            url: "<?php echo USER_URL?>asoffers/del_row",
            data: {
                'id': selected
            },
            success: function (data) {
                if (data == 1) {
                    alert('sorry, Users allocated to this offer');
                } else {
                    $('#div_' + selected).hide();
                }
            },
            error: function () {
                alert("We are unable to do your request. Please contact webadmin");
            }
        });
    }
</script>
<script>
    function del_offr(selected) {
        $.ajax({
            type: "POST",
            url: "<?php echo USER_URL?>asoffers/del_offr",
            data: {
                'id': selected
            },
            success: function (data) {
                if (data == 1) {
                    alert('sorry, Users allocated to this offer');
                } else {
                    $('#ofr_' + selected).hide();
                }
            },
            error: function () {
                alert("We are unable to do your request. Please contact webadmin");
            }
        });
    }
</script>
