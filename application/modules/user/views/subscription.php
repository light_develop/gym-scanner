<?php $this->load->view('includes/header'); ?>
<script src="<?=base_url();?>themes/user/js/jquery.bxslider.min.js"> </script>
<link href="<?=base_url();?>themes/user/css/jquery.bxslider.css" rel="stylesheet" type="text/css" media="all" />
<script>

$(document).ready(function(){
  $('.slider4').bxSlider({
    slideWidth: 300,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10,
	pager:0
  });
});
</script>

<div style="clear:both"></div>

<div class="content_wrap">

<div class="right_kuwait">
  <div class="gym_centers">
    
  <div class="gym_details" >
    <div><b class="cor"><?php echo $this->lang->line("Qortuba Fitness Center"); ?></b>
<div class="star"><img src="<?php echo base_url();?>themes/user/images/star-on.png"/>
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" />
<img src="<?php echo base_url();?>themes/user/images/star-on.png" /></div>
</div>
<div class="height5"></div>
<div>
  <p><?php echo $this->lang->line("L-RIGGAE, AL FARWANIYAH,"); ?></p>

  <p><?php echo $this->lang->line("GOVERNORATE,"); ?></p>

  <p><?php echo $this->lang->line("KUWAIT"); ?></p>
<div class="height5"></div>
<div class="height5"></div>
 </div>
 <div>

   <div class="map_location">
     <div class="text"><?php echo $this->lang->line("Map"); ?></div>
   </div>


   <div class="black_icon"></div>
  <div class="pink_icon"></div>
 </div>
</div>
<div class="gym_image"><img src="<?php echo base_url();?>themes/user/images/image.png" width="117" height="117"  class="b_radius"  /></div>

</div>


<div class="subdetails">
<table width="100%" border="0" style="font-family:Calibri;" cellspacing="0" cellpadding="0">
  <tr class="trhd">
    <td width="25%" class="brd"><?php echo $this->lang->line("Durattion"); ?></td>
    <td width="25%" class="brd"><?php echo $this->lang->line("Price"); ?></td>
    <td width="25%" class="brd"><?php echo $this->lang->line("Exlusive Offer"); ?></td>
    <td width="25%" class="brd">&nbsp;</td>
  </tr>
  <tr>
    <td class="brd" style="border-left:1px solid #dbdbdb;"><?php echo $this->lang->line("1 Month"); ?></td>
    <td class="brd"><?php echo $this->lang->line("US $ 1000"); ?></td>
    <td class="brd"><?php echo $this->lang->line("20%"); ?></td>
    <td class="brd"><input type="checkbox"/></td>
  </tr>
  <tr>
    <td class="brd" style="border-left:1px solid #dbdbdb;"><?php echo $this->lang->line("3 Month"); ?></td>
    <td class="brd"><?php echo $this->lang->line("US $ 1000"); ?></td>
    <td class="brd"><?php echo $this->lang->line("20%"); ?></td>
    <td class="brd"><input type="checkbox"/></td>
  </tr>
  <tr>
    <td class="brd" style="border-left:1px solid #dbdbdb;"><?php echo $this->lang->line("6 Month"); ?></td>
    <td class="brd"><?php echo $this->lang->line("US $ 1000"); ?></td>
    <td class="brd"><?php echo $this->lang->line("20%"); ?></td>
    <td class="brd"><input type="checkbox"/></td>
  </tr>
  <tr>
    <td class="brd" style="border-left:1px solid #dbdbdb;"><?php echo $this->lang->line("12 Month"); ?></td>
    <td class="brd"><?php echo $this->lang->line("US $ 1000"); ?></td>
    <td class="brd"><?php echo $this->lang->line("20%"); ?></td>
    <td class="brd"><input type="checkbox"/></td>
  </tr>
</table>
<div style="height:20px">&nbsp;</div>
  <button class="search_button"><?php echo $this->lang->line("Renew Subscription"); ?></button>
<div style="height:20px">&nbsp;</div>

</div>
<div class="slider">
  <div class="rit_heading"><?php echo $this->lang->line("gallery"); ?></div>
<div style="height:20px">&nbsp;</div>


</div>

</div>

<div class="left_search">
  <h2><?php echo $this->lang->line("Search"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
  <div class="fnt"><?php echo $this->lang->line("I am  Looking"); ?></div>
<div><select class="ddl_select">
    <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>

<div class="height5"></div>
  <div class="fnt"><?php echo $this->lang->line("Country"); ?></div>
<div><select class="ddl_select">
    <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>

<div class="height5"></div>
  <div class="fnt"><?php echo $this->lang->line("City"); ?></div>
<div><select class="ddl_select">
    <option><?php echo $this->lang->line("----------Select----------"); ?></option>
</select></div>


<br/>

  <div><input name="name" type="button" value="<?php echo $this->lang->line("Search"); ?>" class="search_button"/></div>
<div class="height5"></div>
</div>

<br/>

<div class="left_search">
  <h2><?php echo $this->lang->line("Filter"); ?></h2>
<div class="height5"></div>
<div class="height5"></div>
  <div class="fnt"><input name="name" type="checkbox"
                          value="value"/>&nbsp;&nbsp;<?php echo $this->lang->line("Price"); ?></div>
  <div class="fnt"><input name="name" type="checkbox"
                          value="value"/>&nbsp;&nbsp;<?php echo $this->lang->line("By Name"); ?></div>
  <div class="fnt"><input name="name" type="checkbox"
                          value="value"/>&nbsp;&nbsp;<?php echo $this->lang->line("Start Rating"); ?></div>
</div>



<div style="clear:both;"></div>
</div>

</div>
<div class="clr"></div>
<?php $this->load->view('includes/footer'); ?>

</body>
</html>