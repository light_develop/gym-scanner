<link href="<?php echo base_url()?>themes/user/css/style.css" rel="stylesheet" type="text/css">
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/jquery-gmaps-latlon-picker.css"/>
<script src="<?php echo base_url();?>themes/admin/js/jquery-gmaps-latlon-picker.js"></script>
<div class="container">
	<div class="add_gym_wra" id="addgym">
		<h1><?php if (isset($details)) echo "Edit Facility"; else echo "Add Facility"; ?> |<a
				href="<?php echo site_url('facility') ?>"><? echo $this->lang->line('Dashboard') ?></a>|<a
				onclick="goBack()"><? echo $this->lang->line('Back') ?></a></h1>
<?php 	$edit = 0;
	if(isset($details)) {
		$edit = 1;
	}?>
		<span id="ytlInfo"></span>
	  	<form action="<?php if(isset($details)) echo site_url('edit_gym'); else echo site_url('add_gym');?>" class="form-horizontal" method="post" id="gym_det_form" enctype = "multipart/form-data" name="gymdeta">
		<div class="col-lg-12 ad_form_wr">
			<div class="prof_frm">
				<div class="flt"><h2 class="flt"><? echo $this->lang->line('Facility Name') ?></h2></div>
           		<div class="prof_sec_div">
		  			<input type="hidden" name="id" value="<?php if(isset($details)) echo $details->id;?>"/>
		   			<input name="gymname" class="form_sm" required type="text" value="<?php if(isset($details)) echo $details->gymname;?>" />
		  		</div>
			</div>
            <div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Logo') ?></h2></div>
                <div class="prof_sec_div">
                <input name="logo" class="form_sm" type="file" <?php if(!$edit) { ?> <?php } ?>>
                <div class="clear"></div> 
                <?php if(isset($details)){
                    if($details->gym_logo != '')
                        $url = base_url().'uploads/gym/logos/'.$details->gym_logo;
                    else
                        $url = base_url().'no-image.png';?>
                    <div class="map_gym logo_img"><img src="<?php echo $url;?>" style="height:85px;"/></div>
                    <input type="hidden" name="old_logo" value="<?php echo $details->gym_logo;?>">
                    <?php }?>
                </div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Images') ?></h2></div>
				<div class="prof_sec_div">
					<?php if(isset($images)){
						foreach ($images as $key => $value) { 
			              $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
			            	<span style="position:relative" id="img_<?php echo $value->id;?>">
			                <img src="<?php echo $image;?>" width="100px" height="85px"> &nbsp;&nbsp;
			                <i class="icon-remove" name="<?php echo $value->id?>" id="<?php echo $value->gym_images?>" style="position:absolute;margin-top:-10px;margin-left:-10px;color:red;">X</i></span>                      
			            <?php } }else{?> 
						<div id="divname">
						  <div id="light" class="white_content">
							  <a href="javascript:void(0)"
								 onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';$('.imgareaselect-selection').css('display', 'none');$('.imgareaselect-outer').css('display', 'none');"
								 class="crop_btn"><? echo $this->lang->line('crop') ?></a>
						    <div id="crop_img"><img id="uploadPreview" style="display:none;max-width:800px;"/></div>
						  </div>
						  <input name="upload_file[]" class="form_sm" id="uploadImage" type="file" style="float: none;" required>
						  <input type="hidden" id="x" name="x[]"/>
						  <input type="hidden" id="y" name="y[]"/>
						  <input type="hidden" id="w" name="w[]"/>
						  <input type="hidden" id="h" name="h[]"/>
						  <div id="fade" class="black_overlay"></div>
						</div>
						<label id="img_error"></label>
						<p style="padding-left:20px;color:#87191a;margin-top: 0;">
							<? echo $this->lang->line('All images will be cropped to maximum size of 750px X 500px') ?>
						</p>
						<div style=" width:100px;  margin-left:345px; " id="uploadfile1_image"></div>
							  
					<?php } ?>
					<div style="clear:both"></div>   
					<!--<h2>&nbsp;</h2> -->
					<div class="addbtn"><input id="add_images" class="r_addmore" value="ADD MORE" type="button">
					<img src="<?php echo base_url();?>themes/user/images/plus.png"  /></div>
					<div class="clear"></div>
				   
				   	<div id="add_more"></div>
				   	<div class="clear"></div>
				</div>
		   	</div>
		   	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Video') ?></h2></div>
			   	<div class="prof_sec_div">
			   		<?php if(isset($details)){
			   			$videos = $details->gym_video;
			   			$res = explode('~',$details->gym_video);
				  		foreach($res as $value){ ?>		                        
	                        <input name="gym_video[]" class="form_sm youtube" type="text" value="<?php echo $value;?>" />
	                        <div style="clear:both"></div>
						<?php }
					} else { ?>
						<input name="gym_video[]" id="youtube" class="form_sm youtube" type="text"
							   placeholder="<? echo $this->lang->line('ENTER YOUTUBE / VIMEO LINK') ?>"
							   onkeyup="check_url(this.value)"/> &nbsp;&nbsp;
			    	<!-- <span id="ytlInfo"></span> -->
			    	<?php } ?>
				   	<div class="clear"></div>

				   	<!--<h2>&nbsp;</h2> -->
					<div class="addbtn"><input id="add_urls" class="r_addmore" value="ADD MORE" type="button">
					<img src="<?php echo base_url();?>themes/user/images/plus.png"  /></div>
					<div class="clear"></div>
				</div>
			</div>

		    <div id="add_videos"></div>

		    <div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Availability') ?></h2></div>
		        <div class="prof_sec_div">
					<div><input type="checkbox" name="availablity" id="check_availablity" value="1">
						&nbsp;<? echo $this->lang->line('We are available 24 x 7') ?></div>
					<br/>

					<div id="open_close_time"><? echo $this->lang->line('OR') ?><br/> <br/>
						<? echo $this->lang->line('We are open from') ?> &nbsp;
				    <div class="openhrs">
					  <?php if(isset($details)) $open  = explode(",",$details->open_time);?>
					  <?php if(isset($details)) $close = explode(",",$details->close_time);?>
					  <div class="time"><input type="text" class="time start starttime" name="open_time" value="<?php if(isset($open[0])) echo $open[0];?>" ></div>	
					  <select name="before" style="float: left;height:29px;width: 70px;">
						  <option value=""><? echo $this->lang->line('select') ?></option>
						  <option
							  value="am" <?php if (isset($open[1]) && $open[1] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($open[1]) && $open[1] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>		  					  
					  <span style="float:left;">&nbsp; to &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time" value="<?php if(isset($close[0])) echo $close[0];?>" /></div>
					  <select name="after" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
						  <option
							  value="am" <?php if (isset($close[1]) && $close[1] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($close[1]) && $close[1] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>	
					  <input id="add_urls" class="r_addmore weekdays" value="SATURDAY" type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time1" value="<?php if(isset($open[2])) echo $open[2];?>" />	</div>
					  <select name="before1" style="float: left;height:29px;width: 70px;">
						  <option value=""><? echo $this->lang->line('select') ?></option>
						  <option
							  value="am" <?php if (isset($open[3]) && $open[3] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($open[3]) && $open[3] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>
						<span style="float:left;">&nbsp;&nbsp;<? echo $this->lang->line('to') ?>  &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time1" value="<?php if(isset($close[2])) echo $close[2];?>" /></div>
					  <select name="after1" style="float: left;height:29px;width: 70px;">
						  <option value=""><? echo $this->lang->line('select') ?> </option>
						  <option
							  value="am" <?php if (isset($close[3]) && $close[3] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($close[3]) && $close[3] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?>
						  </option>
					  </select>
						<input id="add_urls" class="r_addmore weekdays" value="<? echo $this->lang->line('SUNDAY') ?>"
							   type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time2" value="<?php if(isset($open[4])) echo $open[4];?>" /></div>	
					  <select name="before2" style="float: left;height:29px;width: 70px;">
						  <option value=""><? echo $this->lang->line('select') ?></option>
						  <option
							  value="am" <?php if (isset($open[5]) && $open[5] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($open[5]) && $open[5] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>
						<span style="float:left;">&nbsp; <? echo $this->lang->line('to') ?> &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time2" value="<?php if(isset($close[4])) echo $close[4];?>" /></div>
					  <select name="after2" style="float: left;height:29px;width: 70px;">
						  <option value=""><? echo $this->lang->line('select') ?></option>
						  <option
							  value="am" <?php if (isset($close[5]) && $close[5] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($close[5]) && $close[5] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>	
					  <input id="add_urls" class="r_addmore weekdays" value="<? echo $this->lang->line('MONDAY') ?>">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time3" name="open_time3" value="<?php if(isset($open[6])) echo $open[6];?>" /></div>	
					  <select name="before3" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
						  <option
							  value="am" <?php if (isset($open[7]) && $open[7] == 'am') echo 'selected'; ?>><? echo $this->lang->line('AM') ?></option>
						  <option
							  value="pm" <?php if (isset($open[7]) && $open[7] == 'pm') echo 'selected'; ?>><? echo $this->lang->line('PM') ?></option>
					  </select>					  					  
					  <span style="float:left;">&nbsp; to &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time3" value="<?php if(isset($close[6])) echo $close[6];?>" /></div>
					  <select name="after3" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($close[7]) && $close[7] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($close[7]) && $close[7] == 'pm') echo 'selected';?>>PM</option>
					  </select>
						<input id="add_urls" class="r_addmore weekdays" value="<? echo $this->lang->line('TUESDAY') ?>"
							   type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time4" value="<?php if(isset($open[8])) echo $open[8];?>" />	</div>		
					  <select name="before4" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($open[9]) && $open[9] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($open[9]) && $open[9] == 'pm') echo 'selected';?>>PM</option>
					  </select>				  					  
					  <span style="float:left;">&nbsp; to &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time4" value="<?php if(isset($close[8])) echo $close[8]?>" /></div>
					  <select name="after4" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($close[9]) && $close[9] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($close[9]) && $close[9] == 'pm') echo 'selected';?>>PM</option>
					  </select>	
					  <input id="add_urls" class="r_addmore weekdays" value="<?php echo $this->lang->line("WEDNESDAY"); ?>" type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time5" value="<?php if(isset($open[10])) echo $open[10];?>" />	</div>			  		
					  <select name="before5" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($open[11]) && $open[11] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($open[11]) && $open[11] == 'pm') echo 'selected';?>>PM</option>
					  </select>					  
					  <span style="float:left;">&nbsp; to &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time5" value="<?php if(isset($close[10])) echo $close[10];?>" /></div>
					  <select name="after5" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($open[11]) && $close[11] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($open[11]) && $close[11] == 'pm') echo 'selected';?>>PM</option>
					  </select>	
					  <input id="add_urls" class="r_addmore weekdays" value="<?php echo $this->lang->line("THURSDAY"); ?>" type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
					<div class="openhrs">
					  <div class="time"><input type="text" class="time start starttime" name="open_time6" value="<?php if(isset($open[12])) echo $open[12];?>" />	</div>	
					  <select name="before6" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($open[13]) && $open[13] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($open[13]) && $open[13] == 'pm') echo 'selected';?>>PM</option>
					  </select>			  					  
					  <span style="float:left;">&nbsp; to &nbsp;</span>
					  <div class="time"><input type="text" class="time end endtime" name="close_time6" value="<?php if(isset($close[12])) echo $close[12];?>" /></div>
					  <select name="after6" style="float: left;height:29px;width: 70px;">
						  <option value=""><?php echo $this->lang->line("select"); ?></option>
					  	<option value="am" <?php if(isset($close[13]) && $close[13] == 'am') echo 'selected';?>>AM</option>
						<option value="pm" <?php if(isset($close[13]) && $close[13] == 'pm') echo 'selected';?>>PM</option>
					  </select>	
					  <input id="add_urls" class="r_addmore weekdays" value="FRIDAY" type="text">
					  <img src="<?php echo base_url();?>themes/user/images/DownArrow.png" />
				    </div>
				  </div>
		        </div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('we cater to') ?></h2></div>
				<div class="prof_sec_div" id="select_arrow">
					<?php if(isset($details)){?>
						<select name="speciality" class="form_sm form_clr">
							<option
								value="1" <?php if ($details->speciality == 1) echo 'selected'; ?>><? echo $this->lang->line('Men only') ?></option>
							<option
								value="2" <?php if ($details->speciality == 2) echo 'selected'; ?>><? echo $this->lang->line('Women only') ?></option>
							<option
								value="3" <?php if ($details->speciality == 3) echo 'selected'; ?>><? echo $this->lang->line('Both') ?></option>
						</select>
					<?php } else{?>
					<select name="speciality" class="form_sm form_clr">
						<option value="1" selected><? echo $this->lang->line('Men only') ?></option>
						<option value="2"><? echo $this->lang->line('Women only') ?></option>
						<option value="3"><? echo $this->lang->line('Both') ?></option>
					</select>
					<?php } ?>
				</div>
			</div>
			
			<div id="location_content">
				<div class="prof_frm">
					<div class="flt"><h2><? echo $this->lang->line('Location') ?></h2></div>
					<div class="loactiondiv">
					  <div id="select_arrow1">
					    <div class="select_div">
						<?php $cntry[''] = 'Country';$attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="form_sm" required';
							if(isset($details))
								echo form_dropdown('cntry', $cntry, $details->gcountry,$attributes);
							else
								echo form_dropdown('cntry', $cntry, '',$attributes);
						?>
						</div>
					  </div>
					  <div id="select_arrow1" class="select_arrow1_option">
					  	<div class="select_div">
						<?php $attributes= 'id="state"  class="form_sm" required onchange="get_cities(this.value)"';
							if(isset($details))
								echo form_dropdown('state', $states, $details->gstate,$attributes); 
							else
								echo form_dropdown('state', array(), NULL,$attributes);     
						?>
						</div>
					  </div>
					  <div id="select_arrow1" class="select_arrow1_option">
					  	<div class="select_div">
						<?php $attributes= 'id="city"  class="form_sm" required';   
							if(isset($details))                  
								echo form_dropdown('city', $cities, $details->gcity,$attributes);
							else
								echo form_dropdown('city', array(), NULL,$attributes);                 
						?> 
						</div>
					  </div>
						<div class="prof_sec_div"><input type="text" name="gstreet" onblur="get_map()" id="gstreet"
														 required class="form_sm form_clr"
														 value="<?php if (isset($details)) echo $details->gstreet; ?>"
														 style="width: 100%; margin-top: 20px;"
														 placeholder="<? echo $this->lang->line('STREET ADDRESS') ?>"/>
						</div>
						<div class="prof_sec_div"><input name="gzip" id="zip_code" onblur="get_map();"
														 class="form_sm form_clr" required type="text"
														 value="<?php if (isset($details)) echo $details->gzip; ?>"
														 style="width: 100%; margin-top: 10px;"
														 placeholder="<? echo $this->lang->line('ZIP CODE') ?>"/></div>
					</div>
					<div class="gmaps">
						<div class="map_gym">
							<fieldset class="gllpLatlonPicker" id="custom_id" >
								<input type="hidden" name="search_location" id="search_location" class="gllpSearchField" value="<?php if(isset($details)) echo $details->location;?>" required/>
								<input type="button" name="search" class="gllpSearchButton" value="search" style="display:none;"/>
								<br/><br/>

								<div class="gllpMap"
									 style="margin-top:-50px;"><? echo $this->lang->line('Google Maps') ?></div>
								<input type="hidden" class="gllpLatitude" name="latitude" value="60"/>
								<input type="hidden" class="gllpLongitude" name="longitude" value="30"/>
								<input type="hidden" class="gllpZoom" name="zoom" value="12"/>                                                       
							</fieldset>
						</div>   
					</div> 
				</div>
			
				<div class="prof_frm">
				<input name="cat_id" type="hidden" value="1" readonly>
				<?php if(isset($details)){ $adv = explode(',',$details->advanced);}?>

					<div class="flt"><h2 style="margin-bottom: 0px;"><?php echo $this->lang->line("Tags"); ?></h2></div>
					<div class="prof_sec_div width100">
						<div class="addv_r_wrapp">		  
							<div class="first_rd">
								<input name="adv[pilates]" type="checkbox" value="1" <?php if(isset($details) && in_array('pilates', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('PILATES/YOGA') ?></h6>
								<div class="clear"></div>
							</div>
	
							<div class="second_rd">
								<input name="adv[mma]" type="checkbox" value="1" <?php if(isset($details) && in_array('mma', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('MMA') ?></h6>
								<div class="clear"></div>
							</div>  
					 
							<div class="third_rd">
								<input name="adv[boxing]" type="checkbox" value="1" <?php if(isset($details) && in_array('boxing', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('BOXING') ?></h6>
								<div class="clear"></div>
							</div>  
					 
							<div class="fourth_rd">
								<input name="adv[cross]" type="checkbox" value="1" <?php if(isset($details) && in_array('cross', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('CROSS TRAINING') ?></h6>
								<div class="clear"></div>
							</div>    
	
							<div class="fifth_rd">
								<input name="adv[spinning]" type="checkbox" value="1" <?php if(isset($details) && in_array('spinning', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('SPINNING') ?></h6>
								<div class="clear"></div>
							</div>
	
							<div class="fifth_rd">
								<input name="adv[gymnastics]" type="checkbox" value="1" <?php if(isset($details) && in_array('gymnastics', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('GYMNASTICS') ?></h6>
								<div class="clear"></div>
							</div>
							
							<div class="fifth_rd">
								<input name="adv[martial]" type="checkbox" value="1" <?php if(isset($details) && in_array('martial', $adv)) echo "checked"; ?>/>
								<h6><? echo $this->lang->line('MARTIAL ARTS') ?></h6>
								<div class="clear"></div>
							</div>  
						</div>
					</div>
				</div> 
			</div> <!--location content-->
		   	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Contact Person') ?></h2></div>
				<div class="prof_sec_div"><input name="owner" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->owner;?>"/></div>  
		  	</div>
		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Telephone') ?></h2></div>
				<div class="prof_sec_div"><input name="ow_phone" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->ow_phone;?>"/></div>
		  	</div>
		  	<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Email') ?></h2></div>
				<div class="prof_sec_div"><input name="ow_email" class="form_sm" type="text" required value="<?php if(isset($details)) echo $details->ow_email;?>"/></div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Website') ?></h2></div>
				<div class="prof_sec_div"><input name="site_url" class="form_sm" type="text" value="<?php if(isset($details)) echo $details->site_url;?>"/></div>
			</div>
			<div class="prof_frm">
				<div class="flt"><h2><? echo $this->lang->line('Gym Profile') ?></h2></div>
				<div class="prof_sec_div"><textarea name="description" rows="8" cols="59" required type="text" ><?php if(isset($details)) echo $details->description;?></textarea></div>
		  	</div>		   
		    <div class="prof_frm">
		    	<div class="flt">&nbsp;</div>
				<div class="prof_sec_div"><input name="submit" class="sub_red"
												 value="<? echo $this->lang->line('Save and Next Page') ?>"
												 type="submit" style="margin-left: 0%;"><img
						src="<?php echo base_url(); ?>themes/user/images/Right-arrow.png"/></div>
		   	</div>		   
		</div>
		</form>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery-1.7.2.min.js"> </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="http://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" />
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">
	$("#gym_det_form").validate({
	  rules: {
	    // simple rule, converted to {required:true}
	    cntry: "required",
	    state: "required",
	    state: "required",
	    // compound rule
	    email: {
	      required: true,
	      email: true
	    }
	  }
	});
</script>
<script type="text/javascript">

 function ytVidId(url) {
		var contain1=url.indexOf('youtube');
		var contain2=url.indexOf('vimeo');
		if(contain1>-1)
    	var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
    	else if(contain2 > -1)
    	var p = /http:\/\/(?:www\.|player\.)?(vimeo|youtube)\.com\/(?:embed\/|video\/)?(.*?)(?:\z|$|\?)/;
    	else
    		return false;
    	return (url.match(p)) ? RegExp.$1 : false;
	}
	

$(document).on("change keyup input",'.youtube', function() {
		var url = $(this).val();
	    if (ytVidId(url) !== false) {
	        $('#ytlInfo').html('');
	    } else if(url!="") {
	        $('#ytlInfo').html('give valid url').css('color','red');
	       }
	    
	});
	
function check_url(value){
		if(value==""){
			$('#ytlInfo').html('');
		}
	}
	
	
</script>

<script> 
    function load_dropdown_content(selected_value){ 
  		get_map();
        if(selected_value!=''){
	       var result = $.ajax({
    	         'url' : '<?php echo SUPPLIER_URL;?>get_states/' + selected_value,
    	         'async' : false
	       }).responseText;
    	   $("#state").replaceWith(result);
        }
    }
  function get_cities(state){
  	get_map();
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo SUPPLIER_URL;?>get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function deleteImage(id, imgname)
  {
      if(confirm('Are you sure want to delete this image?'))
      {
        $('#delimg'+id).remove();
        $('#removeimgdiv').append('<input type="hidden" name="remove_gym_image[]" value="'+imgname+'">');
      }
  }
  
</script>
<script type="text/javascript">
	function get_map(){
		var sea_cntry = $( "#cntry option:selected" ).text();
		var sea_state = $( "#state option:selected" ).text();
		var sea_city = $( "#city option:selected" ).text();
		var sea_street = $( "#gstreet" ).val();
		var sea_zip = $( "#zip_code" ).val();
		$('#search_location').val($.trim(sea_street+" "+sea_city+' '+sea_state+' '+sea_cntry+' '+sea_zip));
		//$('#search_location').css('display', 'none');
		//$('#custom_id .gllpSearchButton').css('display', 'none');
		$('#custom_id .gllpSearchButton').trigger('click');
	}
</script>
<script>
    $(function (){
        var img_count = 0;
        $('#add_images').on("click",function(){
            img_count = img_count+1;

			div_id	=	img_count+1
			$("#add_more").append('<div class="map_gym" id="divname_' + img_count + '"><div id="light' + img_count + '" class="white_content"><a href = "javascript:void(0)" onclick = "crop_fn(' + img_count + ')" class="crop_btn"><?php echo $this->lang->line("crop"); ?></a><img id="uploadPreview' + img_count + '" style="display:none;max-width:800px;"/></div><input type="file" name="upload_file[]" id="uploadImage' + img_count + '" class="form_sm valid" style="margin-bottom:10px;float: none;width: 53%;"/><input type="hidden" id="x' + img_count + '" name="x[]" /><input type="hidden" id="y' + img_count + '" name="y[]" /><input type="hidden" id="w' + img_count + '" name="w[]" /><input type="hidden" id="h' + img_count + '" name="h[]" /><button type="button" class="btn btn-warning " onclick="remove_btn(' + img_count + ')" id="uploadPreview"><?php echo $this->lang->line("Remove"); ?></button><div id="fade' + img_count + '" class="black_overlay"></div></div');
            $("#divname_"+img_count).hide();
            $( "#divname_"+img_count ).slideDown( "slow", function() {
            // Animation complete.
            });
			
			  var p = $("#uploadPreview"+img_count);
			  // prepare instant preview
			  $("#uploadImage"+img_count).change(function(){
				var CurrentId    = $(this).closest('div').attr('id'); 
				var CountVal     = CurrentId.split('_');
				var CurrentCount = CountVal[1];
				document.getElementById('light'+CurrentCount).style.display='block';
				document.getElementById('fade'+CurrentCount).style.display='block';
				p.fadeOut();
				var reader, file;   
				var file = this.files[0];
				var images = this.files;
			
				// prepare HTML5 FileReader
				var oFReader = new FileReader();
				oFReader.readAsDataURL(file);
				oFReader.onload = function (oFREvent) {
					 p.attr('src', oFREvent.target.result).fadeIn();
				};
			  
			    function setInfo(i, e) {
					$('#x'+CurrentCount).val(e.x1);
					$('#y'+CurrentCount).val(e.y1);
					$('#w'+CurrentCount).val(e.width);
					$('#h'+CurrentCount).val(e.height);
				}
			  
			  // implement imgAreaSelect plug in (http://odyniec.net/projects/imgareaselect/)
				  $('img#uploadPreview'+CurrentCount).imgAreaSelect({
					//aspectRatio: '1:1', onSelectChange: setInfo
					//parent: "#light"+CurrentCount, handles: true, aspectRatio: '4:4',  fadeSpeed: 200, minHight: 200, minWidth: 200,  x1: 100, y1: 100, x2: 500, y2: 500 ,onSelectChange: setInfo
				    parent: "#light"+CurrentCount, instance: true, zIndex: 12000, handles: true,maxWidth: 500, maxHeight: 300, aspectRatio: 0,  fadeSpeed: 200, x1: 100, y1: 30, x2: 600, y2: 330 ,onSelectEnd: setInfo


				  });
			});
			
        });
    });
    function remove_btn(id){


    $( "#divname_"+id).slideUp( "slow", function() {
        $("#divname_"+id).remove();
		$(".imgareaselect-outer").remove();
		$(".imgareaselect-selection").remove();
      });
    }
	function crop_fn(count){
		//alert(count);
		document.getElementById('light'+count).style.display='none';
		document.getElementById('fade'+count).style.display='none';
		$('.imgareaselect-selection').css('display', 'none');
		$('.imgareaselect-outer').css('display', 'none');
		$('.imgareaselect-selection').parent().css('display', 'none');
	}
</script>
<script>
    $(function (){
        var url_count = 0;
        $('#add_urls').on("click",function(){
            url_count = url_count+1;
            /*$("#add_videos").append('<div class="prof_frm" id="div'+url_count+'"><div class="prof_fst_div">&nbsp;</div><div class="prof_sec_div"><input type="text" required class="form_sm youtube" name="gym_video[]" style="float:none;"/>&nbsp;&nbsp;<button type="button" class="btn btn-warning " onclick="remove_url('+url_count+')" >Remove</button></div></div>');*/
			$("#add_videos").append('<div class="prof_frm" id="div' + url_count + '"><div class="prof_sec_div"><input type="text" required class="form_sm youtube" name="gym_video[]" style="float:none;"/>&nbsp;&nbsp;<button type="button" class="btn btn-warning " onclick="remove_url(' + url_count + ')" ><?php echo $this->lang->line("Remove"); ?></button></div></div>');
            $("#div"+url_count).hide();

            $( "#div"+url_count ).slideDown( "slow", function() {
            // Animation complete.
            });
        });
    });
    function remove_url(id){

    $( "#div"+id).slideUp( "slow", function() {
        $("#div"+id).remove();
      });
    }
</script>

<script type="text/javascript">
    $('.icon-remove').click (function () {

     var id = $(this).attr('name');
     var image = $(this).attr('id');
         var result = $.ajax({
            'url' : '<?php echo SUPPLIER_URL?>del_img/' + id  + '/' + image,
            'async' : false
        }).responseText;
           $('#img_'+id).hide();
     return false;
     });

</script>
<script>
	$(document).ready(function(){
		
		$('#check_availablity').click(function(){
			if($(this).is(':checked'))
			{
				/*$('.start').val('12:00am').attr('readonly', true);
				$('.end').val('11:30pm').attr('readonly', true);*/
				$("#open_close_time").css("display","none");
				$("#open_close_time").removeClass("available_time");
			}
			else
			{
				/*$('.start').val('').attr('readonly', false);
				$('.end').val('').attr('readonly', false);*/
				$("#open_close_time").css("display","block");
				$("#open_close_time").addClass("available_time");
			}
			
		});
		$(".sub_red").click(function(){
				var img = $("#uploadfile1_txt").val();
				if(img == ''){
					$("#img_error").html("This field is required.");
					$("#img_error").css("color","red");
					$("#img_error").css("padding-left","20px");
				}else{
					$("#img_error").html(" ");
				}
		});
		
	});


	
  	$('#timeOnlyExample .time').timepicker({
    	'showDuration': true,
    	'timeFormat': 'g:ia'
	});

</script>
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/user/css/imgareaselect-default.css" />
<script type="text/javascript" src="<?php echo base_url()?>themes/user/js/jquery.imgareaselect.pack.js"></script>

<!--<script src="<?php // echo base_url()?>themes/user/js/jquery.bpopup-0.9.4.min.js" type="text/javascript"></script>-->
<script>
function setInfo(i, e) {
  $('#x').val(e.x1);
  $('#y').val(e.y1);
  $('#w').val(e.width);
  $('#h').val(e.height);
}

$(document).ready(function() {
  var p = $("#uploadPreview");
  // prepare instant preview
  $("#uploadImage").change(function(){
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
    p.fadeOut();
	var reader, file;   
    var file = this.files[0];
	var images = this.files;
    // prepare HTML5 FileReader
    var oFReader = new FileReader();
    oFReader.readAsDataURL(file);

    oFReader.onload = function (oFREvent) {
         p.attr('src', oFREvent.target.result).fadeIn();
    };
	
	var imageHeight = $(p).height();
	//alert(imageHeight);
	var imageWidth = $(p).width();
	//alert(imageWidth);
	if(imageWidth > 800){
		$("#uploadPreview").css('width','100%');
	}
	
  });
 // $('img#uploadPreview').imgAreaSelect({minHight: 300, minWidth: 300,maxWidth: 300, maxHeight: 300, handles: true,onSelectChange: setInfo});
  $('img#uploadPreview').imgAreaSelect({
    // set crop ratio (optional)
		//parent: "#light", aspectRatio: '4:4',  fadeSpeed: 200, minHight: 200, minWidth: 200,  x1: 0, y1: 0, x2: 500, y2: 500 ,onSelectChange: setInfo 
	   parent: "#light",instance: true, zIndex: 12000, handles: true,maxWidth:500, maxHeight: 300, aspectRatio: 0,  fadeSpeed: 200, x1: 100, y1: 30, x2: 600, y2: 330 ,onSelectEnd: setInfo
  });
});
</script>
<style>
.imgareaselect-outer{
	height:0px;
}
#add_videos .prof_sec_div{
	margin-bottom:2%;
	
}
.addv_r_wrapp h6 {
	font-family: 'TwCenMTCondensedRegular';
	float: left;
	display: block;
	font-size: 17px;
	color: #7A7A7A;
	text-transform: uppercase;
	margin-left: 5px;
	margin-top: 6px;
}
.available_time{
	display:block;
	margin-top:-2px;
	float:right;
}
.black_overlay{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.50;
	filter: alpha(opacity=80);
}

.white_content {
	display: none;
/*	position: absolute;
	top: 25%;
	left: 25%;
	width: 50%;
	height: 50%;
	padding: 16px;
	border: 16px solid orange;
	background-color: white;
	z-index:1002;
	overflow: auto;*/
	position: relative;
    top: 25%;
    width: 50%;
    z-index: 1002;
}
.crop_btn{
    background-color: #87191a;
    border-radius: 4px;
    color: #fff;
    float: left;
    font-family: gothic;
    margin-bottom: 3px;
    padding: 3px 10px;
}
.white_content a:hover{
	color:#fff;
}
#addgym .flt{
	float:none !important;
}
#addgym .form_sm {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #c0c0c0;
    box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
    float: left;
    height: 34px;
    padding: 0 5px;
    width: 40%;
	margin-bottom:0px;
	margin-top:0px;
}
#addgym  h2 {
	font-size: 16px;
	color: #605f5f;
	display: block;
	float:none;
	text-transform:uppercase;
	font-family:Gothic !important;
}
#addgym .prof_sec_div {
    float: none;
    width: 75%;
	margin-top: -5px;
	
}
#addgym{
	font-family:Gothic !important;
}
#addgym .r_addmore {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #87191a;
    color: #87191a;
    display: block;
    font-size: 18px;
    margin-bottom: 18px;
    padding: 2px 22px;
	float: left;
    height: 29px;
}
#addgym .form_clr{
    border: 1px solid #87191a;
    color: #87191a;
	border-radius: 0;
}
#addgym .form_aln{
    border: 1px solid #87191a;
    color: #87191a;
	margin-top: 20px;
    width: 80%;
}

#select_arrow select {
    background: none repeat scroll 0 0 transparent !important;
    border: 1px solid #87191a !important;
    height: 29px !important;
    padding: 2px 4px 1px !important;
    width: 276px !important;
	color:#87191a;
	border-radius: 0;
}
#select_arrow1 select {
    background: none repeat scroll 0 0 transparent !important;
    border: 1px solid #87191a !important;
    height: 29px !important;
    padding: 2px 4px 1px !important;
    width: 150px !important;
	color:#87191a;
	border-radius: 0;
}
#addgym #location_content{
    border-bottom: 1px solid #d0d0d0;
    border-top: 1px solid #d0d0d0;
    float: left;
    width: 100%;
	margin-top: 10px;
}
#addgym .sub_red {
    background: none repeat scroll 0 0 #ffffff;
    border: 1px solid #87191a;
    color: #87191a;
    display: block;
    float: left;
    font-size: 20px;
    height: 29px;
    margin-bottom: 30px;
    padding: 0 10px;
}
#addgym .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:20px;
}
#addgym #location_content .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:0px;
}
#addgym .time{
	float: left;
    width: 12%;
}
#addgym .time .error {
    color: #f00;
    float: left;
    font-size: 12px !important;
	font-family:gothic !important;
	padding-left:0px;
}
#addgym .select_div {
    background-color: rgb(255, 255, 255);
    height: 29px;
    width: 89px;
}
#addgym .select_arrow1_option{
	margin-left: 10px;
}
#addgym .openhrs{
    float: right;
    width: 80%;
}
#addgym .width100{
	width:100%;
}
#addgym .gmaps{
	float: right;
	width: 55%;
}
#addgym .loactiondiv{
	float: left;
	width: 35%;
}
#addgym .endtime{
	width:70px !important;
	float:left; 
	height: 29px;
	margin-right: 10px;
}
#addgym .starttime{
	width:70px !important;
	float:left;
	height: 29px;
}
#addgym .weekdays{
	color: #000;
	float: left;
	height: 29px;
	margin-left: 10px;
	padding: 0 5px;
	width: 85px;
}
#addgym .addbtn{
		margin-left: 24%;
    	margin-top: 10px;
	}
@media screen and (max-width: 1200px){
	#addgym .ad_form_wr {
		background: none repeat scroll 0 0 #f5f6f6;
		border: 1px solid #c8c8c8;
		float: left;
		margin-bottom: 5px;
		padding-top: 20px;
	}
	#addgym .time {
		float: left;
		margin-right: 20px;
		width: 14%;
	}
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 67px;
	}
	#addgym .gllpMap {
		height: 200px;
		width: 400px;
	}
	#addgym .width100{
		width:100%;
	}
}
@media screen and (max-width: 995px){
	#addgym .gllpMap {
		height: 150px;
		width: 300px;
	}
	#addgym .select_arrow1_option{
		margin-top: 10px;
		margin-left: 0;
	}
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 83%;
	}
	#addgym .form_sm {
		background: none repeat scroll 0 0 #fff;
		border: 1px solid #c0c0c0;
		box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.1);
		float: left;
		height: 34px;
		margin-bottom: 0;
		margin-top: 0;
		padding: 0 5px;
		width: 100%;
	}
	#addgym #select_arrow1 {
		background: url("themes/user/images/DownArrow.png") no-repeat scroll right top rgba(0, 0, 0, 0);
		float: left;
		overflow: hidden;
		padding: 0;
		width: 55%;
	}
	#addgym .openhrs{
		float: right;
		margin-top: 10px;
		width: 100%;
	}
	#addgym .width100{
		width:100%;
	}
	#addgym .addbtn{
		margin-left: 0%;
    	margin-top: 10px;
	}
}
@media screen and (max-width: 700px){
	#addgym .gllpMap {
		margin-top: 0 !important;
		width: 100%;
	}
	#addgym #select_arrow1 {
		background: url("themes/user/images/DownArrow.png") no-repeat scroll right top rgba(0, 0, 0, 0);
		float: left;
		overflow: hidden;
		padding: 0;
		width: 75%;
	}
	#addgym .gmaps{
		float: left;
    	width: 100%;
	}
	#addgym .map_gym {
		margin-left: auto;
		margin-right: auto;
		width: 100%;
	}
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 95%;
	}
	#select_arrow1 select {
		background: none repeat scroll 0 0 transparent !important;
		border: 1px solid #87191a !important;
		border-radius: 0;
		color: #87191a;
		height: 29px !important;
		padding: 2px 4px 1px !important;
		width: 100% !important;
	}
	#addgym .prof_frm {
		float: left;
		width: 100%;
	}
	#addgym .first_rd {
		float: none;
		width: 14.3939%;
	}
	#addgym .loactiondiv{
		float: left;
		width: 100%;
	}
	#addgym .endtime{
		width:50px;
		float:left; 
		height: 29px;
		margin-right: 10px;
	}
	#addgym .starttime{
		width:50px;
		float:left;
		height: 29px;
	}
	#addgym .weekdays{
		color: #000;
		float: left;
		height: 29px;
		margin-left: 10px;
		padding: 0 5px;
		width: 75px;
	}
	#addgym .openhrs {
		float: right;
		margin-top: 10px;
		width: 450px;
	}
	#addgym .ad_form_wr {
		background: none repeat scroll 0 0 #f5f6f6;
		border: 1px solid #c8c8c8;
		float: left;
		margin-bottom: 5px;
		overflow-x: auto;
		overflow-y: hidden;
		padding-top: 20px;
		width: 100%;
	}
	#open_close_time {
		float: left;
		width: 500px;
	}

}
@media screen and (max-width: 550px){
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 93%;
	}
}
@media screen and (max-width: 350px){
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 87% !important;
	}
}
@media screen and (max-width: 480px){
	#addgym .select_div {
		background-color: rgb(255, 255, 255);
		height: 29px;
		width: 91%;
	}
	#addgym #select_arrow {
		background: url("themes/user/images/DownArrow.png") no-repeat scroll right center rgb(255, 255, 255);
		float: left;
		overflow: hidden;
		padding: 0;
		width: 75%;
	}
}
</style>
