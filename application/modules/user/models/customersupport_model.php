<?php
class customersupport_model extends MY_Model {

    public function __construct() {
      parent::__construct();
      //$this ->table = 'user';
       $this ->result_mode = 'object';
      }
      
      public function insertFaq($is_replied = '0'){
		
		extract($_POST);
		$type = $this->session->userdata('type') == 1?'1':'2';
		$data = array(
			'message'=>$message,
			'user_id'=>$this->session->userdata('user_id'),
			'is_replied'=>$is_replied,
			'user_type'=>$type,
			'createdon'=>date('Y-m-d H:i:s')
		);
		$this->db->insert('faq', $data);
		return $this->db->insert_id();
      }
	  public function getfaq($is_replied = '0')
	  {
		  $result = $this->db->where('is_replied', $is_replied)->order_by('createdon', 'desc')->get('faq')->result();
		  return $result;
	  }
 }

?>
