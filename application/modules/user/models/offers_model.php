<?php
class Offers_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'gym_offers';
	     $this -> result_mode = 'object';
	    }
	    public function get_gym($is_supplier = '0')
	    {
            $res = $this->db->where('is_supplier', $is_supplier)->order_by('gymname')->get('gym')->result();
        	$gyms = array();
	        foreach ($res as $value) {
	          $gyms[$value->id] = $value->gymname;
	        }
	        return $gyms;
   		} 
	    public function get_details($id){
	    	return $res = $this->db->select('o.*,r.gym_id')->from('offer_relation r')
								->join('offer_description o','r.id = o.offer_relation_id')
								->where('r.id',$id)->get()->result();
	    }
	    public function get_all( $user_id = '0')
	    {

			$this->db->select('o.*, g.gymname')->where('o.user_id',$user_id)->from('offer_relation as o');
			
			$result = $this->db->join('gym as g', 'g.id=o.gym_id')->order_by('o.gym_id')->get()->result();			
			return $result;
		}
		
}


?>
