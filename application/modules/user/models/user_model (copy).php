<?php
class User_model extends MY_Model {

    public function __construct() {
      parent::__construct();
      //$this ->table = 'user';
       $this ->result_mode = 'object';
      }
      
      public function get_cats(){
        $res = $this->db->where('status','0')->get('category')->result();
        $cat = array();
        foreach ($res as $value) {
          $cat[$value->id] = $value->categoryname;
        }
        return $cat;
      }
      public function get_cntrs(){
        $res = $this->db->where('status','0')->get('country')->result();
        $cntry = array();
        foreach ($res as $value) {
          $cntry[$value->id] = $value->country_name;
        }
        return $cntry;
      }
      public function get_states($x){
        $res = $this->db->where('country_id',$x)->get('states')->result();
        $states = array();
        foreach ($res as $value) {
          $states[$value->state_code] = $value->state;
        }
        return $states;
      }

      public function get_cities($x){
        $res = $this->db->where('state_code',$x)->get('cities')->result();
        $states = array();
        foreach ($res as $value) {
          $states[$value->id] = $value->city;
        }
        return $states;
      }
      public function user_register(){
        extract($_POST);
        $data = array('first_name'=>$first_name,'last_name'=>$last_name,'user_name'=>$user_name,'password'=>$password,'mobile'=>$mobile,'email'=>$email);
        $res = $this->db->insert('user',$data);
        if($res == 1){
          $id = $this->db->insert_id();
          $this->session->set_userdata('user_id',$id);
        }
        return true;
      }
      public function sup_register(){
        extract($_POST);
        $data = array('level'=>$level,'first_name'=>$first_name,'last_name'=>$last_name,'user_name'=>$user_name,'password'=>$password,'mobile'=>$mobile,'email'=>$email);
        $res = $this->db->insert('supplier',$data);
        if($res == 1){
          $id = $this->db->insert_id();
          $user_data = array('user_id'=>$id,'level'=>$level);
          $this->session->set_userdata($user_data);
        }
        return true;
      }
      public function login(){
        extract($_POST);
        //print_r($_POST); die();
        if(isset($level) && $level == 1){
          $sql = "select * from supplier where user_name = '".$user_name."' and password = '".$password."' and level = $level and status = 0";
          $res = $this->db->query($sql)->row();
          //print_r($res);
          $res1 = $this->db->query($sql)->num_rows();
          //echo $res1;
          if($res1 > 0){
            $user_data = array('user_id'=>$res->id,'level'=>$res->level);
            $this->session->set_userdata($user_data);
            return 1; 
          }else{
            return 0; 
          }
        }else{
          $sql = "select * from user where user_name = '".$user_name."' and password = '".$password."' ";
          $res = $this->db->query($sql)->row();
          //print_r($res);
          $res1 = $this->db->query($sql)->num_rows();
          //echo $res1; die();
          if($res1 > 0){
            $this->session->set_userdata('user_id',$res->id);
            return 1; 
          }else{
            return 0;
          }
        }
      }
      public function get_latest(){
        //$query = "SELECT g.*,cnt.country_name,s.state,c.city,GROUP_CONCAT(CONCAT(m.name,'-',gof.price)) as price FROM gym g,country cnt,states s,cities c,membership m,gym_offers gof where gof.mem_id = m.id and g.id = gof.gym_id and g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 0 group by g.id order by g.id desc limit 3";
        $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 0 group by g.id order by g.id desc limit 3";
        return $result = $this -> db -> query($query) -> result();
      }
      public function getgyms(){
        
            extract($_POST);   
            //print_r($_POST); die();
            if(isset($adv)){
              $this->db->select('g.id,g.gymname,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.comments,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname'); 
              $this->db->from('gym g');
              $this->db->join('category','g.category = category.id');
              $this->db->join('country','g.gcountry = country.id');
              $this->db->join('states','gstate = states.state_code'); 
              $this->db->join('cities','g.gcity = cities.id');
              $this->db->where(array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city));
              $firstime=true;
              foreach ($adv as $key => $value){
                if($firstime)
                  $this->db->like('g.advanced',$key,'%');
                else
                  $this->db->or_like('g.advanced',$key,'%');
                $firstime=false;
              }                             
              return $this->db->get()->result();
            }else{         
              $result = $this->db->select('g.id,g.gymname,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.comments,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname')   
                             ->from('gym g')
                             ->join('category','g.category = category.id')
                             ->join('country','g.gcountry = country.id')
                             ->join('states','gstate = states.state_code')  
                             ->join('cities','g.gcity = cities.id')
                             ->where(array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city))
                             ->get()
                            ->result();
            }
           return $result;                               
      }
      public function get_images($gym_id){
         $result = $this->db->select('g.id,g.gymname,g.description,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.comments,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,cities.city,states.state,category.categoryname')
                            ->from('gym g')
                            ->join('country', 'g.gcountry = country.id')
                            ->join('states','gstate = states.state_code') 
                            ->join('cities','g.gcity = cities.id')
                            ->join('category', 'g.category = category.id')
                            ->where('g.id',$gym_id)
                            ->group_by('g.id')
                            -> get()
                            -> row(); 
         return $result;
      }
      public function getgyms_filter($fil_val){

        $cntry = $this->session->userdata('cntry');
        $state = $this->session->userdata('state');
        $city = $this->session->userdata('city');
        $cat_id = $this->session->userdata('cat_id');
        $gend = $this->session->userdata('speciality');
        if(isset($gend)){
          $gender = $gend;
        }
        else{
          $gender = '';
        }
        $byname = $this->session->userdata('byname');
        $rating = $this->session->userdata('rating');
        $price_range = $this->session->userdata('price'); 
        if(isset($byname) && $byname != ''){
          $filt = "g.gymname";
        }else if(isset($rating) && $rating != ''){
          $filt = "g.star";
        }else if(isset($price_range) && $price_range != ''){
          $filt = "g.price_range";
        }else{
          $filt = "g.id";
        }
        $condition = array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city);
        if($gender != '') $condition['g.speciality'] =$gender;

        $result = $this->db->select('g.id,g.gymname,g.gym_logo,g.star,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.price_range,country.country_name,states.state,cities.city')   
                           ->from('gym g')
                           ->join('country','g.gcountry = country.id')
                           ->join('states','g.gstate = states.state_code')  
                           ->join('cities','g.gcity = cities.id')
                           ->where($condition)
                           ->order_by($filt,'asc')
                           ->get()
                           ->result();  
             return $result;       
      }
      public function get_cnt(){
        extract($_POST);
        //print_r($_POST); die();
        $result = $this->db->select('country.country_name,states.state,cities.city,category.categoryname')
                           ->from('country,states,cities,category')
                           ->where(array('country.id'=>$cntry,'states.state_code'=>$state,'cities.id'=>$city,'category.id'=>$cat_id))
                           ->get()
                           ->row();
        return $result;
      }

      public function storeUserDetails(){

        //print_r($this->session);die;
        extract($_POST);
        //echo $_POST['amount'];
        //echo $_POST['card_number'];
        //echo $_POST['exp_date'];
        $date = $this->session->userdata('joining_date'); 
        //$end_date = date( "m-d-Y", strtotime( "$date +3 month" ) );
        //echo date( "m-d-Y", strtotime( "$date +3 month" ) );

        $data = array(
          'username'=>$this->session->userdata('uname'),
          'mobile'=>$this->session->userdata('phno'),
          'email'=>$this->session->userdata('email'),
          'membership'=>$this->session->userdata('mem_id'),
          'cat_id'=>$this->session->userdata('cat_id'),
          'gym_id'=>$this->session->userdata('gym_id'),
          'price'=>$this->session->userdata('price'),
          'offer'=>$this->session->userdata('offer'),
          'offer_price'=>$this->session->userdata('offer_price'),
          'join_date'=>$this->session->userdata('joining_date'),
          'end_date'=>$this->session->userdata('end_date'),
          'createdon'=> mktime()
        );
        $result = $this->db->insert('user',$data);
        return $result;
      }
      public function getcnt_filter(){
        $cntry = $this->session->userdata('cntry');
        $state = $this->session->userdata('state');
        $city = $this->session->userdata('city');
        $cat_id = $this->session->userdata('cat_id');
        
        $result = $this->db->select('country.country_name,states.state,cities.city,category.categoryname')
                           ->from('country,cities,states,category')
                           ->where(array('country.id'=>$cntry,'states.state_code'=>$state,'cities.id'=>$city,'category.id'=>$cat_id))
                           ->get()
                           ->row();
        return $result;
      }

      public function get_offer_details(){
      extract($_POST);
           $res = $this->db->query("select gym_id,mem_id,price,offer,name from gym_offers o join membership m on o.mem_id = m.id where o.gym_id = '$gym_id'
       and o.mem_id='$mem_id'");
       return $res->row();
      }
      
      public function getcatgs(){
            $result = $this->db->select('id,categoryname')
           ->from('category')
           -> get()
           -> result();
           
           return $result; 
      }
      public function selected_cntry($id){ 
          $qry="select city.id,cities.city,city.country_id from city,country WHERE city.country_id = country.id and country.id = $id order by city_name";
                                                            
            $result = $this->db->query($qry);                 
                  
            $dataArray=$result->result_array();
                  
            $returnarray=array();
                  
            foreach($dataArray as $value){
            
              $returnarray[$value['id']]=$value['city_name'];            
            
            }                 
            return $returnarray;             
          }          
      
      public function get_mapdet($id){
          $result = $this->db->select('g.id,g.gymname,g.gstate,g.gmap_lat,g.gmap_long,g.gstreet,country.country_name,cities.city')
                             ->from('gym g')
                             ->join('country', 'g.gcountry = country.id')
                             ->join('cities', 'g.gcity = city.id')
                             ->where(array('g.id'=>$id))
                             ->get()
                             ->row();
          return $result;
      }
      public function get_offers(){
        extract($_POST);
        $query = "SELECT GROUP_CONCAT(CONCAT(m.id,'-',m.name,'-',price,'-',offer,'-',gym_id)) as price
                  FROM gym_offers
                  join membership m ON mem_id = m.id
                  WHERE gym_id = $renew
                  order by mem_id asc ";
        return $result = $this->db->query($query)->row();
      }
      public function get_cntname($flag){
         $result = $this->db->where('name',$flag)->get('country_flags')->row();
         return $result;    
      }
      
      public function get_aboutus($id){
        $result = $this->db->select('cm.id,cc.content,cc.image,cm.menuname')
                           ->from('cmsmenu cm')
                           ->join('cms_content cc','cc.menu_id = cm.id')
                           ->where(array('cm.id'=>$id))
                           ->get()
                           ->row();
        return $result;
      }

 }

?>
