<?php
class Gym_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'gym';
	     $this -> result_mode = 'object';
	    }
	    public function get_gym($is_supplier = '0')
	    {
             return $this->db->where('is_supplier', $is_supplier)->order_by('gymname')->get('gym')->result();
   		} 
   		public function get_offers($id)		
   		{
			$where = array(
				'gym_id'=>$id,
				'type'=>'1',
				'status'=>'1'
			);
			$result = $this->db->where($where)->where("end_date > CURDATE()")->get('offers')->result();

			return $result;
		}
		public function get_reviews($id)
		{
			$qry = "SELECT * FROM reviews WHERE gym_id = $id 
					ORDER BY createdon DESC ";
					$result = $this->db->query($qry)->result();
					$count =count($result);
					//echo  '<pre>';
					//print_r($result);
					/*for($i=0;$i<$count;$i++){
						$user_level = $result[$i]->user_level;
						if($user_level == "supplier"){
							$qry = "SELECT first_name, last_name FROM supplier 
									WHERE id = $result->user_id ";
						}else{
							$qry = "SELECT first_name, last_name FROM user 
									WHERE id = $result->user_id ";
						}
						$result = $this->db->query($qry)->result();
					}*/
		
			/*$current_userid = $this->session->userdata['user_id'];
			$SQL            = "SELECT is_supplier 	FROM gym WHERE id = $id";
			$res            = $this->db->query($SQL)->result();
			$suppler_id     = $res[0]->is_supplier; 
			
			if($current_userid == $suppler_id){
			
			
			$qry = "SELECT r.*, s.first_name, s.last_name FROM reviews as r
					LEFT JOIN supplier as s ON s.id = r.user_id
					WHERE r.status = '1' AND r.gym_id = $id 
					ORDER BY r.createdon DESC ";
					echo $qry;
			}else{
			$qry = "SELECT r.*, u.first_name, u.last_name FROM reviews as r
					LEFT JOIN user as u ON u.id = r.user_id
					WHERE r.status = '1' AND r.gym_id = $id AND u.id = $current_userid
					ORDER BY r.createdon DESC";
					echo $qry;
			}		
			$result = $this->db->query($qry)->result();*/
			return $result;			
		}
		public function update_recently_viewed($gym_id, $usr_id)
		{
			$data = array(
				'gym_id'=>$gym_id,
				'user_id'=>$usr_id,
				'viewed_on'=>date('Y-m-d H:i:s'),
				'viewed_ip'=>$_SERVER['REMOTE_ADDR']				
			);
			$this->db->insert('recently_viewed', $data);
		}
		public function get_recent_viewed_gym($usr_id, $limit = 3)
		{
			$qry = "SELECT g.* FROM recently_viewed as r 
						INNER JOIN gym as g ON g.id = r.gym_id
						WHERE r.user_id = $usr_id 
						GROUP BY r.gym_id ORDER BY r.viewed_on DESC LIMIT $limit";
					
			$result = $this->db->query($qry)->result();
			return $result;
		}
}


?>
