<?php

class User_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        //$this ->table = 'user';
        $this->result_mode = 'object';
    }

    public function get_cats()
    {
        $res = $this->db->where('status', '0')->get('category')->result();
        $cat = array();
        foreach ($res as $value) {
            $cat[$value->id] = $value->categoryname;
        }
        return $cat;
    }

    public function loadLanguage(MY_Lang $lang)
    {
        if (isset($_COOKIE["lang"])) {
            $lang->load($_COOKIE["lang"]);
        }
    }

    public function get_cntrs()
    {
        $res = $this->db->where('status', '1')->get('country')->result();
        $cntry = array();
        foreach ($res as $value) {
            $cntry[$value->id] = $value->country_name;
        }
        return $cntry;
    }

    public function get_states($x)
    {
        $res = $this->db->where('country_id', $x)->get('states')->result();
        $states = array();
        foreach ($res as $value) {
            $states[$value->state_code] = $value->state;
        }
        return $states;
    }

    public function get_cities($x)
    {
        $res = $this->db->where('state_code', $x)->get('cities')->result();
        $states = array();
        foreach ($res as $value) {
            $states[$value->id] = $value->city;
        }
        return $states;
    }

    public function user_register()
    {
        $datestring = date('Y-m-d H:i:s', time());

        extract($_POST);
        $data = array('first_name' => $first_name, 'last_name' => $last_name, 'user_name' => $user_name, 'password' => md5($password), 'email' => $email, 'gender' => $gender, 'country_list' => $country_list, 'state_select' => $state_select, 'city_select' => $city_select, 'createdon' => $datestring);
        $this->db->insert('user', $data);
        $res = $this->db->insert_id();
        return $res;
    }


    public function user_check()
    {
        extract($_POST);
        $res = $this->db->get_where('user', array('user_name' => $user_name));
        if ($res->num_rows() > 0)
            return false;
        return true;
    }

    public function login()
    {
        extract($_POST);
        $user_name = $this->db->escape($user_name);
        $password = $this->db->escape($password);
        $sql = "select * from user where user_name = $user_name and password = MD5($password) and status = '1' ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            $data = array('user_id' => $res->id, 'level' => 2, 'first_name' => $res->first_name, 'last_name' => $res->last_name, 'user_name' => $res->user_name);

            $this->session->set_userdata($data);

            return 1;
        } else
            return 0;
    }

    public function banCheck($temp)
    {
        $user_name = $temp["user_name"];

        $query = $this->db->get_where('user', array('user_name' => $user_name, 'banned' => '1'));
        $query->result_array();
        $res = $query->num_rows();


        return $res;
    }

    public function user_exist()
    {
        extract($_POST);
        $user_name = $this->db->escape($user_name);
        $sql = "select * from user where user_name = $user_name ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            return 1;
        } else
            return 0;
    }

    public function email_exist()
    {
        extract($_POST);

        $email = $this->db->escape($email);
        $sql = "select * from user where email = $email ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            return 1;
        } else
            return 0;
    }

    public function pass_exist()
    {
        extract($_POST);
        $password = $this->db->escape($password);
        $sql = "select * from user where password = $password ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            return 1;
        } else
            return 0;
    }

    public function get_latest()
    {
        //$query = "SELECT g.*,cnt.country_name,s.state,c.city,GROUP_CONCAT(CONCAT(m.name,'-',gof.price)) as price FROM gym g,country cnt,states s,cities c,membership m,gym_offers gof where gof.mem_id = m.id and g.id = gof.gym_id and g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 0 group by g.id order by g.id desc limit 3";   
        $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 1 group by g.id order by g.id desc limit 3";
        return $result = $this->db->query($query)->result();
    }

    public function getgyms()
    {
        extract($_POST);

        $cond = '';
        $condition = array('g.category' => $cat_id, 'g.gcountry' => $cntry, 'g.gstate' => $state, 'g.gymname' => $search_name);
        $cond = "g.category = $cat_id AND g.gcountry = $cntry AND g.gstate = '" . $state . "' ";
        if ($city != '0') {
            $condition['g.gcity'] = $city;
            $cond = $cond . " AND g.gcity = '" . $city . "'";
        }
        if ($zipcode != '' && $zipcode != 0) {
            $condition['g.gzip'] = $zipcode;
            $cond = $cond . " AND g.gzip = '" . $zipcode . "'";
        }

        $firstime = true;
        $lque = '';

        if (isset($adv)) {

            foreach ($adv as $key => $value) {
                if ($firstime)
                    $lque = "g.advanced LIKE '%" . $key . "%'";
                else
                    $lque = $lque . " OR g.advanced LIKE '%" . $key . "%'";
                $firstime = false;
            }

            $query = "SELECT g.*,c.categoryname,country.country_name,states.state
                  FROM gym g JOIN country ON g.gcountry = country.id JOIN states ON g.gstate = states.state_code JOIN cities ON g.gcity = cities.id JOIN category c ON g.category = c.id
                  WHERE ($cond) AND($lque) GROUP BY g.id ORDER BY g.id asc";

            //echo $query;exit;
            return $this->db->query($query)->result();

        } else {
            //echo "select g.id,g.gymname,g.gym_logo,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname from gym g left join category on g.category = category.id left join country on g.gcountry = country.id left join states on g.gstate = states.state_code left join cities on g.gcity = cities.id where g.category = '".$cat_id."' and g.gcountry = '".$cntry."' and g.gstate = '".$state."' and g.gymname = '".$search_name."'" ;
            $result = $this->db->select('g.id,g.gymname,g.gym_logo,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname')
                ->from('gym g')
                ->join('category', 'g.category = category.id', 'left')
                ->join('country', 'g.gcountry = country.id', 'left')
                ->join('states', 'g.gstate = states.state_code', 'left')
                ->join('cities', 'g.gcity = cities.id', 'left')
                ->where($condition)
                ->get()
                ->result();
        }
        //echo $this->db->last_query();exit;
        return $result;
    }

    /*public function getgyms(){
      extract($_POST);               
      $condition = array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city);
        
      if($zipcode)
        $condition['g.gzip'] = $zipcode;
        
            if(isset($adv)){
              $this->db->select('g.id,g.gymname,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname'); 
              $this->db->from('gym g');
              $this->db->join('category','g.category = category.id');
              $this->db->join('country','g.gcountry = country.id');
              $this->db->join('states','gstate = states.state_code'); 
              $this->db->join('cities','g.gcity = cities.id');
              $this->db->where($condition);
              $firstime=true;
              //print_r($adv);exit;
              foreach ($adv as $key => $value){
                if($firstime)
                  $this->db->like('g.advanced',$key,'%');
                else
                  $this->db->or_like('g.advanced',$key,'%');
                $firstime=false;
              }      
                                     
              $result = $this->db->get()->result();
              echo $this->db->last_query();exit;
              //return  $result;
              
            }else{         
              $result = $this->db->select('g.id,g.gymname,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,states.state,cities.city,category.categoryname')   
                             ->from('gym g')
                             ->join('category','g.category = category.id')
                             ->join('country','g.gcountry = country.id')
                             ->join('states','gstate = states.state_code')  
                             ->join('cities','g.gcity = cities.id')
                             ->where($condition)
                             ->get()
                            ->result();
            }
            //echo $this->db->last_query();exit;              
           return $result;                               
    }*/
    public function get_images($gym_id)
    {
        $user_id = $this->session->userdata('user_id');

        $result = $this->db->select('g.*,country.country_name,cities.city,states.state,category.categoryname')
            ->from('gym g')
            ->join('country', 'g.gcountry = country.id')
            ->join('states', 'gstate = states.state_code')
            ->join('cities', 'g.gcity = cities.id')
            ->join('category', 'g.category = category.id')
            ->where('g.id', $gym_id)
            ->group_by('g.id')
            ->get()
            ->row();
        //echo $this->db->last_query();exit;
        return $result;
    }

    public function get_gym_offer($gym_id, $user_id)
    {
        $result = $this->db->select('rof.is_accept as off_req, ofg.offer_id as sel_offer, g.id,g.gymname,g.description,g.gym_logo,g.gym_video,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,country.country_name,cities.city,states.state,category.categoryname')
            ->from('gym g')
            ->join('country', 'g.gcountry = country.id')
            ->join('states', 'gstate = states.state_code')
            ->join('cities', 'g.gcity = cities.id')
            ->join('category', 'g.category = category.id')
            ->join('offers as rof', 'rof.gym_id = g.id AND rof.type = 2 AND rof.user_id = ' . $user_id, 'left')
            ->join('offers as of', 'of.gym_id = g.id', 'left')
            ->join('offered_gyms as ofg', 'ofg.offer_id = of.id AND ofg.user_id = ' . $user_id, 'left')
            ->where('g.id', $gym_id)
            ->group_by('g.id')
            ->get()
            ->row();
        //echo $this->db->last_query();exit;
        return $result;
    }

    public function getgyms_filter($fil_val)
    {
        //print_r($this->session->userdata);
        $gymname = $this->session->userdata('search_name');
        $cntry = $this->session->userdata('cntry');
        $state = $this->session->userdata('state');
        $city = $this->session->userdata('city');

        $zipcode = $this->session->userdata('zipcode');

        $cat_id = $this->session->userdata('cat_id');
        $gend = $this->session->userdata('speciality');
        if (isset($gend) && $gend != 'all')
            $gender = $gend;
        else
            $gender = '';

        $byname = $this->session->userdata('byname');
        $rating = $this->session->userdata('rating');
        $price_range = $this->session->userdata('price');
        if (isset($byname) && $byname != '') {
            $filt = "g.gymname";
        } else if (isset($rating) && $rating != '') {
            $filt = "g.star";
        } else if (isset($price_range) && $price_range != '') {
            $filt = "g.price_range";
        } else {
            $filt = "g.id";
        }
        if ($gymname != '' && $cntry != '') {
            if ($cntry != 0) {
                $condition = array('g.category' => $cat_id, 'g.gcountry' => $cntry, 'g.gstate' => $state, 'g.gcity' => $city, 'g.gymname' => $gymname);
            } else {
                $condition = array('g.category' => $cat_id, 'g.gymname' => $gymname);
            }
            //echo "select g.id,g.gymname,g.gym_logo,g.star,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.price_range,country.country_name,states.state,cities.city from gym g left join country on g.gcountry = country.id left join states on g.gstate = states.state_code left join cities on g.gcity = cities.id where g.category = '".$cat_id."' and g.gcountry = '".$cntry."' and g.gstate = '".$state."' and g.gcity = '".$city."' and g.gymname = '".$gymname."'";
        } elseif ($gymname != '' && $cntry == '') {
            $condition = array('g.category' => $cat_id, 'g.gymname' => $gymname);
        } else {
            $condition = array('g.category' => $cat_id, 'g.gcountry' => $cntry, 'g.gstate' => $state, 'g.gcity' => $city);
        }

        if ($zipcode)
            $condition['g.gzip'] = $zipcode;

        if ($gender != '') $condition['g.speciality'] = $gender;

        $result = $this->db->select('g.id,g.gymname,g.gym_logo,g.star,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.price_range,country.country_name,states.state,cities.city')
            ->from('gym g')
            ->join('country', 'g.gcountry = country.id')
            ->join('states', 'g.gstate = states.state_code')
            ->join('cities', 'g.gcity = cities.id')
            ->where($condition)
            ->order_by($filt, 'asc')
            ->get()
            ->result();
        //echo $this->db->last_query(); exit;
        return $result;
    }

    public function get_cnt()
    {
        extract($_POST);
        $result = $this->db->select('country.country_name,states.state,cities.city,category.categoryname')
            ->from('country,states,cities,category')
            ->where(array('country.id' => $cntry, 'states.state_code' => $state, 'cities.id' => $city, 'category.id' => $cat_id))
            ->get()
            ->row();
        return $result;
    }

    public function storeUserDetails()
    {

        extract($_POST);
        $date = $this->session->userdata('joining_date');

        $data = array(
            'username' => $this->session->userdata('uname'),
            'mobile' => $this->session->userdata('phno'),
            'email' => $this->session->userdata('email'),
            'membership' => $this->session->userdata('mem_id'),
            'cat_id' => $this->session->userdata('cat_id'),
            'gym_id' => $this->session->userdata('gym_id'),
            'price' => $this->session->userdata('price'),
            'offer' => $this->session->userdata('offer'),
            'offer_price' => $this->session->userdata('offer_price'),
            'join_date' => $this->session->userdata('joining_date'),
            'end_date' => $this->session->userdata('end_date'),
            'createdon' => mktime()
        );
        $result = $this->db->insert('user', $data);
        return $result;
    }

    public function getcnt_filter()
    {
        $cntry = $this->session->userdata('cntry');
        $state = $this->session->userdata('state');
        $city = $this->session->userdata('city');

        $cat_id = $this->session->userdata('cat_id');

        $result = $this->db->select('country.country_name,states.state,cities.city,category.categoryname')
            ->from('country,cities,states,category')
            ->where(array('country.id' => $cntry, 'states.state_code' => $state, 'cities.id' => $city, 'category.id' => $cat_id))
            ->get()
            ->row();
        return $result;
    }

    public function get_offer_details()
    {
        extract($_POST);
        $res = $this->db->query("select gym_id,mem_id,price,offer,name from gym_offers o join membership m on o.mem_id = m.id where o.gym_id = '$gym_id'
       and o.mem_id='$mem_id'");
        return $res->row();
    }

    public function getcatgs()
    {
        $result = $this->db->select('id,categoryname')
            ->from('category')
            ->get()
            ->result();

        return $result;
    }

    public function selected_cntry($id)
    {
        $qry = "select city.id,cities.city,city.country_id from city,country WHERE city.country_id = country.id and country.id = $id order by city_name";

        $result = $this->db->query($qry);

        $dataArray = $result->result_array();

        $returnarray = array();

        foreach ($dataArray as $value) {

            $returnarray[$value['id']] = $value['city_name'];

        }
        return $returnarray;
    }

    public function get_mapdet($id)
    {
        $result = $this->db->select('g.id,g.gymname,g.gstate,g.gmap_lat,g.gmap_long,g.gstreet,country.country_name,cities.city')
            ->from('gym g')
            ->join('country', 'g.gcountry = country.id')
            ->join('cities', 'g.gcity = city.id')
            ->where(array('g.id' => $id))
            ->get()
            ->row();
        return $result;
    }

    public function get_offers()
    {
        extract($_POST);
        $query = "SELECT GROUP_CONCAT(CONCAT(m.id,'-',m.name,'-',price,'-',offer,'-',gym_id)) as price
                  FROM gym_offers
                  join membership m ON mem_id = m.id
                  WHERE gym_id = $renew
                  order by mem_id asc ";
        return $result = $this->db->query($query)->row();
    }

    public function get_cntname($flag)
    {
        $result = $this->db->where('name', $flag)->get('country_flags')->row();
        return $result;
    }

    public function get_aboutus($id)
    {
        $result = $this->db->select('cm.id,cc.content,cc.image,cm.menuname')
            ->from('cmsmenu cm')
            ->join('cms_content cc', 'cc.menu_id = cm.id')
            ->where(array('cm.id' => $id))
            ->get()
            ->row();
        return $result;
    }

    public function get_countdown_time()
    {
        $qry = "SELECT UNIX_TIMESTAMP(end_time) - UNIX_TIMESTAMP(now()) as output FROM countdown_timer";
        $result = $this->db->query($qry)->row();
        return $result->output;
    }

    public function get_gymname($gymname)
    {
        $sql = "select gymname from gym where gymname like '%$gymname%' ";
        $res = $this->db->query($sql)->result_array();
        $count = $this->db->query($sql)->num_rows();
        if ($count > 0) {
            foreach ($res as $row) {
                $result[] = $row['gymname'];
            }
            echo json_encode($result);
        }
    }
}

?>