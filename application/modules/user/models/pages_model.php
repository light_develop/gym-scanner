<?php
class Pages_model extends MY_Model {

    public function __construct() {
      parent::__construct();
    }
      
    public function get_cntname($flag){
       $result = $this->db->where('name',$flag)->get('country_flags')->row();
       return $result;    
    }
    
    public function get_aboutus($id){
      $result = $this->db->select('cm.id,cc.content,cc.image,cm.menuname')
                         ->from('cmsmenu cm')
                         ->join('cms_content cc','cc.menu_id = cm.id')
                         ->where(array('cm.id'=>$id))
                         ->get()
                         ->row();
      return $result;
    }
  }

?>
