<?php
class Supplier_model extends MY_Model {

    public function __construct() {
      parent::__construct();
      //$this ->table = 'user';
       $this ->result_mode = 'object';
      }
    public function email_exist()
    {
        extract($_POST);

        $email = $this->db->escape($email);
        $sql = "select * from supplier where email = $email ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            return 1;
        } else
            return 0;
    }

    public function user_exist()
    {
        extract($_POST);
        $user_name = $this->db->escape($user_name);
        $sql = "select * from supplier where user_name = $user_name ";
        $res = $this->db->query($sql)->row();
        $res1 = $this->db->query($sql)->num_rows();
        if ($res1 > 0) {
            return 1;
        } else
            return 0;
    }



      public function get_cats(){
        $res = $this->db->where('status','0')->get('category')->result();
        $cat = array();
        foreach ($res as $value) {
          $cat[$value->id] = $value->categoryname;
        }
        return $cat;
      }
      public function get_cntrs(){
        $res = $this->db->where('status','1')->get('country')->result();
        $cntry = array();
        foreach ($res as $value) {
          $cntry[$value->id] = $value->country_name;
        }
        return $cntry;
      }
      public function get_states($x){
        $res = $this->db->where('country_id',$x)->get('states')->result();
        $states = array();
        foreach ($res as $value) {
          $states[$value->state_code] = $value->state;
        }
        return $states;
      }

      public function get_cities($x){
        $res = $this->db->where('state_code',$x)->get('cities')->result();
        $states = array();
        foreach ($res as $value) {
          $states[$value->id] = $value->city;
        }
        return $states;
      }
      public function get_mems(){
        $res = $this->db->where('status','0')->get('country')->result();
        $cntry = array();
        foreach ($res as $value) {
          $cntry[$value->id] = $value->country_name;
        }
        return $cntry;
      }
      public function sup_register(){
        extract($_POST);
        $data = array('level'=>$level,'first_name'=>$first_name,'last_name'=>$last_name,'user_name'=>$user_name,'password'=>$password,'email'=>$email,'type'=>$type);
        $this->db->insert('supplier',$data);
        $res = $this->db->insert_id();
        /*if($res == 1){
          $id = $this->db->insert_id();
          $user_data = array('user_id'=>$id,'level'=>$level,'type'=>$type);
          $this->session->set_userdata($user_data);
        }*/
        return $res;
      }
      public function sup_check(){
        extract($_POST);
        $res = $this->db->get_where('supplier',array('user_name'=>$user_name));
        if($res->num_rows()>0)
        return false;
        return true;
      }
      public function login(){
        extract($_POST);
        if(isset($level) && $level == 1){
          $sql = "select * from supplier where user_name = '".$user_name."' and password = '".$password."' and level = $level and status = 1";
          $res = $this->db->query($sql)->row();
          $res1 = $this->db->query($sql)->num_rows();
          if($res1 > 0){

            $data = array(
				'user_id'=>$res->id,
				'first_name'=>$res->first_name,
				'last_name'=>$res->last_name,
				'level'=>$res->level,
				'type'=>$res->type,
				'user_name'=>$res->user_name
			);
            $this->session->set_userdata($data);
            return 1;
          }else
            return 0;
        }
        else
          return 0;
      }
      public function get_latest(){
        //$query = "SELECT g.*,cnt.country_name,s.state,c.city,GROUP_CONCAT(CONCAT(m.name,'-',gof.price)) as price FROM gym g,country cnt,states s,cities c,membership m,gym_offers gof where gof.mem_id = m.id and g.id = gof.gym_id and g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 0 group by g.id order by g.id desc limit 3";
        $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.status = 0 group by g.id order by g.id desc limit 3";
        return $result = $this -> db -> query($query) -> result();
      }
      public function getgyms(){

        extract($_POST);
        $supplier = $this->session->userdata('user_id');
        $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.is_supplier = $supplier group by g.id";
        $result = $this->db-> query($query)-> result();
        return $result;
      }

      public function add_gyms() {
        extract($_POST);
		if($open_time != ''){
	   	    $opentime = $open_time.",".$before.",".$open_time1.",".$before1.",".$open_time2.",".$before2.",".$open_time3.",".$before3.",".$open_time4.",".$before4.",".$open_time5.",".$before5.",".$open_time6.",".$before6;
		}else{
			$opentime = ',,,,,,,,,,,,,';
		}
		if($close_time != ''){
		    $closetime =  $close_time.",".$after.",".$close_time1.",".$after1.",".$close_time2.",".$after2.",".$close_time3.",".$after3.",".$close_time4.",".$after4.",".$close_time5.",".$after5.",".$close_time6.",".$after6;
		}else{
			$closetime = ',,,,,,,,,,,,,';
		}
		/*echo '<pre>';
		print_r($_POST);
		echo '<pre>';
		print_r($_FILES);
		exit;*/
        $udfilename = '';
        $image = $video = '';

        $files = $_FILES;
        $this->load->library('upload');
/*
$p_image = $_FILES['upload_file']['name'];

print_r($p_image);
$i_img = 0;
foreach($p_image as $row)
{
	$image_info = getimagesize($_FILES['upload_file']['tmp_name'][$i_img]);
	$image_width = $image_info[0];
	$image_height = $image_info[1];


	if( ($image_width < 520 || $image_width > 480) && ($image_height < 320 || $image_height > 280) )
	{

	}
	$i_img++;
}
*/
            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            $rand = rand(10000, 80000);
            $new_img = 'logo_img_' . $rand . '.' . $ext;

            $gym_logo_img = substr_replace($new_img , 'png', strrpos($new_img , '.') +1);
            $new_path="./uploads/gym/logos/".$gym_logo_img;
            if(move_uploaded_file($files['logo']['tmp_name'],$new_path)){
              $gym_logo = $gym_logo_img;
            }
            if(isset($gym_video)){
              $videos = array();
              foreach ($gym_video as $key => $value) {
                if($value != '')
                  $videos[] = $value;
              }
              $videos = implode('~', $videos);
            }else
              $videos = '';


            if(isset($adv)){
            $output=array();
              foreach ($adv as $key => $value) {
                  $output[]=$key;
              }
              $output = implode(',', $output);
            }else{
              $output = '';
            }
            if(isset($cert)){
              $certs = implode('~',$cert);
            }
            //print_r($output); die();
			//print_r($_POST);exit;
            $edata = array('gymname'=>ucfirst($gymname),
                           'gym_logo'=>$gym_logo,
                           'gym_video'=>$videos,
                           'open_time'=>$opentime,
                           'close_time'=>$closetime,
                           'description'=>$description,
                           'gstreet'=>$gstreet,
                           'gcity'=>$city,
                           'gstate'=>$state,
                           'gcountry'=>$cntry,
                           'gzip'=>$gzip,
                           'location'=>$search_location,
                           'gmap_lat'=>$latitude,
                           'gmap_long'=>$longitude,
                           'speciality'=>$speciality,
                           'category'=>$cat_id,
                           'advanced'=>$output,
                           'owner'=>$owner,
                           'ow_phone'=>$ow_phone,
                           'ow_email'=>$ow_email,
                           'is_supplier'=>$this->session->userdata['user_id'],
                           'certifications'=>@$certs,
                           'site_url'=>@$site_url,
                           'createdon'=>mktime());

            $this->db->insert('gym', $edata);
            $new_id = $this->db->insert_id();

           $p_image = $_FILES['upload_file']['name'];
            $i = 0;
            foreach ($p_image as $img) {

				$image_info = getimagesize($_FILES['upload_file']['tmp_name'][$i]);

				$image_width = $image_info[0];
				$image_height = $image_info[1];


				if( ($image_width < 520 || $image_width > 480) && ($image_height < 320 || $image_height > 280) )
				{


                $ext = pathinfo($img, PATHINFO_EXTENSION);


                $rand = rand(100000, 800000);

                $new_img = 'package_' . $rand . '.' . $ext;

                $udfilename = substr_replace($new_img , 'png', strrpos($new_img, '.') +1);

                $tmpFilePath = $_FILES['upload_file']['tmp_name'][$i];


                //Make sure we have a filepath
                if ($tmpFilePath != "") {
                    //Setup our new file path
					$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
					$max_file_size = 200 * 1024 ; #200kb
					$nw = $nh = 200; # image with & height

					//&& $_FILES['upload_file']['size'][$i] < $max_file_size
					if ( (! $_FILES['upload_file']['error'][$i]) ) {
						$ext = strtolower(pathinfo($_FILES['upload_file']['name'][$i], PATHINFO_EXTENSION));
						if (in_array($ext, $valid_exts)) {
							$newFilePath = "./uploads/gym/images/" . $udfilename;

						 $size = getimagesize($_FILES['upload_file']['tmp_name'][$i]);

						  # grab data form post request
						  $x = (int) $_POST['x'][$i];
						  $y = (int) $_POST['y'][$i];
						  $w = (int) $_POST['w'][$i];
						  $h = (int) $_POST['h'][$i];
						  # read image binary data
						  $data = file_get_contents($_FILES['upload_file']['tmp_name'][$i]);
						  # create v image form binary data

						  @$dstImg = imagecreatetruecolor($w, $h);
						  $vImg = imagecreatefromstring($data);
						  # copy image
						  @imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $w, $h, $w, $h);
						  # save image
						  @imagejpeg($dstImg, $newFilePath);
						  # clean memory
						  @imagedestroy($dstImg);

						  //echo "<img src='$newFilePath' />";
						}
					}else{

					}
					if($udfilename != ''){
						$img = $udfilename;
					}else{
						$img = '';
					}

                    //Upload the file into the temp dir
                    //if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                       // $img = $udfilename;
                    //}else{
                     // $img = '';
                    //}                          
                }else{

				}
                $data = array('gym_id' => $new_id, 'gym_images' => $img);
                $this->db->insert("gym_images", $data);

				}
                $i++;
            }
			/*$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
			$max_file_size = 200 * 1024; #200kb
			$nw = $nh = 200; # image with & height
			if ( isset($_FILES['upload_file']) ) {
			print "<pre>";
			print_r($_POST);
			print_r($_FILES);
				if (($_FILES['upload_file']['name'][0]!="") ) { // && $_FILES['upload_file']['size'] < $max_file_size
				  # get file extension
				  print $ext = strtolower(pathinfo($_FILES['upload_file']['name'][0], PATHINFO_EXTENSION));
				  # file type validity
				  if (in_array($ext, $valid_exts)) {
					  $path = './uploads/gym/images/' . uniqid()  . '.' . $ext;
					  $size = getimagesize($_FILES['upload_file']['tmp_name'][0]);
					  print_r($size);
					  # grab data form post request
					  $x = (int) $_POST['x'];
					  $y = (int) $_POST['y'];
					  $w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
					  $h = (int) $_POST['h'] ? $_POST['h'] : $size[1];
					  echo $w .'<br>';
					  echo $h .'<br>';
					  echo $nw .'<br>';
					  echo $nh .'<br>';
					  echo $x.'<br>';
					  echo $y.'<br>';
					  # read image binary data
					  $data = file_get_contents($_FILES['upload_file']['tmp_name'][0]);
					  # create v image form binary data
					  $dstImg = imagecreatetruecolor(100, 100);
					  $vImg = imagecreatefromstring($data);

					  # copy image
					  imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, 100, 100, $w, $h);
					  # save image
					  imagejpeg($dstImg, $path);
					  # clean memory
					  imagedestroy($dstImg);
					  echo "<img src='$path' />";
					  exit;

					} else {
					  echo 'unknown problem!';
					}
				} else {
				  echo 'file is too small or large';
				}
			  } else {
				echo 'file not set';
			  }*/
        return true;
      }
      public function edit_gym(){
        extract($_POST);
		if($open_time != ''){
	   	    $opentime = $open_time.",".$before.",".$open_time1.",".$before1.",".$open_time2.",".$before2.",".$open_time3.",".$before3.",".$open_time4.",".$before4.",".$open_time5.",".$before5.",".$open_time6.",".$before6;
		}else{
			$opentime = ',,,,,,,,,,,,,';
		}
		if($close_time != ''){
		    $closetime =  $close_time.",".$after.",".$close_time1.",".$after1.",".$close_time2.",".$after2.",".$close_time3.",".$after3.",".$close_time4.",".$after4.",".$close_time5.",".$after5.",".$close_time6.",".$after6;
		}else{
			$closetime = ',,,,,,,,,,,,,';
		}
        $udfilename = '';
        $image = '';
        $files = $_FILES;
        $this->load->library('upload');
        if($_FILES['logo']['name'] != ''){

            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            $rand = rand(10000, 80000);
            $new_img = 'logo_img_' . $rand . '.' . $ext;

            $gym_logo_img = substr_replace($new_img , 'png', strrpos($new_img , '.') +1);
            $new_path="./uploads/gym/logos/".$gym_logo_img;
            if(move_uploaded_file($files['logo']['tmp_name'],$new_path)){
              $gym_logo = $gym_logo_img;
              unlink('./uploads/gym/logos/'.$old_logo);
            }
        }
        else{
            $gym_logo = $old_logo;
        }
        if(isset($gym_video)){
              $videos = array();
              foreach ($gym_video as $key => $value) {
                if($value != '')
                  $videos[] = $value;
              }
              $videos = implode('~', $videos);
            }else
              $videos = '';

          @$p_image = $_FILES['upload_file']['name'];
          if(isset($_FILES) && $p_image != ''){
            $i = 0;
            foreach ($p_image as $img) {

                $ext = pathinfo($img, PATHINFO_EXTENSION);
                $rand = rand(100000, 800000);
                $new_img = 'package_' . $rand . '.' . $ext;
                $udfilename = substr_replace($new_img , 'png', strrpos($new_img, '.') +1);
                $tmpFilePath = $_FILES['upload_file']['tmp_name'][$i];
                //Make sure we have a filepath
                if ($tmpFilePath != "") {
                    //Setup our new file path
                    $newFilePath = "./uploads/gym/images/" . $udfilename;
                    //Upload the file into the temp dir
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                      $data = array('gym_id' => $id, 'gym_images' => $udfilename);
                      $this->db->insert("gym_images", $data);
                    }
                }

                $i++;
            }
          }
            if(isset($adv)){
              $output=array();
                foreach ($adv as $key => $value) {
                    $output[]=$key;
                }
                $output = implode(',', $output);
              }else{
                $output = '';
              }
            if(isset($cert)){
              $certs = implode('~',$cert);
            }
                $edata['gymname']       = ucfirst($gymname);
                $edata['gym_logo']      = $gym_logo;
                $edata['gym_video']     = $videos;
                $edata['open_time']     = $opentime;
                $edata['close_time']    = $closetime;
                $edata['description']   = $description;
                $edata['gstreet']       = $gstreet;
                $edata['gcity']         = $city;
                $edata['gstate']        = $state;
                $edata['gcountry']      = $cntry;
                $edata['gzip']          = $gzip;
                $edata['location']      = $search_location;
                $edata['gmap_lat']      = $latitude;
                $edata['gmap_long']     = $longitude;
                $edata['speciality']    = $speciality;
                $edata['category']      = $cat_id;
                $edata['advanced']      = $output;
                $edata['owner']         = $owner;
                $edata['ow_phone']      = $ow_phone;
                $edata['ow_email']      = $ow_email;
                $edata['is_supplier']   = $this->session->userdata['user_id'];
                $edata['certifications']= @$certs;
                $edata['site_url']      = @$site_url;

                $this->db->where(array('id'=>$id))
                         ->update('gym',$edata);
        return;
      }
      public function get_det($x){
        $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id and g.id = $x";
        return $result = $this->db->query($query) -> row();
      }
      public function insert_det(){
        extract($_POST);
        $data = array('first_name'=>$first_name,'last_name'=>$last_name,'user_name'=>$user_name,'password'=>$password,'email'=>$email,'reffered_by'=>$this->session->userdata['user_id']);
        $this->db->insert('user',$data);
        return true;
      }
      public function get_users(){
        //$res = $this->db->select('u.*')->from('user u')->join('gym g','g.id = u.gym_id')->where('g.is_supplier',$this->session->userdata['user_id'])->get()->result();
        $res = $this->db->where('reffered_by',$this->session->userdata['user_id'])->get('user')->result();
        return $res;
      }
      public function get_user($x){
        return $this->db->where('id',$x)->get('user')->row();
      }
      public function update_det(){
        extract($_POST);
        $data = array('first_name'=>$first_name,'last_name'=>$last_name,'user_name'=>$user_name,'password'=>$password,'email'=>$email,'reffered_by'=>$this->session->userdata['user_id']);
        $this->db->where('id',$uid)->update('user',$data);
        return true;
      }
      public function view_users($x){
        return $this->db->select('u.*')->from('user u')->join('gym_customers g','g.user_id = u.id')->where('g.gym_id',$x)->get()->result();
        //SELECT u.* FROM (user u) JOIN gym_customers g ON g.user_id = u.id WHERE g.gym_id = 8
        //echo $this->db->last_query(); exit;
      }
  }

?>
