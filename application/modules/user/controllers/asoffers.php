<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();
error_reporting(0);
$apiLoginID = "2EEA5870";// Get API ID from TaxCloud
$apiKey = "B8283100-9A7D-4B9B-BEEF-F1DDB6DA46CA";//Get API KEY from TaxCloud

class Adapter
{
    const WSDL = "https://api.taxcloud.net/1.0/?wsdl";
    const API_ID = "2EEA5870";
    const API_KEY = "B8283100-9A7D-4B9B-BEEF-F1DDB6DA46CA";

    public function __construct($gzip = null, $gstate = null, $price = null)
    {
        $this->gzip = $gzip;
        $this->gstate = $gstate;
        $this->price = $price;
    }

    public function getSoapClient()
    {
        return new SoapClient(self::WSDL);
    }

    public function authorize()
    {
        $_client = $this->getSoapClient();

        $cartItem = new StdClass();
        $cartItem->ItemID = "2"; //$this->_getTime();
        $cartItem->Index = 0;
        $cartItem->displayName = "displayName";
        $cartItem->TIC = "0000";
        $cartItem->Price = $this->price;
        $cartItem->Qty = 1;

        $cartItems = array($cartItem);
        $cart = new StdClass();
        $cart->cartID = 12;
        $cart->cartItems = $cartItems;

        $wrapper = new StdClass();
        $wrapper->deliveredBySeller = false;
        $wrapper->cartItems = $cartItems;
        $wrapper->customerID = 12;

        $address = new StdClass();
        $address->Address1 = "3205 South Judkins Street";
        $address->City = "Minnesota";
        $address->State = $this->gstate;
        $address->Zip5 = $this->gzip;

        $du = new StdClass();
        $du->Address1 = "3206 South Judkins Street";
        $du->City = "Minnesota";
        $du->State = $this->gstate;
        $du->Zip5 = $this->gzip;

        $wrapper->apiKey = self::API_KEY;
        $wrapper->apiLoginID = self::API_ID;
        $wrapper->origin = $address;
        $wrapper->destination = $du;
//convert to array result
        return (object)$_client->__soapCall("Lookup", array($wrapper));
    }

    protected function _getTime()
    {
        $time = new DateTime();
        return $time->format('Y.m.d H:i:s');
    }

    protected function _prepareRequest($request)
    {
        $result = array();

        foreach ($request as $param => $value) {
            switch (gettype($value)) {
                case "string":
                    $result[] = new SoapVar($value, XSD_STRING, null, null, $param);
                    break;
                case "object":
                    $result[] = new SoapVar($value, XSD_DATE, null, null, $param);
                    break;
            }

        }

        return $result;
    }
}


/*$adapter = new Adapter();*/

/*var_dump($adapter->authorize());*/


class Asoffers extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('offers_model', 'offers');
        $this->load->model('gym_model', 'gym');
        $this->load->model("user_model", 'user');

        $this->data['page'] = 'as_offers';
        $this->data['user_id'] = $this->session->userdata('user_id');

    }

    public function index()
    {
        //echo 'hi';exit;
        //$this->data['gyms'] = $this->offers->get_gym( $this->data['user_id']);

        $this->data['offers'] = $this->offers->get_all($this->data['user_id']);

        $this->data['mode'] = 'all';
        $this->load->view('template', $this->data);

    }

    public function add_offer()
    {
        //echo site_url('offers');
        //print_r($this->offers->get_gym( $this->data['user_id']));
        $this->data['gyms'] = $this->offers->get_gym($this->data['user_id']);
        //print_r($this->data['gyms']);
        $this->data['page'] = 'add_offer';
        $this->load->view('template', $this->data);
    }

    public function add()
    {
        if (isset($_POST['gym']) && !empty($_POST['gym'])) {
            extract($_POST);
            $data = array('gym_id' => $gym, 'user_id' => $this->session->userdata('user_id'));
            if ($this->db->insert('offer_relation', $data)) {
                $ofr_id = $this->db->insert_id();
                $lng = 6;
                $ref = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $lng);
                //echo $dr = strtotime(date('Y-m-d H:i:s'));
                foreach ($duration as $key => $value) {
                    $description = '';
                    if (trim($value) != '') {
                        $description = $value . ' Membership + ' . $offer[$key];
                        if (trim($membership[$key]) != '')
                            $description .= ' for US$ ' . ($membership[$key] - ($membership[$key] * $discount[$key] / 100));
                        $rdata = array(
                            'offer_relation_id' => $ofr_id,
                            'duration' => $value,
                            'membership' => $membership[$key],
                            'discount' => $discount[$key],
                            'offer' => $offer[$key],
                            'description' => $description,
                            'reference' => $ref . strtotime(date('Y-m-d H:i:s')),
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('offer_description', $rdata);
                    }
                }
            }
        } else {
            extract($_POST);
            /*print_r($duration);
            print_r($result);*/
            foreach ($result as $res) {
                //echo "else foreach";
                $data = array('gym_id' => $res, 'user_id' => $this->session->userdata('user_id'));

                if ($this->db->insert('offer_relation', $data)) {
                    $ofr_id = $this->db->insert_id();
                    $lng = 6;
                    $ref = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $lng);
                    //echo $dr = strtotime(date('Y-m-d H:i:s'));
                    foreach ($duration as $key => $value) {
                        $description = '';
                        if (trim($value) != '') {
                            $description = $value . ' Membershiddp + ' . $offer[$key];
                            if (trim($membership[$key]) != '')
                                $description .= ' for US$ ' . ($membership[$key] - ($membership[$key] * $discount[$key] / 100));

                            $rdata = array(
                                'offer_relation_id' => $ofr_id,
                                'duration' => $value,
                                'membership' => $membership[$key],
                                'discount' => $discount[$key],
                                'offer' => $offer[$key],
                                'description' => $description,
                                'reference' => $ref . strtotime(date('Y-m-d H:i:s')),
                                'created_on' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('offer_description', $rdata);
                        }
                    }//duration foreach
                }
            } //else foreach
        }


        $this->session->set_flashdata('success', 'Successfully Added');
        redirect(site_url('facility_offers'));
    }

    public function edit($id)
    {

        $this->data['gyms'] = $this->offers->get_gym($this->data['user_id']);
        $res = $this->db->where('id', $id)->get('offer_relation')->num_rows();
        if ($res > 0) {
            $this->data['details'] = $this->offers->get_details($id);
            $this->data['page'] = 'edit_offer';
            $this->load->view('template', $this->data);
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function update_offr()
    {
        if (isset($_POST['gym']) && !empty($_POST['gym'])) {
            extract($_POST);
            if ($gym != $old_gym)
                $this->db->where('id', $relation_id)->update('offer_relation', array('gym_id' => $gym));
            foreach ($duration as $key => $value) {
                $description = '';
                if (trim($value) != '') {
                    $description = $value . ' Membership + ' . $offer[$key];
                    if (trim($membership[$key]) != '')
                        $description .= ' for US$ ' . ($membership[$key] - ($membership[$key] * $discount[$key] / 100));
                    $rdata = array(
                        'duration' => $value,
                        'membership' => $membership[$key],
                        'discount' => $discount[$key],
                        'offer' => $offer[$key],
                        'description' => $description
                    );

                    if (isset($offr_id[$key]))
                        $this->db->where('id', $offr_id[$key])->update('offer_description', $rdata);
                    else {
                        $rdata['offer_relation_id'] = $relation_id;
                        $rdata['created_on'] = date('Y-m-d H:i:s');
                        $this->db->insert('offer_description', $rdata);
                    }
                }
            }
            $this->session->set_flashdata('success', 'Updated Successfully');
            redirect('user/asoffers');
        }
    }

    public function check_gym($gym)
    {
        $res = $this->db->where('gym_id', $gym)->get('offer_relation')->num_rows();
        //print_r($this->db->where('gym_id',$gym)->get('offer_relation'));
        if ($res > 0)
            echo 'Offer already exists for this facility. Please choose "View Current Offers" option from the Dashboard to make changes.';
        else
            echo '';
    }

    public function review_ofr()
    {
        $ofr_id = $_POST['ofr_id'];
        $ref = $_POST['reference'];
        $gym_id = $_POST['gym_id'];
        $hide_param = $_POST['hide_param'];

        $uid = $this->session->userdata['user_id'];
        $gym_det = $this->db->where('id', $gym_id)->get('gym')->row();
        $res = $this->db->where('id', $uid)->get('supplier')->row();

        $this->db->where('id', $ofr_id)->update('offer_description', array('status' => '0'));
        /****** email config settings ******/
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // mail sending process //

        $this->email->from($res->email, 'New Offer');
        $this->email->to('admin@gymscanner.com');
        $this->email->subject('New Offer Posted');

        $val = "<html><body>";
        $val .= "<h4>Dear Gymscanner Team,</h4>";
        $val .= "<p>Client " . $gym_det->gymname . " has posted an offer that needs to go live right now. Please review it to make sure it conforms with gymscanner.com policies and publish it as soon as possible.</p>";
        $val .= "<div><br/>";
        $val .= "<p><b>Client name :</b>" . $gym_det->gymname . "</p>";
        $val .= "<p><b>Contact :</b>" . $res->first_name . "</p>";
        $val .= "<p><b>Email :</b>" . $res->email . "</p>";
        $val .= "<p><b>Contact telephone :</b>" . $gym_det->ow_phone . "</p>";
        $val .= "<p><b>Offer reference number :</b>" . $ref . "</p>";
        $val .= "</div>";
        $val .= "<p><a href='" . base_url() . "admin/'>click here</a></p>";
        $val .= "<p style='font-size:10pt;font-family: Arial;'>Gymscanner.com</p>";
        $val .= "</body></html>";

        $this->email->message($val);
        $this->email->send();
        // end mail sending process //
        /****** email config settings ******/
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // mail sending process //

        $this->email->from('admin@gymscanner.com', 'Gym Scanner');
        $this->email->to($res->email);
        $this->email->subject('Waiting Approval');

        $val = "<html><body>";
        $val .= "<h4>Dear " . $res->first_name . ",</h4>";
        $val .= "<p>Thank you for creating your facility offer on Gymscanner.com</p>";
        $val .= "<p>Your offer will go live as soon as it is approved by Gymscanner.com's team. This usually takes a few minutes, but can take longer depending on traffic.</p>";
        $val .= "<p>As soon as your offer is made live and ready to accept customers, you will be notified via email.</p>";
        $val .= "<p>From everyone here at Gymscanner.com, we thank you.</p>";
        $val .= "<p style='font-size:10pt;font-family: Arial;'>With best regards,<br>Gymscanner.com</p>";
        $val .= "</body></html>";

        $this->email->message($val);
        $this->email->send();
        // end mail sending process //
        echo '1';
    }

    public function rev_ofr()
    {
        $ofr_id = $_POST['ofr_id'];
        $ref = $_POST['reference'];
        $gym_id = $_POST['gym_id'];
        echo $ref;
    }

    public function change_status()
    {
        $status = $_POST['status'];
        $status = ($status == 0) ? '1' : '0';
        $this->db->where('id', $_POST['id'])->update('offer_description', array('status' => $status));
    }

    public function del_row()
    {
        $id = $_POST['id'];
        $res = $this->db->where('offr_id', $id)->get('gym_customers')->num_rows();
        if ($res > 0)
            echo 1;
        else {
            $this->db->where('id', $id)->delete('offer_description');
            echo 0;
        }
    }

    public function del_offr()
    {
        $id = $_POST['id'];
        $res = $this->db->select('c.id')->from('gym_customers c')->join('offer_description d', 'd.id = c.offr_id')->join('offer_relation o', 'd.offer_relation_id = o.id')->where('o.id', $id)->get()->num_rows();
        if ($res > 0)
            echo 1;
        else {
            $this->db->where('id', $id)->delete('offer_relation');
            $this->db->where('offer_relation_id', $id)->delete('offer_description');
            echo 0;
        }
    }

    public function view_offers($id)
    {
        $res = $this->db->where('id', $id)->get('gym')->num_rows();
        if ($res > 0) {
            $this->session->set_userdata('gym_id', $id);
            $data['gym_det'] = $this->db->where('id', $id)->get('gym')->row();
            $data['gym_offers'] = $this->db->select('o.*')->from('offer_description o')->join('offer_relation r', 'r.id=o.offer_relation_id')->where(array('r.gym_id' => $id, 'o.status' => '1'))->get()->result();
            $data['page'] = 'gym_offers';
            $this->load->view('template', $data);
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function offr_submit()
    {
        if (isset($_POST['gym_id']))
            /* var_dump($_POST['gym_id']);*/
            $this->session->set_userdata('gym_id', $_POST['gym_id']);
        if (isset($_POST['join_date']))
            $this->session->set_userdata('join_date', $_POST['join_date']);
        if (isset($_POST['offer']))
            $this->session->set_userdata('offr_id', $_POST['offer']);
        else
            $this->session->set_userdata('offr_id', '0');
        if (isset($_SESSION['msg'])) {
            $this->session->set_flashdata('error', $_SESSION['msg']);
            unset($_SESSION['msg']);
            redirect(site_url('choose_ofr'), 'refresh');
        }
        $data['page'] = 'payment';
        $data['gym_name'] = $this->db->select('gymname,price')->from('gym')->where('id', $this->session->userdata['gym_id'])->get()->row();
        $data['ofr_det'] = $this->db->where('id', $this->session->userdata['offr_id'])->get('offer_description')->row();
        $price = $data['ofr_det']->membership * (1 - $data['ofr_det']->discount / 100);
        $data['tax_gym_gzip'] = $this->db->select('gzip')->get_where('gym', array('id' => $_POST['gym_id']))->result_array();
        $data['tax_gym_gstate'] = $this->db->select('gstate')->get_where('gym', array('id' => $_POST['gym_id']))->result_array();
        $data['price'] = $price;
        $tax = new Adapter($data['tax_gym_gzip'][0]['gzip'], $data['tax_gym_gstate'][0]['gstate'], $price);
        $result_taxcloud_request = $tax->authorize();

        if (isset($result_taxcloud_request->LookupResult->CartItemsResponse->CartItemResponse->TaxAmount)) {
            $data['tax'] = (float)$result_taxcloud_request->LookupResult->CartItemsResponse->CartItemResponse->TaxAmount;
            $data['total_price'] = (float)$data['tax'] + (float)$data['price'];

        } else {
            $data['tax'] = 0;
        }

        $this->load->view('template', $data);
    }

    public function pay_sub()
    {
        //print_r($this->session->all_userdata());
        //print_r($_SESSION['msg']);
        $this->session->set_flashdata('error', $_SESSION['msg']);
        $data['page'] = 'payment';
        $this->load->view('template', $data);
    }

    public function payment($amountPay = null)
    {
        //error_reporting(0);
        //print_r($_POST);
        //print_r($this->session->all_userdata());exit;
        include('lib/Stripe.php');
        $stripe = array(
            "secret_key" => "sk_test_xgctLrPVjhz4aFHAPFaC5NWH",
            "publishable_key" => "pk_test_nGGZ7SLPUISwLsJw9gWjd2UJ"
        );

        Stripe::setApiKey($stripe['secret_key']);

        $token = $_POST['stripeToken'];


        $customer = Stripe_Customer::create(array(
            'email' => $_POST['email'],
            'card' => $token
        ));
        if ($this->session->userdata['offr_id'] == '0') {
            $dets = $this->db->where('id', $this->session->userdata['gym_id'])->get('gym')->row();
            /* $amt = $dets->price * 100;*/

        } else {
            $dets = $this->db->where('id', $this->session->userdata['offr_id'])->get('offer_description')->row();
            /* $amt = ($dets->membership - ($dets->membership * $dets->discount / 100)) * 100;*/

        }
        //echo $amt;exit;
        $charge = Stripe_Charge::create(array(
            'customer' => $customer->id,
            'amount' => $amountPay,//old var $amt
            'currency' => 'usd'
        ));
        if (isset($charge)) {
            $pin = 'gym-' . substr(str_shuffle(strtotime(date('H:i:s'))), 0, 4);
            $ch = (array)$charge;
            $res = array();
            $i = 1;
            foreach ($ch as $key => $val):
                //print_r($val); echo "##";exit;
                if ($i == 2) {
                    if (isset($val['id']))
                        $res['id'] = $val['id'];
                    if (isset($val['paid']))
                        $res['paid'] = $val['paid'];
                    if (isset($val['amount']))
                        $res['amount'] = $val['amount'];
                    if (isset($val['currency']))
                        $res['currency'] = $val['currency'];
                }
                $i++;
            endforeach;
            //print_r($res); exit;
            if (isset($this->session->userdata['type']))
                $table = 'supplier';
            else
                $table = 'user';
            $data = array('user_id' => $this->session->userdata['user_id'],
                'gym_id' => $this->session->userdata['gym_id'],
                'offr_id' => $this->session->userdata['offr_id'],
                'joining_date' => $this->session->userdata['join_date'],
                'transaction_id' => $res['id'],
                'ofr_pin' => $pin,
                'amount' => $res['amount'] / 100);
            if ($this->db->insert('gym_customers', $data)) {
                $sus_msg = 'Successfully charged $' . $charge->amount / 100;
                $this->session->set_flashdata('success', $sus_msg);
                $cus_id = $this->db->insert_id();
                $det = $this->db->select('u.first_name,u.last_name,u.email,o.duration,o.membership,o.discount,o.offer,o.description,c.joining_date,c.ofr_pin,c.transaction_id,c.amount,g.gymname,g.ow_email,g.ow_phone,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,city.city,s.state,cnt.country_name')
                    ->from('gym_customers c')
                    ->join('offer_description o', 'o.id = c.offr_id', 'left')
                    ->join($table . ' as u', 'u.id = c.user_id')
                    ->join('gym g', 'g.id = c.gym_id')
                    ->join('country cnt', 'g.gcountry = cnt.id')
                    ->join('states s', 'g.gstate = s.state_code')
                    ->join('cities city', 'g.gcity = city.id')
                    ->where('c.id', $cus_id)
                    ->get()->row();
                //echo $this->db->last_query();
                //echo $cus_id;
                //print_r($det);echo "<br>";
                $duration = $det->duration != '' ? $det->duration : '1 month';
                if ($det->discount != '') {

                    //$discount = $det->discount != ''?$det->discount:'0';
                    $spe_data = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Special Offer:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $det->discount . '% discount</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
                } else {
                    $spe_data = '';
                }
                if ($det->offer != '') {

                    //$discount = $det->discount != ''?$det->discount:'0';
                    $spe_ofr = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Exclusive Perk:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $det->offer . '</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
                } else {
                    $spe_ofr = '';
                }
                $date = date_create($det->joining_date);
                date_add($date, date_interval_create_from_date_string("$duration"));
                $end_date = date_format($date, 'Y-m-d');
                $map_img = "http://maps.googleapis.com/maps/api/staticmap?center=" . $det->gmap_lat . "," . $det->gmap_long . "&zoom=13&size=600x300&maptype=roadmap&markers=color:blue|label:S|" . $det->gmap_lat . "," . $det->gmap_long;
                $logo_img = base_url() . 'themes/user/images/logo.png';
                $data['det'] = $det;
                $data['end_date'] = $end_date;
                //$data['map_img'] = $map_img;
                $data['spe_data'] = $spe_data;
                $data['spe_ofr'] = $spe_ofr;
                $data['duration'] = $duration;
                //echo 'img src="'.$map_img.'"';exit;
                //$det = $this->db->where('id',$this->session->userdata['user_id'])->get('user')->row();
                //$cust_det = $this->db->where('id',$cus_id)->get('gym_customers')->row();
                //$cust_det = $this->db->where('id',$cus_id)->get('gym_customers')->row();


                /****** email config settings ******/
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                // mail sending process //

                $this->email->from('admin@gymscanner.com', 'Gym Scanner');
                $list = array($_POST['email'], 'order@gymscanner.com');
                $this->email->to($list);
                //$this->email->to($_POST['email']);
                $this->email->subject('Gymscanner order receipt');

                $val = '<html><head><meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Thanks</title>
</head>

<body>

<table class="ecxmaincontent" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="720">
  <tr>
    <td colspan="3" style="text-align: center; color: #003580; font-weight: bold; font-size: 14px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 18px">
    <font color="#333333">Thanks, ' . ucfirst($det->first_name) . '! Your membership is now confirmed.
    </font></td>
  </tr>
  <tr>
    <td colspan="3" valign="top">
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="ecxlogo_section" colspan="1">
        <table border="0" width="100%">
          <tr>
            <td width="102">
            <img border="0" src="' . $logo_img . '" width="430" height="74"></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td width="356">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="356">&nbsp;</td>
  </tr>
  <tr>
    <td style="width: 360px; padding-bottom: 20px" valign="top">
    <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-top: 10px" valign="top">
            <font color="#333333"><b>Booking number: </b></font>
            </td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-right: 12px; padding-top: 10px" align="right" valign="top">
            <font color="#333333">' . substr($det->transaction_id, 3) . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>PIN Code: </b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">' . $det->ofr_pin . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>Email:</b></font></td>
            <td id="ecxemailAddress" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">' . $det->email . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px; padding-bottom: 10px" valign="top">
            <font color="#333333"><b>Booked by:</b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px; padding-bottom: 10px" align="right" valign="top">
            <font color="#333333">' . ucfirst($det->first_name) . ' ' . ucfirst($det->last_name) . ' </font></td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-top: 10px" valign="top">
            <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
                <font color="#333333"><b>Your reservation:</b></font></td>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $duration . ' membership subscription at ' . ucfirst($det->gymname) . '</font>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            ' . $spe_data . '
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            ' . $spe_ofr . '
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-bottom: 10px" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="color: #003580; font-weight: bold; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; border-left: 1px solid #003580; padding-left: 12px; background-color: #E3E3E1" valign="top">
            <font color="#333333">Total Price</font></td>
            <td style="color: #003580; font-weight: normal; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; white-space: nowrap; border-right: 1px solid #003580; padding-right: 12px; background-color: #E3E3E1" align="right" valign="top">
            <font color="#333333">US$ ' . $det->amount . '/- </font></td>
          </tr>
          <tr>
            <td colspan="2" style="color: #000; font-weight: normal; font-size: 11px; font-family: Helvetica, Arial, Sans-serif; line-height: 14px; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; background-color: #E3E3E1" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" style="color: #333; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-bottom: 1px solid #003580; padding-left: 6px; padding-right: 12px; padding-bottom: 10px; background-color: #E3E3E1">
            <p class="ecxcurrency_disclaimer_message" style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Details:</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Start date: ' . $det->joining_date . '<br>
            End Date: ' . $end_date . '</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td style>&nbsp;</td>
    <td style valign="top">
    <table style="color: #003580; padding: 10px" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" style="color: #003580; font-weight: bold; font-size: 16px; padding-bottom: 10px">
        <font color="#333333">' . ucfirst($det->gymname) . ', ' . ucfirst($det->city) . '</font><br>
&nbsp;</td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Address:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">' . ucfirst($det->gstreet) . ', ' . ucfirst($det->city) . '<br>
        ' . ucfirst($det->state) . ', ' . $det->gzip . '</font></td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Phone:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">+1 ' . $det->ow_phone . '</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold" valign="top">
        <font color="#333333">Email:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">' . $det->ow_email . '</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; padding-bottom: 10px" valign="top">
        <font color="#333333">Getting there:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; padding-bottom: 10px" valign="top">
        <font color="#333333">Show directions</font> </td>
      </tr>
      <tr>
        <td class="ecxgooglemap" colspan="2" style="height: 255px" align="center">
        <img src="' . $map_img . '" style="display: block" border="0" height="255" width="354"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td class="ecxmybooking_bar" colspan="3">
    <table style="color: #FFF; font-family: Helvetica, Arial, Sans-serif; font-size: 15px; line-height: 15px; font-weight: normal; text-align: center; background: #333333" cellpadding="5" width="100%">
      <tr>
        <td style="color: #FFFFFF">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 20px; font-size: 11px; line-height: 20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px; padding-bottom: 0">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Terms &amp; Conditions</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <font color="#333333">This is a non-transferable membership 
        subscription. You confirm that you are at least 18 years of age. 
        Please consult your local physician before joining any fitness 
        regime. Neither Gymscanner nor any fitness facility will be held 
        responsible for any medical complications that may arise due to 
        your prior or existing health conditions.<br>
        <br>
        Please see our terms and conditions here.</font><p>&nbsp;</td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td valign="top">
    <table style="padding-left: 10px; padding-right: 10px; padding-top: 0; padding-bottom: 0" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Special Requests</h2>
        </td>
      </tr>
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="font-weight: normal">None mentioned</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="display: inline; bottom: 8px; left: 2px; color: #003580; font-weight: bold; font-size: 14px">
        Important Information</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="text-indent: 0">If you have any questions, you may 
        contact ' . $det->gymname . ' directly at <br>
        +1 ' . $det->ow_phone . '</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 10px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
        <span style="font-size: 8pt; font-weight: 700">Need help?</span><span style="font-weight: 400"><br>
        <span style="font-size: 8pt">Our Customer Service team is also 
        here to help. Send us a message</span></span></font></p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 10px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #cdcdcd; padding-top: 10px">
    <h2 style="color: #333333; font-weight: bold; font-size: 12px">Payment<br>
    <span style="font-weight: 400">You have now confirmed and guaranteed 
    your membership subscription via credit card.</span></h2>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
      <b><font color="#333333">Cancel your membership subscription</font></b></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: italic">
      <font color="#333333">This reservation can be cancelled free of 
      charge 24 hours prior to start date.</font></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: normal">
      <font color="#333333">Any cancellation or modification thereafter 
      might incur fees that are determined by the facility.</font></div>
    <p style="font-family: Georgia, Serif; color: #003580; font-size: 16px; text-align: right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #e3e3e1">
    <table style="background-color: #FFFFFF" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table class="ecxstackonmobile" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="700">
          <tr>
            <td style="padding-left: 0; padding-right: 0; padding-top: 10px; padding-bottom: 20px">
            <div style="color: #828993; text-align: center">
              Copyright © 2014 Gymscanner.com. All rights 
              reserved.<br>
              This email was sent by Gymscanner.com Minnesota, USA</div>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

</body>

</html>';

                $this->email->message($val);
                $this->email->send();
                // end mail sending process //

                /****** email config settings ******/
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                // mail sending process //

                $this->email->from('order@gymscanner.com', 'Gymscanner');
                //$this->email->to($list);
                $this->email->to($det->ow_email);
                $this->email->subject('Order through Gymscanner.com');

                $val = '<html><head><meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Thanks</title>
</head>

<body>

<table class="ecxmaincontent" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="720">
  <tr>
    <td colspan="3" style="text-align: center; color: #003580; font-weight: bold; font-size: 14px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 18px">
    <font color="#333333">Dear ' . ucfirst($det->gymname) . ', you have an order through Gymscanner.com
    </font></td>
  </tr>
  <tr>
    <td colspan="3" valign="top">
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="ecxlogo_section" colspan="1">
        <table border="0" width="100%">
          <tr>
            <td width="102">
            <img border="0" src="' . $logo_img . '" width="430" height="74"></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td width="356">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="356">&nbsp;</td>
  </tr>
  <tr>
    <td style="width: 360px; padding-bottom: 20px" valign="top">
    <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-top: 10px" valign="top">
            <font color="#333333"><b>Booking number: </b></font>
            </td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-right: 12px; padding-top: 10px" align="right" valign="top">
            <font color="#333333">' . substr($det->transaction_id, 3) . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>PIN Code: </b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">' . $det->ofr_pin . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>Email:</b></font></td>
            <td id="ecxemailAddress" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">' . $det->email . '</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px; padding-bottom: 10px" valign="top">
            <font color="#333333"><b>Booked by:</b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px; padding-bottom: 10px" align="right" valign="top">
            <font color="#333333">' . ucfirst($det->first_name) . ' ' . ucfirst($det->last_name) . ' </font></td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-top: 10px" valign="top">
            <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
                <font color="#333333"><b>Your reservation:</b></font></td>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $duration . ' membership subscription at ' . ucfirst($det->gymname) . '</font>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            ' . $spe_data . '
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            ' . $spe_ofr . '
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-bottom: 10px" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="color: #003580; font-weight: bold; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; border-left: 1px solid #003580; padding-left: 12px; background-color: #E3E3E1" valign="top">
            <font color="#333333">Total Price</font></td>
            <td style="color: #003580; font-weight: normal; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; white-space: nowrap; border-right: 1px solid #003580; padding-right: 12px; background-color: #E3E3E1" align="right" valign="top">
            <font color="#333333">US$ ' . $det->amount . '/- </font></td>
          </tr>
          <tr>
            <td colspan="2" style="color: #000; font-weight: normal; font-size: 11px; font-family: Helvetica, Arial, Sans-serif; line-height: 14px; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; background-color: #E3E3E1" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" style="color: #333; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-bottom: 1px solid #003580; padding-left: 6px; padding-right: 12px; padding-bottom: 10px; background-color: #E3E3E1">
            <p class="ecxcurrency_disclaimer_message" style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Details:</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Start date: ' . $det->joining_date . '<br>
            End Date: ' . $end_date . '</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td style>&nbsp;</td>
    <td style valign="top">
    <table style="color: #003580; padding: 10px" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" style="color: #003580; font-weight: bold; font-size: 16px; padding-bottom: 10px">
        <font color="#333333">' . ucfirst($det->gymname) . ', ' . ucfirst($det->city) . '</font><br>
&nbsp;</td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Address:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">' . ucfirst($det->gstreet) . ', ' . ucfirst($det->city) . '<br>
        ' . ucfirst($det->state) . ', ' . $det->gzip . '</font></td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Phone:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">+1 ' . $det->ow_phone . '</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold" valign="top">
        <font color="#333333">Email:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">' . $det->ow_email . '</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; padding-bottom: 10px" valign="top">
        <font color="#333333">Getting there:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; padding-bottom: 10px" valign="top">
        <font color="#333333">Show directions</font> </td>
      </tr>
      <tr>
        <td class="ecxgooglemap" colspan="2" style="height: 255px" align="center">
        <img src="' . $map_img . '" style="display: block" border="0" height="255" width="354"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td class="ecxmybooking_bar" colspan="3">
    <table style="color: #FFF; font-family: Helvetica, Arial, Sans-serif; font-size: 15px; line-height: 15px; font-weight: normal; text-align: center; background: #333333" cellpadding="5" width="100%">
      <tr>
        <td style="color: #FFFFFF">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 20px; font-size: 11px; line-height: 20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px; padding-bottom: 0">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Terms &amp; Conditions</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <font color="#333333">This is a non-transferable membership 
        subscription. You confirm that you are at least 18 years of age. 
        Please consult your local physician before joining any fitness 
        regime. Neither Gymscanner nor any fitness facility will be held 
        responsible for any medical complications that may arise due to 
        your prior or existing health conditions.<br>
        <br>
        Please see our terms and conditions here.</font><p>&nbsp;</td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td valign="top">
    <table style="padding-left: 10px; padding-right: 10px; padding-top: 0; padding-bottom: 0" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Special Requests</h2>
        </td>
      </tr>
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="font-weight: normal">None mentioned</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="display: inline; bottom: 8px; left: 2px; color: #003580; font-weight: bold; font-size: 14px">
        Important Information</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="text-indent: 0">If you have any questions, you may 
        contact ' . $det->gymname . ' directly at <br>
        +1 ' . $det->ow_phone . '</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 10px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
        <span style="font-size: 8pt; font-weight: 700">Need help?</span><span style="font-weight: 400"><br>
        <span style="font-size: 8pt">Our Customer Service team is also 
        here to help. Send us a message</span></span></font></p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 10px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #cdcdcd; padding-top: 10px">
    <h2 style="color: #333333; font-weight: bold; font-size: 12px">Payment<br>
    <span style="font-weight: 400">You have now confirmed and guaranteed 
    your membership subscription via credit card.</span></h2>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
      <b><font color="#333333">Cancel your membership subscription</font></b></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: italic">
      <font color="#333333">This reservation can be cancelled free of 
      charge 24 hours prior to start date.</font></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: normal">
      <font color="#333333">Any cancellation or modification thereafter 
      might incur fees that are determined by the facility.</font></div>
    <p style="font-family: Georgia, Serif; color: #003580; font-size: 16px; text-align: right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #e3e3e1">
    <table style="background-color: #FFFFFF" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table class="ecxstackonmobile" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="700">
          <tr>
            <td style="padding-left: 0; padding-right: 0; padding-top: 10px; padding-bottom: 20px">
            <div style="color: #828993; text-align: center">
              Copyright © 2014 Gymscanner.com. All rights 
              reserved.<br>
              This email was sent by Gymscanner.com Minnesota, USA</div>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

</body>

</html>';

                $this->email->message($val);
                $this->email->send();
                // end mail sending process //
            }
        }


        //echo '<h1>Successfully charged '.$charge->amount.'</h1>';
        $data['page'] = 'payment_success';
        $this->load->view('template', $data);

    }

    public function order_det()
    {
        //print_r($_POST);
        extract($_POST);
        $transaction_id = trim('ch_' . $boooking_number);
        $ofr_pin = trim($pin_code);
        $sql = "select * from gym_customers where ofr_pin='$ofr_pin' and transaction_id='$transaction_id' ";
        $res = $this->db->query($sql)->result_array();
        $count = $this->db->query($sql)->num_rows();
        //echo $count;
        //print_r($res);
        $cus_id = $res[0]['id'];
        if ($count > 0) {
            if (isset($this->session->userdata['type'])) {
                $table = 'supplier';
            } else {
                $table = 'user';
            }
            //echo "select u.first_name,u.last_name,u.email,o.duration,o.membership,o.discount,o.offer,o.description,c.joining_date,c.ofr_pin,c.transaction_id,c.amount,g.gymname,g.ow_email,g.ow_phone,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,city.city,s.state,cnt.country_name from gym_customers c left join offer_description o on o.id = c.offr_id left join ".$table." u on u.id = c.user_id left join gym g on g.id = c.gym_id left join country cnt on g.gcountry = cnt.id left join states s on g.gstate = s.state_code left join cities city on g.gcity = city.id where c.id = '".$cus_id."' " ;
            $det = $this->db->select('u.first_name,u.last_name,u.email,o.duration,o.membership,o.discount,o.offer,o.description,c.joining_date,c.ofr_pin,c.transaction_id,c.amount,g.gymname,g.ow_email,g.ow_phone,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,city.city,s.state,cnt.country_name')
                ->from('gym_customers c')
                ->join('offer_description o', 'o.id = c.offr_id', 'left')
                ->join($table . ' as u', 'u.id = c.user_id', 'left')
                ->join('gym g', 'g.id = c.gym_id', 'left')
                ->join('country cnt', 'g.gcountry = cnt.id', 'left')
                ->join('states s', 'g.gstate = s.state_code', 'left')
                ->join('cities city', 'g.gcity = city.id', 'left')
                ->where('c.id', $cus_id)
                ->get()->row();


            //$dets = $this->db->query($sql)->result_array();
            //$det  = $dets[0];
            //echo '<pre>';
            //print_r($det);exit;
            $duration = $det->duration != '' ? $det->duration : '1 month';
            if ($det->discount != '') {
                $spe_data = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Special Offer:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $det->discount . '% discount</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
            } else {
                $spe_data = '';
            }
            if ($det->offer != '') {

                //$discount = $det->discount != ''?$det->discount:'0';
                $spe_ofr = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Exclusive Perk:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">' . $det->offer . '</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
            } else {
                $spe_ofr = '';
            }
            $date = date_create($det->joining_date);
            date_add($date, date_interval_create_from_date_string("$duration"));
            $end_date = date_format($date, 'Y-m-d');
            $map_img = "http://maps.googleapis.com/maps/api/staticmap?center=" . $det->gmap_lat . "," . $det->gmap_long . "&zoom=13&size=600x300&maptype=roadmap&markers=color:blue|label:S|" . $det->gmap_lat . "," . $det->gmap_long;
            $logo_img = base_url() . 'themes/user/images/logo.png';
            $data['det'] = $det;
            $data['end_date'] = $end_date;
            //$data['map_img'] = $map_img;
            $data['spe_data'] = $spe_data;
            $data['spe_ofr'] = $spe_ofr;
            $data['duration'] = $duration;
            $data['page'] = 'payment_success';
            $this->load->view('template', $data);
        } else {
            redirect(site_url());
        }

    }

}

?>