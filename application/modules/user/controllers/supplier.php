<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends MY_Controller {


	public function __construct()
	{
		parent::__construct();	
		$this->load->model("supplier_model",'supplier');		    
	}

	public function index()
	{
	  	$this->data['cat']  = $this->supplier->get_cats();
		$this->data['cntry']  = $this->supplier->get_cntrs();
		$this->data['latest'] = $this->supplier->get_latest();
		//$this->data['page'] = "main";
	   $this->load-> view('main', $this->data);		
	}
	public function get_states($id)
	{		
		extract($_POST);
		$data = $this->supplier->get_states($id); 
       	$attributes= 'id="state"  class="form_sm required" onchange="get_cities(this.value)"';
       	$data['0'] = 'State'; 
        echo form_dropdown('state',$data, '0',$attributes);	
	}

	public function get_cities($x){
		$data = $this->supplier->get_cities($x);
		$attributes= 'id="city"  class="form_sm required"';
        $city[''] = '';                      
        echo form_dropdown('city',$data, '',$attributes);	
	}
	
	public function check_login(){
		
		if(isset($this->session->userdata['user_id'])){
			//$user_name = $this->session->userdata['user_name'];
			$user = $this->session->userdata['user_id'];
			//echo '<pre>';
			//print_r($this->session->userdata);
			$res = $this->db->where('id',$user)->get('supplier')->num_rows();
			
			if($user != '' && $res > 0)
				return 1;
			else
				return 0;
		} 
	}
	public function signup_conf($x=''){
		
		if($x != '' && $x != 0 ){
			$this->db->where('id',$x)->update('supplier',array('status'=>'1'));
			redirect('user');
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function signup_sup($x=''){
		if($x != '' && $x != 0 ){
			$this->db->where('id',$x)->update('supplier',array('status'=>'1'));
			$data['id'] = $x;
			$data['level'] = '1'; 
			$this->load->view('signup',$data);
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function register(){	
		extract($_POST);
		//print_r($_POST); exit;
		
		if($_POST['level'] == 1){
			$unique=$this->supplier->sup_check();
			if($unique)
			{
			$res = $this->supplier->sup_register();
			if($res){
				/****** email config settings ******/
				$config['protocol'] = 'sendmail';
		        $config['mailpath'] = '/usr/sbin/sendmail';
		        $config['charset'] = 'iso-8859-1';
		        $config['wordwrap'] = TRUE;
		        $config['smtp_port'] = 25;
		        $config['mailtype'] = 'html';
		        $this->email->initialize($config);

				// mail sending process //

				$this->email->from('admin@gymscanner.com', 'Gym Scanner');
		        $this->email->to($email);
		        $this->email->subject('Confirmation Message');
		         
		        $val = "<html><body>";
		        $val .="<h4>Dear ".ucfirst($first_name).",</h4>";
		        $val .="<p>Thank you registering with GymScanner.com</p>";
		        $val .="<p>To confirm your registration, please <a href='".SUPPLIER_URL."signup_sup/".$res."'>click here</a></p>";
		        $val .="<p>Your Credentials are:</p>";
		        $val .="<p><b>Username :</b>".$user_name."</p>";
		        $val .="<p><b>Password :</b>".$password."</p>";
		        $val .="<p>From the Gymscanner.com team, we thank you for getting involved. Stay fit.</p>";
		        $val .="<div><br/>";
		        $val .="With Regards,<br/>";
		        $val .="Team Gymscanner<br/>";
		        $val .="Gymscanner.com<br/>";
		        $val .="1005 West Champlin Drive<br/>";
		        $val .="Minnesota 55030, USA<br/>";        
		        $val .="</div>";
		        $val .="</div><br/><hr style='color:#800000;' />";
		        $val .="<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
		        $val .="</div></body></html>";	
			
		        $this->email->message($val);
		        $this->email->send();
				// end mail sending process //

                //send notification to ADMIN

                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                // mail sending process //

                $this->email->from('admin@gymscanner.com', 'Gym Scanner');
                $this->email->to('admin@gymscanner.com');
                $this->email->subject('Gym makes an account');

                $val = "<html><body>";
                $val .="<h4>Dear Admin,</h4>";
                $val .="<p>You have a new gym that has signed up. The details are:</p><br/>";

                $val .="<p><b>Name :</b>".ucfirst($first_name)."</p><br/>";
                $val .="<p><b>Username :</b>".$user_name."</p><br/>";
                $val .="<p><b>Email :</b>".$email."</p><br/>";
                $val .="<p><b>Registered on :</b>".date("F j, Y, g:i a")."</p><br/>";

                $val .="<div><br/>";
                $val .="With Regards,<br/>";
                $val .="Team Gymscanner<br/>";
                $val .="Gymscanner.com<br/>";
                $val .="1005 West Champlin Drive<br/>";
                $val .="Minnesota 55030, USA<br/>";
                $val .="</div>";
                $val .="</div><br/><hr style='color:#800000;' />";
                $val .="<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
                $val .="</div></body></html>";

                $this->email->message($val);
                $this->email->send();


				echo '1';
			}else
				echo 'error';
			}
			else
				echo 'User already exist.';
		}	
		
	}

    public function exist_email()
    {
        extract($_POST);

        //echo $email;
        $res = $this->supplier->email_exist();
        echo $res;
    }
    public function exist_user()
    {
        extract($_POST);
        $res = $this->supplier->user_exist();
        echo $res;
    }
	public function sup_redirect(){
		$this->data['page'] = 'dash-bord';
		$this->load->view('template', $this->data);
	}
	public function login_redirect(){
		if($this->session->userdata['type'] == 1)
			$this->data['cnt'] = $this->db->where('is_supplier',$this->session->userdata['user_id'])->get('gym')->num_rows();

		$this->data['users'] = $this->db->select('u.id')->from('gym_customers u')->join('gym g','g.id = u.gym_id')->where('g.is_supplier',$this->session->userdata['user_id'])->get()->num_rows();
		$this->data['res'] = $this->supplier->getgyms();//exit;
		$this->data['page'] = 'dash-bord';
		$this->load->view('template', $this->data);
	}
	public function login(){
		$res = $this->supplier->login();
		if($res == 1){
			echo site_url('facility');
		}
		else
			echo 'Credentials are incorrect';		
	}
	public function logout(){
		$this->session->unset_userdata('user_id');
		redirect('supplier');
	}
	public function profile_view($x= ''){
		if($x != '' && $x != 0){
			$res = $this->db->where(array('id'=>$x,'status'=>'1'))->get('supplier')->num_rows();
			if($res > 0){
				$data['res'] = $this->db->where(array('id'=>$x,'status'=>'1'))->get('supplier')->row();
				$data['page'] = 'profile';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function add(){
	
		//$user = $this->session->userdata['user_name'];
		$user_id = $this->session->userdata['user_id'];
		$sqlqry = "select type from supplier where id = '$user_id' ";
		$rec = $this->db->query($sqlqry)->row();
		//print_r($rec);
		$this->session->userdata['type'] = $rec->type;
		//print_r($this->session->userdata);
		if($this->session->userdata['type'] == 1)
			$page = 'add_gym';
		else
			$page = 'add_trainer';
		$data['cntry'] = $this->supplier->get_cntrs();            
		$data['cat'] = $this->supplier->get_cats();
		$data['page'] = $page;
		$this->load->view('template',$data);
	}
	public function add_gym(){
		
		//echo "<pre>";print_r($_POST);
		//print_r($_FILES); exit();
		$res = $this->supplier->add_gyms();
		if ($res) {
			redirect(site_url('facility'));
	    }
	}
	public function edit_details($x=''){
		if($x != '' && $x != 0){
			$res = $this->db->where('id',$x)->get('gym')->num_rows();
			if($res > 0){
				$data['details'] = $this->db->where('id',$x)->get('gym')->row();
				$data['images'] = $this->db->where('gym_id',$x)->get('gym_images')->result();
				$data['cntry'] = $this->supplier->get_cntrs();
				$data['states'] = $this->supplier->get_states($data['details']->gcountry);
				$data['cities'] = $this->supplier->get_cities($data['details']->gstate);            
				$data['cat'] = $this->supplier->get_cats();
				//print_r($data['images']); die();
				if($this->session->userdata['type'] == 2)
					$data['page'] = 'add_trainer';
				else
					$data['page'] = 'add_gym';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function edit_gym(){
		$res = $this->supplier->edit_gym();
		redirect(site_url('facility'));
	}
	public function add_trainer(){
		$res = $this->supplier->add_trainer();
		if ($res) {
	        redirect(site_url('facility'), 'refresh');
	    }
	}
	public function view_details($x=''){
		$user = $this->session->userdata['user_name'];
		$sqlqry = "select type from supplier where user_name = '$user' ";
		$rec = $this->db->query($sqlqry)->row();
		$this->session->userdata['type'] = $rec->type;
	
		if($x != '' && $x != 0){
			$res = $this->db->where('id',$x)->get('gym')->num_rows();
			if($res > 0){
				$data['details'] = $this->supplier->get_det($x);
				//echo '<pre>';
				//print_r($data['details']);
				$data['images'] = $this->db->where('gym_id',$x)->get('gym_images')->result();
				//echo '<pre>';
				//print_r($data['images']);
				$data['page'] = 'sup_gyms';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function del_img($id,$img){
		$this->db->where('id',$id)->delete('gym_images');
			unlink('./uploads/gym/images/'.$img);
	}
	public function change_status(){
        $status=$_POST['status'];
        $status= ($status == 1)? '3' : '1';
        $this->db->where('id',$_POST['id'])->update('gym',array('status'=>$status));   
    }
    public function del_gym(){
    	$gym_id = $_POST['id'];
    	$res = $this->db->where('gym_id',$gym_id)->get('gym_customers')->num_rows();
    	if($res > 0){
    		echo 1;
    	}else{
    		$this->db->where('id',$gym_id)->delete('gym');
    		$imgs = $this->db->where('gym_id',$gym_id)->get('gym_images')->result();
    		foreach ($imgs as $key => $value) {
    			unlink('./uploads/gym/images/'.$value->gym_images);
    		}
    		$this->db->where('gym_id',$gym_id)->delete('gym_images');
    	}
    }
    public function get_users(){
	
    	if($this->check_login() == 1 ){
    		$data['res'] = $this->supplier->get_users();
    		$data['page'] = 'users';
    		$this->load->view('template',$data);
    	}else{
    		$data['page'] = 'error';
    		$this->load->view('template',$data);
    	}
    }
    public function add_users(){
    	if($this->check_login() == 1 ){
    		$data['page'] = 'add_users';
    		$this->load->view('template',$data);
    	}else{
    		$data['page'] = 'error';
    		$this->load->view('template',$data);
    	}
    }
    public function chk_usr($x){
    	$res = $this->db->like('user_name',$x)->get('user')->num_rows();
    	if($res > 0)
    		echo '1';
    	else
    		echo '0';
    }
    public function add_user(){
    	extract($_POST);
	    $this->form_validation->set_rules('first_name', 'First Name', 'required');
	    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
	    $this->form_validation->set_rules('user_name', 'User Name','required|is_unique[user.user_name]');
	    $this->form_validation->set_rules('password', 'Password', 'required');
	    $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[user.email]');
        if ($this->form_validation->run() == FALSE) {
            $data['page'] = 'add_users';
            $this->load->view('template', $data);
		}else{
			if ($this->supplier->insert_det()) {
          		$this->session->set_flashdata('success', 'New User details Added Successfully...');
          		redirect(site_url('get_users'));
        	}
		}
	}
	public function view_user($x = ''){
		if($x != '' && $x != 0){
			$res = $this->db->where('id',$x)->get('user')->num_rows();
			if($res > 0){
				$data['details'] = $this->supplier->get_user($x);
				$data['page'] = 'user_view';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function edit_user($x=''){
		if($x != '' && $x != 0){
			$res = $this->db->where('id',$x)->get('user')->num_rows();
			if($res > 0){
				$data['details'] = $this->supplier->get_user($x);
				$data['page'] = 'add_users';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
	}
	public function user_edit(){
		extract($_POST);
		if($old_name == $user_name){
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
		    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
		    $this->form_validation->set_rules('password', 'Password', 'required');
		    $this->form_validation->set_rules('email', 'Email','required|valid_email');
	        if ($this->form_validation->run() == FALSE) {
	        	$data['details'] = $this->supplier->get_user($uid);
	            $data['page'] = 'add_users';
	            $this->load->view('template', $data);
			}else{
				if ($this->supplier->update_det()) {
	          		$this->session->set_flashdata('success', 'User details Updated Successfully...');
	          		redirect(site_url('get_users'));
	        	}
			}
		}else{
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
		    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
		    $this->form_validation->set_rules('user_name', 'User Name','required|is_unique[user.user_name]');
		    $this->form_validation->set_rules('password', 'Password', 'required');
		    $this->form_validation->set_rules('email', 'Email','required|valid_email');
	        if ($this->form_validation->run() == FALSE) {
	        	$data['details'] = $this->supplier->get_user($uid);
	            $data['page'] = 'add_users';
	            $this->load->view('template', $data);
			}else{
				if ($this->supplier->update_det()) {
	          		$this->session->set_flashdata('success', 'User details Updated Successfully...');
	          		redirect(site_url('get_users'));
	        	}
			}
		}
	}
	public function del_usr(){
    	$usr_id = $_POST['id'];
    	$res = $this->db->where('user_id',$usr_id)->get('gym_customers')->num_rows();
    	if($res > 0){
    		echo 1;
    	}else{
    		$this->db->where('id',$usr_id)->delete('user');
    	}
    }
    public function change_status_user(){
    	$status=$_POST['status'];
        $status= ($status == 1)? '3' : '1';
        $this->db->where('id',$_POST['id'])->update('user',array('status'=>$status));	
    }
    public function view_customers($x = ''){
    	if($x != '' && $x != 0){
			$res = $this->db->where('id',$x)->get('gym')->num_rows();
			if($res > 0){
				$data['res'] = $this->supplier->view_users($x);
				$data['page'] = 'customers';
				$this->load->view('template',$data);
			}else{
				$data['page'] = 'error';
				$this->load->view('template',$data);
			}
		}else{
			$data['page'] = 'error';
			$this->load->view('template',$data);
		}
    }
}
