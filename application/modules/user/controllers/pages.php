<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("pages_model",'pages');	
		$this->load->model('user_model','user');
		$this->load->model("user_model", 'user');
		$this->load->database();
		$this->data['cntry'] = $this->user->get_cntrs();
	}
	public function get_page(){
		$id = $this->uri->segment(3);
		$this->data['content'] = $this->pages->get_aboutus($id);
		$this->data['page'] = 'pages';
		$this->load->view('template',$this->data);
	}
	public function about(){
		$this->data['page'] = 'aboutus';
		$this->load->view('template',$this->data);
	}
	public function termscond(){
		$this->data['page'] = 'termsandcond';
		$this->load->view('template',$this->data);
	}
	public function privacy(){
		$this->data['page'] = 'privacy';
		$this->load->view('template',$this->data);
	}
	public function refund(){
		$this->data['page'] = 'refund';
		$this->load->view('template',$this->data);
	}
	public function faq(){
		$this->data['page'] = 'faq';
		$this->load->view('template',$this->data);
	}
	public function trainer(){
		$this->data['page'] = 'trainer';
		$this->load->view('template',$this->data);
	}
	public function subscribe(){
		extract($_POST);
		$date = date('Y-m-d');
		$res = $this->db->where('email',$email)->get('subscribers')->num_rows();
		if($res > 0){
			echo $res = 0;
		}else{
			$email_to 		= "admin@gymscanner.com"; 
			$email_subject 	= "New User - Mobile App";
			$email_message	= 'Dear Admin, Subscribed new user: '.$email;

			$config['protocol']	= 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] 	= 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['smtp_port']= 25;
			$config['mailtype'] = 'html';
			
			$this->email->initialize($config);
			
            $this->email->from('info@gymscanner.com', 'Gymscanner');
			$list = array($email_to);
			$this->email->to($list);
            $this->email->subject('Subscribed Mobile App');
           	$this->email->message($email_message);
			$this->email->send();
			
        	$data = array('email' => $email,'created_on'=>$date);
        	$res = $this->db->insert('subscribers',$data);
        	echo $res;
    	}
	}
	public function contact(){
		$data['cat']  = $this->user->get_cats();
		$data['cntry']  = $this->user->get_cntrs();
		$data['page'] = 'contact';
		$this->load->view('template',$data);
	}
	public function get_states($id)
	{
		extract($_POST);
		$data = $this->user->get_states($id);
       	$attributes= 'id="state"  class="form_small" onchange="get_cities(this.value)"';
       	$data[''] = 'State';
        echo form_dropdown('state',$data, '',$attributes);
	}

	public function get_cities($x){
		$data = $this->user->get_cities($x);
		$attributes= 'id="city"  class="form_small"';
        $city[''] = '';
        echo form_dropdown('city',$data, '',$attributes);
	}
	public function contact_sub(){
		//print_r($_POST);
		extract($_POST);
		/****** email config settings ******/
		$config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

		$data = array('company_name' => $cname,
					  'cat_id' => $cat_id,
					  'company_email' => $cemail,
					  'company_url' => $url,
					  'telephone' => $tel_ph,
					  'cnt_id' => $cntry,
					  'city_id' => $city,
					  'contact_name' => $uname,
					  'ph_no' => $phno,
					  'email' => $con_email,
					  'created_on' => mktime() );
		$this->db->insert('contact_details',$data);
		$cat_name = $this->db->get_where('category',array('id'=>$cat_id))->row();
		$cnt_name = $this->db->get_where('country',array('id'=>$cntry))->row();
		$state_name = $this->db->get_where('states',array('state_code'=>$state))->row();
		$city_name = $this->db->get_where('cities',array('id'=>$city))->row();

		// mail sending process //

		$this->email->from('admin@gymscanner.com', 'Gym Scanner');
        $this->email->to($con_email);
        $this->email->subject('Your contact form has been submitted');
         
        $val = "<html><body>";
        $val .="Dear ".$uname.",";
        $val .="<h3>".$this->lang->line("Thanks for Showing your Interest.")."</h3>";
        $val .="<h4>Your details are:</h4>";
        $val .="<table  cellspacing='1' width='100%'>";
        $val .="<tr style='color:blue;background:lightblue;'>
			        <th>Comapny Name</th>
			        <th>Category Name</th>
			        <th>Company email</th>
			        <th>Telephone</th>
			        <th>City</th>
			        <th>State</th>
			        <th>Country</th>
					<th>Contact Name</th>
					<th>Contact Mobile</th>
					<th>Contact email</th>
      			</tr>";
        $val .="<tr style='background:lightgreen;' valign='top'>
		          <td>" . $cname . "</td>
		          <td>" . $cat_name->categoryname . "</td>
				  <td>" . $cemail . "</td>
				  <td>" . $tel_ph . "</td>
			 	  <td>" . $city_name->city . "</td>
				  <td>" . $state_name->state . "</td>
				  <td>" . $cnt_name->country_name . "</td>			        
				  <td>" . $uname. "</td>
				  <td>" . $phno. "</td>
				  <td>" . $con_email. "</td>";	
        $val .="</tr></table>";        
        $val .="<div ><br/>";
        $val .="With Regards,<br/>";
        $val .="Team Gymscanner<br/>";
        $val .="Gymscanner.com<br/>";
        $val .="1005 West Champlin Drive<br/>";
        $val .="Minnesota 55030, USA<br/>";        
        $val .="</div>";
    	$val .="</div><br/><hr style='color:#800000;' />";
    	$val .="<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
    	$val .="</div></body></html>";		
	
        $this->email->message($val);
        $this->email->send();
		// end mail sending process //
		$this->data['page'] = 'contact_conf';
		$this->load->view('template',$this->data);
	}
	
}
