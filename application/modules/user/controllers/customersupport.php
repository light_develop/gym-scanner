<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CustomerSupport extends MY_Controller {


	public function __construct()
	{
		parent::__construct();	
		
		$this->load->model("customersupport_model",'customersupport');		    
	}

	public function index()
	{
		$this->data['page'] = "customersupport";
		$this->data['all_faq'] = $this->customersupport->getfaq();
	   $this->load-> view('template', $this->data);		
	}
	public function addnew($is_replied = '')
	{
		if(count($_POST)>0)
		{
			$insId = $this->customersupport->insertFaq($is_replied);
			$this ->session-> set_flashdata('success', 'Your query successfully added');
		}
		redirect('user/customersupport');
	
	}
}
