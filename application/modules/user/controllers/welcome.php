<?php 

class Welcome extends CI_Controller 
{
    public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("countries");
        $this->load->library("pagination");
    }
 	/*
    public function example1() {
        $config = array();
        $config["base_url"] = base_url() . "user/welcome/example1";
        $config["total_rows"] = $this->countries->record_count();
        $config["per_page"] = 20;
        $config["uri_segment"] = 3;
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->countries->fetch_countries($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
        $this->load->view("user/example1", $data);
    }
    */

    public function example1() {
    $config["base_url"] = base_url() . "user/welcome/example1";
    $config["total_rows"] = $this->countries->record_count();
    $config["per_page"] = 20;
    $config["uri_segment"] = 3;
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = round($choice);
 
    $this->pagination->initialize($config);
 
    $page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;
    $data["results"] = $this->countries->fetch_countries($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
 
    $this->load->view("user/example1", $data);
}
}




?>
