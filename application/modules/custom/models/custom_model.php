<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Custom_model extends CI_Model {

    public function get_all_custom() {
        $unew=$this->db->get('gym_custom');
        return $unew->result();
    }
    public function get_all(){
                        $this->db->select('gym_custom.id as id, gym.id as gymId, gym.gymname as gymname, gym_custom.mem_id as memid, gym_custom.price as price, gym_custom.offer as offer, gym_custom.start as  start, gym_custom.end as end, gym_custom.status as status');
                        $this->db->from('gym_custom');
                        $this->db->join('gym', 'gym_custom.gym_id = gym.id');

                        return $this->db->get()->result();
   			}
    public function get_gym(){
                        return $this->db->get('gym')->result();
   			}    
    public function get_gym_with_id($id){
                     $this->db->select('gym_custom.id as id, gym.id as gymId, gym.gymname as gymname,gym_custom.mem_id as memid, gym_custom.price as price, gym_custom.offer as offer, gym_custom.start as  start, gym_custom.end as end, gym_custom.status as status');
                    $this->db->where('gym_custom.id', $id);
                    $this->db->from('gym_custom');
                    $this->db->join('gym', 'gym_custom.gym_id = gym.id');
                    return array_pop($this->db->get()->result());
                }    
        public function custom_insert($data)
        {
        $this->db->insert('gym_custom', $data); 
        }
        public function update_data($id, $data)
        {
            $this->db->where('id', $id);
            $this->db->update('gym_custom', $data); 
        }
               public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			 $query=$this->db->delete('gym_custom');
   			return 1;
    
    			}
    		public function deactivate($deactivateid){
   
    			    $this->db->set('status', 0);
			       $this->db->where('id', $deactivateid);
			       $this->db->update('gym_custom');

    			}
    
     		public function activate($activate){
   
    		     $this->db->set('status', 1);
		        $this->db->where('id', $activate);
		        $this->db->update('gym_custom');

   		 }
}



