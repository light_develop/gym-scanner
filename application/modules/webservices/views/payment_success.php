<p>Thank you for ordering with Gymscanner.com<br/>
  Please check your email address for your order details and when you can start on your fitness goals.</p>
<p>You may also call our hotline number or get in touch via email if you have any questions. Please have your order number ready for easy processing.</p>
<!-- <p>Please <a href="<?=base_url()?>">click here</a> to go back to the main page.</p> -->
      