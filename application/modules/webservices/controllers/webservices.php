<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservices extends  CI_Controller {

    private $response = array();
    private $status = 'no';
    private $message = "No data is available";
   function __construct()
   {
      parent::__construct();
      $this->load->model('webservices_model', 'webservices');
        date_default_timezone_set('Asia/Calcutta');
        $this->db->query('SET UNIQUE_CHECKS=0');
    }

    public function get_details(){
	    $response['status'] = 'Yes';
	    $categories = $this->webservices->get_catgs();
	    foreach ($categories as $key => $value) {
	      $categories[$key] = array('cat_id'=>$value->id,
	                              'cat_name'=>$value->categoryname
	              );
    	} 
	    $country = $this->webservices->get_cntrys(); 
	    foreach ($country as $key => $value) {
	      $country[$key] = array('cntry_id'=>$value->id,
	                              'cntry_name'=>$value->country_name
	              );
	    } 
	    /*$states = $this->webservices->get_states();
	    foreach ($states as $key => $value) {
	      $states[$key] = array('state_code'=>$value->state_code,
	                            'state_name'=>$value->state,
	                            'cntry_id'=>$value->country_id
	              );
	    }*/
	    /*$cities = $this->webservices->get_cities();
	    foreach ($cities as $key => $value) {
	      $cities[$key] = array('city_id'=>$value->id,
	                            'city_name'=>$value->city,
	                            'state_code'=>$value->state_code
	              );
	    }*/
	    $response['categories'] = $categories;
	    $response['countries'] = $country;
	    //$response['states'] = $states;
	    //$response['cities'] = $cities;
	    echo json_encode($response);
	  }
	public function get_states(){
	    $cntry = $_POST['country_id'];
	    $cnt = $this->db->where('id',$cntry)->get('country')->num_rows();
	    if($cnt > 0){
	      $res = $this->db->where('country_id',$cntry)->get('states')->result();
	        if(count($res) > 0){
		      foreach ($res as $key => $value) {
		        $data[$key] = array('state_name'=>$value->state,
		                            'state_code'=>$value->state_code,
		                            'cntry_id'=>$value->country_id);
		      }
		      $response['states'] = $data;
		      $response['status'] = 'yes';
		    }else{
		    	$response['status'] = $this->status;
		    	$response['message'] = $this->message;
		    }
	    }else{
	      $response['status'] = $this->status;
	      $response['message'] = $this->message;
	    }
	    echo json_encode($response);
	}
    public function get_cities(){
	    $state_code = $_POST['state'];
	    $cnt = $this->db->where('state_code',$state_code)->get('states')->num_rows();
	    if($cnt > 0){
	      $res = $this->db->where('state_code',$state_code)->get('cities')->result();
	      foreach ($res as $key => $value) {
	        $data[$key] = array('id'=>$value->id,
	                            'city_name'=>$value->city,
	                            'state_code'=>$value->state_code);
	      }
	      $response['cities'] = $data;
	      $response['status'] = 'yes';
	    }else{
	      $response['status'] = $this->status;
	      $response['message'] = $this->message;
	    }
	    echo json_encode($response);
	}
  public function get_gyms($gym_type,$cnt_code,$state_code,$city_id,$zipcode,$adv){
      //print_r($adv);exit;
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();

            
      $getvalues = $this->webservices->get_gyms($gym_type,$cnt_code,$state_code,$city_id,$zipcode,$adv);
      if (count($getvalues) > 0){
        foreach($getvalues as $items):
          $responsekeys = array();
          $responsekeys['gym_id']          = $items->id;
          $responsekeys['gym_name']        = $items->gymname;
          $responsekeys['price']           = '$'.$items->price;
          $responsekeys['gym_logo']        = $_SERVER["HTTP_HOST"].'/uploads/gym/logos/'.$items->gym_logo;
          $responsekeys['open_time']       = $items->open_time;
          $responsekeys['close_time']      = $items->close_time;
          $responsekeys['gym_star']        = $items->star;
          $responsekeys['gym_street']      = $items->gstreet;
          $responsekeys['gym_city']        = $items->city;
          $responsekeys['gym_state']       = $items->state;
          $responsekeys['gym_country']     = $items->country_name;
          $responsekeys['gym_zip']         = $items->gzip;
          $responsekeys['gym_address']     = $items->gstreet.','.$items->city.','.$items->gstate.','.$items->country_name.','.$items->gzip;
         
          $splty = $items->speciality;
          if($splty == 1){$spl ="M";}else if($splty == 2){$spl = "F";}else{ $spl = "A";}

          $responsekeys['gym_speciality']  = $spl;
          $responsekeys['gym_category']    = $items->categoryname;
          $responsekeys['gym_owner']       = $items->owner;
          $responsekeys['gym_owner_phone'] = $items->ow_phone;
          $responsekeys['gym_owner_email'] = $items->ow_email;
          $responsekeys['lat']             = $items->gmap_lat;
          $responsekeys['lngt']            = $items->gmap_long;
          $responsekeys['gym_status']      = $items->status;
               
          $images     = $items->images;
          $img = explode(',',$images);
          foreach ($img as $key => $value) {
            $newimg[] = $_SERVER["HTTP_HOST"].'/uploads/gym/images/'.$value;
          }
          $responsekeys['gym_images'] = $newimg;  
               
          array_push($response['details'],$responsekeys);
        endforeach;
        $response['response'] = "yes";
      }else{
          $response['response'] = "no";
      }
    echo json_encode($response);
  }
  public function sign_up(){
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $uname = $_POST['user_name'];
    $pwd   = $_POST['pwd'];
    $email = $_POST['email'];
    $res = $this->db->where('user_name',$uname)->get('user')->num_rows();
    if($res > 0){
      $response['status'] = $this->status;
      $response['message'] = 'User name exist';
    }else{
      $data = array('first_name'=>$fname,'last_name'=>$lname,'user_name'=>$uname,'password'=>$pwd,'email'=>$email,'reffered_by'=>'-1','status'=>'1','createdon'=>date('Y-m-d'));
      if($this->db->insert('user',$data)){
        $id = $this->db->insert_id();

        /****** email config settings ******/
            $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

            // mail sending process //

            $this->email->from('randyali80@gmail.com', 'Gym Scanner');
                $this->email->to($email);
                $this->email->subject('Gymscanner order receipt');
                 
                $val = "<html><body>";
                $val .="<h4>Dear ".ucfirst($fname).",</h4>";
                $val .="<p>Thank you registering with GymScanner.com</p>";
                $val .="<p>Your Credentials are:</p>";
                $val .="<p>User Name :".$uname."</p>";
                $val .="<p>Password :".$pwd."</p>";
                $val .="<p>From the Gymscanner.com team, we thank you for getting involved. Stay fit.</p>";
                $val .="<div ><br/>";
                $val .="With Regards,<br/>";
                $val .="Team Gymscanner<br/>";
                $val .="Gymscanner.com<br/>";
                $val .="1005 West Champlin Drive<br/>";
                $val .="Minnesota 55030, USA<br/>";        
                $val .="</div></body></html>";  
          
                $this->email->message($val);
                $this->email->send();
            // end mail sending process //
        $response['user_id'] = $id;
        $response['status'] = 'yes';
      }else{
        $response['status'] = $this->status;
        $response['message'] = $this->message;
      }
    }
    echo json_encode($response);    
  }
  public function sign_in(){
    $uname = $_POST['user_name'];
    $pwd = $_POST['pwd'];
    $res = $this->db->where(array('user_name'=>$uname,'password'=>$pwd,'status'=>'1'))->get('user')->num_rows();
    if($res > 0){
      $res = $this->db->where(array('user_name'=>$uname,'password'=>$pwd,'status'=>'1'))->get('user')->row();
      $details[] = array('user_id'=>$res->id,'first_name'=>$res->first_name,'last_name'=>$res->last_name,'user_name'=>$res->user_name,'password'=>$res->password,'email'=>$res->email);
      $response['status'] = 'yes';
      $response['details'] = $details;
    }else{
        $response['status'] = $this->status;
        $response['message'] = 'Credentials Incorrect';
      }
    echo json_encode($response);
  }
  public function get_reviews(){
    $gym_id = $_POST['gym_id'];
    $res = $this->db->where('gym_id',$gym_id)->get('reviews')->num_rows();
    if($res > 0){
      $res = $this->webservices->get_reviews($gym_id);
      foreach ($res as $key => $value) {
        $data[$key] = array('user'=>$value->username,'gymname'=>$value->gymname,'review'=>$value->comment,'date'=>$value->createdon);
      }
      $response['reviews'] = $data;
      $response['status'] = 'yes';
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }
  public function add_reviews(){
    $gym_id = $_POST['gym_id'];
    $user_id = $_POST['user_id'];
    $review = $_POST['review'];
    $data =array('gym_id'=>$gym_id,'user_id'=>$user_id,'user_type'=>'2','comment'=>$review,'createdon'=>date('Y-m-d H:i:s'));
    if($this->db->insert('reviews',$data)){
      $response['status'] = 'yes';
    }else{
      $response['status'] = $this->status;
      $response['message'] = 'Please try again';
    }
    echo json_encode($response);
  }
  public function send_enq(){
    $message = $_POST['message'];
    $user_id = $_POST['user_id'];
    $is_replied = $_POST['reply'];
    if($message != '' && $user_id != ''){
      $data = array('user_id'=>$user_id,'message'=>$message,'is_replied'=>$is_replied,'createdon'=>date('Y-m-d H:i:s'));
      if($this->db->insert('faq',$data)){
        $response['status'] = 'yes';
      }else{
        $response['status'] = $this->status;
        $response['message'] = 'Please try again';
      }
    }else{
      $response['status'] = $this->status;
    }
    echo json_encode($response);
  }
  public function get_enq(){
    $user_id = $_POST['user_id'];
    $res = $this->db->where('user_id',$user_id)->get('faq')->num_rows();
    if($res > 0){
      $all_faq = $this->webservices->getfaq($user_id);
      foreach($all_faq as $key=>$val) {
        $data['faq_id'] = $val->id;
        $data['user_name'] = $val->first_name;
        $data['message'] = $val->message;
        $data['createdon'] = $val->createdon;
        $rpl = $this->webservices->get_rpl($val->id);
        $data['reply'] = $rpl;
        $det[$key] = $data;
      }
      $response['faq'] = $det;
      $response['status'] = 'yes';
    }else{
      $response['status'] = $this->status;
      $response['message'] = 'Not found';
    }
    echo json_encode($response);
  }
  public function advance_search(){
    $cat_id = $_POST['cat_id'];
    if($cat_id != '' && $cat_id != 0){
      if($cat_id == 1 || $cat_id == 2){
        $adv = array('pilates'=>'PILATES/YOGA','mma'=>'MMA','boxing'=>'BOXING','cross'=>'CROSS TRAINING','spinning'=>'SPINNING','gymnastics'=>'GYMNASTICS','martial'=>'MARTIAL ARTS');
        if($cat_id == 2)
          $adv += array('weight'=>'WEIGHT LOSS','cond'=>'CONDITIONING','building'=>'BODY BUILDING'); 
        $response['status'] = 'yes';
        $response['details'] = $adv;
      }else{
        $response['status'] = $this->status;
        $response['message'] = $this->message;
      }
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }
  public function profile(){
    $user_id = $_POST['user_id'];
    $cnt = $this->db->where('id',$user_id)->get('user')->num_rows();
    if($cnt > 0){
      $res = $this->db->where('id',$user_id)->get('user')->row();
      $data[] = array('user_id'=>$res->id,'first_name'=>$res->first_name,'last_name'=>$res->last_name,'user_name'=>$res->user_name,'password'=>$res->password,'email'=>$res->email); 
      $response['status'] = 'yes';
      $response['details'] = $data;
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }
  public function edit_profile(){
    $user_id = $_POST['user_id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $password = $_POST['password'];
    $email = $_POST['email'];

    $cnt = $this->db->where('id',$user_id)->get('user')->num_rows();
    if($cnt > 0){
      $data = array('first_name'=>$first_name,'last_name'=>$last_name,'password'=>$password,'email'=>$email);
      if($this->db->where('id',$user_id)->update('user',$data)){
        $response['status'] = 'yes';
        $response['message'] = 'User details updated successfully';
      } else{
        $response['status'] = $this->status;
        $response['message'] = 'Please try again';
      }
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }
  public function get_offers(){
    $gym_id = $_POST['gym_id'];
    $res = $this->db->where('id',$gym_id)->get('gym')->num_rows();
    if($res > 0){
      $offrs = $this->webservices->get_offrs($gym_id);
      if(count($offrs) > 0){
        foreach ($offrs as $key => $value) {
        $details[$key] = array('offr_id'=>$value->id,
                              'duration'=>$value->duration,
                              'membership'=>'$'.$value->membership.'.00',
                              'discount'=>$value->discount,
                              'offer'=>$value->offer,
                              'description'=>$value->description);
        }
        $response['status'] = 'yes';
        $response['offers'] = $details;
      }else{
        $response['status'] = 'no';
        $response['message'] = 'No offers for this gym';
      }
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }
  public function payment(){    
    if(isset($_POST['gym_id'])){
    	$gym_id = $_POST['gym_id'];
		  $this->session->set_userdata('gym_id',$_POST['gym_id']);
    }
  	if(isset($_POST['user_id'])){
  		$user_id = $_POST['user_id'];
  		$this->session->set_userdata('user_id',$_POST['user_id']);
  	}
  	if(isset($_POST['join_date'])){
  		$join_date = $_POST['join_date'];
  		$this->session->set_userdata('join_date',$_POST['join_date']);
  	}
  	if(isset($_POST['offr_id'])){
  		$ofr_id = $_POST['offr_id'];
  		$this->session->set_userdata('offr_id',$_POST['offr_id']);
  	}
  	if(isset($_SESSION['msg'])){
  		$this->session->set_flashdata('error',$_SESSION['msg']);
  		unset($_SESSION['msg']);
  		redirect(site_url('charge_pay'),'refresh');
  	}
      $this->load->view('payment');
  }
  public function charge_payment(){
    	//error_reporting(0);
    	//print_r($_POST);
    	//print_r($this->session->all_userdata());exit;
    	include('lib/Stripe.php');
    	$stripe = array(
		  "secret_key"      => "sk_test_xgctLrPVjhz4aFHAPFaC5NWH",
		  "publishable_key" => "pk_test_nGGZ7SLPUISwLsJw9gWjd2UJ"
		);

		Stripe::setApiKey($stripe['secret_key']);

		$token  = $_POST['stripeToken'];


		$customer = Stripe_Customer::create(array(
		      'email' => $_POST['email'],
		      'card'  => $token
		));
		if($this->session->userdata['offr_id'] == '0'){
			$dets = $this->db->where('id',$this->session->userdata['gym_id'])->get('gym')->row();
			$amt = $dets->price*100;
		}else{
			$dets = $this->db->where('id',$this->session->userdata['offr_id'])->get('offer_description')->row();
			$amt = ($dets->membership-($dets->membership*$dets->discount/100))*100;
		}
		//echo $amt;exit;
		$charge = Stripe_Charge::create(array(
		      'customer' => $customer->id,
		      'amount'   => $amt,
		      'currency' => 'usd'
		));
		if(isset($charge)){

			$pin = 'gym-'.substr(str_shuffle(strtotime(date('H:i:s'))),0,4);
      $ch = (array)$charge;
      $res = array();
      $i = 1;
      foreach($ch as $key=>$val):
        //print_r($val); echo "##";
      if($i == 2)
      {
        if(isset($val['id']))
          $res['id'] = $val['id']; 
        if(isset($val['paid']))
          $res['paid'] = $val['paid']; 
        if(isset($val['amount']))
          $res['amount'] = $val['amount']; 
        if(isset($val['currency']))
          $res['currency'] = $val['currency'];
      }
      $i++;
      endforeach;
      //print_r($res); exit;
      $data = array('user_id'=>$this->session->userdata['user_id'],
              'gym_id'=>$this->session->userdata['gym_id'],
              'offr_id'=>$this->session->userdata['offr_id'],
              'joining_date'=>$this->session->userdata['join_date'],
              'transaction_id'=>$res['id'],
              'ofr_pin'=>$pin,
              'amount'=>$res['amount']/100,
              'created_on' => date('Y-m-d H:i:s'));
      if($this->db->insert('gym_customers',$data)){
        $sus_msg = 'Successfully charged $'.$charge->amount/100;
        $this->session->set_flashdata('success', $sus_msg);
        $cus_id = $this->db->insert_id();
        $det = $this->db->select('u.first_name,u.last_name,u.email,o.duration,o.membership,o.discount,o.offer,o.description,c.joining_date,c.ofr_pin,c.transaction_id,c.amount,g.gymname,g.ow_email,g.ow_phone,g.gstreet,g.gzip,g.gmap_lat,g.gmap_long,city.city,s.state,cnt.country_name')
                ->from('gym_customers c')
                ->join('offer_description o','o.id = c.offr_id','left')
                ->join($table.' as u','u.id = c.user_id')
                ->join('gym g','g.id = c.gym_id')
                ->join('country cnt','g.gcountry = cnt.id')
                ->join('states s','g.gstate = s.state_code')
                ->join('cities city','g.gcity = city.id')
                ->where('c.id',$cus_id)
                ->get()->row();
        //echo $this->db->last_query();
        //echo $cus_id;
        //print_r($det);echo "<br>";
          $duration = $det->duration != ''?$det->duration:'1 month';
        if($det->discount != ''){

          //$discount = $det->discount != ''?$det->discount:'0';
          $spe_data = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Special Offer:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">'.$det->offer.'</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
        }else{
          $spe_data = '';
        }
        if($det->offer != ''){

          //$discount = $det->discount != ''?$det->discount:'0';
          $spe_ofr = '<table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">   
              <tr valign="top">
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" width=" 75">
                <font color="#333333"><b>Exclusive Perk:</b></font></td>
                <td class="ecxpb-confirmation-date-checkin" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">'.$det->discount.'% discount</font><i style="color: #555555; white-space: nowrap">
                </i></td>
              </tr>
            </table>';
        }else{
          $spe_ofr = '';
        }
        $date = date_create($det->joining_date);
        date_add($date, date_interval_create_from_date_string("$duration"));
        $end_date = date_format($date, 'Y-m-d');
        $map_img = "http://maps.googleapis.com/maps/api/staticmap?center=".$det->gmap_lat.",".$det->gmap_long."&zoom=13&size=600x300&maptype=roadmap&markers=color:blue|label:S|".$det->gmap_lat.",".$det->gmap_long;
        $logo_img =base_url().'themes/user/images/logo.png';
        $data['det'] = $det;
        $data['end_date'] = $end_date;
        //$data['map_img'] = $map_img;
        $data['spe_data'] = $spe_data;
        $data['spe_ofr'] = $spe_ofr;
        $data['duration'] = $duration;
        //echo 'img src="'.$map_img.'"';exit;
        //$det = $this->db->where('id',$this->session->userdata['user_id'])->get('user')->row();
        //$cust_det = $this->db->where('id',$cus_id)->get('gym_customers')->row();
        //$cust_det = $this->db->where('id',$cus_id)->get('gym_customers')->row();
      
    
    /****** email config settings ******/
            $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

            // mail sending process //

            $this->email->from('randyali80@gmail.com', 'Gymscanner');
                $this->email->to($_POST['email']);
                $this->email->subject('Gymscanner order receipt');
                 
                $val = '<html><head><meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Thanks</title>
</head>

<body>

<table class="ecxmaincontent" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="720">
  <tr>
    <td colspan="3" style="text-align: center; color: #003580; font-weight: bold; font-size: 14px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 18px">
    <font color="#333333">Thanks, '.ucfirst($det->first_name).'! Your membership is now confirmed.
    </font></td>
  </tr>
  <tr>
    <td colspan="3" valign="top">
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="ecxlogo_section" colspan="1">
        <table border="0" width="100%">
          <tr>
            <td width="102">
            <img border="0" src="'.$logo_img.'" width="430" height="74"></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td width="356">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="356">&nbsp;</td>
  </tr>
  <tr>
    <td style="width: 360px; padding-bottom: 20px" valign="top">
    <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-top: 10px" valign="top">
            <font color="#333333"><b>Booking number: </b></font>
            </td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-right: 12px; padding-top: 10px" align="right" valign="top">
            <font color="#333333">'.substr($det->transaction_id,3).'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>PIN Code: </b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">'.$det->ofr_pin.'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>Email:</b></font></td>
            <td id="ecxemailAddress" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">'.$det->email.'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px; padding-bottom: 10px" valign="top">
            <font color="#333333"><b>Booked by:</b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px; padding-bottom: 10px" align="right" valign="top">
            <font color="#333333">'.ucfirst($det->first_name).' '.ucfirst($det->last_name).' </font></td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-top: 10px" valign="top">
            <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
                <font color="#333333"><b>Your reservation:</b></font></td>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">'.$duration.' membership subscription at '.ucfirst($det->gymname).'</font>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            '.$spe_data.'
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            '.$spe_ofr.'
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-bottom: 10px" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="color: #003580; font-weight: bold; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; border-left: 1px solid #003580; padding-left: 12px; background-color: #E3E3E1" valign="top">
            <font color="#333333">Total Price</font></td>
            <td style="color: #003580; font-weight: normal; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; white-space: nowrap; border-right: 1px solid #003580; padding-right: 12px; background-color: #E3E3E1" align="right" valign="top">
            <font color="#333333">US$ '.$det->amount.'/- </font></td>
          </tr>
          <tr>
            <td colspan="2" style="color: #000; font-weight: normal; font-size: 11px; font-family: Helvetica, Arial, Sans-serif; line-height: 14px; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; background-color: #E3E3E1" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" style="color: #333; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-bottom: 1px solid #003580; padding-left: 6px; padding-right: 12px; padding-bottom: 10px; background-color: #E3E3E1">
            <p class="ecxcurrency_disclaimer_message" style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Details:</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Start date: '.$det->joining_date.'<br>
            End Date: '.$end_date.'</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td style>&nbsp;</td>
    <td style valign="top">
    <table style="color: #003580; padding: 10px" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" style="color: #003580; font-weight: bold; font-size: 16px; padding-bottom: 10px">
        <font color="#333333">'.ucfirst($det->gymname).', '.ucfirst($det->city).'</font><br>
&nbsp;</td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Address:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">'.ucfirst($det->gstreet).', '.ucfirst($det->city).'<br>
        '.ucfirst($det->state).', '.$det->gzip.'</font></td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Phone:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">+1 '.$det->ow_phone.'</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold" valign="top">
        <font color="#333333">Email:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">'.$det->ow_email.'</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; padding-bottom: 10px" valign="top">
        <font color="#333333">Getting there:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; padding-bottom: 10px" valign="top">
        <font color="#333333">Show directions</font> </td>
      </tr>
      <tr>
        <td class="ecxgooglemap" colspan="2" style="height: 255px" align="center">
        <img src="'.$map_img.'" style="display: block" border="0" height="255" width="354"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td class="ecxmybooking_bar" colspan="3">
    <table style="color: #FFF; font-family: Helvetica, Arial, Sans-serif; font-size: 15px; line-height: 15px; font-weight: normal; text-align: center; background: #333333" cellpadding="5" width="100%">
      <tr>
        <td style="color: #FFFFFF">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 20px; font-size: 11px; line-height: 20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px; padding-bottom: 0">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Terms &amp; Conditions</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <font color="#333333">This is a non-transferable membership 
        subscription. You confirm that you are at least 18 years of age. 
        Please consult your local physician before joining any fitness 
        regime. Neither Gymscanner nor any fitness facility will be held 
        responsible for any medical complications that may arise due to 
        your prior or existing health conditions.<br>
        <br>
        Please see our terms and conditions here.</font><p>&nbsp;</td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td valign="top">
    <table style="padding-left: 10px; padding-right: 10px; padding-top: 0; padding-bottom: 0" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Special Requests</h2>
        </td>
      </tr>
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="font-weight: normal">None mentioned</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="display: inline; bottom: 8px; left: 2px; color: #003580; font-weight: bold; font-size: 14px">
        Important Information</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="text-indent: 0">If you have any questions, you may 
        contact '.$det->gymname.' directly at <br>
        +1 '.$det->ow_phone.'</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 10px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
        <span style="font-size: 8pt; font-weight: 700">Need help?</span><span style="font-weight: 400"><br>
        <span style="font-size: 8pt">Our Customer Service team is also 
        here to help. Send us a message</span></span></font></p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 10px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #cdcdcd; padding-top: 10px">
    <h2 style="color: #333333; font-weight: bold; font-size: 12px">Payment<br>
    <span style="font-weight: 400">You have now confirmed and guaranteed 
    your membership subscription via credit card.</span></h2>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
      <b><font color="#333333">Cancel your membership subscription</font></b></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: italic">
      <font color="#333333">This reservation can be cancelled free of 
      charge 24 hours prior to start date.</font></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: normal">
      <font color="#333333">Any cancellation or modification thereafter 
      might incur fees that are determined by the facility.</font></div>
    <p style="font-family: Georgia, Serif; color: #003580; font-size: 16px; text-align: right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #e3e3e1">
    <table style="background-color: #FFFFFF" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table class="ecxstackonmobile" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="700">
          <tr>
            <td style="padding-left: 0; padding-right: 0; padding-top: 10px; padding-bottom: 20px">
            <div style="color: #828993; text-align: center">
              Copyright © 2014 Gymscanner.com. All rights 
              reserved.<br>
              This email was sent by Gymscanner.com Minnesota, USA</div>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

</body>

</html>';
          
                $this->email->message($val);
                $this->email->send();
            // end mail sending process //

        /****** email config settings ******/
            $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['smtp_port'] = 25;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

            // mail sending process //

            $this->email->from('randyali80@gmail.com', 'Gymscanner');
                $this->email->to($det->ow_email);
                $this->email->subject('Order through Gymscanner.com');
                 
                $val = '<html><head><meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Thanks</title>
</head>

<body>

<table class="ecxmaincontent" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="720">
  <tr>
    <td colspan="3" style="text-align: center; color: #003580; font-weight: bold; font-size: 14px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 18px">
    <font color="#333333">Dear '.ucfirst($det->gymname).', you have an order through Gymscanner.com
    </font></td>
  </tr>
  <tr>
    <td colspan="3" valign="top">
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="ecxlogo_section" colspan="1">
        <table border="0" width="100%">
          <tr>
            <td width="102">
            <img border="0" src="'.$logo_img.'" width="430" height="74"></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td width="356">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="356">&nbsp;</td>
  </tr>
  <tr>
    <td style="width: 360px; padding-bottom: 20px" valign="top">
    <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-top: 10px" valign="top">
            <font color="#333333"><b>Booking number: </b></font>
            </td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-right: 12px; padding-top: 10px" align="right" valign="top">
            <font color="#333333">'.substr($det->transaction_id,3).'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>PIN Code: </b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">'.$det->ofr_pin.'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px" valign="top">
            <font color="#333333"><b>Email:</b></font></td>
            <td id="ecxemailAddress" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px" align="right" valign="top">
            <font color="#333333">'.$det->email.'</font></td>
          </tr>
          <tr>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; padding-left: 12px; padding-bottom: 10px" valign="top">
            <font color="#333333"><b>Booked by:</b></font></td>
            <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-right: 1px solid #003580; padding-right: 12px; padding-bottom: 10px" align="right" valign="top">
            <font color="#333333">'.ucfirst($det->first_name).' '.ucfirst($det->last_name).' </font></td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-top: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-top: 10px" valign="top">
            <table style="color: #003580" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
                <font color="#333333"><b>Your reservation:</b></font></td>
                <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="right">
                <font color="#333333">'.$duration.' membership subscription at '.ucfirst($det->gymname).'</font>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            '.$spe_data.'
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px" valign="top">
            '.$spe_ofr.'
            </td>
          </tr>
          <tr>
            <td colspan="2" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 14px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; padding-bottom: 10px" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table style="color: #003580; width: 100%" border="0" cellpadding="4" cellspacing="0">
          <tr>
            <td style="color: #003580; font-weight: bold; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; border-left: 1px solid #003580; padding-left: 12px; background-color: #E3E3E1" valign="top">
            <font color="#333333">Total Price</font></td>
            <td style="color: #003580; font-weight: normal; font-size: 16px; font-family: Helvetica, Arial, Sans-serif; line-height: 25px; white-space: nowrap; border-right: 1px solid #003580; padding-right: 12px; background-color: #E3E3E1" align="right" valign="top">
            <font color="#333333">US$ '.$det->amount.'/- </font></td>
          </tr>
          <tr>
            <td colspan="2" style="color: #000; font-weight: normal; font-size: 11px; font-family: Helvetica, Arial, Sans-serif; line-height: 14px; border-left: 1px solid #003580; border-right: 1px solid #003580; padding-left: 12px; padding-right: 12px; background-color: #E3E3E1" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" style="color: #333; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #003580; border-right: 1px solid #003580; border-bottom: 1px solid #003580; padding-left: 6px; padding-right: 12px; padding-bottom: 10px; background-color: #E3E3E1">
            <p class="ecxcurrency_disclaimer_message" style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Details:</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            Start date: '.$det->joining_date.'<br>
            End Date: '.$end_date.'</p>
            <p style="font-weight: normal; text-indent: 0; padding-left: 8px; padding-right: 8px; padding-top: 0; padding-bottom: 0">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td style>&nbsp;</td>
    <td style valign="top">
    <table style="color: #003580; padding: 10px" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" style="color: #003580; font-weight: bold; font-size: 16px; padding-bottom: 10px">
        <font color="#333333">'.ucfirst($det->gymname).', '.ucfirst($det->city).'</font><br>
&nbsp;</td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Address:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">'.ucfirst($det->gstreet).', '.ucfirst($det->city).'<br>
        '.ucfirst($det->state).', '.$det->gzip.'</font></td>
      </tr>
      <tr>
        <td style="font-weight: normal; font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px" valign="top">
        <font color="#333333"><b>Phone:</b></font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">+1 '.$det->ow_phone.'</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold" valign="top">
        <font color="#333333">Email:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" valign="top">
        <font color="#333333">'.$det->ow_email.'</font></td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: bold; padding-bottom: 10px" valign="top">
        <font color="#333333">Getting there:</font></td>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; padding-bottom: 10px" valign="top">
        <font color="#333333">Show directions</font> </td>
      </tr>
      <tr>
        <td class="ecxgooglemap" colspan="2" style="height: 255px" align="center">
        <img src="'.$map_img.'" style="display: block" border="0" height="255" width="354"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td class="ecxmybooking_bar" colspan="3">
    <table style="color: #FFF; font-family: Helvetica, Arial, Sans-serif; font-size: 15px; line-height: 15px; font-weight: normal; text-align: center; background: #333333" cellpadding="5" width="100%">
      <tr>
        <td style="color: #FFFFFF">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 20px; font-size: 11px; line-height: 20px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px; padding-bottom: 0">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Terms &amp; Conditions</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <font color="#333333">This is a non-transferable membership 
        subscription. You confirm that you are at least 18 years of age. 
        Please consult your local physician before joining any fitness 
        regime. Neither Gymscanner nor any fitness facility will be held 
        responsible for any medical complications that may arise due to 
        your prior or existing health conditions.<br>
        <br>
        Please see our terms and conditions here.</font><p>&nbsp;</td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td valign="top">
    <table style="padding-left: 10px; padding-right: 10px; padding-top: 0; padding-bottom: 0" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="color: #333333; font-weight: bold; font-size: 14px">
        Special Requests</h2>
        </td>
      </tr>
      <tr bgcolor="#FFFFCC">
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="font-weight: normal">None mentioned</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top: 1px solid #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px; padding-top: 10px">
        <h2 style="display: inline; bottom: 8px; left: 2px; color: #003580; font-weight: bold; font-size: 14px">
        Important Information</h2>
        </td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom-color: #cdcdcd; padding-left: 12px; padding-right: 12px">
        <p style="text-indent: 0">If you have any questions, you may 
        contact '.$det->gymname.' directly at <br>
        +1 '.$det->ow_phone.'</td>
      </tr>
      <tr>
        <td style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; border-left: 1px solid #cdcdcd; border-right: 1px solid #cdcdcd; border-top-color: #cdcdcd; border-bottom: 1px solid #cdcdcd; padding-left: 12px; padding-right: 12px; padding-bottom: 10px">
        <p style="color: #003580; font-size: 12px"><br>
        <font color="#333333">
        <span style="font-size: 8pt; font-weight: 700">Need help?</span><span style="font-weight: 400"><br>
        <span style="font-size: 8pt">Our Customer Service team is also 
        here to help. Send us a message</span></span></font></p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="height: 10px">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #cdcdcd; padding-top: 10px">
    <h2 style="color: #333333; font-weight: bold; font-size: 12px">Payment<br>
    <span style="font-weight: 400">You have now confirmed and guaranteed 
    your membership subscription via credit card.</span></h2>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal">
      <b><font color="#333333">Cancel your membership subscription</font></b></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: italic">
      <font color="#333333">This reservation can be cancelled free of 
      charge 24 hours prior to start date.</font></div>
    <div style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal; font-style: normal">
      <font color="#333333">Any cancellation or modification thereafter 
      might incur fees that are determined by the facility.</font></div>
    <p style="font-family: Georgia, Serif; color: #003580; font-size: 16px; text-align: right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="border-top: 1px solid #e3e3e1">
    <table style="background-color: #FFFFFF" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
        <table class="ecxstackonmobile" style="font-family: Helvetica, Arial, Sans-serif; font-size: 11px; line-height: 15px; font-weight: normal" align="center" border="0" cellpadding="0" cellspacing="0" width="700">
          <tr>
            <td style="padding-left: 0; padding-right: 0; padding-top: 10px; padding-bottom: 20px">
            <div style="color: #828993; text-align: center">
              Copyright © 2014 Gymscanner.com. All rights 
              reserved.<br>
              This email was sent by Gymscanner.com Minnesota, USA</div>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

</body>

</html>';
          
                $this->email->message($val);
                $this->email->send();
            // end mail sending process //
      }
		}
		
		
		//echo '<h1>Successfully charged '.$charge->amount.'</h1>';
		$this->load->view('payment_success',$data);

	}	

  public function get_price(){
    $gym_id = $_POST['gym_id'];
    $res = $this->db->where('id',$gym_id)->get('gym')->num_rows();
    if($res > 0){
      $offrs = $this->db->where(array('gym_id'=>$gym_id,'is_accept'=>'1','type'=>'1'))->get('offers')->result();
      //print_r($offrs);
      if(count($offrs) > 0){
        foreach ($offrs as $key => $value) {
          $details[$key] = array('id'=>$value->id,
                                  'offer_name'=>$value->name,
                                  'offer'=>$value->offer,
                                  'start_date'=>$value->start_date,
                                  'end_date'=>$value->end_date,
                                  'description'=>$value->description);

        }
        $response['status'] = 'yes';
        $response['offers'] = $details;
      }else{
        $response['status'] = 'no';
        $response['message'] = 'No offers for this gym';
      }
    }else{
      $response['status'] = $this->status;
      $response['message'] = $this->message;
    }
    echo json_encode($response);
  }


  public function get_images($gymid)
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_images($gymid); 

           if (count($getvalues) > 0)
            {
                foreach($getvalues as $items):

                    $responsekeys = array();
                    $responsekeys['gym_id']          = $items->id;
                    $responsekeys['gym_name']        = $items->gymname;
                    $responsekeys['gym_logo']        = $_SERVER["HTTP_HOST"].'/uploads/thumbs/gym/logo_imgs/'.$items->gym_logo;
                    $responsekeys['lat']             = $items->gmap_lat;
                    $responsekeys['lngt']            = $items->gmap_long;

                    $images     = $items->images;
                    $img = explode(',',$images);
                    foreach($img as $imge):
                        $neg = $imge;
                        $thumbs[] = $neg?$_SERVER["HTTP_HOST"].'/uploads/thumbs/gym/thumb_small/small-'.$neg:$neg;
                        $original[] = $neg?$_SERVER["HTTP_HOST"].'/uploads/thumbs/gym/thumb_big/big-'.$neg:$neg;
                    endforeach;                       
                    $responsekeys['thumb_imgs']      = $thumbs;
                    $responsekeys['original_imgs']   = $original;
                       
                    array_push($response['details'],$responsekeys);
                endforeach;
                $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }
   
  public function get_locations()
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      //if($_POST){
            $cnt_code = (!empty($_POST['cntry_code']))? $_POST['cntry_code'] : '1';
            $city_id = (!empty($_POST['city_id']))? $_POST['city_id'] : '1';
           $getvalues = $this->webservices->get_locs($cnt_code,$city_id);  //echo "<pre/>";print_r($getvalues[0]);exit;
           if (count($getvalues) > 0)
            {
                  $data = $getvalues;
                  
                    $responsekeys = array();
                    $responsekeys['map'] = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$data->name.$data->cityname.'&destination='.$data->cityname.'&sensor=false';
                   array_push($response['details'],$responsekeys);
                $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
     // }
    echo json_encode($response);
  }   
  
  public function get_maps($gymid)
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_maps($gymid);  
           if (count($getvalues) > 0)
            {    
               foreach($getvalues as $items):             
                    $responsekeys = array();
                    $responsekeys['lat'] = $items->gmap_lat;
                    $responsekeys['lngt'] = $items->gmap_long;
                   array_push($response['details'],$responsekeys);
                endforeach;
                $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }   

  public function aboutpg()
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $id =2;
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_abtpg($id);  
           //print_r($getvalues); exit();
           if (isset($getvalues))
            {       $items = $getvalues;
                    $responsekeys = array();
                    $responsekeys['content'] = $items->content;
                    $responsekeys['image'] = $_SERVER["HTTP_HOST"].'/uploads/thumbs/cms_content/'.$items->image;
                   array_push($response['details'],$responsekeys);
                $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }   
  public function contactpg()
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $id =3;
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_abtpg($id);  
           if (isset($getvalues))
            {       $items = $getvalues;
                    $responsekeys = array();
                    $responsekeys['content'] = $items->content;
                    $responsekeys['image'] = $_SERVER["HTTP_HOST"].'/uploads/thumbs/cms_content/'.$items->image;
                   array_push($response['details'],$responsekeys);
                $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }   

  public function get_ofrs($gymid)
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_ofrs($gymid);  
           if (isset($getvalues))
            {       
              $items = $getvalues;
                if(isset($items->price) && count($items->price)> 0){
                  $newprice = explode(',',$items->price);
                  foreach ($newprice as $row) {
                    $resprice = explode('-',$row);
                    $responsekeys = array();
                    $responsekeys['ofr_id'] = $resprice[0];
                    $responsekeys['ofr_name'] = $resprice[1];
                    $responsekeys['price'] = $resprice[2];
                    $responsekeys['offr'] = $resprice[3]."%";
                    
                    array_push($response['details'],$responsekeys);
                    $response['response'] = "yes";
                  } 
                }
                else{
                  $response['response'] = "no";
                }              
            }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }
  public function get_flag($cntry)
  {
      $this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_flag($cntry);  
           if (isset($getvalues))
            {  
                    $responsekeys = array();
                    $responsekeys['cntryname'] = $getvalues->name;
                    $responsekeys['flag'] = $_SERVER["HTTP_HOST"].'/themes/user/country_flags/'.$getvalues->country_id.'.gif';
                    
                    array_push($response['details'],$responsekeys);
                    $response['response'] = "yes";
            }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }


  public function authorized_form() {
    //if($_POST){
    //$this->db->query('SET UNIQUE_CHECKS=0');
      //$response = array();
      //$response['details'] = array();
    //if($this->input->server('REQUEST_METHOD') === 'POST'){
    //if($_POST){
      //print_r($_POST); exit;
      extract($_POST);
      $data=array('mem_id'=>$mem_id,'gym_id'=>$gym_id);
      //echo $_POST['cat_id'];
      $this->session->set_userdata($data);
      print_r($this->session->userdata);//exit;
      //$details = $this->input->post();
      //echo $this->input->post('cat_id');
      //$responsekeys = array();
      //$responsekeys['cat_id'] = $_POST['cat_id'];
      //$responsekeys['mem_id'] = $_POST['mem_id'];
      //array_push($response['details'],$responsekeys);
      //$response['response'] = "yes";
      $this->load->view('user/payment_method_mobile');     
      //      }
         //else
         //{
           //$response['response'] = "no";
         //}
         //echo json_encode($response);
      //print_r($_POST);
      
      //echo $data['amt'] = $amtval;
      //$data['amt'] = 100;       
      //$this->load->view('user/payment_method_mobile',$data);         
    //}
  } 

public function payment_final_mobile() {
  $this->db->query('SET UNIQUE_CHECKS=0');
      $response1 = array();
      include('AuthorizeNet.php');

        if ($_SERVER['HTTP_HOST'] == 'localhost') {

            define("AUTHORIZENET_API_LOGIN_ID", "6pS7c2cbrdFq");
            define("AUTHORIZENET_TRANSACTION_KEY", "5u8955zE3PK4z534");
            define("AUTHORIZENET_SANDBOX", true);
        } else {

            define("AUTHORIZENET_API_LOGIN_ID", "9AM6dm4p5c");
            define("AUTHORIZENET_TRANSACTION_KEY", "64ALPz2dJ7sk62kkb");
            define("AUTHORIZENET_SANDBOX", false);
        }
        $sale = new AuthorizeNetAIM;
  //echo $_POST['joining_date'];
//echo $_POST['end_date'];
        $sale->amount = $_POST['price'];
        $sale->card_num = $_POST['card_number'];
        $sale->exp_date = $_POST['exp_date'];
        $response = $sale->authorizeAndCapture();
      // print_r($response);die;
$this->session->set_userdata(array('uname'=>$_POST['uname'],'phno'=>$_POST['phno'],'email'=>$_POST['email'],'joining_date'=>$_POST['joining_date'],'end_date'=>$_POST['end_date'],'mem_id'=>$_POST['mem_id'],'price'=>$_POST['price'],'offer'=>$_POST['offer'],'offer_price'=>$_POST['offer_price'],'gym_id'=>$_POST['gym_id'],'cat_id'=>$_POST['cat_id']));

        $this->webservices->storeUserDetails();
         if ($response->response_code != 1) {
          
            $trans_id = $response->transaction_id;         
           
    $response1['response'] = $response->response_reason_text;     
        } else {

          //$this->webservices->storeUserDetails();
          //need to check here weather the response is accepted or declined
            $trans_ids = $response->transaction_id;            
          //redirect('user/paypal_payment_success');
      $response1['response'] = "success"; 
    }

    echo json_encode($response1);
  }

public function paypal_payment_success_mobile() {

      
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        
        $this->email->from('randyali80@gmail.com', 'Gym Scanner');
        $this->email->to($this->session->userdata('email'));
        $this->email->subject('Your renewal subscription has been activated');
         
        $val = "<html><body>";
        $val .="Dear ".$this->session->userdata('uname').",";
        $val .="<h3>Your renewal subscription details are :</h3>";
        $val .="<table  cellspacing='1' width='100%'>";
        $val .="<tr style='color:blue;background:lightblue;'>
              <th>Name</th>
              <th>Phone Number</th>
              <th>Price</th>
              <th>Offer</th>
              <th>Offer price</th>
              <th>Suscription of</th>
          <th>From date</th>
          <th>To date</th>
            </tr>";
        $val .="<tr style='background:lightgreen;' valign='top'>
              <td>" . $this->session->userdata('uname') . "</td>
              <td>" . $this->session->userdata('phno') . "</td>
          <td>" . $this->session->userdata('price') . "</td>
          <td>" . $this->session->userdata('offer') . "</td>
          <td>" . $this->session->userdata('offer_price') . "</td>
          <td>" . $this->session->userdata('offer_name') . "</td>
              
          <td>" .  date('d-m-Y'). "</td>
          <td>" .  date('d-m-Y'). "</td>";  
        $val .="</tr></table>";        
        $val .="<div ><br/>";
        $val .="With Regards,<br/>";
        $val .="Gym Scanner Management<br/>";        
        $val .="</div></body></html>";  
  
        $this->email->message($val);
        $this->email->send();
        
        $this->data['page'] = 'user/payment_success';
    $this->load->view('template',$this->data);      
    }

    
}   