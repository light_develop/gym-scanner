<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservices_model extends CI_Model {


  public function get_catgs()
  { 	
    return $this->db->where('status','0')->get('category')->result();    			      			
  }
  public function get_cntrys()
  { 	
    return $this->db->where('status','1')->get('country')->result();
  }
  public function get_states(){
    return $this->db->get('states')->result();
  }
  public function get_cities(){
    return $this->db->get('cities')->result();
  }
  /*public function get_gyms($cat_id,$cntry,$state,$city,$zipcode){

    $condition = array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city);
    if($zipcode != '' && $zipcode != 0)
      $condition['g.gzip'] = $zipcode; 

    if($adv == 0 || $adv == ''){
    $result = $this->db->select('g.*,c.categoryname,country.country_name,states.state,cities.city,GROUP_CONCAT(gi.gym_images) images')   
                     ->from('gym g')
                     ->join('country','g.gcountry = country.id')
                     ->join('states','g.gstate = states.state_code')  
                     ->join('cities','g.gcity = cities.id')
                     ->join('gym_images gi','g.id = gi.gym_id')
                     ->join('category c','g.category = c.id')
                     ->where($condition)
                     ->order_by('g.id','asc')
                     ->group_by('g.id')
                     ->get()
                     ->result();  
            //echo $this->db->last_query(); exit;
             return $result; 
  }*/
  /*public function get_gyms($cat_id,$cntry,$state,$city,$zipcode,$adv){

    $condition = array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city);
    if($zipcode != '' && $zipcode != 0)
      $condition['g.gzip'] = $zipcode; 
    //echo $adv;
    if($adv != '0'){
      //print_r($adv);
      $advs = explode('_',$adv);
      //print_r($advs);exit;
      $result = $this->db->select('g.*,c.categoryname,country.country_name,states.state,cities.city,GROUP_CONCAT(gi.gym_images) images')   
                     ->from('gym g')
                     ->join('country','g.gcountry = country.id')
                     ->join('states','g.gstate = states.state_code')  
                     ->join('cities','g.gcity = cities.id')
                     ->join('gym_images gi','g.id = gi.gym_id')
                     ->join('category c','g.category = c.id')
                     ->where($condition);
                     $firstime=true;
              
              foreach ($advs as $key => $value){
                //if($value != '' && $value != 0){
                  if($firstime)
                    $this->db->like('g.advanced',$value,'%');
                  else
                    $this->db->or_like('g.advanced',$value,'%');
                  $firstime=false;
                //}
              }      
                                     
              $result = $this->db->order_by('g.id','asc')->group_by('g.id')->get()->result();
              echo $this->db->last_query();exit;
              return  $result;
    }else{
      $result = $this->db->select('g.*,c.categoryname,country.country_name,states.state,cities.city,GROUP_CONCAT(gi.gym_images) images')   
                         ->from('gym g')
                         ->join('category c','g.category = c.id')
                         ->join('country','g.gcountry = country.id')
                         ->join('states','gstate = states.state_code')  
                         ->join('cities','g.gcity = cities.id')
                         ->join('gym_images gi','g.id = gi.gym_id')
                         ->where($condition)
                         ->order_by('g.id','asc')
                         ->group_by('g.id')
                         ->get()
                        ->result();
      echo $this->db->last_query();exit;
    }
  }*/
  public function get_gyms($cat_id,$cntry,$state,$city,$zipcode,$adv){
    $cond = '';
    $condition = array('g.category'=>$cat_id,'g.gcountry'=>$cntry,'g.gstate'=>$state,'g.gcity'=>$city);
    $cond = "g.category = $cat_id AND g.gcountry = $cntry AND g.gstate = '".$state."' AND g.gcity = $city";

    if($zipcode != '' && $zipcode != 0){
      $condition['g.gzip'] = $zipcode; 
      $cond = $cond." AND g.gzip = '".$zipcode."'";
    }
    

    if($adv != '0'){
      $advs = explode('_',$adv);
      $firstime=true;
        $lque = '';
      foreach ($advs as $key => $value){
        if($value != ''){
          if($firstime)
            $lque = "g.advanced LIKE '%".$value."%'";
          else
            $lque = $lque." OR g.advanced LIKE '%".$value."%'";
          $firstime=false;
        }
      } 
      $query = "SELECT g.*,c.categoryname,country.country_name,states.state,cities.city,GROUP_CONCAT(gi.gym_images) images
                FROM gym g JOIN country ON g.gcountry = country.id JOIN states ON g.gstate = states.state_code JOIN cities ON g.gcity = cities.id JOIN gym_images gi ON g.id = gi.gym_id JOIN category c ON g.category = c.id
                WHERE ($cond) AND($lque) GROUP BY g.id ORDER BY g.id asc";                                             
      return $this->db->query($query)->result();
    }else{
      $result = $this->db->select('g.*,c.categoryname,country.country_name,states.state,cities.city,GROUP_CONCAT(gi.gym_images) images')   
                         ->from('gym g')
                         ->join('category c','g.category = c.id')
                         ->join('country','g.gcountry = country.id')
                         ->join('states','gstate = states.state_code')  
                         ->join('cities','g.gcity = cities.id')
                         ->join('gym_images gi','g.id = gi.gym_id')
                         ->where($condition)
                         ->order_by('g.id','asc')
                         ->group_by('g.id')
                         ->get()
                        ->result();
                        return $result;
    }
  }
  public function get_reviews($x){
    $query = "select r.comment,r.createdon, gym.gymname,CASE WHEN user_type = '2' THEN user.first_name ELSE supplier.first_name END AS username from reviews r,user,supplier,gym where gym.id ='".$x."' and gym.id = r.gym_id and CASE WHEN user_type = '2' THEN user.id = r.user_id ELSE supplier.id = r.user_id END group by r.id order by createdon desc";
    return $this->db->query($query)->result();
  }
  public function getfaq($user_id){
    $result = $this->db->select('f.id,f.message,f.createdon,u.first_name')->from('faq f')->join('user u','u.id=f.user_id')->where(array('user_id'=>$user_id,'is_replied'=>0))->order_by('createdon', 'desc')->get()->result();
    return $result;
  }	
  public function get_rpl($rpl_id){
    $query = "select f.message,f.createdon,CASE WHEN user_type = '1' THEN supplier.first_name WHEN user_type = '2' THEN user.first_name ELSE admin.username END AS username from faq f,user,supplier,admin where is_replied = $rpl_id and CASE WHEN user_type = '1' THEN supplier.id = f.user_id WHEN user_type = '2' THEN user.id = f.user_id ELSE admin.id = 1 END group by f.id order by f.createdon";
    return $this->db->query($query)->result();
  } 
  public function get_offrs($gym){
    $result = $this->db->select('o.*')->from('offer_description o')->join('offer_relation r','o.offer_relation_id = r.id')->where(array('r.gym_id'=>$gym,'o.status'=>'1'))->order_by('o.created_on','desc')->get()->result();
    return $result;
  }


      public function get_abtpg($id){
        $query = "SELECT id,menu_id,content,image FROM cms_content
                  where menu_id = $id";
        return $result = $this->db->query($query)->row();
      }
      public function get_ofrs($gymid){
        $query = "SELECT GROUP_CONCAT(CONCAT(m.id,'-',m.name,'-',price,'-',offer)) as price
              FROM gym_offers
              join membership m ON mem_id = m.id
              WHERE gym_id = $gymid
              order by mem_id asc ";
        return $result = $this->db->query($query)->row();
      }

   public function storeUserDetails(){

        $data = array(
          'username'=>$this->session->userdata('uname'),
          'mobile'=>$this->session->userdata('phno'),
          'email'=>$this->session->userdata('email'),
          'membership'=>$this->session->userdata('mem_id'),
          'cat_id'=>$this->session->userdata('cat_id'),
          'gym_id'=>$this->session->userdata('gym_id'),
          'price'=>$this->session->userdata('price'),
          'offer'=>$this->session->userdata('offer'),
          'offer_price'=>$this->session->userdata('offer_price'),
	'join_date'=>$this->session->userdata('joining_date'),
	'end_date'=>$this->session->userdata('end_date'),
          'createdon'=> mktime()
        );
        $result = $this->db->insert('user',$data);
        return $result;
      }
   
} 
