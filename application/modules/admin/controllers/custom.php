<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom extends CI_Controller {

         function __construct()
        {
            parent::__construct();
            $user = $this->session->userdata('user');
            if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
            $this->load->model('custom_model', 'custom');
            $this ->data['page'] = 'custom_view';

        }
 
	public function index()
	{
                $this -> data['offers'] = $this->custom->get_all();	
		$this->data['mode'] = 'all';
		$this->load -> view('template', $this -> data);
		
	}
	


        public function add()
	{

		if(isset($_POST['gym']) && !empty($_POST['gym']))
		{
			    extract($_POST);
			    //print_r($_POST); exit;
	      $rdata = array(
                      'gym_id'=>$gym,
                      'mem_id'=>$member,
                      'price'=>$price,
                      'offer'=>$offer,
                      'start'=>$start,
                      'end'=>$end,
                      'status'=>$status,
        ); 
            $id = $this -> custom -> custom_insert ($rdata);	
            $this->session->set_flashdata('success', 'Successfully Added');
            redirect('custom');
        
        
		}
		else
		{	
                        $this->data['gyms']=$this->custom->get_gym();
			$this -> data['mode'] = 'add';
			$this -> load -> view('template', $this -> data);
		}
	}
  	
	public function view(){
	      $record = $this->uri->segment(3);
	      $this->data['result'] = $this->custom->get_gym_with_id($record);
              $this->data['gyms']=$this->custom->get_gym();
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	      //echo '<pre>';print_r($result);		
	}
	
	public function edit(){

       if(isset($_POST['gym']) && !empty($_POST['gym'])){
       	    extract($_POST);
       	   $edata = array(
                      'gym_id'=>$gym,
                      'mem_id'=>$member,
                      'price'=>$price,
                      'offer'=>$offer,
                      'start'=>$start,
                      'end'=>$end,
                      'status'=>$status,
        ); 
           $id=$this->uri->segment(3);
				 $this->custom->update_data($id,$edata);	
				 $this->session->set_flashdata('success', 'Successfully Updated');
				 redirect('custom');
       }	     	
       elseif($this->uri->segment(3)){
	     	    $record = $this->uri->segment(3);
	     	    $this->data['result'] = $this->custom->get_gym_with_id($record);
                    $this->data['gyms']=$this->custom->get_gym();
	          $this->data['mode'] = 'edit';
	          $this -> load -> view('template', $this -> data);
	     	}
	     	else
	        redirect('custom');
	}
	
	public function delete()	
	
	{
	  $res = $this->custom->delete_data($_POST['id']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'Please try again');
			}		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(3))
			{
				$this->custom->deactivate($this->uri->segment(3));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('custom');
			
	
			}else
			{
			redirect('custom');
			}	
	
	}
		
		public function active()
		{
	
				if($this->uri->segment(3))
				{
		
					$this->custom->activate($this->uri->segment(3));
					$this -> data['mode'] = 'all';
					$this->session->set_flashdata('success', 'Successfully Activated');
					redirect('custom');
			
	
				}else
				{
					redirect('custom');
	
				}
	
		}
	
}

