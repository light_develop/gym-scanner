<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("customer_model",'customers');
		$this->data['page'] = 'customers';
	}

	public function index()
	{
	 	$this->data['customers'] = $this->customers->getusers();	
		$this->data['page'] = 'customers';
		$this->data['mode'] = 'all';
		$this->load->view('template', $this->data);	
	}
	public function add(){

		if(isset($_POST['user_id'])){
		    extract($_POST);
		    	$this->form_validation->set_rules('user_id', 'User', 'required');
		    	$this->form_validation->set_rules('category', 'Category', 'required');
		    	$this->form_validation->set_rules('gym_id', 'gym_id');
		    	if ($this->form_validation->run() == FALSE) {
	            	$this->data['page'] = 'customers';
	            	$this->data['mode'] = 'add';
	            	$this->load->view('template', $this->data);
				}else{	
					$cust = array('user_id'=>$user_id,'gym_id'=>$gym_id,'reffered_by'=>'0','is_activate'=>'1','reffered_on'=>date('Y-m-d H:i:s'),'activated_on'=>date('Y-m-d H:i:s'));
		    		if(isset($offer_name)){
		    			$req = array('gym_id'=>$gym_id,'user_id'=>$user_id,'name'=>$offer_name,'description'=>$description,'start_date'=>$start_date,'end_date'=>$end_date,'is_accept'=>'1','status'=>'0','createdon'=>date('Y-m-d H:i:s'));
		    			if($this->db->insert('offers',$req)){
		    				$ofrd_id = $this->db->insert_id();
		    				$ofr = array('offer_id'=>$ofrd_id,'user_id'=>$user_id,'offered_by'=>'0','offered_on'=>date('Y-m-d H:i:s'));
		    				$this->db->insert('offered_gyms',$ofr);
		    			}
		    		}else if(isset($ofr_id)){	
		    			$ofr = array('offer_id'=>$ofr_id,'user_id'=>$user_id,'offered_by'=>'0','offered_on'=>date('Y-m-d H:i:s'));
		    			$this->db->insert('offered_gyms',$ofr);
		    		}
		    		$this->db->insert('gym_customers',$cust); 
		    		redirect(ADMIN_URL.'customers'); 
        		}
		}else{
			$this->data['mode'] = 'add';
			$this->data['users'] = $this->customers->get_users();
			$this->load->view('template', $this->data);
		}
	}
	public function get_gyms(){
		$cat = $_POST['id'];
		$res = $this->db->select('id,gymname')->from('gym')->where('category',$cat)->get()->result();
		foreach ($res as $key => $value) {
			$gyms[$value->id] = $value->gymname;
		}
		$attributes = 'class="span6 required" id="gym_id" onchange="get_offers(this.value)"';
		$gyms[''] = 'Choose';
		echo form_dropdown('gym_id',$gyms,'',$attributes);
	}
	public function get_offers(){
		$gym = $_POST['id'];
		$data['res'] = $this->db->where(array('gym_id'=>$gym,'type'=>'1','status'=>'1'))->get('offers')->result();
		//print_r($res);exit;
		echo $this->load->view('ofrs',$data);
		//print_r($data);
	}
	public function view(){
	      $record = $this->uri->segment(4);
	      $result = $this->customers->getuser($record);
	      $this->data['customers'] = $result;
	      $this->data['mode'] = 'view';
	      $this->load->view('template', $this -> data);
	}
	
	public function edit(){

       if(isset($_POST['adduser'])){
       	    extract($_POST);
       	    $con  = array('gym_id' => $gym_id,
		    			  'mem_id' => $mem_id );
       	    $this->load->model('offers_model','ofr');
		    $ofr_det = $this->ofr->get($con);

		    $this->load->model('memship_model','mem');
		    $mem_det = $this->mem->get('id',$mem_id);

		    $due = $mem_det->name;
		    $price = $ofr_det->price;
		    $ofr = $ofr_det->offer;
		    $ofr_price = $price -($price * $ofr/100);
		    $joindate = date('Y-m-d',strtotime($datepicker));
		    $end_date = date('Y-m-d',strtotime("$joindate + $due months"));
       	    $edata = array(
                      'email'=>$email,
                      'updatedon'=>mktime()
        	); 
        $this->customers->update($id,$edata);
        $this->session->set_flashdata('success', 'Successfully Updated');
		redirect('admin/customers');
       }	     	
       elseif($this->uri->segment(4)){
     	    $record = $this->uri->segment(4);

     	    $result = $this->customers->getuser($record);
     	    $this->session->set_userdata('gym',$result->gym_id);
          	$gym = $result->gym_id; 
          	$this->data['customers'] = $result;
          	$this->data['mode'] = 'edit';
          	$this->data['gymbycat'] = $this->customers->gymsByCat($this->session->userdata('category'));
          	$this -> load -> model ('gyms_model','gym');
		    $this -> data['gyms'] = $this -> gym -> dropdown('id','gymname');
		    $this->load->model('cat_model','cat');
		    $this -> data['cat'] = $this -> cat -> dropdown('id','categoryname');

	        $this -> load -> view('template', $this -> data);
     	}
     	else
	    {
		    redirect('admin/customers');
	    }
	}	
}

?>
