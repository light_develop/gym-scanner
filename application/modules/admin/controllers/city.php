	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("city_model",'city');
		$this ->data['page'] = 'citylist';
	}

	public function index()
	{
		$this->data['mode'] = 'all';
		$this->load->model('country_model','country');
		$this->data['countries'] = $this->country->dropdown('id','country_name');
		//$this->data['cities'] = $this->city->get_citylist();	
		$this->load -> view('template', $this->data);
		
	}
	public function get_states($id)
	{		
		extract($_POST);
		$data = $this->city->get_states($id); 
       	$attributes= 'id="state"  class="span6 required" onchange="get_cities(this.value)" ';
       	$data['0'] = 'State'; 
        echo form_dropdown('state',$data, '0',$attributes);	
	}
	public function get_cities($id){
		$this->data['cities'] = $this->city->get_citylist($id);
		echo $this->load->view('cities',$this->data);
	}
	public function add()
	{

		if(isset($_POST['city_name']))
		{
			//print_r($_POST);exit;
			    extract($_POST);
			    if($_FILES['userfile']['name'] != ''){

			      /************* Excel uploading Begin *************/
			      $config['upload_path'] = './document/';
				  $config['allowed_types'] = '*';
				  $config['max_size'] = '0';

				  $result_array=array();

				  $this->load->library('upload', $config);

				  if ( ! $this->upload->do_upload())
				  {
				   $error = array('error' => $this->upload->display_errors());
				   $result_array[]=array("error"=>"yes","message"=>"Unable to upload file please contact admin");
				  }

				  else
				  {
				    set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
				    require $_SERVER['DOCUMENT_ROOT'].'/gym/excellnew/Classes/PHPExcel/IOFactory.php';

				    $data = array('upload_data' => $this->upload->data());
				    $inputFileName = $_SERVER['DOCUMENT_ROOT'].'/gym/document/'.$data['upload_data']['file_name'];  // File to read
					try {
					 $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} catch(Exception $e) {
					 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					foreach ($sheetData as $row) {
						foreach ($row as $key => $value) {
							$rdata = array('city'=>ucfirst($value),
						      			   'state_code'=>$state,
					                       'status'=>1
					        ); 
        					$this -> city -> insert ($rdata);
						}
						
					}
				}
				/************* Excel uploading End *************/
				$this->session->set_flashdata('success', 'Successfully Added');
				redirect('admin/city');
			}
			else{

			    $rdata = array('city'=>ucfirst($city_name),
	      				 'state_code'=>$state,
                         'status'=>1
        		); 
		        if(!$this->city->count_all_results('city',$rdata['city']))
			    {
				    $id = $this -> city -> insert ($rdata);	
				    $this->session->set_flashdata('success', 'Successfully Added');
				    redirect('admin/city');
			    }
			    else
			    {
				    $this -> session -> set_flashdata('error','You are trying to add already existing City');
				    redirect('admin/city/add');
			    }           
			}
		}
		else
			{			
				$this->data['mode'] = 'add';
				$this->load->model('country_model','country');
				$this->data['countries'] = $this->country->dropdown('id','country_name');
				$this->load->view('template', $this->data);
			}
	}
  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $this->data['result'] = $this->city->get_list($record);
	      $this->data['mode'] = 'view';
	      $this ->load->view('template', $this ->data);
	}
	
	public function edit(){

       if(isset($_POST['addmem'])){
       	//print_r($_POST);exit;
       	    extract($_POST);
       	    $edata = array(
                      'city'=>$city_name,
                      'state_code'=>$state
            ); 
        $member = $this->city->get_list($id);
        if($member->city != $edta['city_name']){
        	   if(!$this ->city ->count_all_results('city',$edata['city']))
			           {
				            $this->city->update($id,$edata);	
				            $this->session->set_flashdata('success', 'Successfully Updated');
				            redirect('admin/city');
			           }
			        else
			           {
				            $this->session-> set_flashdata('error','You are trying to add already existing City');
				            redirect('admin/city/edit/'.$id);
			           }  
        	
        	}
        else{
               $this->city->update($id,$edata);
               $this->session->set_flashdata('success', 'Successfully Updated');
	           redirect('admin/city');
			}
       }	     	
       elseif($this->uri->segment(4)){
	     	$record = $this->uri->segment(4);
	     	$this->load->model('country_model','country');
			$this->data['countries'] = $this->country->dropdown('id','country_name');
	     	$det = $this->city->get_list($record);
	     	$this->data['states'] = $this->city->get_states($det->cnt_id);
	     	$this->data['result'] = $det;
	        $this->data['mode'] = 'edit';
	        $this ->load-> view('template', $this->data);
	    }
	    else
		{
	        redirect('admin/city');
		}
	}
	public function change_status(){
        $status=$_POST['status'];
        $status= ($status == 1)? '0' : '1';
        $this->db->where('id',$_POST['id'])->update('cities',array('status'=>$status));   
    }
	public function delete(){
		$city = $_POST['id'];
		$res = $this->db->where('gcity',$city)->get('gym')->num_rows();
		if($res > 0){
			echo 'failed';
			$result = $this->session->set_flashdata('delete', 'Please try again');
		}else{
			$this->db->where('id',$city)->delete('cities');
			echo 'success';
			$result = $this->session->set_flashdata('delete', 'Successfully Deleted');
		}
	  	$res = $this->city->delete_data($_POST['id']); 		
	}
	
}


?>
