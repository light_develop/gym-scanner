<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offers extends MY_Controller
{


    public function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (!isset($user) || !is_object($user))
            redirect('admin/sessionexp');
        $this->load->model('offers_model', 'offers');
        $this->data['page'] = 'as_offers';
    }

    public function index()
    {
        $this->data['offers'] = $this->offers->get_all();
        $this->data['mode'] = 'all';
        $this->load->view('template', $this->data);

    }

    public function add_offer()
    {

        $this->data['gyms'] = $this->offers->get_gym();
        $this->data['mode'] = 'add';
        $this->load->view('template', $this->data);
    }

    public function add()
    {
        if (isset($_POST['gym']) && !empty($_POST['gym'])) {
            extract($_POST);
            $data = array('gym_id' => $gym, 'user_id' => $this->session->userdata('user_id'));
            if ($this->db->insert('offer_relation', $data)) {
                $ofr_id = $this->db->insert_id();

                foreach ($duration as $key => $value) {
                    $description = '';
                    if (trim($value) != '') {
                        $description = $value . ' Membership + ' . $offer[$key];
                        if (trim($membership[$key]) != '')
                            $description .= ' for US$ ' . ($membership[$key] - ($membership[$key] * $discount[$key] / 100));
                        $rdata = array(
                            'offer_relation_id' => $ofr_id,
                            'duration' => $value,
                            'membership' => $membership[$key],
                            'discount' => $discount[$key],
                            'offer' => $offer[$key],
                            'description' => $description,
                            'status' => '0',
                            'created_on' => date('Y-m-d H:i:s')
                        );
                        $id = $this->db->insert('offer_description', $rdata);
                    }
                }
            }

            $this->session->set_flashdata('success', 'Successfully Added');
            redirect(ADMIN_URL . 'offers');
        }
    }

    public function edit($id)
    {

        $this->data['gyms'] = $this->offers->get_gym();
        $res = $this->db->where('id', $id)->get('offer_relation')->num_rows();
        if ($res > 0) {
            $this->data['details'] = $this->offers->get_details($id);
            $this->data['mode'] = 'edit';
            $this->load->view('template', $this->data);
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function update_offr()
    {
        if (isset($_POST['gym']) && !empty($_POST['gym'])) {
            extract($_POST);
            if ($gym != $old_gym)
                $this->db->where('id', $relation_id)->update('offer_relation', array('gym_id' => $gym));
            foreach ($duration as $key => $value) {
                $description = '';
                if (trim($value) != '') {
                    $description = $value . ' Membership + ' . $offer[$key];
                    if (trim($membership[$key]) != '')
                        $description .= ' for US$ ' . ($membership[$key] - ($membership[$key] * $discount[$key] / 100));
                    $rdata = array(
                        'duration' => $value,
                        'membership' => $membership[$key],
                        'discount' => $discount[$key],
                        'offer' => $offer[$key],
                        'description' => $description
                    );
                    if (isset($offr_id[$key]))
                        $this->db->where('id', $offr_id[$key])->update('offer_description', $rdata);
                    else {
                        $rdata['offer_relation_id'] = $relation_id;
                        $rdata['created_on'] = date('Y-m-d H:i:s');
                        $this->db->insert('offer_description', $rdata);
                    }
                }
            }
            $this->session->set_flashdata('success', 'Updated Successfully');
            redirect(ADMIN_URL . 'offers');
        }
    }

    public function check_gym($gym)
    {
        $res = $this->db->where('gym_id', $gym)->get('offer_relation')->num_rows();
        if ($res > 0)
            echo 'Already offers exist for this facility';
        else
            echo '';
    }

    public function send_restrict()
    {
        $message = $_POST["message"];
        $email = $_POST["email"];
        $subject = "Offer Restriction";
        $headers = 'From: GymScanner '.'<admin@gymscanner.com>' . "\r\n";
       /* $headers = "From: GymScanner webmaster@example.com\r\n";*/
        mail($email, $subject, $message, $headers);

        /*$config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);


        $this->email->from('admin@gymscanner.com','Gymscanner');
        $this->email->to($email);
        $this->email->subject($subject);

        $this->email->message($message);
        $this->email->send();*/

    }

    public function change_status()
    {
        $status = $_POST['status'];
        $id = $_POST['id'];

        if ($status == 0) {
            $query = "select r.* ,o.created_on, gym.gymname,CASE WHEN r.user_id = '0' THEN admin.email ELSE supplier.email END AS email,CASE WHEN r.user_id = '0' THEN admin.username ELSE supplier.first_name END AS username from offer_description o,offer_relation r,admin,supplier,gym where gym.id = r.gym_id and o.id = $id and r.id = o.offer_relation_id and CASE WHEN r.user_id = '0' THEN admin.id = 1 ELSE supplier.id = r.user_id END group by r.id";
            $res_set = $this->db->query($query)->row();
            /****** email config settings ******/
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['smtp_port'] = 25;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            // mail sending process //

            $this->email->from('admin@gymscanner.com', 'Gym Scanner');
            $this->email->to($res_set->email);
            $this->email->subject('Approved');

            $val = "<html><body>";
            $val .= "<h4>Dear " . $res_set->username . ",</h4>";
            $val .= "<p>Your offer posted on " . $res_set->created_on . " for " . $res_set->gymname . " has now been approved by gymscanner.com and is now live and ready to accept customers.</p>";
            $val .= "<p>We wish you all the best and stay fit.</p>";
            $val .= "<p style='font-size:10pt;font-family: Arial;'>Gymscanner.com</p>";
            $val .= "</body></html>";

            $this->email->message($val);
            $this->email->send();
            // end mail sending process //
        }
        $status = ($status == 1) ? '2' : '1';
        $this->db->where('id', $_POST['id'])->update('offer_description', array('status' => $status));
    }

    public function del_row()
    {
        $id = $_POST['id'];
        $res = $this->db->where('offr_id', $id)->get('gym_customers')->num_rows();
        if ($res > 0)
            echo 1;
        else {
            $this->db->where('id', $id)->delete('offer_description');
            echo 0;
        }
    }

    public function del_offr()
    {
        $id = $_POST['id'];
        $res = $this->db->select('c.id')->from('gym_customers c')->join('offer_description d', 'd.id = c.offr_id')->join('offer_relation o', 'd.offer_relation_id = o.id')->where('o.id', $id)->get()->num_rows();
        if ($res > 0)
            echo 1;
        else {
            $this->db->where('id', $id)->delete('offer_relation');
            $this->db->where('offer_relation_id', $id)->delete('offer_description');
            echo 0;
        }
    }
}

?>
