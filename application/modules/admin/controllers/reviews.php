<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("reviews_model",'reviews');
	}

	public function index()
	{
	 	$this->data['details'] = $this->reviews->getdets();	
		$this->data['page'] = 'reviews';
		$this->data['mode'] = 'all';
		$this->load->view('template', $this->data);	
	}
	public function change_status(){
		$status=$_POST['status'];
        $status= ($status == 1)? '0' : '1';
        $this->db->where('id',$_POST['id'])->update('reviews',array('is_edit'=>$status));
	}

	public function languages() {
		$this->data['details'] = $this->reviews->getdets();
		$this->data['page'] = 'languages';
		$this->data['mode'] = 'all';
		$this->load->view('template', $this->data);
	}
}

?>
