<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Controller {

	public function __construct()
	{
		parent::__construct();   
		 $user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp');  
                   
		   $this -> load -> model ('banners_model','banner');
     $this -> data['page'] = 'banners';
	}
	
	public function index()
	{
 		
		$this -> data['banners'] = $this->banner->getbanners();
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		//print_r($this->data); exit;
			
	}	

	public function add()
	{
		
		if(isset($_POST['addbanner']))
		{	
		  extract($_POST);
		  //print_r($_POST); exit;

				$banner = array(
				'gym_id' => $gym_id,
				'banner_title' => $banner_title,
				'description' => $description,
				'status' => $status	
				);
			
			if(!empty($_FILES) && $_FILES['image']['error'] !=4){
				$image = $this -> banner -> do_upload_image('image');
				if(is_array($image)){
					$this -> session -> set_flashdata('error',$image['upload_error']);
					redirect('admin/banners/add');
				}else
					$banner ['image'] = $image; 
			}
			if(!$this -> banner ->count_all_results('gym_id',$banner['gym_id']))
			{
				$id = $this -> banner -> insert ($banner);	
				$this -> session -> set_flashdata('success','Successfully added');
				redirect('admin/banners');
			}
			else
			{
				$this -> session -> set_flashdata('error','You are trying to add already existing Gym');
				redirect('admin/banners/add');
			}
		}
		else
		{			
			$this -> data['mode'] = 'add';
			$this -> load -> model ('gyms_model','gym');
			$this -> data['gyms'] = $this -> gym -> dropdown('id','gymname');
			$this -> load -> view('template', $this -> data);
		}
	}

	public function edit()
	{ 

		if(isset($_POST['gym_id']))
		{
		   extract($_POST);
			$banner = array(
				'description' => $description,
				'gym_id' => $gym_id,			
				'status' => $status,
				'banner_title' => $banner_title			
				);
			$getid = $this->banner->getid($id); 
			if($getid[0]->gym_id != $banner['gym_id']){
			if(!$this -> banner ->count_all_results('gym_id',$banner['gym_id']))
			{ 
				if(!empty($_FILES) && $_FILES['image']['error'] !=4){
					$image = $this -> banner -> do_upload_image('image');
					if(is_array($image)){
						$this -> session -> set_flashdata('error',$image['upload_error']);
						redirect('admin/banners/edit/'.$id);
					}else
					{
						$record = $this -> banner -> get ($id);
						if(file_exists($this -> banner ->original_path.'/'.$this->banner->table.'/'.$record->image)){
							unlink($this -> banner ->original_path.'/'.$this->banner->table.'/'.$record->image);
							unlink($this -> banner ->thumbs_path.'/'.$this->banner->table.'/'.$record->image);
						}
						$banner ['image'] = $image;
					}
				}
				$this -> banner -> update ($id,$banner);
				$this->session->set_flashdata('success', 'Successfully Updated');
				redirect('admin/banners');
			}
			else
			{
				$this -> session -> set_flashdata('error','You are trying to add alredy exist banner');
				redirect('admin/banners/edit/'.$id);
			}
			}else{
				 if(!empty($_FILES) && $_FILES['image']['error'] !=4){
					$image = $this -> banner -> do_upload_image('image');
					if(is_array($image)){
						$this -> session -> set_flashdata('error',$image['upload_error']);
						redirect('admin/banners/edit/'.$id);
					}else
					{
						$record = $this -> banner -> get ($id);
						if(file_exists($this -> banner ->original_path.'/'.$this->banner->table.'/'.$record->image)){
							unlink($this -> banner ->original_path.'/'.$this->banner->table.'/'.$record->image);
							unlink($this -> banner ->thumbs_path.'/'.$this->banner->table.'/'.$record->image);
						}
						$banner ['image'] = $image;
					}
				}
				$this -> banner -> update ($id,$banner);
				$this->session->set_flashdata('success', 'Successfully Updated');
				redirect('admin/banners');		
				
			}
		}
		elseif($this->uri->segment(4))
		{			
			$record = $this->uri->segment(4);
			if($record)
			{
				$this -> load -> model ('gyms_model','gym');
				$this -> data['gym'] = $this -> gym -> dropdown('id','gymname');
				$resset = $this -> banner -> getdet ($record);
				$this -> data['banner'] = $resset[0];
				$this -> data['mode'] = 'edit';
				$this -> load -> view('template', $this -> data);
			}
		}
		else
		{
			redirect('admin/banners');
		}
	}
	
	public function view()
	{
		if($this->uri->segment(4))
		{			
		$record = $this->uri->segment(4);
			
				$result = $this -> banner -> getdet($record);
				$this -> data['mode'] = 'view';
				$this -> data['banner'] = $result[0];
				$this -> load -> view('template', $this -> data);
			
		}
		else
		{
			redirect('admin/banners');
		}
	}
	
	public function delete()
	{
	  $res = $this->banner->delete_data($_POST['id']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'Try again');
			}
		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(4))
			{
				$this->banner->deactivate_banner($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/banners');
			
	
			}else
			{
			redirect('admin/banners');
			}	
	
	}
		
		public function active()
		{
	
				if($this->uri->segment(4))
				{
		
					$this->banner->activate_banner($this->uri->segment(4));
					$this -> data['mode'] = 'all';
					$this->session->set_flashdata('success', 'Successfully Activated');
					redirect('admin/banners');
			
	
				}else
				{
					redirect('admin/banners');
	
				}
	
		}
	
}

?>