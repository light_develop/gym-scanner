<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
           
//	    $user = $this->session->userdata('user');
//print_r($user);
	}
	public function index()
	{
            log_message('error', 'something');
		if($this->input->post('username'))
		{
			$this -> load -> model('login_model','login');
			$user = array(
				'username' => $this->input->post('username'),
				'password' => sha1($this->input->post('password'))
				);
			$result = $this -> login -> login($user);

			if($result){
				$this->session->set_flashdata('success', 'Successfully Logged in');
				redirect(ADMIN_URL.'dashboard');
			}
			else{
				$this->session->set_flashdata('error', 'Username or Password is wrong please try again');
				redirect(ADMIN_URL.'login');
			}
		}
		else
			$this->load->view('login');
	}

	public function login()
	{
		if($this->input->post('username'))
		{
			$this -> load -> model('login_model','login');
			$user = array(
				'username' => $this->input->post('username'),
				'password' => sha1($this->input->post('password'))
				);
			$result = $this -> login -> login($user);
			if($result){
				$this->session->set_flashdata('success', 'Successfully Logged in');
				redirect('dashboard');
			}
			else{
				$this->session->set_flashdata('error', 'Username or Password is wrong please try again');
				redirect('admin/login');
			}
		}
		else
			$this->load->view('login');
	}
	public function logout()
	{
		$user = $this->session->userdata('user');              
                
    		if(isset($user) ||  is_object($user)) 
                     $this->session->unset_userdata('user');
                
		redirect('admin');
	}
	 public function sessionexp(){ 
       $this->load->view('sessionexp');	 	
	 	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>
