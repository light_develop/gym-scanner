<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Languages extends MY_Controller
{


    public function __construct()
    {
        error_reporting(E_ALL);
        header("Content-type: text/html; charset=utf-8");

        parent::__construct();

        $user = $this->session->userdata('user');
        if (!isset($user) || !is_object($user))
            redirect('admin/sessionexp');

        $this->load->helper('form');         // form open in view

        /* database initialize uncomment install if you will initialize database first time */
        $this->load->database();
       // $this->_install("1.2");
    }

    public function index()
    {
        $this->data['page'] = 'languages';
        $this->data["languageSaved"] = (!$data = $this->getLanguageData()) ? array() : $data;
        $this->load->view('template', $this->data);
    }

    public function getLanguageData()
    {
        return $this->db->get("languages_translate")->result_array();
    }

    protected function _install($v)
    {
        if($v == "1.0") {
            $this->db->query("
                          CREATE TABLE `languages`
                                  (`lang_id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                   `lang_name` VARCHAR(255),
                                   `status` INT(1))
                                  ");
            $this->db->query("ALTER TABLE `languages` ADD COLUMN `image` MEDIUMBLOB NOT NULL AFTER `status`;");

            $this->db->query("
                            CREATE TABLE `languages_translate` (entity_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                      main_lang TEXT,
                                      second_lang TEXT,
                                      lang_id INT(10) NOT NULL,
                                      FOREIGN KEY (lang_id) REFERENCES languages(lang_id)
         )");
            $this->db->query("ALTER TABLE `languages_translate` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;");

            /* trigger if we drop language */
            $this->db->query("
                            CREATE TRIGGER
                            `WHEN_LANGUAGE_REMOVE` BEFORE DELETE
                            ON `languages` FOR EACH ROW
                            DELETE FROM `languages_translate` WHERE languages_translate.lang_id = OLD.lang_id;"
            );
        }

        if($v == "1.1") {
            $this->db->query("ALTER TABLE languages ADD COLUMN image TEXT CHARACTER SET ascii");
        }
    }

    public function settings()
    {
        $this->data['page'] = 'languagesSettings';
        $this->load->view('template', $this->data);
    }

    public function addNewLanguage() {
        $data = $this->input->post();

        if(isset($data["lang_name"])) {
            $this->db->insert("languages", array("lang_name" => $data["lang_name"], "status" => 0));
        }

        redirect(ADMIN_URL . 'languages/settings');
    }

    public function changeLanguage() {
        $langId = $this->input->get("lang_id");
        $status = $this->input->get("status");

        if($langId !==0 && $status === 'null') {
            $this->db->delete('languages', array('lang_id' => $langId));


        } elseif($langId && ($status == "0" || $status == "1")) {
            $this->db->update("languages", array("status" => $status), array("lang_id" => $langId));
        }

        redirect(ADMIN_URL . 'languages/settings');
    }

    public function saveLanguages()
    {
        $data = $this->input->post();

        if (isset($data["remove"])) {
            $this->db->where_in('entity_id', array_keys($data["remove"]));
            $this->db->delete("languages_translate");
        }
        if (isset($data["insert"]) && is_array($data["insert"])) {
            if ($this->db->insert_batch("languages_translate", $data["insert"])) {
                $this->session->set_flashdata('success', 'New data saved ok!');
            } else {
                $this->session->set_flashdata('error', 'Problem with insering');
            }
        }

        if (isset($data["update"]) && is_array($data["update"])) {
            if ($this->db->update_batch("languages_translate", $data["update"], 'entity_id')) {
                $this->session->set_flashdata('success', 'Updated ok');
            }
        }

        $langCode = ($get = $this->input->get("default_lang")) ? "?default_lang=" . $get : "";

        redirect(ADMIN_URL . 'languages' . $langCode);

    }

    public function saveIcon() {
        $image = $this->input->post("image");
        $_id = $this->input->post("id");

        if(!$image || !$_id) return false;

        $this->db->update("languages", array("image"=>$image), array("lang_id" => $_id));
    }
}

?>
