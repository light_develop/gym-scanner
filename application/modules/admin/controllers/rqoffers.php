<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rqoffers extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 	
        $this->load->model('offers_model', 'offers'); 
		$this->load->model('mmemship_model', 'mmem');            
            
        $this -> data['page'] = 'rq_offers'; 	
	}

	public function index()
	{
        $this -> data['offers'] = $this->offers->get_all(2);
                          	
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		
	}
	


        public function add()
	{

		if(isset($_POST['gym']) && !empty($_POST['gym']))
		{
			    extract($_POST);
				$rdata = array(
                      'gym_id'=>$gym,
                      'name'=>$name,
                      'type'=>'2',
                      'start_date'=>$this->change_dbdate_format($start_date),
                      'end_date'=>$this->change_dbdate_format($end_date),
                      'description'=>$description,
                      'status'=>$status,
                      'createdon'=>date('Y-m-d H:i:s')
				); 
        
            $id = $this -> db -> insert ('offers', $rdata);	
            $this->session->set_flashdata('success', 'Successfully Added');
            redirect('admin/rqoffers');
		}
		else
		{	
            $this->data['gyms']=$this->mmem->get_gym();
			$this -> data['mode'] = 'add';
			$this -> load -> view('template', $this -> data);
		}
	}
  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $this->data['result'] = $this->offers->get_offers_by_id($record);
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	      //echo '<pre>';print_r($result);		
	}
	
	public function edit(){

       if(isset($_POST['gym']))
       {
           extract($_POST);
       	   $rdata = array(
                      'gym_id'=>$gym,
                      'name'=>$name,
                      'start_date'=>$this->change_dbdate_format($start_date),
                      'end_date'=>$this->change_dbdate_format($end_date),
                      'description'=>$description,
                      'is_accept'=>$is_accept,
                      'status'=>$status,
                      'updatedon'=>date('Y-m-d H:i:s')
				);
				
			$this->db->where('id', $id)->update('offers', $rdata);
			$this->session->set_flashdata('success', 'Successfully Added');
            redirect('admin/rqoffers');
       }	     	
       elseif($this->uri->segment(4)){
		   
	     	  $record = $this->uri->segment(4);
	     	  $this->data['result'] = $this->offers->get_offers_by_id($record);
              $this->data['gyms']=$this->mmem->get_gym();
	          $this->data['mode'] = 'edit';
	          
	          $this -> load -> view('template', $this -> data);
	     	}
	     	else
		    {
			        redirect('admin/rqoffers');
		    }
	}
	
	public function delete()	
	
	{
	  $res = $this->offers->delete_data($_POST['id']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'Please try again');
			}		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(4))
			{
				$this->offers->deactivate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/rqoffers');
			
	
			}else
			{
			redirect('admin/rqoffers');
			}	
	
	}
		
		public function active()
		{	
			if($this->uri->segment(4))
			{
	
				$this->offers->activate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('success', 'Successfully Activated');
				redirect('admin/rqoffers');
			}
			else
			{
				redirect('admin/rqoffers');

			}
	
		}
	
}


?>
