<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class users extends MY_Controller
{


    public function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (!isset($user) || !is_object($user))
            redirect('admin/sessionexp');
        $this->load->model("user_model", 'user');
        $this->data['page'] = 'users';
    }

    public function index()
    {
        $this->data['users'] = $this->user->getusers();
        $this->data['page'] = 'users';
        $this->data['mode'] = 'all';
        $this->load->view('template', $this->data);
    }

    public function add()
    {

        if (isset($_POST['user_name'])) {
            extract($_POST);
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('user_name', 'User Name', 'required|is_unique[user.user_name]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'users';
                $this->data['mode'] = 'add';
                $this->load->view('template', $this->data);
            } else {
                $rdata = array('user_name' => $user_name,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'password' => $password,
                    'reffered_by' => '0',
                    'createdon' => date('Y-m-d')
                );
                if ($this->db->insert('user', $rdata)) {
                    $this->session->set_flashdata('success', 'New User details Added Successfully...');
                    redirect(ADMIN_URL . 'users');
                }
            }
        } else {
            $this->data['mode'] = 'add';
            $this->load->view('template', $this->data);
        }
    }

    public function edit_user()
    {
        extract($_POST);
        if ($old_name == $user_name) {
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $data['details'] = $this->user->get_user($uid);
                $data['page'] = 'users';
                $data['mode'] = 'edit';
                $this->load->view('template', $data);
            } else {
                if ($this->user->update_det()) {
                    $this->session->set_flashdata('success', 'User details Updated Successfully...');
                    redirect(ADMIN_URL . 'users');
                }
            }
        } else {
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('user_name', 'User Name', 'required|is_unique[user.user_name]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $data['user'] = $this->user->get_user($uid);
                $data['page'] = 'users';
                $data['mode'] = 'edit';
                $this->load->view('template', $data);
            } else {
                if ($this->user->update_det()) {
                    $this->session->set_flashdata('success', 'User details Updated Successfully...');
                    redirect(ADMIN_URL . 'users');
                }
            }
        }
    }

    public function sendWarning($userId)
    {
        //add warning flag in to the DB
        $this->db->where('id', $userId);
        $this->db->update('user', array('warning' => 1));
        //select user info from DB
        $this->db->select('first_name, last_name, email');
        $this->db->from('user');

        $query = $this->db->get();;
        $query_assoc = $query->result_array();
        extract($query_assoc[0]);

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // mail sending process //

        $this->email->from('admin@gymscanner.com', 'Gym Scanner');
        $this->email->to($email);
        $this->email->subject('Confirmation Message');

        $val = "<html><body>";
        $val .= "<h4>Dear " . ucfirst($first_name) . ",</h4>";
        $val .= "<p>You were assigned a warning. </p>";
        $val .= "<p> In the future, for violation of the rules of use of this resource, you will be banned. </p><br/>";
        $val .= "<div><br/>";
        $val .= "</div>";
        $val .= "<p> From the Gymscanner.com team, we thank you for getting involved. Stay fit. </p>";
        $val .= "<div ><br/>";
        $val .= "With Regards,<br/>";
        $val .= "Team Gymscanner<br/>";
        $val .= "Gymscanner.com<br/>";
        $val .= "1005 West Champlin Drive<br/>";
        $val .= "Minnesota 55030, USA<br/>";
        $val .= "</div>";
        $val .= "</div><br/><hr style='color:#800000;' />";
        $val .= "<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
        $val .= "</div></body></html>";

        $this->email->message($val);
        $this->email->send();


        redirect(ADMIN_URL . 'users');

    }

    public function banUser($userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('user', array('banned' => 1));

        $this->db->select('first_name, last_name, email');
        $this->db->from('user');

        $query = $this->db->get();;
        $query_assoc = $query->result_array();
        extract($query_assoc[0]);

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // mail sending process //

        $this->email->from('admin@gymscanner.com', 'Gym Scanner');
        $this->email->to($email);
        $this->email->subject('Confirmation Message');

        $val = "<html><body>";
        $val .= "<h4>Dear " . ucfirst($first_name) . ",</h4>";
        $val .= "<p> You have been banned by the administrator for violating the rules of use of this resource. </p>";
        $val .= "<p> On the removal of restrictions and the ban, contact the administrator! </p><br/>";
        $val .= "<div><br/>";
        $val .= "</div>";
        $val .= "<p> From the Gymscanner.com team, we thank you for getting involved. Stay fit. </p>";
        $val .= "<div ><br/>";
        $val .= "With Regards,<br/>";
        $val .= "Team Gymscanner<br/>";
        $val .= "Gymscanner.com <br/>";
        $val .= " 1005 West Champlin Drive<br/>";
        $val .= " Minnesota 55030, USA<br/>";
        $val .= "</div>";
        $val .= "</div><br/><hr style='color:#800000;' />";
        $val .= "<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
        $val .= "</div></body></html>";

        $this->email->message($val);
        $this->email->send();


        redirect(ADMIN_URL . 'users');
    }

    public function unBanUser($userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('user', array('warning' => 0, 'banned' => 0));
        $this->db->select('first_name, last_name, email');
        $this->db->from('user');

        $query = $this->db->get();;
        $query_assoc = $query->result_array();
        extract($query_assoc[0]);

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['smtp_port'] = 25;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // mail sending process //

        $this->email->from('admin@gymscanner.com', 'Gym Scanner');
        $this->email->to($email);
        $this->email->subject('Confirmation Message');

        $val = "<html><body>";
        $val .= "<h4>Dear " . ucfirst($first_name) . ",</h4>";
        $val .= "<p> Congratulations you are no longer limited and you are no longer in the ban list! </p>";

        $val .= "<br/>";
        $val .= "<div><br/>";
        $val .= "</div>";
        $val .= "<p> From the Gymscanner.com team, we thank you for getting involved. Stay fit. </p>";
        $val .= "<div ><br/>";
        $val .= "With Regards,<br/>";
        $val .= "Team Gymscanner<br/>";
        $val .= "Gymscanner.com <br/>";
        $val .= " 1005 West Champlin Drive<br/>";
        $val .= " Minnesota 55030, USA<br/>";
        $val .= "</div>";
        $val .= "</div><br/><hr style='color:#800000;' />";
        $val .= "<p style='font-size:7.5pt;font-family: Arial;color: gray;'>Email Confidentiality Notice and Disclaimer: This Email and any files transmitted with it are confidential and are intended solely for the use of the individual or entity to which they are addressed. Access to this email by anyone else is unauthorized. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is prohibited. Alternatively, if you are sending this unsubscribe request or if you have received this in error using a different email address, please ensure that you include the original address that you registered with, so that we can identify your record correctly and action your request. To unsubscribe send a blank email to unsubscribe@gymscanner.com with the subject 'unsubscribe' Allow 24 hours to take effect.</p>";
        $val .= "</div></body></html>";

        $this->email->message($val);
        $this->email->send();


        redirect(ADMIN_URL . 'users');

    }

    public function view($x = '')
    {
        if ($x != '' && $x != 0) {
            $res = $this->db->where('id', $x)->get('user')->num_rows();
            if ($res > 0) {
                $data['user'] = $this->user->get_user($x);
                $data['page'] = 'users';
                $data['mode'] = 'view';
                $this->load->view('template', $data);
            } else {
                $data['page'] = 'error';
                $this->load->view('template', $data);
            }
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function edit($x = '')
    {

        if ($x != '' && $x != 0) {
            $res = $this->db->where('id', $x)->get('user')->num_rows();
            if ($res > 0) {
                $data['user'] = $this->user->get_user($x);
                $data['page'] = 'users';
                $data['mode'] = 'edit';
                $this->load->view('template', $data);
            } else {
                $data['page'] = 'error';
                $this->load->view('template', $data);
            }
        } else {
            $data['page'] = 'error';
            $this->load->view('template', $data);
        }
    }

    public function change_status()
    {
        $status = $_POST['status'];
        $status = ($status == 1) ? '2' : '1';
        $this->db->where('id', $_POST['id'])->update('user', array('status' => $status));
    }

    public function removeSelectedUser($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('user');
        redirect(ADMIN_URL . 'users');
    }

    public function del_usr()
    {
        $usr_id = $_POST['id'];
        $res = $this->db->where('user_id', $usr_id)->get('gym_customers')->num_rows();
        if ($res > 0) {
            echo 1;
        } else {
            $this->db->where('id', $usr_id)->delete('user');
        }
    }
}


?>
