<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("notifications_model",'notifications');
		$this -> data['page'] = 'notification';
	}

	public function index()
	{
	 	$this -> data['notifications'] = $this->notifications->get_allnotes();	
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		
	}
	


		public function add()
	{

		if(isset($_POST['duration']))
		{
			    extract($_POST);
			    //print_r($_POST); exit;
	      $rdata = array('duration'=>$duration,
                      'membership'=>$mem_id,
                      'status'=>$status,
                      'createdon'=>mktime()
        ); 
        if(!$this -> notifications ->count_all_results('membership',$rdata['membership']))
			      {
				        $id = $this -> notifications -> insert ($rdata);	
				        $this->session->set_flashdata('success', 'Successfully Added');
				        redirect('admin/notifications');
			      }
			    else
			     {
				       $this -> session -> set_flashdata('error','You are trying to add already existing Membership');
				       redirect('admin/notifications/add');
			     }          
		}
		else
		{			
			$this -> data['mode'] = 'add';			
	 		$this->load->model('memship_model','mem');
	 		$this->data['mem'] = $this->mem->dropdown('id','name');
			$this -> load -> view('template', $this -> data);
		}
	}
  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $resultset = $this->notifications->get_notes($record);
	      $this->data['result'] = $resultset[0];
	      //echo '<pre>';print_r($resultset);	 exit();
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	      	
	}
	
	public function edit(){

       if(isset($_POST['adduser'])){
       	    extract($_POST);
       	    $edata = array(
                      'duration'=>$duration,
                      'membership'=>$mem_id,
                      'status'=>$status,
                      'updatedon'=>mktime()
            ); 
        $member = $this->notifications->get($id);
		    if($member->membership != $edta['membership']){
			   if(!$this -> notifications ->count_all_results('membership',$edata['membership']))
			           {
				             $this->notifications->update($id,$edata);	
				             $this->session->set_flashdata('success', 'Successfully Updated');
				             redirect('admin/notifications');
			           }
			        else
			           {
				             $this -> session -> set_flashdata('error','You are trying to add already existing Membership');
				             redirect('admin/notifications/edit/'.$id);
			           }          	
		    	}
		    else{
		       $this->notifications->update($id,$edata);
		       $this->session->set_flashdata('success', 'Successfully Updated');
				           redirect('admin/notifications');
			}
       }	     	
       elseif($this->uri->segment(4)){
	     	$record = $this->uri->segment(4);
	     	$this->data['result'] = $this->notifications->get($record);	     	    		
	 		$this->load->model('memship_model','mem');
	 		$this->data['mem'] = $this->mem->dropdown('id','name');
	        $this->data['mode'] = 'edit';
	        $this -> load -> view('template', $this -> data);
	    }
	    else
		    {
		        redirect('admin/notifications');
		    }
	}
	
	public function delete()	
	
	{
	  $res = $this->notifications->delete_data($_POST['id']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'First You have to Delete child menus');
			}
		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(4))
			{
				$this->notifications->deactivate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/notifications');
			
	
			}else
			{
			redirect('admin/notifications');
			}	
	
	}
		
		public function active()
		{	
			if($this->uri->segment(4))
			{		
				$this->notifications->activate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('success', 'Successfully Activated');
				redirect('admin/notifications');	
			}else
			{
				redirect('admin/notifications');

			}
	
		}
	
}


?>
