<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("state_model",'state');
		$this -> data['page'] = 'statelist';
	}

	public function index(){
		$this-> data['mode'] = 'all';
		$this-> data['states'] = $this->state->get_statelist();	
		$this-> load -> view('template', $this -> data);		
	}

	public function add()
	{
		if(isset($_POST['state_name']))
		{
			//print_r($_POST);exit;
			    extract($_POST);
			    if($_FILES['userfile']['name'] != ''){

			      /************* Excel uploading Begin *************/
			      $config['upload_path'] = './document/';
				  $config['allowed_types'] = '*';
				  $config['max_size'] = '0';

				  $result_array=array();

				  $this->load->library('upload', $config);

				  if ( ! $this->upload->do_upload())
				  {
				   $error = array('error' => $this->upload->display_errors());
				   $result_array[]=array("error"=>"yes","message"=>"Unable to upload file please contact admin");
				  }

				  else
				  {
				    set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
				    require $_SERVER['DOCUMENT_ROOT'].'/gym/excellnew/Classes/PHPExcel/IOFactory.php';

				    $data = array('upload_data' => $this->upload->data());
				    $inputFileName = $_SERVER['DOCUMENT_ROOT'].'/gym/document/'.$data['upload_data']['file_name'];  // File to read
					try {
					 $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} catch(Exception $e) {
					 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					foreach ($sheetData as $row) {
						foreach ($row as $key => $value) {
							$rdata = array('state'=>ucfirst($value),
										   'state_code'=>strtoupper($state_code),
						      			   'country_id'=>$country_id,
					                       'status'=>1
					        ); 
        					$this->state->insert ($rdata);
						}
						
					}
				}
				/************* Excel uploading End *************/
				$this->session->set_flashdata('success', 'Successfully Added');
				redirect('admin/state');
			}
			else{

			    $rdata = array('state'=>ucfirst($state_name),
			    		'state_code'=>strtoupper($state_code),
	      				 'country_id'=>$country_id,
                         'status'=>1
        		); 
		        if(!$this->state->count_all_results('state',$rdata['state_name']))
			    {
				    $id = $this->state->insert ($rdata);	
				    $this->session->set_flashdata('success', 'Successfully Added');
				    redirect('admin/state');
			    }
			    else
			    {
				    $this -> session -> set_flashdata('error','You are trying to add already existing state');
				    redirect('admin/state/add');
			    }           
			}
		}
		else
			{			
				$this->data['mode'] = 'add';
				$this->load->model('country_model','country');
				$this->data['countries'] = $this->country->dropdown('id','country_name');
				$this->load-> view('template', $this->data);
			}
	}  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $this->data['result'] = $this->state->get_list($record);
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	}	
	public function edit(){

       if(isset($_POST['addmem'])){
       	    extract($_POST);
       	    $edata = array(
                      'state'=>$state_name,
                      'state_code'=>$state_code,
                      'country_id'=>$country_id,
                      'status'=>$status
            ); 
        $member = $this->state->get_list($state_code);
        if($member->state != $edata['state']){
        	   if(!$this -> state ->count_all_results('state',$edata['state']))
			           {
				            $this->db->where('state_code',$state_code)->update('states',$edata);
				            $this->session->set_flashdata('success', 'Successfully Updated');
				            redirect('admin/state');
			           }
			        else
			           {
				            $this ->session -> set_flashdata('error','You are trying to add already existing state');
				            redirect('admin/state/edit/'.$state_code);
			           }  
        	
        	}
        else{
               $this->db->where('state_code',$state_code)->update('states',$edata);
               $this->session->set_flashdata('success', 'Successfully Updated');
	           redirect('admin/state');
			}
       }	     	
       elseif($this->uri->segment(4)){
	     	$record = $this->uri->segment(4);
	     	$this->load->model('country_model','country');
			$this->data['countries'] = $this->country->dropdown('id','country_name');
	     	$this->data['result'] = $this->state->get_list($record);
	        $this->data['mode'] = 'edit';
	        $this ->load-> view('template', $this -> data);
	    }
	    else
		{
	        redirect('admin/state');
		}
	}	
	public function delete(){
	    if($this->db->where('state_code',$_POST['id'])->delete('states')){
			echo 'success';
			$result = $this->session->set_flashdata('delete', 'Successfully Deleted');
		}
		else{
			echo 'failed';
			$result = $this->session->set_flashdata('delete', 'Please try again');
		}		
	}			
	public function deactivate()
	{
		if($this->uri->segment(4)){
			$this->state->deactivate($this->uri->segment(4));
			$this -> data['mode'] = 'all';
			$this->session->set_flashdata('delete', 'Successfully Deactivated');
			redirect('admin/state');	
		}else{
			redirect('admin/state');
		}	
	}		
	public function active(){

		if($this->uri->segment(4))
		{	
			$this->state->activate($this->uri->segment(4));
			$this->data['mode'] = 'all';
			$this->session->set_flashdata('success', 'Successfully Activated');
			redirect('admin/state');
		}else{
			redirect('admin/state');
		}	
	}
	
}


?>
