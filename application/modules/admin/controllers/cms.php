<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends CI_Controller {
    
    function __construct() {
        parent::__construct();
         $user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
                   redirect('admin/sessionexp');  
                     
        $this->load->model('cms_model', 'cms');	
        
    }
   
    public function index()
    {
    	
    	//$data = $this -> cms -> get_all(array('status'=>1));
    	//print_r($data); exit();
    $data = $this -> cms -> getmenus();
    
    //print_r($data); exit;
		$this -> data['mode'] = 'all';
		if(count($data) > 0){
			    $this -> data['menus'] =$this->cms->menus($data);
        $this->load->view('cms/menu',$this -> data);
   }else{
        $this->load->view('cms/menu',$this -> data);   	
   	}
    }
	public function addcms(){
			
			$this -> data['mode'] = 'add';
			$this -> data['addmenu'] = $this -> cms -> dropdown('id','menuname',array('submenu_id'=>0));
		        $this -> load -> view('cms/menu', $this -> data);
		
	   }
	public function addmenu(){
		$res = $this->cms->addmenu(); //print_r($res);  exit;
		$this->session->set_flashdata('success', 'Successfully Added');
		redirect('admin/cms','refresh');	
    }
      	
	public function editcms(){
	
		$record = $this -> cms -> get ($this->uri->segment(4));

				if(is_object($record))
				{
	
				$this -> data['mode'] = 'edit';
				//$this -> db -> where('submenu_id'=>0);
				$this -> data['addmenus'] = $this -> cms -> dropdown('id','menuname',array('submenu_id'=>0));
				$this->data['details']=$record;
				    $this -> load -> view('cms/menu', $this -> data);
				    
				}else{
				    
				    redirect('admin/cms');
				    
				    }
	}
	public function editmenu(){
			//print_r($_POST);exit;
			$updatedata=array("menuname"=>$this->input->post("menuname"),
			                  "submenu_id"=>$this->input->post("id")
			   );
	
	
		$id=$this->input->POST("mid");
	
		$this->db->where("id",$id);
		$this->db->update("cmsmenu",$updatedata);
	
		$this->session->set_flashdata('success', 'Successfully Updated');
			redirect('admin/cms');
	
    }
    public function viewcms(){
		$record = $this -> cms -> get ($this->uri->segment(4));

				if(is_object($record))
				{
	
					$this -> data['mode'] = 'view';	
					$this->data['details']=$record;
					$sub_id = $this->data['details']->submenu_id;
					if($sub_id != 0){
					$res = $this->cms->viewmenu($sub_id);
					$this->data['submenu'] = $res[0];}
				    $this -> load -> view('cms/menu', $this -> data);
				}
    }
	public function delete(){
		 $res = $this->cms->delete($_POST['sid']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			    //redirect('admin/cms','refresh');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'First You have to Delete child menus');
				  //redirect('admin/cms','refresh');
			}
		
	}
	public function deactivate(){
	
		if($this->uri->segment(4)){
		
			$this->cms->deactivate_menu($this->uri->segment(4));
			$this->session->set_flashdata('delete', 'Successfully Deactivated');
			redirect('admin/cms');
			
		}else{
	
			redirect('admin/cms');	
		}
	
	}
		
	public function active(){
	
		if($this->uri->segment(4)){
		
			$this->cms->activate_menu($this->uri->segment(4));
			$this->session->set_flashdata('success', 'Successfully Activated');
			redirect('admin/cms');
				
		}else{
	
			redirect('admin/cms');	
		}		
	}
	public function getmname(){
    	//print_r($_POST); exit;
    	  //extract($_POST);
    	  
    	  $result = $this -> cms -> checkmenuname($_POST['menuname'],$_POST['sid']);
    	 //print_r($result);
    	  if($result > 0)
    	  {
    	  	  echo '0'; 	  	    	  
    	  }else{
    	  	echo '1';
	  }
						
    	}
}
?>