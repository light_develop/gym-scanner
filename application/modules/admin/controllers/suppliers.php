<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("suppliers_model",'suppliers');
		$this -> data['page'] = 'suppliers';
	}

	public function index(){
	 	$this ->data['suppliers'] = $this->suppliers->getsuppliers();
		$this ->data['mode'] = 'all';
		$this ->load -> view('template', $this->data);	
	}
	public function add(){
		if(isset($_POST['first_name'])){
		    extract($_POST);
      		$rdata = array('user_name'=>$user_name,
      					'first_name'=>$first_name,
      					'last_name'=>$last_name,
      					'password'=>$password,
                        'mobile'=>$mobile,
                        'email'=>$email
        	);   
	        $this->db->insert('supplier',$rdata);	
			redirect('admin/suppliers');
		}
		else
		{			
			$this ->data['mode'] = 'add';
			$this ->load -> view('template', $this->data);
		}
	}  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $this->data['supplier'] = $this->suppliers->getsupplier($record);
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	}
	public function change_status(){
        $status=$_POST['status'];
        $status= ($status == 1)? '0' : '1';
        $this->db->where('id',$_POST['id'])->update('supplier',array('status'=>$status));   
    }
	public function edit(){

       if(isset($_POST['first_name'])){
       	    extract($_POST);
       	    $edata = array('user_name'=>$user_name,
      					'first_name'=>$first_name,
      					'last_name'=>$last_name,
      					'password'=>$password,
                        'mobile'=>$mobile,
                        'email'=>$email
        	); 
        $this->suppliers->update($id,$edata);
        $this->session->set_flashdata('success', 'Successfully Updated');
		redirect('admin/suppliers');
       }	     	
       elseif($this->uri->segment(4)){
     	    $record = $this->uri->segment(4);
     	    $this->data['supplier'] = $this->suppliers->getsupplier($record);
          	$this->data['mode'] = 'edit';
	        $this->load->view('template', $this->data);
     	}
     	else
	    {
		    redirect('admin/users');
	    }
	}
	
	public function delete()
	{
	    $res = $this->user->delete_data($_POST['id']); 
			if($res == 1){
				echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				echo 'failed';
				$result = $this->session->set_flashdata('delete', 'First You have to Delete child menus');
			}		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(4))
			{
				$this->user->deactivate_user($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/users');
			
	
			}else
			{
			redirect('admin/users');
			}	
	
	}
		
	public function active()
	{

			if($this->uri->segment(4))
			{
	
				$this->user->activate_user($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('success', 'Successfully Activated');
				redirect('admin/users');
		

			}else
			{
				redirect('admin/users');
			}
	}
	public function gymsByCat($id){

		$data=$this->user->gymsByCat($id);
		//print_r($data);
       
       $attributes= 'id="gym_id"  class="chosen span6 required" onchange="get_ofrs(this.value)"';
       $data[''] = 'choose gym';
                      
       echo form_dropdown('gym_id',$data, '',$attributes);
	}
	public function ofrsByGym($gym){
		$data['offrs'] = $this->user->get_offers($gym);
		echo $this->load->view('users/ofr.php',$data);
	}

	
}


?>
