<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservices extends  CI_Controller {

	function __construct()
	{
      parent::__construct();
      $this->load->model('webservices_model', 'webservices');
					date_default_timezone_set('Asia/Calcutta');
  }

  public function get_categories()
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();

      $getvalues = $this->webservices->get_catgs(); //echo "<pre/>";print_r($getvalues);exit;

      if (count($getvalues) > 0)
      {
	       foreach($getvalues as $items):

			   $responsekeys = array();
			   $responsekeys['cat_id'] = $items->id;
      	$responsekeys['cat_name'] = $items->categoryname;
     		array_push($response['details'],$responsekeys);
     		endforeach;
     		$response['response'] = "yes";
      }
      else
      {
       $response['response'] = "no";
      }
   
    echo json_encode($response);
  }

  public function get_cntrs()
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      $cntry = $this->uri->segment(4);
      
      if($cntry == ''){

           $getvalues = $this->webservices->get_cntrys();  	

      if (count($getvalues) > 0)
      {
	       foreach($getvalues as $items):

			   $responsekeys = array();
			   $responsekeys['cntry_id'] = $items->id;
      	$responsekeys['cntry_name'] = $items->name;
     		$responsekeys['cntry_code'] = $items->code;
     		array_push($response['details'],$responsekeys);
     		endforeach;
     		$response['response'] = "yes";
      }
      else
      {
       $response['response'] = "no";
      }
     }
     else{
      	    $getvalues = $this->webservices->get_cntrycity($cntry); 
      	    if (count($getvalues) > 0)
             {
	                  foreach($getvalues as $items):

			                     $responsekeys = array();
			                     $responsekeys['cntry_id'] = $items->id;
      	                  $responsekeys['cntry_name'] = $items->name;
     		                  $responsekeys['cntry_code'] = $items->code;
     		                  $responsekeys['city_id'] = $items->cityid;
     		                  $responsekeys['city_name'] = $items->cityname;
     		                  array_push($response['details'],$responsekeys);
     		            endforeach;
     		            $response['response'] = "yes";
             }
           else
            {
                $response['response'] = "no";
            }
     }
    echo json_encode($response);
  }

 public function get_cities($cnt_code)
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();

           $getvalues = $this->webservices->get_cities($cnt_code);
           if (count($getvalues) > 0)
            {
	              foreach($getvalues as $items):

			              $responsekeys = array();
			              $responsekeys['city_id'] = $items->id;
      	           $responsekeys['city_name'] = $items->cityname;
     		           $responsekeys['cntry_code'] = $items->countrycode;
     		           array_push($response['details'],$responsekeys);
     		        endforeach;
     		        $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }
  
  public function get_gyms()
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();

      //if($_POST){
      	   //$gym_type = (!empty($_POST['gym_type']))? $_POST['gym_type'] : '1';
      	   //$cnt_code = (!empty($_POST['cnt_code']))? $_POST['cnt_code'] : '1';
      	   //$city_id = (!empty($_POST['city_id']))? $_POST['city_id'] : '1';
      	    
           $getvalues = $this->webservices->get_gyms($gym_type,$cnt_code,$city_id);
           if (count($getvalues) > 0)
            {
	              foreach($getvalues as $items):

			              $responsekeys = array();
			              $responsekeys['gym_id']          = $items->id;
      	           $responsekeys['gym_name']        = $items->gymname;
     		           $responsekeys['gym_timings']     = $items->timings;
     		           $responsekeys['gym_star']        = $items->star;
     		           $responsekeys['gym_street']      = $items->gstreet;
     		           $responsekeys['gym_city']        = $items->cityname;
     		           $responsekeys['gym_state']        = $items->gstate;
     		           $responsekeys['gym_country']     = $items->name;
     		           $responsekeys['gym_zip']         = $items->gzip;
     		           $responsekeys['gym_speciality']  = $items->speciality;
     		           $responsekeys['gym_category']    = $items->category;
     		           $responsekeys['gym_owner']       = $items->owner;
     		           $responsekeys['gym_owner_phone'] = $items->ow_phone;
     		           $responsekeys['gym_owner_email'] = $items->ow_email;
     		           $responsekeys['lat']             = $items->gmap_lat;
			              $responsekeys['lngt']            = $items->gmap_long;
     		           $responsekeys['gym_status']      = $items->status;
     		           
     		           $images     = $items->gimages;
     		           $img = explode('[@]',$images);
     		           foreach($img as $imge):
     		               $neg = $imge;
     		               $newimg[] = $neg?'192.168.0.101/gym/uploads/thumbs/gym/'.$neg:$imge;
     		           endforeach;     		           
     		           
     		           $responsekeys['gym_images'] = $newimg;
     		           
     		           array_push($response['details'],$responsekeys);
     		        endforeach;
     		        $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
     // }
    echo json_encode($response);
  }
  
  public function get_images($gymid)
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_images($gymid);
           if (count($getvalues) > 0)
            {
	              foreach($getvalues as $items):

			              $responsekeys = array();
			              $responsekeys['gym_id']          = $items->id;
      	           $responsekeys['gym_name']        = $items->gymname;
     		           
     		           $images     = $items->gimages;
     		           $img = explode('[@]',$images);
     		           foreach($img as $imge):
     		               $neg = $imge;
     		               $newimg[] = $neg?'192.168.0.101/gym/uploads/thumbs/gym/'.$neg:$neg;
     		           endforeach;
     		           
     		           $responsekeys['gym_images'] = $newimg;
     		           $responsekeys['lat']             = $items->gmap_lat;
			              $responsekeys['lngt']            = $items->gmap_long;
     		           
     		           array_push($response['details'],$responsekeys);
     		        endforeach;
     		        $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }
   
  public function get_locations()
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      //if($_POST){
      	    $cnt_code = (!empty($_POST['cntry_code']))? $_POST['cntry_code'] : '1';
      	    $city_id = (!empty($_POST['city_id']))? $_POST['city_id'] : '1';
           $getvalues = $this->webservices->get_locs($cnt_code,$city_id);  //echo "<pre/>";print_r($getvalues[0]);exit;
           if (count($getvalues) > 0)
            {
	                $data = $getvalues[0];
	                
			              $responsekeys = array();
			              $responsekeys['map'] = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$data->name.$data->cityname.'&destination='.$data->cityname.'&sensor=false';
     		           array_push($response['details'],$responsekeys);
     		        $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
     // }
    echo json_encode($response);
  }   
  
  public function get_maps($gymid)
  {
     	$this->db->query('SET UNIQUE_CHECKS=0');
      $response = array();
      $response['details'] = array();
      
           $getvalues = $this->webservices->get_maps($gymid);  
           if (count($getvalues) > 0)
            {	   
               foreach($getvalues as $items):             
			              $responsekeys = array();
			              $responsekeys['lat'] = $items->gmap_lat;
			              $responsekeys['lngt'] = $items->gmap_long;
     		           array_push($response['details'],$responsekeys);
     		        endforeach;
     		        $response['response'] = "yes";
           }
         else
         {
           $response['response'] = "no";
         }
    echo json_encode($response);
  }   
  
}

// http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false

?>


