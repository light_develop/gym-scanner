<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_details extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("contact_details_model",'contact_details');
		$this -> data['page'] = 'contact_details';
	}

	public function index()
	{
	    $this -> data['contacts'] = $this->contact_details->getcontacts();	
		$this -> data['page'] = 'contact_details';
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		
	}
	
}


?>
