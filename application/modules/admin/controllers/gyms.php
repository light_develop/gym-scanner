<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gyms extends MY_Controller {

	public function __construct()
	{
		parent::__construct();   
		 $user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp');  
                   
		$this->load -> model ('gyms_model','gym');
     	$this->data['page'] = 'gyms';
	}
	
	public function index()
	{ 		
		$this->data['gyms'] = $this->gym->get_allgyms();
		$this->data['images'] = $this->gym->get_allimages();
		$this->data['mode'] = 'all';
		$this->load->view('template', $this->data);			
	}	
	public function add(){

		extract($_POST);	
     	if(isset($_POST['addgym']))
		{ 	    
	        $result = $this->gym->addimages();
	        $this->session->set_flashdata('success', 'Successfully Added');
	        if ($result) {
	            redirect('admin/gyms', 'refresh');
	        }		       
	   	}
		  else
		{			
			$this->data['mode'] = 'add';
			$this->data['cntry'] = $this->gym->get_countries();            
			$this->data['cat'] = $this->gym->get_cats();

			$this->load -> model('memship_model','memship');
			$mem_res = $this->memship-> get_all();
			$this->data['mem'] = $mem_res;
			$this ->load -> view('template', $this -> data);
		}
	}	
	public function get_states($id)
	{		
		extract($_POST);
		$data = $this->gym->get_states($id); 
       	$attributes= 'id="state"  class="chosen span6 required" onchange="get_cities(this.value)"';
       	$data['0'] = 'State'; 
        echo form_dropdown('state',$data, '0',$attributes);	
	}
	public function get_cities($x){
		$data = $this->gym->get_cities($x);
		$attributes= 'id="city"  class="chosen span6 required"';
        $city[''] = '';                      
        echo form_dropdown('city',$data, '',$attributes);	
	}
	public function change_status(){
        $status=$_POST['status'];
        $status= ($status == 1)? '2' : '1';
        $this->db->where('id',$_POST['id'])->update('gym',array('status'=>$status));   
    }
	public function get_city($id)
	{		
		$data=$this->gym->selected_cntry($id); 
       
        $attributes= 'id="city"  class="chosen span6 required"';
        $city[''] = '';                      
            echo form_dropdown('city',$data, '',$attributes);	
	}
 	public function view(){
 	
 	    $record = $this->uri->segment(4);
 	    if($record){
 	    	
 	         $result	= $this->gym->getgym($record);
 	         $this->data['gym'] = $result;
 	         $results = $this->gym->getimgs($record);
 	         $this->data['gimgs'] = $results;

 	         $this->data['mode'] = 'view';
 	         $this->load->view('template',$this->data);	
 	    }
 	    else{ 	               	
   	    	  redirect('admin/menu');
 	    	}
 	}
 
 public function edit(){
 
 	    if(isset($_POST['addgym'])){ 
            $result = $this->gym->updategyms();
            redirect('admin/gyms');	
 	    	}
       
 	    else if($this->uri->segment(4)){
 	    	$record =  $this->uri->segment(4);

 	        $result	= $this->gym->getgym($record);
 	        $data['gimgs'] = $this->gym->getimgs($record);
 	        $data['adv'] = explode(",",str_replace(" ","",$result->advanced));
 	        $data['gym'] = $result;
 	        $cntry_id = $result->cnt; 
 	        $state = $result->gstate; 

 	        $data['cntry'] = $this->gym->get_countries();
 	        $data['cat'] = $this->gym->get_cats();
 	        $data['states'] = $this->gym->get_states($result->cnt);
 	        $data['city'] = $this->gym->get_cities($state);
 	        
			$this ->load -> model ('memship_model','memship');
			$mem_res = $this->memship-> get_all();
			
			$this ->load -> model ('suppliers_model','suppliers');
			$suppliers = $this->suppliers-> get_suppliers_dropdown();			
			$data['suppliers'] = $suppliers;

			$data['mem'] = $mem_res;
			$data['page'] = 'gyms';
 	        $data['mode'] = 'edit';
 	       // echo "<pre>";print_r($data['gym']); exit;
 	        $this->load->view('template',$data);	
 	    } 
 	    else{ 	               	
   	    	  redirect('admin/gyms');
 	    	}
 	
 	}
	public function del_img(){
		$id=$this->uri->segment(4);     
        $del = $this->db->where('id',$id)->delete('gym_images');    
        if($del){
            unlink('./uploads/gym/images/'.$id);
        }
	}
	public function del_gym(){
    	$gym_id = $_POST['id'];
    	$res = $this->db->where('gym_id',$gym_id)->get('gym_customers')->num_rows();
    	if($res > 0){
    		echo 1;
    	}else{
    		$this->db->where('id',$gym_id)->delete('gym');
    		$imgs = $this->db->where('gym_id',$gym_id)->get('gym_images')->result();
    		foreach ($imgs as $key => $value) {
    			unlink('./uploads/gym/images/'.$value->gym_images);
    		}
    		$this->db->where('gym_id',$gym_id)->delete('gym_images');
    	}
    }	
}

?>
