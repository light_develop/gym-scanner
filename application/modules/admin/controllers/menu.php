<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {

	public function __construct()
	{
		parent::__construct();   
		 $user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp');  
                   
		   $this -> load -> model ('menu_model','menu');
     $this -> data['page'] = 'cms/menucontent';
	}
	
	public function index()
	{
 		
		$this -> data['menus'] = $this->menu->getmenus();
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		//print_r($this->data); exit;
			
	}	

	public function add()
	{
		
		if(isset($_POST['addmenu']))
		{	
		  extract($_POST);
		  //print_r($_POST); exit;

		  if(is_numeric($mainmenu)){
        		  	$menuid = $mainmenu;
		  	} else {
		  	    $menuid = $submenu	; 			
		  	}
		  //$menu_id = $this->menu->menuid($menuid)	;	
				$menu = array(
				'menu_id' => $menuid,
				'content' => $this -> input -> post('description'),
				'status' => $this -> input ->post('status'),	
				);
			
			if(!empty($_FILES) && $_FILES['image']['error'] !=4){
				$image = $this -> menu -> do_upload_image('image');
				if(is_array($image)){
					$this -> session -> set_flashdata('error',$image['upload_error']);
					redirect('admin/menu/add');
				}else
					$menu ['image'] = $image; 
			}
			if(!$this -> menu ->count_all_results('menu_id',$menu['menu_id']))
			{
				$id = $this -> menu -> insert ($menu);	
				redirect('admin/menu');
			}
			else
			{
				$this -> session -> set_flashdata('error','You are trying to add already existing menu');
				redirect('admin/menu/add');
			}
		}
		else
		{			
			$this -> data['mode'] = 'add';
			$this -> load -> model ('cms_model','cms');
			$this -> data['addmenus'] = $this -> cms -> dropdown('id','menuname',array('submenu_id'=>0));
			$this -> data['submenus'] = $this -> cms -> dropdown('id','menuname',array('submenu_id != ' => 0));
			$this -> load -> view('template', $this -> data);
		}
	}

	public function edit()
	{

		if(isset($_POST['id']))
		{
			$menu = array(
				'content' => $this -> input -> post('description'),
				'menu_id' => $this -> input -> post('menu_list'),			
				'status' => $this -> input -> post('status')			
				);
			$getid = $this->menu->getid($_POST['id']);
			extract($_POST);
			//echo "<pre>";print_r($_POST); echo "<br>";
			//echo "<pre>";print_r($_FILES); exit();
			if($getid->menu_id != $menu['menu_id']){
				if(!$this -> menu ->count_all_results('menu_id',$menu['menu_id']))
				{
					if(isset($remove_gym_image)){
						if(file_exists('./uploads/thumbs/cms_content/'.$oldimage))
			              {
			                unlink('./uploads/thumbs/cms_content/'.$oldimage);
			                unlink('./uploads/original/cms_content/'.$oldimage);
			              }
			              $menu ['image'] = '';
					} 
			              $this -> menu -> update ($_POST['id'],$menu);
					if(!empty($_FILES) && $_FILES['image']['error'] !=4){
						$image = $this -> menu -> do_upload_image('image');
						if(is_array($image)){
							$this -> session -> set_flashdata('error',$image['upload_error']);
							redirect('admin/menu/edit/'.$_POST['id']);
						}else
						{ 
							$record = $this -> menu -> get ($_POST['id']);
							if(file_exists($this -> menu ->original_path.'/'.$this->menu->table.'/'.$record->image)){
								unlink($this -> menu ->original_path.'/'.$this->menu->table.'/'.$record->image);
								unlink($this -> menu ->thumbs_path.'/'.$this->menu->table.'/'.$record->image);
							}
							$menu ['image'] = $image;
						}
						if(file_exists('./uploads/thumbs/cms_content/'.$oldimage))
			              {
			                unlink('./uploads/thumbs/cms_content/'.$oldimage);
			                unlink('./uploads/original/cms_content/'.$oldimage);
			              }
					}
					
					$this -> menu -> update ($_POST['id'],$menu);
					$this->session->set_flashdata('success', 'Successfully Updated');
					redirect('admin/menu');
				}
				else
				{
					$this -> session -> set_flashdata('error','You are trying to add alredy exist menu');
					redirect('admin/menu/edit/'.$_POST['id']);
				}
			}else{
				if(isset($remove_gym_image)){
					if(file_exists('./uploads/thumbs/cms_content/'.$oldimage))
		              {
		                unlink('./uploads/thumbs/cms_content/'.$oldimage);
		                unlink('./uploads/original/cms_content/'.$oldimage);
		              }
		              $menu ['image'] = '';
		              
				}
				$this -> menu -> update ($_POST['id'],$menu);
				if(!empty($_FILES) && $_FILES['image']['error'] !=4){
					$image = $this -> menu -> do_upload_image('image');
					if(is_array($image)){
						$this -> session -> set_flashdata('error',$image['upload_error']);
						redirect('admin/menu/edit/'.$_POST['id']);
					}else
					{ 
						$record = $this -> menu -> get ($_POST['id']);
						if(file_exists($this -> menu ->original_path.'/'.$this->menu->table.'/'.$record->image)){
							unlink($this -> menu ->original_path.'/'.$this->menu->table.'/'.$record->image);
							unlink($this -> menu ->thumbs_path.'/'.$this->menu->table.'/'.$record->image);
						}
						$menu ['image'] = $image;
						$this -> menu -> update ($_POST['id'],$menu);
					}					
				}
				
				$this->session->set_flashdata('success', 'Successfully Updated');
				redirect('admin/menu');		
				
			}
		}
		elseif($this->uri->segment(4))
		{			
			$record = $this->uri->segment(4);
			if($record)
			{
				$this -> load -> model ('cms_model','cms');
				$this -> data['menu_list'] = $this -> cms -> dropdown('id','menuname');
				$menu_select_list = $this -> menu -> menuid ($record);
				$this -> data['menu'] = $menu_select_list[0];
				$this -> data['mode'] = 'edit';
				$this -> load -> view('template', $this -> data);
			}
		}
		else
		{
			redirect('admin/menu');
		}
	}
	
	public function view()
	{
		if($this->uri->segment(4))
		{			
		$record = $this->uri->segment(4);
			
				$result = $this -> menu -> getdet($record);
				$this -> data['mode'] = 'view';
				$this -> data['menu'] = $result[0];
				$this -> load -> view('template', $this -> data);
			
		}
		else
		{
			redirect('admin/menu');
		}
	}
	
	public function delete()
	{
	  $res = $this->menu->delete_data($_POST['sid']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'Try again');
			}
		
	}
			
	public function deactivate()
	{
	
			if($this->uri->segment(4))
			{
				$this->menu->deactivate_menu($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/menu');
			
	
			}else
			{
			redirect('admin/menu');
			}	
	
	}
		
		public function active()
		{
	
				if($this->uri->segment(4))
				{
		
					$this->menu->activate_menu($this->uri->segment(4));
					$this -> data['mode'] = 'all';
					$this->session->set_flashdata('success', 'Successfully Activated');
					redirect('admin/menu');
			
	
				}else
				{
					redirect('admin/menu');
	
				}
	
		}
	
}

?>