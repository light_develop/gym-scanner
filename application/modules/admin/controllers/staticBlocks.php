<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class StaticBlocks extends MY_Controller
{
    public function __construct()
    {
        error_reporting(-1);
        header("Content-type: text/html; charset=utf-8");
        parent::__construct();

        $user = $this->session->userdata('user');
        if (!isset($user) || !is_object($user))
            redirect('admin/sessionexp');
        $this->load->helper('form');         // form open in view
        // form open in view
        $this->load->database();
        //$this->_install(1.2);
    }

    public function getStaticBlocks() {
        $start = $this->input->post("start");
        $offset = $this->input->post("offset");
        $allResults = $this->db->count_all_results("static_blocks");
        $_moreColumns = false;

        if(!$start && !$offset) {
            $staticBlocks = $this->db->get("static_blocks", 20, 0);
            if($allResults && $allResults > 20) {
                $_moreColumns = true;
            }
        } else {
            $staticBlocks = $this->db->get("static_blocks", $offset, $start);
            if($allResults && $allResults > $offset + $start) {
                $_moreColumns = true;
            }
        }

        return array("static_blocks" => $staticBlocks->result_array(), "more_columns" => $_moreColumns);

    }

    public function edit() {
        $toUpdate = $this->input->post();

        if(is_array($toUpdate) && isset($toUpdate["entity_id"])) {
            $this->db->update("static_blocks", $toUpdate, array("entity_id" => $toUpdate["entity_id"]));
        }

        redirect(ADMIN_URL . 'staticBlocks');
    }

    public function index() {
        $this->data['page'] = 'staticBlocks';
        $this->data['staticBlocksInfo'] = $this->getStaticBlocks();
        $this->load->view('template', $this->data);
    }

    protected function _install($version) {
        /* v 1.0 */
        if($version == 1.0) {
            $this->db->query("CREATE TABLE `static_blocks` (entity_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                                            static_block_title VARCHAR(255),
                                                            static_block_id VARCHAR(255),
                                                            static_block_content TEXT,
                                                            lang_code VARCHAR(255)
                                                        ) ");
            $this->db->query("ALTER TABLE `static_blocks` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;");
        }

        /* v 1.1 */
        if($version == 1.1) {
            $this->db->query("ALTER TABLE `static_blocks` DROP COLUMN lang_code;");
            $this->db->query("ALTER TABLE `static_blocks` ADD COLUMN `lang_id` INT(10);");
            $this->db->query("ALTER TABLE `static_blocks` ADD CONSTRAINT FK__Employees__UserI__04E4BC85 FOREIGN KEY(lang_id) REFERENCES languages(lang_id)");
        }
    }

    public function addNew() {
        $data = $this->input->post();

        if($data) {
            $data["static_block_content"] = htmlspecialchars_decode($data["static_block_content"]);
            $this->db->insert("static_blocks", $data);
        }

        redirect(ADMIN_URL . 'staticBlocks');
    }

}