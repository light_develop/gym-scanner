<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                $user = $this->session->userdata('user');

                if(!isset($user) ||  !is_object($user))
                   redirect('admin/sessionexp'); 
            
		$this->load->model("dashboard_model",'dashboard');              
	}

	public function index()
	{
		$this -> data['page'] = 'dashboard';
		$this->data['res'] = $this->dashboard->get_cnts();
		$this->data['gyms'] = $this->dashboard->get_gymdet();
		$this->data['users'] = $this->dashboard->get_usrdet();
		$this -> load -> view('template', $this -> data);
	}
	public function profile()
	{

		$userdata = $this -> dashboard -> get_profile_data($this->session->userdata['user']->id);
		//print_r($userdata);exit;
		$this -> data['page'] = 'profile';
		$this -> data['mode'] = 'view';
		$this -> data['profiledata'] = $userdata[0];		
		$this -> load -> view('template', $this -> data);
		
	}

 public function profile_edit()
	{
		//echo $this->uri->segment(4); exit;
		$userdata = $this -> dashboard -> get_profile_data($this->uri->segment(4));
		//$this -> data['user'] = $this -> user -> get ($this -> data ['login_details'] -> id);
		
		//echo "<pre>";print_r($this -> data['user']);echo "<pre>";exit;
		
		$this -> data['page'] = 'profile';
		$this -> data['mode'] = 'edit';
		$this -> data['profiledata'] = $userdata[0];	
		$this -> load -> view('template' , $this -> data);
	}

	public function profile_update()
	{

		$data=array(
		"username"=>$this->input->post("username"),
		"id"=>$this->input->post("userid"),
		"email"=>$this->input->post("email")

		);
			
 		$this->load->model("dashboard");
 		$this -> data['mode'] = 'view';
	    $return=$this->dashboard->update_profile($data);
	    $this->session->set_userdata('dashboard', $data);
		redirect('admin/dashboard/profile/'.$return);
	}

	public function password()
	{
		$this -> data['page'] = 'password';	
		$this -> load -> view('template', $this -> data);
	}

	public function update_password(){
		
		$data=array(
		"oldpassword"=>$this->input->post("Password"),
		"newpassword"=>$this->input->post("new_password")
		);
		
		$this->load->model("dashboard");
		$result=$this->dashboard->change_password($data);
		redirect('admin/dashboard/password/'.$result);


	}


}
