<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MY_Controller {


	public function __construct()
	{
		parent::__construct();	
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("faq_model",'faq');		    
	}

	public function index()
	{
		$this->data['page'] = "faq";
		$this->data['all_faq'] = $this->faq->getfaq();
	   $this->load-> view('template', $this->data);		
	}
	public function addnew($is_replied = '')
	{
		if(count($_POST)>0)
		{
			$insId = $this->faq->insertFaq($is_replied);
			$this -> session -> set_flashdata('success', 'Your query successfully added');
		}
		redirect('admin/faq');
	
	}
}
