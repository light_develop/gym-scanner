<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if(!isset($user) ||  !is_object($user))
          redirect('admin/sessionexp'); 
		$this->load->model("country_model",'country');
		$this -> data['page'] = 'cntrylist';
	}

	public function index()
	{
	 $this -> data['countries'] = $this->country->get_all();	
	 //print_r($this->data); die();
		$this -> data['mode'] = 'all';
		$this -> load -> view('template', $this -> data);
		
	}

		public function add()
	{

		if(isset($_POST['country_name']))
		{
			    extract($_POST);
			    if($_FILES['userfile']['name'] != ''){

			      /************* Excel uploading Begin *************/
			      $config['upload_path'] = './document/';
				  $config['allowed_types'] = '*';
				  $config['max_size'] = '0';

				  $result_array=array();

				  $this->load->library('upload', $config);

				  if ( ! $this->upload->do_upload())
				  {
				   $error = array('error' => $this->upload->display_errors());
				   $result_array[]=array("error"=>"yes","message"=>"Unable to upload file please contact admin");
				  }

				  else
				  {
				    set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
				    require $_SERVER['DOCUMENT_ROOT'].'/gym/excellnew/Classes/PHPExcel/IOFactory.php';

				    $data = array('upload_data' => $this->upload->data());
				    $inputFileName = $_SERVER['DOCUMENT_ROOT'].'/gym/document/'.$data['upload_data']['file_name'];  // File to read
					try {
					 $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} catch(Exception $e) {
					 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					foreach ($sheetData as $row) {
						foreach ($row as $key => $value) {
							$rdata = array('country_name'=>ucfirst($value),
                      					   'status'=>$status
        					); 
        					$this -> country -> insert ($rdata);
						}
						
					}
				}
				/************* Excel uploading End *************/
				$this->session->set_flashdata('success', 'Successfully Added');
				redirect('admin/country');
			}
			else{

			      $rdata = array('country_name'=>ucfirst($country_name),
		                      'status'=>$status
		        ); 
		        if(!$this -> country ->count_all_results('country_name',$rdata['country_name']))
					      {
						        $this -> country -> insert ($rdata);	
						        $this->session->set_flashdata('success', 'Successfully Added');
						        redirect('admin/country');
					      }
					    else
					     {
						       $this -> session -> set_flashdata('error','You are trying to add already existing Country');
						       redirect('admin/country/add');
					     }          
				}
			}
		else
			{			
				$this -> data['mode'] = 'add';
				$this -> load -> view('template', $this -> data);
			}
	}
  	
	public function view(){
	      $record = $this->uri->segment(4);
	      $this->data['result'] = $this->country->get($record);
	      $this->data['mode'] = 'view';
	      $this -> load -> view('template', $this -> data);
	      //echo '<pre>';print_r($result);		
	}
	
	public function edit(){

       if(isset($_POST['addmem'])){
       	    extract($_POST);
       	    $edata = array(
                      'country_name'=>$country_name,
                      'status'=>$status
            ); 
        $member = $this->country->get($id);
        if($member->country_name != $edta['country_name']){
        	   if(!$this -> country ->count_all_results('country_name',$edata['country_name']))
			           {
				             $this->country->update($id,$edata);	
				             $this->session->set_flashdata('success', 'Successfully Updated');
				             redirect('admin/country');
			           }
			        else
			           {
				             $this -> session -> set_flashdata('error','You are trying to add already existing Country');
				             redirect('admin/country/edit/'.$id);
			           }  
        	
        	}
        else{
               $this->country->update($id,$edata);
               $this->session->set_flashdata('success', 'Successfully Updated');
				           redirect('admin/country');
				    }
       }	     	
       elseif($this->uri->segment(4)){
	     	    $record = $this->uri->segment(4);
	     	    $this->data['result'] = $this->country->get($record);
	          $this->data['mode'] = 'edit';
	          $this -> load -> view('template', $this -> data);
	     	}
	     	else
		    {
			        redirect('admin/country');
		    }
	}
	
	public function delete()	
	
	{
	  $res = $this->country->delete($_POST['id']); 
					if($res == 1){
						echo 'success';
			    $result = $this->session->set_flashdata('delete', 'Successfully Deleted');
			}
			else{
				  echo 'failed';
				  $result = $this->session->set_flashdata('delete', 'Please try again');
			}		
	}
			
	public function deactivate()
	{
			if($this->uri->segment(4))
			{
				$this->country->deactivate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('delete', 'Successfully Deactivated');
				redirect('admin/country');
			
	
			}else
			{
			redirect('admin/country');
			}	
	
	}
		
	public function active()
	{

			if($this->uri->segment(4))
			{
	
				$this->country->activate($this->uri->segment(4));
				$this -> data['mode'] = 'all';
				$this->session->set_flashdata('success', 'Successfully Activated');
				redirect('admin/country');
		

			}else
			{
				redirect('admin/country');

			}

	}
	
}


?>
