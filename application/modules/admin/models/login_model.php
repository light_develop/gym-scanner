<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this -> table = 'admin';
	    $this -> result_mode = 'object';
    }

    /**
     * Thsi will check the login details
     * if user exist set details set into session
     * otherwise send false
     *
     * @param [array] [$user] [form user details]
     * @return boolian
     */
    public function login($user)
    {
    	$user = $this -> get ($user);
    	if(is_object($user))
    	{
    		$this->session->set_userdata('user',$user);
    		return true;
    	}
    	else return false;
    }

    public function is_logged_in()
    {
    	$user = $this->session->userdata('user');
    	if(!isset($user) ||  is_object($user)) return false; 
    	else return false;    	
    }
}