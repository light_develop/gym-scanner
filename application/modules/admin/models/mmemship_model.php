<?php
class Mmemship_model extends MY_Model {
    		public function get_all(){
                        $this->db->select('membership.id as id, gym.id as gymId, gym.gymname as gymname, membership.name as months, membership.status as status');
                        $this->db->from('membership');
                        $this->db->join('gym', 'membership.gym_id = gym.id');

                        return $this->db->get()->result();
   			}
    		public function get_gym(){
                        return $this->db->get('gym')->result();
   			}    
                public function get_gym_with_id($id){
                     $this->db->select('membership.id as id, gym.id as gymId, gym.gymname as gymname, membership.name as months, membership.status as status');
                    $this->db->where('membership.id', $id);
                    $this->db->from('membership');
                    $this->db->join('gym', 'membership.gym_id = gym.id');
                    return array_pop($this->db->get()->result());
                }                          

 }


?>
