<?php
class Notifications_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'notification';
	     $this -> result_mode = 'object';
	    }

        public function get_allnotes(){
          $result = $this->db->select('nt.id,nt.membership,nt.duration,nt.status,mem.name')
                             ->from('notification nt')
                             ->join('membership mem','mem.id = nt.membership')
                             ->get()
                             ->result();
          return $result;
        }
        public function get_notes($id){
          $result = $this->db->select('nt.id,nt.membership,nt.duration,nt.status,mem.name')
                             ->from('notification nt')
                             ->join('membership mem','mem.id = nt.membership')
                             ->where(array('nt.id'=>$id))
                             ->get()
                             ->result();
          return $result;
        }
    		public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			 $query=$this->db->delete('notification');
   			return 1;
    
    			}
    
    		public function deactivate($deactivateid){
   
    			    $this->db->set('status', 0);
			       $this->db->where('id', $deactivateid);
			       $this->db->update('notification');

    			}
    
     		public function activate($activate){
   
    		     $this->db->set('status', 1);
		        $this->db->where('id', $activate);
		        $this->db->update('notification');

   		 }
	    
 }


?>
