<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this -> table = 'banner';
	    $this -> result_mode = 'object';
    }

 public function getbanners(){
     $result = $this->db->select('ban.*,gym.gymname')
           ->from('banner ban')
           ->join('gym gym', 'ban.gym_id = gym.id')
           -> get()
           -> result();
           //echo $this -> db -> last_query(); exit;
           return $result; 
 	}
 	public function getdet($id){
    		
         $result = $this->db->select('ban.*,gym.gymname')
           ->from('banner ban')
           ->join('gym gym', 'ban.gym_id = gym.id ')
           ->where(array('ban.id'=>$id))
           -> get()
           -> result();    		
    	      return $result;
 	}
  
 public function getid($id){
    		
    	    $data = $this->db->get_where('banner',array('id'=>$id));
    	    return $data->result();
 	}
 		
 	public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			  $query=$this->db->delete('banner');
   			 return 1;
    
 	}
    
    
 public function deactivate_banner($deactivateid){
    
   
    			$this->db->set('status', 0);
	    		$this->db->where('id', $deactivateid);
			   $this->db->update('banner');

 }
    
 public function activate_banner($activate){
    
   
    		$this->db->set('status', 1);
	    	$this->db->where('id', $activate);
		   $this->db->update('banner');

 }
 	   	
}
?>
