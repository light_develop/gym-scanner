<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Cms_model extends MY_Model {
    
    public function __construct() {
	    parent::__construct();
	    $this -> table = 'cmsmenu';
	    $this -> result_mode = 'object';
    }
    
  public function getmenus(){
  	
      /*$data = $this->db->select('*')
      												->from('cmsmenu')
                   ->order_by("submenu_id", "desc")
                   ->order_by("menuname", "asc")
                   -> get()
                   ->result(); */
      $sql = "SELECT * FROM `cmsmenu` order by submenu_id=0 desc,menuname";
      $data = $this->db->query($sql)->result();
      //$data->last_query(); exit;	
      return $data;
  	
  	
  	}    
    
   public function menus($data){

		foreach($data as $row){
			if($row->submenu_id != 0){
			$query=$this->db->get_where("cmsmenu",array("id"=>$row->submenu_id));
			$resultdata=$query->row();
		
			$alldata[]=array("id"=>$row->id,
						 "menuname"=>$row->menuname,
						 "submenu_name"=>$resultdata->menuname,
						 "status"=>$row->status,
						);
			}
			else{
				$alldata[]=array("id"=>$row->id,
						 "menuname"=>$row->menuname,
						 "submenu_name"=>'Main Menu',
						 "status"=>$row->status,
						);
			}
			}
	
     return $alldata;
       
    }
   	   
   public function addmenu(){
   	
       extract($_POST); 
       //print_r($mainmenu);exit;
       $data = array(           
           'menuname'=>$menuname,
           'submenu_id'=>$id
            );
	//$getresult = $this->checkmenuname($menuname,$id);
	//if($getresult<=0){
		   $this->db->insert('cmsmenu',$data);
	//}
               
  }    
  
    public function viewmenu($id){
		$sub_id = $id;
		$data = $this->db->get_where("cmsmenu",array('id'=>$sub_id));
		return $data->result();
	}
	public function delete($delid){ 
		$res = $this->db->get_where('cmsmenu',array('submenu_id'=>$delid)); 
		//$res1 = $this->db->get_where('cms_content',array('menu_id'=>$delid)); 
		$res->num_rows(); 
		//$res1->num_rows(); 
		if($res->num_rows() > 0 ){
			return 0;
		}else{
			$this->db->where('id',$delid); 
 			$query=$this->db->delete('cmsmenu');
   $this->db->where('menu_id',$delid);
   $q = $this->db->delete('cms_content');
   			return 1;
		}
	}
	public function deactivate_menu($deactivateid){
   
    	$this->db->set('status', 0);
		$this->db->where('id', $deactivateid);
		$this->db->update('cmsmenu');

    }
    
    public function activate_menu($activate){
      
    	$this->db->set('status', 1);
		$this->db->where('id', $activate);
		$this->db->update('cmsmenu');

    }
	function checkmenuname($menuname,$id){
	
	$q = "select * from cmsmenu where menuname = '".$menuname."' AND submenu_id='".$id."'";
		$result = $this->db->query($q);
	
		return $result->num_rows();
  	}
	
  }
?>
