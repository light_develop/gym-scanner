<?php
class Country_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'country';
	     $this -> result_mode = 'object';
	    }
	    public function delete($delid){
    
         $this->db->where('id',$delid); 
         $query=$this->db->delete('country'); 

         return 1;
    
          }    
    
        public function deactivate($deactivateid){
   
              $this->db->set('status', 0);
             $this->db->where('id', $deactivateid);
             $this->db->update('country');

          }
    
     public function activate($activate){
    
          $this->db->set('status', 1);
           $this->db->where('id', $activate);
          $this->db->update('country');

       }
}


?>
