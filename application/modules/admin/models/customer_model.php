<?php
class Customer_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this->table = 'gym_customers';
	     $this->result_mode = 'object';
	  }
	    
    public function getusers(){    		
  	  $result = $this->db->select('u.id,u.first_name,u.last_name,u.user_name,u.email,u.status,g.gymname')
         ->from('user u')
         ->join('gym_customers c', 'c.user_id = u.id')
         ->join('gym g', 'g.id = c.gym_id')
         -> get()
         -> result();           
      return $result;
  	}
    public function get_users(){
      
      $query ='SELECT u.id,u.user_name FROM `user` as u WHERE u.id not in (select c.user_id from gym_customers as c)';
      $res = $this->db->query($query)->result(); 
      foreach ($res as $key => $value) {
        $users[$value->id] = $value->user_name;       
      }     
      return $users;
    }
  	public function getuser($id){
  		
        $result = $this->db->select('u.id,u.first_name,u.last_name,u.user_name,u.email,u.status,g.gymname')
         ->from('user u')
         ->join('gym_customers c', 'c.user_id = u.id')
         ->join('gym g', 'g.id = c.gym_id')
         ->where(array('u.id'=>$id))
         -> get()
         -> row();
         return $result;
  	}
 }

?>
