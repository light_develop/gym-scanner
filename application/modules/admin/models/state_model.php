<?php
class State_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this -> table = 'states';
	    $this -> result_mode = 'object';
	}
	public function get_statelist(){
		$query = "SELECT s.state,s.country_id,s.state_code,s.status,cnt.country_name
				 FROM states s 
				 join country cnt ON cnt.id = s.country_id";
		$result = $this->db->query($query) -> result();
        return $result;
	}
	public function get_list($id){
		$query = "SELECT s.state,s.country_id,s.state_code,s.status,cnt.country_name
				 FROM states s 
				 join country cnt ON cnt.id = s.country_id
				 where s.state_code = '".$id."' ";
		$result = $this->db->query($query) -> row();
        return $result;
	}
	public function delete($delid){    
        $this->db->where('state_code',$delid); 
        $query=$this->db->delete('states');
        return 1;
    }    
    public function deactivate($deactivateid){   
        $this->db->set('status', 0);
        $this->db->where('state_code', $deactivateid);
        $this->db->update('states');
    }    
	public function activate($activate){
	    $this->db->set('status', 1);
	    $this->db->where('state_code', $activate);
	    $this->db->update('states');
	}	    
}

