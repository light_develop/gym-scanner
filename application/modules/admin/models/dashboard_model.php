<?php
class dashboard_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this -> table = 'admin';
    }
    public function get_cnts(){
    	$sql = "SELECT ( SELECT COUNT(*) FROM gym ) AS count1, ( SELECT COUNT(*) FROM user ) AS count2 FROM dual";
    	return $this->db->query($sql)->row();
    }
	public function get_profile_data($id){

	$query= $this->db->get_where('admin', array('id' => $id));
	return $query->result_array();

	}

	public function update_profile($data){

		$id= $data['id'];
		$query = $this->db->get_where('admin', array('id' => $id));	
		$queryres=$query->result_array();
		
		if($query->num_rows()>0){	
			if($id==$queryres[0]['id']){
				$username = $data['username'];
				$email = $data['email'];	
				$this->db->where('id', $id);
	            $this->db->update('admin', array('username'=>$username,'email'=>$email));	
				return 1;
			}else{
				return 2;
			}
		}
	}
	public function get_gymdet(){
		$query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id group by g.id order by g.id desc limit 10";
        $result = $this -> db -> query($query) -> result(); 
        return $result;  
	}
	/*public function get_gymdet(){
		$query = "SELECT g.id,g.gymname,g.gym_logo,g.gym_video,g.timings,g.star,g.comments,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.owner,g.ow_phone,g.ow_email,g.status,GROUP_CONCAT(CONCAT(m.name,'-',gof.price)) as price,c.categoryname,cnt.country_name,city.city_name
              FROM gym g
              left join gym_offers gof ON g.id = gof.gym_id
              left join membership m ON gof.mem_id = m.id
              left join category c ON g.category = c.id
              left join country cnt ON g.gcountry = cnt.id
              left join city city ON g.gcity = city.id
              GROUP BY g.id ORDER BY g.id desc LIMIT 10";
      $result = $this -> db -> query($query) -> result();
 
      return $result; 	
	}*/
	public function get_usrdet(){
		$result = $this->db->select('u.id,u.first_name,u.last_name,u.user_name,u.email,g.gymname')
           ->from('user u')
           ->join('gym_customers c', 'c.user_id = u.id')
           ->join('gym g', 'g.id = c.gym_id')
           ->order_by ('u.id','desc')
           ->LIMIT(10)
           -> get()
           -> result();           
        return $result;
	}
    public function change_password($data){
	

	$selected_userid=$this->session->userdata('user')->id;
	$query= $this->db->get_where('admin', array('id' => $selected_userid));
	$resarray= $query->result_array();	
	if($this->encrypt->sha1($data['oldpassword'])==$resarray[0]['password'])
		{
		$password = $this->encrypt->sha1($data['newpassword']);
		$this->db->where('id', $selected_userid);
	        $this->db->update('admin', array('password'=>$password));	
		return 1;
		}else{
		return 2;	
		}

	}



}


?>
