<?php
class City_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this->table = 'cities';
	    $this->result_mode = 'object';
	}
	public function get_citylist($id){
		$query = "SELECT c.*,s.state FROM cities c join states s ON s.state_code = c.state_code where c.state_code='".$id."'";
		$result = $this->db->query($query)->result();
        return $result;
	}
	public function get_states($x){
        $res = $this->db->where('country_id',$x)->get('states')->result();
        $states = array();
        foreach ($res as $value) {
          $states[$value->state_code] = $value->state;
        }
        return $states;
    }
	public function get_list($id){
        $query = "SELECT c.*,s.state_code,s.state,cnt.id cnt_id,cnt.country_name FROM cities c,country cnt,states s where cnt.id = s.country_id and s.state_code = c.state_code and c.id= '".$id."' ";
		/*$query = "SELECT city.id,city.city_name,city.country_id,city.status,cnt.country_name
				 FROM city 
				 join country cnt ON cnt.id = city.country_id
				 where city.id = $id ";*/
		$result = $this->db->query($query)->row();
        return $result;
	}
	public function delete($delid){    
        $this->db->where('id',$delid); 
        $query=$this->db->delete('city');
        return 1;
    }    
    public function deactivate($deactivateid){

          $this->db->set('status', 0);
         $this->db->where('id', $deactivateid);
         $this->db->update('city');

    }
    public function activate($activate){
    
        $this->db->set('status', 1);
        $this->db->where('id', $activate);
        $this->db->update('city');

    }	    
}

