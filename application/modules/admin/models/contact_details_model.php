<?php
class Contact_details_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'contact_details';
	     $this -> result_mode = 'object';
	    }
	    
	    public function getcontacts(){
    		
    	      $result = $this->db->select('c.id,c.company_name,c.company_email,c.company_url,c.telephone,c.contact_name,c.ph_no,c.email,cat.categoryname,cnt.country_name as cnt,city.city_name as city')
           ->from('contact_details c')
           ->join('country cnt', 'c.cnt_id = cnt.id')
           ->join('category cat', 'c.cat_id = cat.id')
           ->join('city city', 'c.city_id = city.id')
           -> get()
           -> result();           
           return $result; 
    	}	    
 }


?>
