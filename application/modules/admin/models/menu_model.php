<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends MY_Model {

	public function __construct() {
	    parent::__construct();
	    $this -> table = 'cms_content';
	    $this -> result_mode = 'object';
    }

    
    public function menuid($sid)
    {    	
				$result = $this->db->select('cc.*,cm.menuname')
           ->from('cms_content cc')
           ->join('cmsmenu cm', 'cc.menu_id = cm.id')
           ->where(array('cc.id'=>$sid))
           -> get()
           -> result();
           //echo $this -> db -> last_query(); exit;
           return $result; 
					  		    	
    	}
    	
    	public function getmenus(){
    		
    	      $result = $this->db->select('cc.*,cm.menuname')
           ->from('cms_content cc')
           ->join('cmsmenu cm', 'cc.menu_id = cm.id')
           -> get()
           -> result();
           return $result;  
           
           
    	}
    	
    	public function getdet($id){
    		
         $result = $this->db->select('cc.*,cm.menuname')
           ->from('cms_content cc')
           ->join('cmsmenu cm', 'cc.menu_id = cm.id ')
           ->where(array('cc.id'=>$id))
           -> get()
           -> result();    		
    	      return $result;
    	}
    	
    	public function getid($id){
    		
    	    $data = $this->db->get_where('cms_content',array('id'=>$id));
    	    return $data->row();
    	}
    	
    	
    	 	public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			 $query=$this->db->delete('cms_content');
   			return 1;
    
    			}
    
    
    		public function deactivate_menu($deactivateid){
    
   
    			$this->db->set('status', 0);
			$this->db->where('id', $deactivateid);
			$this->db->update('cms_content');

    			}
    
     		 public function activate_menu($activate){
    
   
    		$this->db->set('status', 1);
		$this->db->where('id', $activate);
		$this->db->update('cms_content');

   		 }
    	
}
?>
