<?php
class Reviews_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this->table = 'reviews';
	     $this->result_mode = 'object';
	  }
	    
    public function getdets(){    		
  	  $query = 'select r.* , gym.gymname,CASE WHEN user_type = "2" THEN user.first_name ELSE supplier.first_name END AS username from reviews r,user,supplier,gym where gym.id = r.gym_id and CASE WHEN user_type = "2" THEN user.id = r.user_id ELSE supplier.id = r.user_id END group by r.id order by gym_id';
      return $this->db->query($query)->result();
  	}
    
 }

?>
