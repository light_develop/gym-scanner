<?php
class Refer_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this -> table = 'referfriend';
	     $this -> result_mode = 'object';
	    }
	    
	    public function getusers(){
    		
    	      $result = $this->db->select('rg.*')
           ->from('referfriend rg')
           -> get()
           -> result();
           return $result; 
    	}
    	public function getuser($id){
    		
          $result = $this->db->select('rg.*,g.gymname,cat.categoryname')
           ->from('referfriend rg')
           ->join('gym g', 'rg.gym_id = g.id')
           ->join('category cat', 'rg.membership = cat.id')
           ->where(array('rg.id'=>$id))
           -> get()
           -> result();
           return $result;
    	}
    		public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			 $query=$this->db->delete('referfriend');
   			return 1;
    
    			}
    
    		public function deactivate_user($deactivateid){
   
    			    $this->db->set('status', 0);
			       $this->db->where('id', $deactivateid);
			       $this->db->update('referfriend');

    			}
    
     		public function activate_user($activate){
   
    		     $this->db->set('status', 1);
		        $this->db->where('id', $activate);
		        $this->db->update('referfriend');

   		 }
	    
 }


?>
