<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gyms_model extends MY_Model {

  public function __construct() {
      parent::__construct();
      $this -> table = 'gym';
      $this -> result_mode = 'object';
  }
    
  public function get_allgyms(){
      $query = "SELECT g.*,cnt.country_name,s.state,c.city FROM gym g,country cnt,states s,cities c where g.gcountry = cnt.id and g.gstate = s.state_code and g.gcity = c.id group by g.id";
      $result = $this -> db -> query($query) -> result();
      return $result; 
  }  
  public function get_countries(){
    $res = $this->db->where('status','1')->get('country')->result();
    $cntry = array();
    foreach ($res as $value) {
      $cntry[$value->id] = $value->country_name;
    }
    return $cntry;
  }
  public function get_states($x){
    $res = $this->db->where('country_id',$x)->get('states')->result();
    $states = array();
    foreach ($res as $value) {
      $states[$value->state_code] = $value->state;
    }
    return $states;
  }
  public function get_cities($x){
    $res = $this->db->where('state_code',$x)->get('cities')->result();
    $states = array();
    foreach ($res as $value) {
      $states[$value->id] = $value->city;
    }
    return $states;
  }
  public function get_cats(){
    $res = $this->db->where('status','0')->get('category')->result();
    $cats = array();
    foreach ($res as $value) {
      $cats[$value->id] = $value->categoryname;
    }
    return $cats;
  }
  public function get_mems(){
    $res = $this->db->where('status','0')->get('membership')->result();
    $mems = array();
    foreach ($res as $value) {
      $mems[$value->id] = $value->name;
    }
    return $mems;
  }
  public function get_allimages(){
    $data = $this->db->select('id,gym_id,GROUP_CONCAT(gym_images) as gimages')
                    ->from('gym_images')
                    ->group_by('gym_id')
                    ->get()
                    ->result();
    return $data;
  }
  public function get_cntrys(){
      $data = $this->db->select('id,name')
                       ->from('country')
                       ->order_by('name','asc')
                       ->get()
                       ->result();
      return $data;
  }

 private function set_upload_options($filename)
  {   
//  upload an image options
    $config = array();
    $config['upload_path'] = './uploads/gym/logos/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = '0';
    $config['overwrite']     = FALSE;
    $config['file_name']     = $filename;

    return $config;
  }
  private function set_video_upload($filename)
  {   
//  upload an image options
    $config = array();
    $config['upload_path'] = './uploads/gym/videos/';
    $config['max_size']      = '100000000000000';
    $config['overwrite']     = FALSE;
    $config['file_name']     = $filename;

    return $config;
  }
    function addimages() {
        extract($_POST);
        //print_r($_POST);die();
        $udfilename = '';
        $image = '';
        $files = $_FILES;
        $this->load->library('upload');

            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            $rand = rand(10000, 80000);
            $new_img = 'logo_img_' . $rand . '.' . $ext;

            $gym_logo = substr_replace($new_img , 'png', strrpos($new_img , '.') +1);
            if(!file_exists('./uploads/gym/logos/'.$gym_logo)){
                $_FILES['userfile']['name']= $files['logo']['name'];
                $_FILES['userfile']['type']= $files['logo']['type'];
                $_FILES['userfile']['tmp_name']= $files['logo']['tmp_name'];
                $_FILES['userfile']['error']= $files['logo']['error'];
                $_FILES['userfile']['size']= $files['logo']['size'];
                        
                $this->upload->initialize($this->set_upload_options($gym_logo));
                $this->upload->do_upload();                           
            }  
            if(isset($gym_video)){
              $videos = array();
              foreach ($gym_video as $key => $value) {
                if($value != '')
                  $videos[] = $value;
              }
              $videos = implode('~', $videos);
            }else
              $videos = '';  

                if(isset($adv)){   
                $output=array();
                  foreach ($adv as $key => $value) {
                      $output[]=$key;
                  }
                  $output = implode(',', $output);
                }else{
                  $output = '';
                }
                //print_r($output); die();
                $edata = array('gymname'=>ucfirst($gymname),
                               'gym_logo'=>$gym_logo,
                               'gym_video'=>$videos,
                               'open_time'=>$open_time,
                               'close_time'=>$close_time,
                               'star'=>$star,
                               'description'=>$description,
                               'gstreet'=>$gstreet,
                               'gcity'=>$city,
                               'gstate'=>$state,
                               'gcountry'=>$cntry,
                               'gzip'=>$gzip,
                               'location'=>$search_location,
                               'gmap_lat'=>$latitude,
                               'gmap_long'=>$longitude,
                               'speciality'=>$speciality,
                               'category'=>$cat_id,
                               'advanced'=>$output,
                               'price_range'=>$pricerange,
                               'price'=>$price,
                               'owner'=>$owner,
                               'ow_phone'=>$ow_phone,
                               'ow_email'=>$ow_email,
                               'createdon'=>mktime());    

                $this->db->insert('gym', $edata); 
                $new_id = $this->db->insert_id();  

                $p_image = $_FILES['upload_file']['name'];
                $i = 0;
                foreach ($p_image as $img) {

                    $ext = pathinfo($img, PATHINFO_EXTENSION);


                    $rand = rand(100000, 800000);

                    $new_img = 'package_' . $rand . '.' . $ext;

                    $udfilename = substr_replace($new_img , 'png', strrpos($new_img, '.') +1);

                    $tmpFilePath = $_FILES['upload_file']['tmp_name'][$i];
                    //Make sure we have a filepath
                    if ($tmpFilePath != "") {
                        //Setup our new file path
                        $newFilePath = "./uploads/gym/images/" . $udfilename;
                        //Upload the file into the temp dir
                        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                            $img = $udfilename;
                        }else{
                          $img = '';
                        }                          
                    }
                    $data = array('gym_id' => $new_id, 'gym_images' => $img);
                    $this->db->insert("gym_images", $data);

                    $i++;
                }
            return true;
    } 
    public function getgym($id){
      $query = "SELECT g.id,g.gymname,g.description,g.advanced,g.gym_logo,g.gym_video,g.location,g.open_time,g.close_time,g.star,g.gstreet,g.gcity,g.gstate,g.gcountry,g.gzip,g.gmap_lat,g.gmap_long,g.speciality,g.category,g.price_range,g.price,g.owner,g.ow_phone,g.ow_email,g.status, g.is_supplier,c.categoryname,
                cnt.country_name,cnt.id cnt,city.city,st.state
              FROM gym g
              left join category c ON g.category = c.id
              left join country cnt ON g.gcountry = cnt.id
              left join states st ON g.gstate = st.state_code
              left join cities city ON g.gcity = city.id
              WHERE g.id = $id
              GROUP BY g.id";
      $result = $this->db->query($query)->row();
      return $result;
    }  
    public function getimgs($id){
      $query = "SELECT * FROM gym_images WHERE gym_id = $id";
      return $result = $this->db-> query($query) -> result();           
    }  
    
    public function updategyms(){
      
        extract($_POST);
        //echo "<pre>";print_r($_POST); echo "<hr>"; print_r($_FILES);         exit;
        $udfilename = '';
        $image = '';
        $files = $_FILES;
        $this->load->library('upload');
        if($_FILES['logo']['name'] != ''){

            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            $rand = rand(10000, 80000);
            $new_img = 'logo_img_' . $rand . '.' . $ext;

                $gym_logo = substr_replace($new_img , 'png', strrpos($new_img , '.') +1);
                if(!file_exists('./uploads/gym/logos/'.$gym_logo))
                        {
                            $_FILES['userfile']['name']= $files['logo']['name'];
                            $_FILES['userfile']['type']= $files['logo']['type'];
                            $_FILES['userfile']['tmp_name']= $files['logo']['tmp_name'];
                            $_FILES['userfile']['error']= $files['logo']['error'];
                            $_FILES['userfile']['size']= $files['logo']['size'];
                                    
                            $this->upload->initialize($this->set_upload_options($gym_logo));
                                $this->upload->do_upload();  
                        }
        }
        else{
            $gym_logo = $old_image;
        }

          $p_image = $_FILES['upload_file']['name'];
          if(isset($_FILES) && $p_image != ''){
            $i = 0;
            foreach ($p_image as $img) {

                $ext = pathinfo($img, PATHINFO_EXTENSION);
                $rand = rand(100000, 800000);
                $new_img = 'package_' . $rand . '.' . $ext;
                $udfilename = substr_replace($new_img , 'png', strrpos($new_img, '.') +1);
                $tmpFilePath = $_FILES['upload_file']['tmp_name'][$i];
                //Make sure we have a filepath
                if ($tmpFilePath != "") {
                    //Setup our new file path
                    $newFilePath = "./uploads/gym/images/" . $udfilename;
                    //Upload the file into the temp dir
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                      $data = array('gym_id' => $id, 'gym_images' => $udfilename);
                      $this->db->insert("gym_images", $data);
                    }
                }

                $i++;
            }
          }
          if(isset($gym_video)){
            $videos = array();
              foreach ($gym_video as $key => $value) {
                if($value != '')
                    $videos[] = $value;
                }
                $videos = implode('~', $videos);
          }else
            $videos = ''; 
          
            if(isset($adv)){   
              $output=array();
                foreach ($adv as $key => $value) {
                    $output[]=$key;
                }
                $output = implode(',', $output);
              }else{
                $output = '';
              }
                $edata['gymname']       = ucfirst($gymname);
                $edata['gym_logo']      = $gym_logo;  
                $edata['gym_video']     = $videos;
                $edata['open_time']     = $open_time;
                $edata['close_time']    = $close_time;
                $edata['star']          = $star;
                $edata['description']   = $description;
                $edata['gstreet']       = $gstreet;
                $edata['gcity']         = $city;
                $edata['gstate']        = $state;
                $edata['gcountry']      = $cntry;
                $edata['gzip']          = $gzip;
                $edata['location']      = $search_location;
                $edata['gmap_lat']      = $latitude;
                $edata['gmap_long']     = $longitude;
                $edata['speciality']    = $speciality;
                $edata['category']      = $cat_id;
                $edata['advanced']      = $output;
                $edata['price_range']   = $pricerange;
                $edata['price']   		= $price;
                $edata['owner']         = $owner;
                $edata['ow_phone']      = $ow_phone;
                $edata['ow_email']      = $ow_email;
                $edata['is_supplier']	= ($made_by == 1)?'0':$is_supplier;

                $this->db->where(array('id'=>$id))
                         ->update('gym',$edata);                
        return;
    }

      public function selected_cntry($id){
 
          $qry="select id,city_name from city where country_id = '$id' order by city_name";
                                                            
            $result = $this->db->query($qry);                 
                  
            $dataArray=$result->result_array();
                  
            $returnarray=array();
                  
            foreach($dataArray as $value){
            
              $returnarray[$value['id']]=$value['city_name'];            
            
            }
                  
            return $returnarray;
          
          }  
      public function get_memname($id){
        $result = $this -> db ->get('membership');
        return $result->result();
      }  
    
    public function delete_data($delid){
    
         $this->db->where('id',$delid); 
          $query=$this->db->delete('gym');
         return 1;
    
          }   
     
}
 
