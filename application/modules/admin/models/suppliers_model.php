<?php
class Suppliers_model extends MY_Model {

		public function __construct() {
	    parent::__construct();
	    $this ->table = 'supplier';
	     $this -> result_mode = 'object';
	    }
	    
	    public function getsuppliers(){
    		
    	      $result = $this->db->select('id,first_name,last_name,user_name,mobile,email,level,status')->from('supplier')-> get()-> result();           
           return $result; 
    	}
    	public function getsupplier($id){
    		
          $result = $this->db->select('id,first_name,last_name,user_name,password,mobile,email,level,status')
           ->from('supplier')
           ->where('id',$id)
           -> get()
           -> row();
           return $result;
    	}
    		public function delete_data($delid){
    
   			 $this->db->where('id',$delid); 
  			 $query=$this->db->delete('user');
   			return 1;
    
    			}
    
    		public function deactivate_user($deactivateid){
   
    			    $this->db->set('status', 0);
			       $this->db->where('id', $deactivateid);
			       $this->db->update('user');

    			}
    
     		public function activate_user($activate){
   
    		     $this->db->set('status', 1);
		        $this->db->where('id', $activate);
		        $this->db->update('user');
        }
        public function gymsByCat($id){

          $qry="select id,gymname  from gym where category = $id order by gymname";
                                                            
            $result = $this->db->query($qry);                 
                  
            $dataArray=$result->result_array();
                  
            $returnarray=array();
                  
            foreach($dataArray as $value){
            
              $returnarray[$value['id']]=$value['gymname'];            
            
            }                 
            return $returnarray;  
        }
        public function get_offers($gym){
        $query = "SELECT GROUP_CONCAT(CONCAT(m.id,'-',m.name,'-',price,'-',offer,'-',gym_id)) as price
                  FROM gym_offers
                  join membership m ON mem_id = m.id
                  WHERE gym_id = $gym
                  order by mem_id asc ";
        return $result = $this->db->query($query)->row();
      }
	  public function get_suppliers_dropdown(){
		$res = $this->db->where('status','0')->order_by('first_name')->get('supplier')->result();
		$result = array();
		foreach ($res as $value) {
		  $result[$value->id] = ucfirst($value->first_name).' '.ucfirst($value->last_name);
		}
		return $result;
	  }      
	    
 }


?>
