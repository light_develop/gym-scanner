<?php
class Faq_model extends MY_Model {

    public function __construct() {
      parent::__construct();
      //$this ->table = 'user';
       $this ->result_mode = 'object';
      }
      
      public function insertFaq($is_replied = '0'){
		
		extract($_POST);
		$data = array(
			'message'=>$message,
			'user_id'=>$this->session->userdata('user_id'),
			'is_replied'=>$is_replied,
			'visible_flag'=>$visible_flag,
			'user_type'=>'0',
			'createdon'=>date('Y-m-d H:i:s')
		);
		$this->db->insert('faq', $data);
		return $this->db->insert_id();
      }
	  public function getfaq($is_replied = '0')
	  {
		  $result = $this->db->where('is_replied', $is_replied)->order_by('createdon', 'desc')->get('faq')->result();
		  return $result;
	  }
 }

?>
