<?php set_time_limit (0); ?>
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->

  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
      <div class="widget-title">
          <h4> <i class="icon-reorder"> </i> All Cities </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>city/add" class="icon-plus"></a> </span>
        </div>
        <div class="widget-body form">

          <?php if($this -> session -> flashdata('error') !='') { ?>
            <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
          <?php } ?> 
          <div class="control-group">
            <label class="control-label"> Country </label>
            <div class="controls">
              <?php $countries[''] = 'Country';$attributes= 'id="country_id" class="span6 required" onchange="load_dropdown_content(this.value)"';
                echo form_dropdown('country_id', $countries, '',$attributes);
              ?>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label"> States </label>
            <div class="controls">
              <?php $states[''] = 'State';$attributes= 'id="state" class="span6 required"';
                echo form_dropdown('state', array(), '',$attributes);
              ?>
            </div>
          </div>
          <!-- <div class="form-actions">
            <button type="submit" class="btn btn-success" id=""> Submit </button>
          </div> -->
          <div id="city_list"></div>
        </div>
      </div>
    </div>
  </div>

<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Cities </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>city" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>city/add" class="form-horizontal" method="post" id="add_notify" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Country List </label>
                  <div class="controls">
                    <?php $countries[''] = 'Country';$attributes= 'id="country_id" class="span6 required" onchange="load_dropdown_content(this.value)"';
                    echo form_dropdown('country_id', $countries,'',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> States </label>
                  <div class="controls">
                    <?php $states[''] = 'State';$attributes= 'id="state" class="span6 required"';
                      echo form_dropdown('state', array(), '',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="city_name" value="" id="city_name"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City List (Excel) </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="userfile" id="userfile"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
                                  <option value="1"  selected="selected">Active</option>                   
                                  <option value="0">Inactive</option>                   
                   </select>
                </div>
                </div> -->
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_city"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>city" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>city/edit" class="form-horizontal" method="post" id="add_notify" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Country List </label>
                  <div class="controls">
                    <?php $cntry[''] = 'Country';$attributes= 'id="country_id" class="span6 required" onchange="load_dropdown_content(this.value)"';
                    echo form_dropdown('country_id', $countries,$result->cnt_id,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> States </label>
                  <div class="controls">
                    <?php $attributes= 'id="state" class="span6 required"';
                      echo form_dropdown('state', $states,$result->state_code,$attributes);
                    ?>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label"> City Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $result->id ?>">
                   <input type="text" name="city_name" class="span6 required" value="<?php echo $result->city; ?>" />
                </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">    
                  <?php $status = $result->status; ?>             
                   <select name="status" id="status" class="chosen span6 required" >                   
                      <option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
                      <option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                    
                   </select>
                </div>
                </div>  -->           
                <br />
                <div class="form-actions">
                  <button type="submit" name="addmem" class="btn btn-success" id="saveuser"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'):

 ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> City View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>city" class="icon-arrow-left"></a></span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>  
                      <tr>
                        <td class="span3"> Country Name : </td>
                        <td> <?php echo $result->country_name; ?> </td>
                      </tr>
                      <tr>
                        <td class="span3"> State Name : </td>
                        <td> <?php echo $result->state; ?> </td>
                      </tr>                      
                      <tr>
                        <td class="span3"> City Name : </td>
                        <td> <?php echo $result->city; ?> </td>
                      </tr>                                 
<!--                       <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($result -> status==1)?"Active":"In Active";?></td>
                      </tr> -->
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script>
  jQuery(document).ready(function(){

    $("#gym_form").validate({
      
      rules: {
          "country_id":"required",
          "state":"required",
          }    
    });

    $("#add_city").click(function(){
        var cnt = $('#country_id').val();
        var state = $('#state').val();
        var city_name = $("#city_name").val();
        var userfile = $("#userfile").val();
        if(cnt == ''){
          alert('please choose Country');
          return false;
        }
        if(state == '' || state == 0){
          alert('please choose State');
          return false;
        }
        if(city_name == '' && userfile == ''){
          alert('please choose atleast one inserting option');
          return false;
        }
    });   
  });
  $("#saveuser").click(function(){
        var cnt = $('#country_id').val();
        var state = $('#state').val();
        var city_name = $("#city_name").val();
        if(cnt == ''){
          alert('please choose Country');
          return false;
        }
        if(state == '' || state == 0){
          alert('please choose State');
          return false;
        }
        if(city_name == ''){
          alert('please enter city');
          return false;
        }
    });   
  function load_dropdown_content(selected_value){ 

       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo ADMIN_URL;?>city/get_states/' + selected_value,
                   'async' : false
             }).responseText;
         $("#state").replaceWith(result);
      }
  }
  function get_cities(selected_value){ 
    console.log(selected_value);
       if(selected_value!=''){
             var result = $.ajax({
                   'url' : '<?php echo ADMIN_URL;?>city/get_cities/' + selected_value,
                   'async' : false
             }).responseText;

         $("#city_list").html(result);
          // begin first table
        $('#sample_1').dataTable({
            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0]
            }]
        });
      }
  }
  function getid(id){
    
      if (confirm('Do you want to delete this city?')) 
      {
        $.ajax({
          type: 'POST',
          url: '<?php echo ADMIN_URL;?>city/delete',
          data: { id: id}
        }).done(function(response){  
        if(response == 'success'){
               window.location.reload(true);
               return true;
        }else{
               window.location.reload(true);
               return false;
       }});
      }
      else{
        return false;           
      }
  }               
</script>
<script type="text/javascript">
  function status_change_gym(id,status,aid)
  {
    $.ajax({
        type : "POST",
        url : "<?php echo ADMIN_URL;?>city/change_status",
        data : {
                    'id': id,
                    'status':status
               },
        success : function(data) {
            if(status == '0'){
              $('#'+aid).removeClass('btn btn-info').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
              $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
            }else if(status == '1'){
              $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
              $('#'+aid).attr('onclick','status_change_gym('+id+',2,"'+aid+'")');
            }else if(status == '2'){
              $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
              $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
            }else{
              $('#'+aid).removeClass('btn btn-warning').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
              $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
            }
        },
        error : function() {
           alert("We are unable to do your request. Please contact webadmin");
        }
    });
  }
</script>