
<div id="main-content">
  <div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"></div>
  </div>
  
 
  <link href="<?=base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<?php
/**
 * [Check the mode of view, if all it will list all users]
 * @var [string]
 */
if(isset($mode) && $mode == 'all'):?>   
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Users </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo site_url()?>administrator/users/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Name </th>
              <th> UserName </th>
              <th> Email </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
       
          <?php if(isset($users)  && count($users)){ $i = 1; ?>          
          <?php foreach ($users as $user)  { ?>
            <tr class="odd gradeX">
           
             <td> <?=ucfirst($user->first_name.' '.$row -> last_name)?> </td>
             <td> <?=$user->username;?> </td>
              <td> <?=$user->email;?> </td>
              <td class="hidden-phone" style="width:155px">
              <!-- <a href="#" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a> -->
                <a class="btn mini purple editcity" href="<?=site_url()?>users/edit/<?=$user -> id?>"> <i class="icon-edit"> </i> Edit </a>
               <!--  <a href="#" class="btn mini black"> <i class="icon-trash"> </i> Delete </a> -->
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Users </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo site_url() ?>administrator/users" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
               <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>

                <form action="<?php echo ADMIN_URL ?>add" class="form-horizontal" method="post" id="user_profile_form" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="name"/>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Username </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                       <input type="text" name="username" placeholder="Please Enter Username">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Password </label>
                  <div class="controls">
                    <input type="password" name="password" placeholder="Please Enter Password">
                  </div>
                </div>                
                  <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input type="text" name="email" placeholder="Please Enter email address">
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>
    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Profile Edit</h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?=ADMIN_URL?>dashboard/profile" class="icon-arrow-left"> </a> </span>
                <span class="tools"> <a href="<?=ADMIN_URL?>dashboard/profile_edit/<?=$profiledata['id']?>" class="icon-edit"> </a> </span>
              </div>
              <div class="widget-body form">            

              <form action="<?=site_url()?>dashboard/profile_update" class="form-horizontal" method="post" id="user_profile_form"/>
                <div class="error"> </div>
                <div class="control-group">
                  <label class="control-label"> Username </label>
                    <div class="controls">
                    <input type="hidden" name="userid" value="<?php echo $this->session->userdata['user']->id?>" >
                     <input class="span6 required" type="text" name="username" value="<?=$profiledata['username'] ?>" readonly>
                     <span id="usernameid" class="error"></span>
                    </div>
                </div> 
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="<?=$profiledata['email']?>">
                  </div>
                </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="update_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'view'): ?>

<div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Profile </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                 
                <span class="tools"> <a href="<?= ADMIN_URL?>dashboard/profile_edit/<?= $profiledata['id']?>" class="icon-edit"> </a> </span>
              </div>
              <div class="widget-body">
                <div class="span3">
                </div>
                <div class="span6">                 
                 <h4><?php ucwords($profiledata['username'])?></h4>
                  <table class="table table-borderless">
                    <tbody>
                      <tr> <td class="span2"> User Name :</td> <td> <?=$profiledata['username']?>  </td> </tr>
                      <tr> <td class="span2"> Email :</td> <td> <?=$profiledata['email']?>  </td> </tr>
                    </tbody>
                  </table>
                </div>
                <div class="space5">
                </div>
              </div>
            </div>
          </div>
        </div>      
         
<?php endif; ?>

  </div>
</div>
<script type="text/javascript" src="<?=base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script type="text/javascript">
  jQuery(document).ready(function(){

  $("#user_profile_form").validate({
  
    rules: {
        "username":"required",
        "email":{required:true,email: true}
        }    
    });

    }); 
</script>
                                                         



