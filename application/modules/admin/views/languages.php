<div id="main-content">
    <div class="">
        <link rel="stylesheet" type="text/css"
              href="<?php echo base_url(); ?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/admin/css/style_datepicker.css"/>
        <script src="<?php echo base_url(); ?>themes/admin/js/framework/main.js"></script>
        <link href="<?php echo base_url(); ?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css"
        <link href="<?php echo base_url(); ?>themes/admin/css/admin-languages.css"
              rel="stylesheet"/>
    </div>
    <style>
        a.btn.btn-danger {
            margin-left: 1%;
            width: 14%;
            margin-top: -1%;
        }

        textarea {
            resize: none;
            row: 4;
            width: 35%;
            margin-left: 2px;
        }

        .span9.admin_languages {

            left: 4%;
        }

        .settings_widget {
            position: relative;

            margin-left: -2px;
        }

        .buttons-set {
            display: inline-block;
            margin-left: 2px;
        }

        select#lang_id {
            margin-left: 1px;
        }

        div#main-content {
            display: flex;

        }

        .body {
            background-color: white;

        }

        .navbar-inner {
            border-radius: 0;
            height: 21px;
            padding-top: -3px;
            min-height: 33px;
        }

        .container-fluid.span12 {
            position: relative;

        }
        #lang_id > option:first-letter{
            text-transform: uppercase;
        }
        #lang_id > option:first-letter{
            text-transform: uppercase;
        }
    </style>
    <div class="container-fluid span12">
        <div class="settings_widget ">
            <div class="admin_language_settings">
                <div class="body">

                    <div class="navbar">
                        <div class="navbar-inner">
                            <h5><i class="icon-reorder"> </i> Languages </h5>
                        </div>
                    </div>
                    <div class="container-fluid">

                        <div class="span9 admin_languages">
                            <div class="admin_language_panel">
                                <?php echo $this->getLanguagesSelect("id=lang_id"); ?>
                                <?php echo form_open('admin/languages/saveLanguages', array("id" => "lang_save_form", "onsubmit" => "langSubmit(); return false;")); ?>
                                <div class="buttons-set">
                                    <a class="btn btn-info  " href="javascript:addInput();">Add Input Fields</a>
                                    <button type="submit" id="submit-btn" class="btn btn-success no-display">Submit
                                    </button>
                                </div>
                            </div>


                            <?php foreach ($languageSaved as $language): ?>
                                <div class="lang-id-<?php echo $language["lang_id"] ?> no-display">
                                    <textarea onchange="showSubmitButton()" class="lang-input" validation="notempty"
                                              name="update[<?php echo $language["entity_id"] ?>][main_lang]"
                                              value="<?php echo $language["main_lang"] ?>"><?php echo $language["main_lang"] ?></textarea>
                                    <textarea onchange="showSubmitButton()" class="lang-input" validation="notempty"
                                              name="update[<?php echo $language["entity_id"] ?>][second_lang]"
                                              value="<?php echo $language["second_lang"] ?>"><?php echo $language["second_lang"] ?></textarea>
                                    <a href="javascript: void(null)" onclick="changeName(this)">Remove</a>
                                    <input type="hidden" name="update[<?php echo $language["entity_id"] ?>][lang_id]"
                                           value="<?php echo $language["lang_id"] ?>"/>
                                    <input type="hidden" name="update[<?php echo $language["entity_id"] ?>][entity_id]"
                                           value="<?php echo $language["entity_id"] ?>"/>
                                </div>
                            <?php endforeach; ?>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $color = ($this->session->flashdata("success")) ? "green" : "red"; ?>
    <p style="color: <?php echo $color; ?>">
        <?php echo $this->session->flashdata("success") ?>
        <?php echo $this->session->flashdata("error") ?>
    </p>
</div>
<?php /*

 TODO check for empty fields
 */ ?>
<script>
    var firstId = 0;
    var initLang = false;
    var form = $1('lang_save_form');
    function addInput() {
        if ($$1('#lang_id')[0].value != "0") {
            showSubmitButton();
        } else {
            return false;
        }

        $1('submit-btn').style.display = 'block';
        var container = _createElement("div", {"id": "container-" + firstId}, form);
        var mainLang = _createElement("textarea", {
            "id": "main-lang-" + firstId,
            "class": "lang-input",
            "name": "insert" + "[" + firstId + "]" + "[main_lang]",
            validation: "notempty",
            placeholder: "Key Language"
        }, container);
        var secondLang = _createElement("textarea", {
            "id": "second-lang-" + firstId,
            "name": "insert" + "[" + firstId + "]" + "[second_lang]",
            "class": "lang-input",
            validation: "notempty",
            placeholder: "Result Language"
        }, container);
        var langCode = _createElement("input", {
            "id": "lang-code-" + firstId,
            "name": "insert" + "[" + firstId + "]" + "[lang_id]",
            type: "hidden",
            value: $1('lang_id').value
        }, container);
        var remove = _createElement("a", {
            "href": "javascript: void(null)",
            "class": "btn btn-danger",
            "onclick": "changeName(this, 1)",
            "inner": "Remove"
        }, container);
        firstId++;
    }

    function langCodeChange(langCode) {
        if (typeof($$1('div.no-display')[0]) != "undefined") {
            for (i = 0; i < $$1('div.no-display').length; i++) {
                $$1('div.no-display')[i].style.display = "none";
            }
        }

        if (typeof($$1('.lang-id-' + langCode.value)[0]) != "undefined") {
            for (i = 0; i < $$1('.lang-id-' + langCode.value).length; i++) {
                $$1('.lang-id-' + langCode.value)[i].style.display = "block";
            }
        }
    }

    $1('lang_id').onchange = function () {
        langCodeChange(this);
    }

    function langSubmit() {
        if (validate($$1('.lang-input'))) {
            form.action += "?default_lang=" + $1('lang_id').value;
            submit();
        }
    }

    function showSubmitButton() {
        $1('submit-btn').style.display = "block";
    }

    function changeName(remove, insert) {
        var parentContainer = remove.parentNode;
        if (insert) {
            parentContainer.parentNode.removeChild(parentContainer);
            return false;
        } else {
            parentContainer.style.display = "none";
        }

        if (parentContainer.querySelectorAll("textarea, input")[0]) {
            var textareas = parentContainer.querySelectorAll("textarea, input");

            for (i = 0; i < textareas.length; i++) {
                textareas[i].name = textareas[i].name.replace(/^update(.*)$/, "remove$1");
            }
            showSubmitButton();
        }
    }

    var httpgetParams = httpget(location.href);

    if (httpgetParams && typeof(httpgetParams["default_lang"])) {
        if (typeof(httpgetParams["default_lang"]) != "undefined") {
            $1('lang_id').value = httpgetParams["default_lang"];
            langCodeChange($1('lang_id'));
        }
    }

</script>

<script src="<?php echo base_url(); ?>themes/admin/js/framework/main.js"></script>
