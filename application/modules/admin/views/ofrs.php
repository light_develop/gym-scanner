<div class="control-group">
    <label class="control-label"> Offers </label>
    <div class="controls">
	<table border="1px">
		<tr>
			<td class="sapn2">Offer Name</td>
			<td class="sapn2">Description</td>
			<td class="sapn2">Start Date</td>
			<td class="sapn2">End Date</td>
			<td class="sapn2">Select</td>
		</tr>
		<?php if(count($res) > 0){ foreach ($res as $key => $value) { ?>
			<tr>
				<td class="sapn2"><?= $value->name;?></td>
				<td><?= $value->description;?></td>
				<td><?= $value->start_date;?></td>
				<td><?= $value->end_date;?></td>
				<td align="center"><input type="radio" name="ofr_id" value="<?= $value->id;?>"/></td>
			</tr>
		<?php } }else { ?>
			<tr><td colspan='5' align="center">No records</td> </tr>
		<?php } ?>
	</table>
    </div>
</div>
<div class="control-group">
    <label class="control-label">  </label>
    <div class="controls">
    </div>
</div>