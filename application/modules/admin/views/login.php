<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"></html><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Login page</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_responsive.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_default.css" rel="stylesheet" id="style_color" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body id="login-body">
	<div class="login-header">
		<div id="logo" class="center">
			<img src="<?php echo base_url()?>themes/admin/img/logo.png" alt="logo" class="center" />
		</div>
	</div>
	<div id="login">
		<form id="loginform" class="form-vertical no-padding no-margin" method="post" action="<?php echo ADMIN_URL;?>" />
		<div class="lock">
			<i class="icon-lock"></i>
		</div>
		<div class="control-wrap">
			<h4>User Login</h4>
			<?php if($this -> session -> flashdata('error') !=''){ ?>
				<div class="alert">
					<p style="float:right"> X </p>
					<span class="error"><?php echo $this -> session -> flashdata('error');?></span>
				</div>
			<?php } ?>
			<div class="control-group">
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-user"></i></span><input
							id="input-username" type="text" name="username" class="required" placeholder="username" />
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-key"></i></span><input
							id="input-password" type="password" class="required" name="password" placeholder="Password" />
					</div>
					<div class="clearfix space5"></div>
				</div>
			</div>
		</div>
		<input type="submit" id="login-btn" class="btn btn-block login-btn" value="Login" />
        </form>
	</div>
	<div id="login-copyright">2013 &copy; Provab Dashboard.</div>
	<script src="<?php echo base_url()?>themes/admin/js/jquery-1.8.3.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/jquery.blockui.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>themes/admin/js/scripts.js" type="text/javascript"></script>
	<script type="text/javascript">jQuery(document).ready(function(){App.initLogin()});</script>
	<script>
		jQuery(document).ready(function(){
			$('#loginform').validate();$('.error').css('color','red');
			$('.alert p').click(function(){$('.alert').hide('slowly')});
		});
	</script>
</body>
</html>