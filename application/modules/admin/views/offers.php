<style>
    table {
        table-layout: fixed;
        width:100%
    }
    td {
        word-wrap:break-word;
    }
</style>
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Offers </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>offers/add" class="icon-plus"></a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
                <th>Gym Name </th>
              <th> Duration </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>

          <?php

          //echo "<pre>";print_r($offers);echo "</pre>";exit;
           if(isset($offers)  && count($offers)){ $i = 1;

          ?>

          <?php foreach ($offers as $row)  { ?>
            <tr class="odd gradeX">
               <td><?php echo $row->gymname; ?></td>
             <td> <?php echo $row->months." months";?> </td>
              <td class="hidden-phone">
              <a href="<?php echo ADMIN_URL;?>offers/view/<?php echo $row -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>offers/edit/<?php echo $row -> id?>"> <i class="icon-edit"> </i> Edit </a> 
                <span class="btn btn-danger" onclick="getid(<?php echo $row-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                <?php if($row->status==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>offers/deactivate/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Active</a>              
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>offers/active/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
                <?php endif; ?>
                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Membership </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>offers" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>offers/add" class="form-horizontal" method="post" id="add_notify" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Select Gym </label>
                  <div class="controls">
                      <select name="gym" class="required">
                          <option value="">Choose Gym</option>
                          <?php if(isset($gyms))
                              foreach($gyms as $gym) {
                              ?>
                          <option value="<?php echo $gym->id; ?>"><?php echo $gym->gymname; ?></option>
                            <?php  } ?>
                      </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Membership Days (Months) </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="membership" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
                                  <option value="1"  selected="selected">Active</option>                   
                                  <option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_notif"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'):  ?>

    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>offers" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>offers/edit" class="form-horizontal" method="post" id="add_notify" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                  <div class="control-group">
                  <label class="control-label"> Select Gym </label>
                  <div class="controls">
                      <select name="gym" class="required">
                          <option value="">Choose Gym</option>
                          <?php if(isset($gyms))
                              foreach($gyms as $gym) {
                              ?>
                          <option value="<?php echo $gym->id; ?>" <?php if($result->gymId==$gym->id) echo 'selected'; ?> ><?php echo $gym->gymname; ?></option>
                            <?php  } ?>
                      </select>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label"> Duration Days (Months) </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $result -> id; ?>">
                    <input type="text" name="membership" class="span6 required" value="<?php echo $result->months; ?>" />
                </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">    
                  <?php $status = $result->status; ?>             
                   <select name="status" id="status" class="chosen span6 required" >                   
            <option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
            <option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                    
                   </select>
                </div>
                </div>            
                <br />
                <div class="form-actions">
                  <button type="submit" name="addmem" class="btn btn-success" id="saveuser"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'):
 ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Notification View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>offers" class="icon-arrow-left"></a></span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody> 
                      <tr>
                        <td class="span3"> Gym Name : </td>
                        <td> <?php echo $result->gymname; ?> </td>
                      </tr> 
                      <tr>
                        <td class="span3"> Membership : </td>
                        <td> <?php echo $result->months." months"; ?> </td>
                      </tr>                                 
                      <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($result -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script>
  jQuery(document).ready(function(){

$("#add_notify").validate({
  rules: {
    // simple rule, converted to {required:true}
    name: "required"    
  }
});
 
  });
   function getid(id){
    
      //alert(id);  
      if (confirm('Do you want to delete this menu?')) 
      {
      $.ajax({
    type: 'POST',
                url: '<?php echo ADMIN_URL;?>offers/delete',
                data: { id: id}
            })
            .done(function(response){  
                      if(response == 'success'){
                             window.location.reload(true);
                             return true;
                      }else{
                             window.location.reload(true);
                             return false;
                     }});
              }
           else{
                return false;           
            }
            }
               
</script>


