
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
<?php
/**
 * [Check the mode of view, if all it will list all menus]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') :  ?>   
<!-- Start Listing All Menus -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Banners </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>banners/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
      
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Gym Name </th>
              <th> Banner Title </th>
              <th> Image </th>
              <th class="hidden-phone"> Description </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php if(isset($banners) && is_array($banners) && count($banners)){ $i = 1;?>
          <?php foreach ($banners as $key => $banner) { 
        $gymname = str_replace('[@]', '<br/>',  $banner->gymname);
        $description = str_replace('[@]', '<br/><br/>',  $banner->description);
        
          ?>
            <tr class="odd gradeX">
             <td><a href="<?php echo ADMIN_URL;?>banners/view/<?php echo $banner -> id ;?>">
             <?php echo ucfirst($gymname) ; ?></a>  </td>
             <td> <?php  echo ucfirst($banner->banner_title); ?></td>
             <td> <img src="<?php echo base_url()?>uploads/thumbs/banner/<?php echo $banner -> image; ?>" alt="image" /> </td>
             <td> <?php echo $description; ?> </td>
              <td class="hidden-phone" style="width:330px">
              <a href="<?php echo ADMIN_URL;?>banners/view/<?php echo $banner -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>banners/edit/<?php echo $banner -> id?>"> <i class="icon-edit"> </i> Edit </a>
                <span class="btn btn-danger" onclick="getid(<?php echo $banner-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                <?php if($banner->status==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>banners/deactivate/<?php echo $banner->id?>"> <i class="icon-ok icon-white"> </i>Active</a>
                
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>banners/active/<?php echo $banner->id?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
                         
                <?php endif; ?>
                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Banner </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>banners" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL;?>banners/add" class="form-horizontal" method="post" id="banner_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                
                
                <div class="control-group">
                  <label class="control-label"> Select Gym </label>
                  <div class="controls">
                    <?php $gyms[''] = 'Select Gym '; $attributes= 'id="gym_id" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('gym_id', $gyms, '',$attributes);
                    ?>
                  </div>
                </div> 
                <div class="control-group">
                  <label class="control-label"> Banner Title </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please Banner Title" id="banner_title" name="banner_title" value=""/>
                  </div>
                </div>                
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"> </textarea>
                  </div>
                </div>   
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1"  selected="selected">Active</option>                   
																	<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>               
                <br />
                <div class="form-actions">
                  <button type="submit" name="addbanner" class="btn btn-success" id="savebanner"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Edit Banner </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>banners" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL; ?>banners/edit" class="form-horizontal" method="post" id="menu_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Gym List </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $banner -> id ?>">
                    <?php $attributes= 'id="gym_id" class="chosen span6 required" tabindex="6" required';
                    
                    //echo "<pre>";print_r($food_select_categories);echo "</pre>";
                    echo form_dropdown('gym_id', $gym, $banner->gym_id,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Banner Title </label>
                  <div class="controls">
                    <input class="span6 required" type="text" id="banner_title" name="banner_title" value="<?php echo ucfirst($banner->banner_title);?>"/>
                  </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    <img src="<?php echo base_url();?>uploads/thumbs/banner/<?php echo $banner->image?>" width="90" alt="image">
                    </div>
                  </div>
                </div>                
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"><?php echo $banner -> description ?> </textarea>
                  </div>
                </div> 
                <div class="control-group">
                  <label class="control-label"> Status </label>
                  <div class="controls">
                  
                    <?php $status = $banner->status; ?>
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
																	<option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                   
                   </select>
                   
                </div>
                </div> 
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="editbanner" name="editbanner"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'):

 ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Banner </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL ?>banners" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Gym Name : </td>
                        <td> <?php echo ucfirst($banner -> gymname);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Banner Title : </td>
                        <td> <?php echo ucfirst($banner -> banner_title);?></td>
                      </tr>                                    
                      <?php if($banner -> image) {?>
                      <tr>
                        <td class="span3"> Image : </td>
                        <td> <img src="<?php echo base_url(); ?>uploads/thumbs/banner/<?php echo $banner -> image; ?>" alt=""></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td class="span3"> Description : </td>
                        <td> <?php echo $banner -> description;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($banner -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
  </div>
</div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script> 
  jQuery(document).ready(function(){

  $("#banner_form").validate({
    rules: {
    	   "banner_title":"required",
        image:{ required: true, extension: "gif|jpeg|jpg|png"}
        },
        image: {
          image:{ extension:"File must be in gif / jpeg / jpg / png formats"}
        }
    });
    $('#savebanner').click(function(){ 
       var k = $('#gym_id').val();       
      if(k==''){
        $("#gym_id").after('<label for="gym_id" generated="true" class="error">Please select gym.</label>' );
        return false;
      } 
    });
 
  });
  function getid(id){
  	
      if (confirm('Do you want to delete this banner?')) 
      {
 	   	$.ajax({
		type: 'POST',
                url: '<?php echo ADMIN_URL;?>banners/delete',
                data: { id: id}
            })
            .done(function(response){   
				              if(response == 'success'){
					                   window.location.reload(true);
					                   return true;
				              }else{
				              	     window.location.reload(true);
					                   return false;
				             }});
  	          }
  	       else{
                return false;  	       	
  	       	}
  	       	}
 
</script>


