          <table class="table table-striped table-bordered" id="sample_1">
            <thead>
              <tr>
                <th> City Name </th>
                <th class="hidden-phone">Actions </th>
              </tr>
            </thead>
            <tbody>

            <?php if(isset($cities)  && count($cities)){ $i = 1;?>

            <?php foreach ($cities as $row)  { ?>
              <tr class="odd gradeX">

               <td> <?php echo $row->city;?> </td>
                <td class="hidden-phone">
                <a href="<?php echo ADMIN_URL;?>city/view/<?php echo $row -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                  <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>city/edit/<?php echo $row -> id?>"> <i class="icon-edit"> </i> Edit </a> 
                  <span class="btn btn-danger" onclick="getid(<?php echo $row-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                  <!--
                  <?php if($row->status==1){?>
                  <a class="btn btn-success" href=""> <i class="icon-ok icon-white"> </i>Active</a>
                  <a class="btn btn-success" id="<?php echo $row->id; ?>" onclick="status_change_city(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-up"> </i></a>
                  <?php } else if($row->status == 0){?>
                    <a class="btn btn-danger" href=""> <i class="icon-ok icon-white"> </i>Inactive</a>
                    <a class="btn btn-danger" id="<?php echo $row->id; ?>" onclick="status_change_city(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                  <?php } ?>
                  -->              
                </td>
              </tr>
              <?php $i++; } } ?>
            </tbody>
          </table>