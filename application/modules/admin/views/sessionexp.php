<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"></html><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Login page</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_responsive.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_default.css" rel="stylesheet" id="style_color" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body id="login-body">
	<div class="login-header">
		<div id="logo" class="center">
			<img src="<?php echo base_url()?>themes/admin/img/logo.png" alt="logo" class="center" />
		</div>
	</div>
    <div id="login" >
		
		<div class="control-wrap">
                    <h4><strong>Your session time expired</strong></h4>
			
			
		</div>
            <input type="button" onclick="window.location='login'" id="login-btn" class="btn btn-block login-btn" value="Login Again" />
                
	</div>
	<div id="login-copyright">2013 &copy; Provab Dashboard.</div>

</body>
</html>