
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Users </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" >
              <thead>
                <tr>                 
                  <th> Company Name </th>    
                  <th> Category </th>
                  <th> Company Email </th>
                  <th> URL </th>
                  <th> TelePhone </th>
                  <th> Country </th>
                  <th> City </th>
                  <th> Contact Person </th>
                  <th> Mobile </th>
                  <th> Email </th>
                </tr>
              </thead>
              <tbody>
              <?php if(isset($contacts) && is_array($contacts) && count($contacts)){ $i = 1;?>
              <?php foreach ($contacts as $key => $row) { 
                    $company = str_replace('[@]', '<br/>',  $row->company_name);        
              ?>
                <tr class="odd gradeX">
                 <td><?php echo ucfirst($company);?></td>
                 <td><?php echo $row->categoryname;?></td>
                 <td><?php echo $row->company_email;?></td>
                 <td><?php echo $row->company_url;?></td>
                 <td><?php echo $row->telephone;?></td>
                 <td><?php echo $row->cnt;?></td>
                 <td><?php echo $row->city;?></td>
                 <td><?php echo ucfirst($row->contact_name);?></td>
                 <td><?php echo $row->ph_no;?></td>
                 <td><?php echo $row->email;?></td>
                <?php $i++; } } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>
<!-- End listing block -->
<?php endif; ?>
  </div>
</div>
