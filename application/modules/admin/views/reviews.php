
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Reviews </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>customers/add" class="icon-plus"> Add User</a> </span>
      </div>
      <div class="widget-body">
     <?php if($this->session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this ->session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this->session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this ->session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> Sl No. </th>  
              <th> Gym Name </th>             
              <th> User Name </th>
              <th> User Type </th>
              <th> Review </th>
              <th> Rating </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php
           if(isset($details)  && count($details)){ $i = 1; ?>
          <?php foreach ($details as $row)  { ?>
            <tr class="odd gradeX">
              <td><?php echo $i;?></td>
               <td> <?php echo ucfirst($row->gymname);?> </td>
               <td> <?php echo ucfirst($row->username);?> </td>
               <td> <?php echo $row->user_type == '2'? 'User':'Supplier';?> </td>
               <td> <?php echo $row->comment;?> </td>
               <td> <?php echo $row->gym_rating;?> </td>           
              <td class="hidden-phone"><div style="clear:both;">
                <?php if($row->is_edit == 0){?>
                  <a class="btn btn-info" id="<?php echo $row->id; ?>" onclick="status_change(<?php echo $row->id; ?>,<?php echo $row->is_edit; ?>,'<?php echo $row->id; ?>')" > <i class="icon-lock"> </i></a>
                <?php } else if($row->status == 1){ ?>
                  <a class="btn btn-success" id="<?php echo $row->id; ?>" onclick="status_change(<?php echo $row->id; ?>,<?php echo $row->is_edit; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-up"> </i></a>
                <?php } ?>
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript">
  function status_change(id,status,aid)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo ADMIN_URL;?>reviews/change_status",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '0'){
                  $('#'+aid).removeClass('btn btn-info').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change('+id+',1,"'+aid+'")');
                }else if(status == '1'){
                  $('#'+aid).removeClass('btn btn-success').addClass('btn btn-info').html('<i class="icon-lock"></i>');
                  $('#'+aid).attr('onclick','status_change('+id+',0,"'+aid+'")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>
