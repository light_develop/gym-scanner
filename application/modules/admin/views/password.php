<?php  
session_start();
 if (!$this->session->userdata('user'))
 {
 	 	redirect('admin/logbtn');
 	}

?>
 <div id="main-content">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
            <div id="theme-change1" class="hidden-phone">
          
   
            </div>
         
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4>
                  <i class="icon-user">
                  </i>
                  Change Password
                </h4>

<div  >

<?php 

	$result=$this->uri->segment(4);
	
	if($result==1 Or $result==2){
		if($result==1){
		echo "<script>alert('Password Updated Sucessfully')</script>";
		}else{
		echo "<script>alert('Previous password do not match')</script>";
		}

	}
?>

<style>
.table th, .table td {
	border-top:0px solid #ddd
}

</style>

<script>

	function validate(){
	
	var originalpass=document.pass_form.Password.value;
	var pass=document.pass_form.new_password.value;
	var re_pass=document.pass_form.re_pass.value;

	if(originalpass==""){
		     $('#err_msg').css('color', 'red').html('Please enter main password');
		}else if(pass==""){
		     $('#err_msg1').css('color', 'red').html('Please enter new password');
		}else if(re_pass==""){
       $('#err_msg2').css('color', 'red').html('Please enter Re password');
		}else{
		if(pass==re_pass){

		document.pass_form.submit();
		}else{

		$('#err_msg2').css('color', 'red').html('Password mismatch please check');
		}

	}

}

</script>

<?php echo form_open_multipart('admin/dashboard/update_password','name="pass_form"'); ?>

              <div class="widget-body">

		<input type="hidden" name="userid" value="h">
             
                <div class="span6" style="width: 100%">
                  <h4>
                   
                    <br /> 
             
                  </h4>
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span2">
                          Password :
                        </td>
                        <td>
                        			
 			<input type="password" name="Password" id="Password"> <span id="err_msg"> </span>
                         
                        </td>
                      </tr>
                      <tr>
                        <td class="span2">
                          New Password :
                        </td>
                        <td>
			 <input type="password" name="new_password" id="new_password"> <span id="err_msg1"> </span>
                         
                        </td>
                      </tr>
			        <tr>
                        <td class="span2">
                          Re-Enter Password :
                        </td>
                        <td >

			
			 <input type="password" name="re_pass" id="re_pass"><span id="err_msg2"> </span>
                         
                        </td>
                      </tr>
                     
			<tr>
                     
                        <td class="span2"></td>
                        
                        <td>
                        <input type="button" class="btn btn-success" name="button" value="Update"  onclick="validate()">
                        </td>
                      </tr>

                    </tbody>
                  </table>
                
                  </div></div>
                </div>

<?php echo form_close(); ?>

</div>
                <div class="span3">
                 
                   
                 
          
                </div>
                <div class="space5">
             
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
