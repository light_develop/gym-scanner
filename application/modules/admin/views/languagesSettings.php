<div id="main-content">
    <div class="">
        <link rel="stylesheet" type="text/css"
              href="<?php echo base_url(); ?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/admin/css/style_datepicker.css"/>
        <script src="<?php echo base_url(); ?>themes/admin/js/framework/main.js"></script>
        <link href="<?php echo base_url(); ?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css"
              rel="stylesheet"/>
    </div>
    <style>
        .container-fluid.admin_language_settings {

        }

        .settings_widget {
            position: relative;

        }

        input#language_input {
            height: 24px;
        }

        #main-content > div.settings_widget.container-fluid > div > div > div:nth-child(3) > table > tbody > tr > td:first-letter {
            text-transform: uppercase;
        }

        .body {
            background-color: white;
        }

        .navbar-inner {
            height: 29px;
            min-height: 33px;
            padding-bottom: 3px;
            border-radius: 0;
        }

        div#main-content {
            display: flex;

        }

        .body {
            background-color: white;

            /* border: 0; */
        }
    </style>
    <div class="settings_widget container-fluid span12">
        <div class="admin_language_settings">
            <div class="body">

                <div class="navbar">
                    <div class="navbar-inner">
                        <h5><i class="icon-reorder"> </i> Language Settings </h5>
                    </div>
                </div>
                <div class="container-fluid">
                    <?php $languages = $this->getLanguages(); ?>
                    <?php echo form_open('admin/languages/addNewLanguage', array("class" => "add-new-language-holder ", "onsubmit" => "addNewLanguage(this); return false;")); ?>
                    <input id="language_input" name="lang_name" validation="notempty" placeholder="Language Name"/>
                    <button id="language_button_settings" class="btn btn-success">Add New Language</button>
                    </form>
                </div>
                <div class="container-fluid">
                    <?php if (is_array($languages) && isset($languages[0])): ?>
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                            <th class="admin_setting_table_header">Language</th>
                            <th class="admin_setting_table_header">Remove Language</th>
                            <th class="admin_setting_table_header">Language Status</th>
                            <th class="admin_setting_table_header">Insert Flag Image</th>
                            <th class="admin_setting_table_header">Flag Image</th>
                            </thead>
                            <?php foreach ($languages as $language): ?>
                                <tr class="lang-<?php echo $language["lang_id"]; ?>">
                                    <td class="language_name"><?php echo $language["lang_name"]; ?></td>
                                    <td class="remove" style="text-align: center">

                                        <a lang_id="<?php echo $language["lang_id"]; ?>"
                                           class="btn btn-small btn-danger"

                                           onclick="changeLanguageSettings(this)"><i
                                                class="icon-remove icon-white"></i>Remove
                                        </a>

                                    </td>
                                    <td class="remove" style="text-align: center">
                                        <a
                                            lang_id="<?php echo $language["lang_id"]; ?>"
                                            status="<?php if ($language["status"]) {
                                                echo 0;
                                            } else {
                                                echo 1;
                                            } ?>" class=" btn btn-small btn-danger"

                                            onclick="changeLanguageSettings(this)">
                                            <?php if ($language["status"]) {
                                                echo "Disable";
                                            } else {
                                                echo "Enable";
                                            } ?></a>
                                    </td>
                                    <td>
                                        <input type="file" onchange="loadFile(this)"/>
                                    </td>
                                    <td>
                                        <image src="<?php if (!empty($language["image"])) {
                                            echo $language["image"];
                                        } ?>"
                                               id="<?php echo $language["lang_id"]; ?>" width="30" height="30"/>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function addNewLanguage(form) {
        if (validate($$1('input[name=lang_name]'))) {
            form.submit();
        }
    }

    function changeLanguageSettings(link) {
        var langId = link.getAttribute("lang_id");
        var status = link.getAttribute("status");
        var statusUrl = (typeof(status) != "undefined") ? "&status=" + status : "";

        location.href = "<?php echo base_url(); ?>admin/languages/changeLanguage?lang_id=" + langId + statusUrl;
    }

    function loadFile(input) {
        var image = input.parentNode.parentNode.querySelector("img");

        function setSrc(event) {
            image.src = event.srcElement.result;

            $.ajax({
                url: "saveIcon",
                type: "POST",
                data: {image: image.src, id: image.id}
            });
        }

        loadImage(input.files[0], setSrc);
        return false;
    }


</script>