<?php  
session_start();
 if (!$this->session->userdata('user'))
 {
 	 	redirect('admin/login');
 	}

?>
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
    <style>
    #timings label{display: inline; padding: 0 15px 0 0;}
    .days div { margin:10px 0;}
    #timings select{margin: 0 10px 0 0;}
    /* .ft1,.ft2,.ft3,.ft4,.ft5,.ft6,.ft7{
      border:1px solid #ccc;
      padding:10px;
      margin-top:5px;
      width:26%;
    
    } */
    
    
    
    .button_accordian{
     margin-left: 7.5%;
   padding: 8px 30px;

color: #797777;
  
  border:none;
   
  border-left: 1px solid #e0dede;
  border-right: 1px solid #e0dede;
  
  


font-size:12px;
font-family:arial, helvetica, sans-serif;
text-decoration:none;
 display:inline-block;
 font-weight:bold; 
 
 background-color: #E6E6E6;
 background-image: -webkit-gradient(linear, left top, left bottom, from(#E6E6E6), to(#CCCCCC));
 background-image: -webkit-linear-gradient(top, #f2f2f2, #fff);
 background-image: -moz-linear-gradient(top, #f2f2f2, #fff);
 background-image: -ms-linear-gradient(top, #f2f2f2, #fff);
 background-image: -o-linear-gradient(top, #f2f2f2, #fff);
 background-image: linear-gradient(to bottom, #fff, #f2f2f2);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E6E6E6, endColorstr=#CCCCCC);
 
 
} 


 .button_accordian:focus{     outline: 0;     outline-style:none;     outline-width:0;    
    
    </style>
<?php
/**
 * [Check the mode of view, if all it will list all restaurants]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') : ?>   
<!-- Start Listing All Cities -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Restaurants </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo site_url()?>administrator/restaurants/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Food Name </th>
              <th> Image </th>
              <th class="hidden-phone"> Address </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php if(isset($restaurants) && is_array($restaurants) && count($restaurants)){ $i = 1;?>
          <?php foreach ($restaurants as $key => $restaurant) { ?>
            <tr class="odd gradeX">
             <td><a href="<?php echo base_url();?>administrator/restaurants/view/<?php echo $restaurant -> id ;?>"><?php echo $restaurant -> name ; ?></a>  </td>
             <td> <img src="<?php echo base_url()?>uploads/thumbs/restaurants/<?php echo $restaurant -> image; ?>" alt="image" /> </td>
             <td> <?php echo $restaurant -> address; ?> </td>
              <td class="hidden-phone" style="width:155px">
              <a href="<?php echo site_url()?>administrator/restaurants/view/<?php echo $restaurant -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo site_url()?>administrator/restaurants/edit/<?php echo $restaurant -> id?>"> <i class="icon-edit"> </i> Edit </a>
               <!--  <a href="#" class="btn mini black"> <i class="icon-trash"> </i> Delete </a> -->
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>


<script>
function change_accord(obj){

$(".acoordian").hide();
$("#"+obj.name).show();
$( ".button_accordian" ).css( "color", "#797777" );
document.getElementById(obj.name+1).style.color=" #979797";


}

</script>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <!--<h4> <i class="icon-reorder"> </i> Add Food </h4>-->
               
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo site_url() ?>administrator/restaurants" class="icon-arrow-left"> </a> </span>
                
                <!--
                <td><input type="button" class="button_accordian" id="profile1" for="ApproveButton" name="profile" value="Profile" onclick="change_accord(this)"></td>
<td><input type="button" name="food" class="button_accordian" id="food1" value="Food" onclick="change_accord(this)"></td>
<td><input type="button" name="timing" class="button_accordian" id="timing1" value="Timing" onclick="change_accord(this)"></td>
<td><input type="button" name="offer" class="button_accordian" id="offer1" value="Offer" onclick="change_accord(this)"></td>
<td><input type="button" name="hotdeals" class="button_accordian" id="hotdeals1" value="Deals Of The Week" onclick="change_accord(this)"></td>
 <script>document.getElementById("profile1").style.color=" #979797";</script>  -->             
             
              </div>
              <div class="widget-body form">
    <form action="<?php echo site_url() ?>administrator/restaurants/add" class="form-horizontal" method="post"  id="restaurant_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                
                <div  class="acoordian" id="profile" ><!--From nameto description-->
                
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="name"/>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> City </label>
                  <div class="controls">
                    <?php $cities[''] = 'Select Cities'; $attributes= 'id="cities" class="span6 required" tabindex="6" required';
                      echo form_dropdown('cities', $cities, '',$attributes);
                    ?>
                  </div>
                </div>
                <br />
                
                     <div class="control-group">
                  <label class="control-label"> Locations </label>
                  <div class="controls" id="locationsd">
                    <?php $attributes= 'id="locations" class="span6 required" multiple="multiple" required';
                    
                    //echo "<pre>";print_r($locations);echo "</pre>";exit;
                      echo form_dropdown('locations[]', $locations, '',$attributes);
                    ?>
                  </div>
                </div>
                
                        <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label"> Address </label>
                  <div class="controls">
                    <textarea name="address" id="" cols="150" rows="4" name="address" placeholder="Please enter address"></textarea>
                  </div>
                </div>
                <br />
              
                
              	     <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    
                     <textarea class="span12 wysihtml5" rows="6" name="description"> </textarea>
                  </div>
                </div>

		</div>
                  
                <!--  <div  class="acoordian" id="food" hidden> 
                <div class="control-group">
                  <label class="control-label"> Delivary Fee </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter price" name="delivary_fee"/>
                  </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Delivary Minimum </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter price" name="delivary_min"/>
                  </div>
                </div>
                <br />
        
                <div class="control-group">
                  <label class="control-label"> Foods </label>
                  <div class="controls">
                    <?php $attributes= 'id="foods" class="chosen span6 required" multiple="multiple" tabindex="9" required';
                      echo form_dropdown('foods[]', $foods, '',$attributes);
                    ?>
                  </div>
                </div>
                
           	</div>
           	
           	<div  class="acoordian" id="timing" hidden>

           

                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <script>
                        function times(x)
                        {
                            if (x == 2) $("#timings").show();
                            else  $("#timings").hide();
                        }
                        
                        function rem(x, y)
                        {
                        
                      
                            if (confirm('Are You Sure To Delete')){
                            //by me
                            fromarray[y][x]=0;
                            toarr[y][x]=0;
                            //end by me
                                $("#ft" + y + x).remove();
                                
                                }
                        }
                        function more(x)
                        {
                            var len = $(".ft" + x).length;
                            var i = 1;
                            var last_id = "";
                            $(".ft" + x).each(function() {
                                if (i == len)
                                    last_id = this.id;
                                i++;
                            });
                            var current_id = last_id.split('ft' + x);
                            var nextid = parseInt(current_id[1]) + 1;
                            if ($("#frm" + x + current_id[1]).val() != "" && $("#to" + x + current_id[1]).val() != "")
                            {
                                var create = '<div id="ft' + x + nextid + '" class="ft' + x + '"><span><label>From</label><select id="frm' + x + nextid + '" class="fx' + x + '" onchange="sel_from(' + nextid + ',this.value,' + x + ')" name="frmx[' + x + '][]"><option value="">Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select class="tx' + x + '" id="to' + x + nextid + '" name="to[' + x + '][]" onchange="to(' + nextid + ',this.value,' + x + ')" disabled><option value="">Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span><i class="icon-remove" onclick="rem(' + nextid + ',' + x + ')"> </i></div>';
                                $("#ft" + x + current_id[1]).after(create.trim());
                            }
                            else {
                                if ($("#frm" + x + current_id[1]).val() == "") {
                                    alert('Please select from time');
                                    $("#frm" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                                if ($("#to" + x + current_id[1]).val() == "") {
                                    alert('Please select to time');
                                    $("#to" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                            }


                        }
                        function dayselect(x)
                        {
                            //alert(document.getElementById("days"+x).checked);
                            if (document.getElementById("days" + x).checked)
                            {
                                $("#adding" + x).hide();
                                $(".ft" + x).hide();
                            }
                            else
                            {
                                $("#adding" + x).show();
                                $(".ft" + x).show();
                            }
                        }
                        
                        //by me
                    	var fromarray = [];
                        fromarray[1] = [];
                        fromarray[2] = [];
                        fromarray[3] = [];
                        fromarray[4] = [];
                        fromarray[5] = [];
                        fromarray[6] = [];
                        fromarray[7] = [];
                        
                        
                        //check value is initilized to check whether to disable "to" attribute or not
                        var checkValue=0;
                            
  
                        //
                        
                        function sel_from(x, y, z)
                        {
                            if (y != '')
                            {
                                var fm = $("#frm" + z + x).val();
                                var tr = $("#to" + z + x).val();
                               
                               //by me
                               fromarray[z][x]=y;
                               
				

                               for(var i=1;i<=x;i++){
                             
                               
                              // alert(fromarray[z][i]);
                              
                              //alert(i);
                              
                            //alert(fromarray[z][i]);
                              
                             //alert(toarr[z][i]);
                               
                               if(y>fromarray[z][i] && y<=toarr[z][i]){
                               
                               alert("The selected time is already between above time");
                               
                               fromarray[z][i]=0;
                               
                                   var a=(z*10)+x;
                        var b="frm"+a;
                       // alert(b);
                     // var selectTags = document.getelementbyid(b);
                     
                     //alert("no hello");

				document.getElementById(b).selectedIndex = 0;
				 $("#to" + z + x).attr('disabled');
				checkValue=1;
                               
                               }
                               
                               
                               }
                               
                               
                               if(checkValue==0){
                               //alert("hello");
                               $("#to" + z + x).removeAttr('disabled');
                               
                               
                               
                               }else{
                              
                               checkValue=0;
                               }
                               
                 		//alert(fromarray[z][x]);
                               /// end by me
                               
                                var k = 1;
                                var l = 1;
                                var found = "";
                                $("select#frm" + z + x + " option").each(function() {
                                    if (this.value == fm)
                                        found = k;

                                    k++;
                                });

                                $("select#to" + z + x + " option").each(function() {
                                    if (l <= found)
                                    {
                                    }
                                    l++;
                                });
                               // $("#to" + z + x).removeAttr('disabled');
                            }
                        }
                        
                        
                        
                        //by me
                          var toarr = [];
                        toarr[1] = [];
                        toarr[2] = [];
                        toarr[3] = [];
                        toarr[4] = [];
                        toarr[5] = [];
                        toarr[6] = [];
                        toarr[7] = [];
                        
                         function to(x, y, z)
                        {
                        
                        toarr[z][x]=y;
                        
                        //alert( fromarray[x]);
                        
                        if(fromarray[z][x]>y){
   
                        alert("Please enter time greater than from");
                        var a=(z*10)+x;
                        var b="to"+a;
                       // alert(b);
                     // var selectTags = document.getelementbyid(b);
 			document.getElementById(b).selectedIndex = 0;
                        
                        }else{
                        
                        
                        
                        
                        
                        }
                        
                        }
                        ///end by me
                        

                      </script>
                      <label><input type="radio" name="timings" value="1" checked onclick="times(1)" />All Days</label>
                      <label><input type="radio" name="timings" value="2" id="sel" onclick="times(2)" />Selected</label>
                      <div id="timings" style="display:none">

                                    <div class="days" id="day1">
                                        <label><input type="checkbox" value="1" name="day_select[]" id="days1" onclick="dayselect(1)" title="click here to select all day" /><input type="text" name="day[1]" value="Monday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(1)" id="adding1"> </i>
                                        <div id="ft11" class="ft1"><span><label>From</label><select id="frm11" name="frmx[1][]" class="fx1" onchange="sel_from(1, this.value, 1)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to11" class="tx1" name="to[1][]" onchange="to(1, this.value, 1)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day2">
                                        <label><input type="checkbox" value="2" id="days2" onclick="dayselect(2)"  name="day_select[]" title="click here to select all day" /><input type="text" name="day[2]" value="Tuesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(2)" id="adding2"> </i> 
                                        <div id="ft21" class="ft2"><span><label>From</label><select id="frm21" name="frmx[2][]" class="fx2"  onchange="sel_from(1, this.value, 2)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to21"  class="tx2" name="to[2][]" onchange="to(1, this.value, 2)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day3">
                                        <label><input type="checkbox" value="3" id="days3" onclick="dayselect(3)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[3]" value="Wednesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(3)" id="adding3"> </i>
                                        <div id="ft31" class="ft3"><span><label>From</label><select class="fx3" id="frm31" name="frmx[3][]"  onchange="sel_from(1, this.value, 3)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to31" class="tx3" onchange="to(1, this.value, 3)" name="to[3][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day4">
                                        <label><input type="checkbox" value="4" id="days4" onclick="dayselect(4)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[4]" value="Thursday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(4)" id="adding4"> </i>
                                        <div id="ft41" class="ft4"><span><label>From</label><select class="fx4" id="frm41" name="frmx[4][]"  onchange="sel_from(1, this.value, 4)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to41" class="tx4"  name="to[4][]" onchange="to(1, this.value, 4)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day5">
                                        <label><input type="checkbox" value="5" id="days5" onclick="dayselect(5)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[5]" value="Friday" readonly class="day_input"/></label><i class="icon-plus" onclick="more(5)" id="adding5"> </i>
                                        <div id="ft51" class="ft5"><span><label>From</label><select class="fx5" id="frm51" name="frmx[5][]"  onchange="sel_from(1, this.value, 5)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to51" class="tx5" name="to[5][]" onchange="to(1, this.value, 5)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day6">
                                        <label><input type="checkbox" value="6" id="days6" onclick="dayselect(6)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[6]" value="Saturday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(6)" id="adding6"> </i>
                                        <div id="ft61" class="ft6"><span><label>From</label><select id="frm61" class="fx6" name="frmx[6][]"  onchange="sel_from(1, this.value, 6)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to61" class="tx6" name="to[6][]" onchange="to(1, this.value, 6)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day7">
                                        <label><input type="checkbox" value="7" id="days7" onclick="dayselect(7)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[7]" value="Sunday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(7)" id="adding7"> </i>
                                        <div id="ft71" class="ft7"><span><label>From</label><select id="frm71" class="fx7" name="frmx[7][]"  onchange="sel_from(1, this.value, 7)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to71" class="tx7" name="to[7][]" onchange="to(1, this.value, 7)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>


                                    </div>
                  </div>
                </div>
                </div>

	</div>

                <div  class="acoordian" id="offer" hidden>

		<h1>Offer</h1>

		</div>


		<div  class="acoordian" id="hotdeals" hidden >

		
		
		 <div class="control-group">
                  <label class="control-label"> Title </label>
                  <div class="controls">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="title"/>
                </div>
                </div>
                <br />
                
                
                	 <!--<div class="control-group">
                  <label class="control-label"> Start Date </label>
                  <div class="controls">
          
        

</head>
<body>
 
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-datepicker/css/datepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/compiled/timepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-colorpicker/css/colorpicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/daterangepicker.css" />
    

  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/daterangepicker.js"></script>

 <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
 
  <div class="control-group">
                  <label class="control-label"> Start and end date </label>
                  <div class="controls">
                  
                  <div class="input-prepend">
             <span class="add-on"><i class="icon-calendar"></i></span>
             <input type="text" class="m-ctrl-medium date-range" name="start_end_date" />
            </div>
                  
                  
                  
                </div>
                </div>
                <br />
                
                 <div class="control-group">
                  <label class="control-label">  </label>
                  <div class="controls">
	           </div>
                </div>
               <br />

		 <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image_deal" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
		
		
		     <div class="control-group">
                  <label class="control-label"> Description </label>

                  <div class="controls">
                    <textarea  rows="6" name="deal_description"> </textarea>
                  </div>
                </div>

		</div>-->
              
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="saverestaurant"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>


<script>
function change_accord(obj){

$(".acoordian").hide();
$("#"+obj.name).show();
$( ".button_accordian" ).css( "color", "#797777" );
document.getElementById(obj.name+1).style.color=" #979797";


}

</script>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <!--<h4> <i class="icon-reorder"> </i> Edit Food </h4>-->
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo site_url() ?>administrator/restaurants" class="icon-arrow-left"> </a> </span>
                
                 <!--<td><input type="button" class="button_accordian" id="profile1" for="ApproveButton" name="profile" value="Profile" onclick="change_accord(this)"></td>
<td><input type="button" name="food" class="button_accordian" id="food1" value="Food" onclick="change_accord(this)"></td>
<td><input type="button" name="timing" class="button_accordian" id="timing1" value="Timing" onclick="change_accord(this)"></td>
<td><input type="button" name="offer" class="button_accordian" id="offer1" value="Offer" onclick="change_accord(this)"></td>
<td><input type="button" name="hotdeals" class="button_accordian" id="hotdeals1" value="hotdeals" onclick="change_accord(this)"></td>-->
 <script>document.getElementById("profile1").style.color=" #979797";</script>      
              </div>
              <div class="widget-body form">
                <form action="<?php echo site_url() ?>administrator/restaurants/edit" class="form-horizontal" method="post" id="restaurant_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                   <input type="hidden" name="id" value="<?php echo $restaurant -> name;?>">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="name" value="<?php echo $restaurant -> name ?>" />
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> City </label>
           
                              <div class="controls">
                    <?php //$cities[''] = 'Select Cities'; $attributes= 'id="cities" class="span6 required" tabindex="1" required';
                     // echo form_dropdown('cities', $cities, $city,$attributes);
                    ?>
                    
                      
                    <?php $cities[''] = 'Select Cities'; $attributes= 'id="cities" class="span6 required" tabindex="6" required';
                   // echo "<pre>";print_r($city['id']);echo "</pre>";exit;
                      echo form_dropdown('cities', $cities, $city['id'],$attributes);
                    ?>
                
                  </div>
                
                <br />
                    <div class="control-group">
                  <label class="control-label"> Address </label>
                  <div class="controls">
                  
                   <textarea class="span6 required number1"  placeholder="Please enter price" name="address" /><?php echo $restaurant -> address;?></textarea>
                </div>
                
                <br />
          
                
                
                <br />
  
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    <img src="<?php echo base_url();?>uploads/thumbs/restaurants/<?php echo $restaurant->image?>" width="90" alt="image">
                    </div>
                  </div>
                </div>
                
                
                <input type="hidden" name="editid" value="<?php echo $this->uri->segment(4)?>">
               <!-- <div class="control-group">
                  <label class="control-label"> Foods </label>
                  <div class="controls">
                    <?php $attributes= 'id="foods" name="foods" class="chosen span6 required" multiple="multiple" tabindex="6" required';
                    echo form_dropdown('foods[]', $foods, $restaurant_select_categories,$attributes);
                    ?>
                  </div>
                </div>-->
                
                 <div class="control-group">
                  <label class="control-label"> Locations </label>
                  <div class="controls">
                    <?php $attributes= 'id="locations" class="span6 required" multiple="multiple" required';
                   // echo "<pre>";print_r($restaurant_locations);echo "</pre>";
                    echo form_dropdown('locations[]', $all_locations, $restaurant_locations,$attributes);
                    ?>
                  </div>
                </div>
                
                
                
                               <!-- <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <script>
                        function times(x)
                        {
                            if (x == 2) $("#timings").show();
                            else  $("#timings").hide();
                        }
                        
                        function rem(x, y)
                        {
                            if (confirm('Are You Sure To Delete'))
                                $("#ft" + y + x).remove();
                        }
                        function more(x)
                        {
                            var len = $(".ft" + x).length;
                            var i = 1;
                            var last_id = "";
                            $(".ft" + x).each(function() {
                                if (i == len)
                                    last_id = this.id;
                                i++;
                            });
                            var current_id = last_id.split('ft' + x);
                            var nextid = parseInt(current_id[1]) + 1;
                            if ($("#frm" + x + current_id[1]).val() != "" && $("#to" + x + current_id[1]).val() != "")
                            {
                                var create = '<div id="ft' + x + nextid + '" class="ft' + x + '"><span><label>From</label><select id="frm' + x + nextid + '" class="fx' + x + '" onchange="sel_from(' + nextid + ',this.value,' + x + ')" name="frmx[' + x + '][]"><option value="">Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select class="tx' + x + '" id="to' + x + nextid + '" name="to[' + x + '][]" disabled><option value="">Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span><i class="icon-remove" onclick="rem(' + nextid + ',' + x + ')"> </i></div>';

                                $("#ft" + x + current_id[1]).after(create.trim());
                            }
                            else {
                                if ($("#frm" + x + current_id[1]).val() == "") {
                                    alert('Please select from time');
                                    $("#frm" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                                if ($("#to" + x + current_id[1]).val() == "") {
                                    alert('Please select to time');
                                    $("#to" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                            }


                        }
                        function dayselect(x)
                        {
                            //alert(document.getElementById("days"+x).checked);
                            if (document.getElementById("days" + x).checked)
                            {
                                $("#adding" + x).hide();
                                $(".ft" + x).hide();
                            }
                            else
                            {
                                $("#adding" + x).show();
                                $(".ft" + x).show();
                            }
                        }
                        function sel_from(x, y, z)
                        {
                            if (y != '')
                            {
                                var fm = $("#frm" + z + x).val();
                                var tr = $("#to" + z + x).val();
                               
                                var k = 1;
                                var l = 1;
                                var found = "";
                                $("select#frm" + z + x + " option").each(function() {
                                    if (this.value == fm)
                                        found = k;

                                    k++;
                                });

                                $("select#to" + z + x + " option").each(function() {
                                    if (l <= found)
                                    {
                                    }
                                    l++;
                                });
                                $("#to" + z + x).removeAttr('disabled');
                            }
                        }

                      </script>
                     <label><input type="radio" name="timings" value="1" checked onclick="times(1)" />All Days</label>
                      <label><input type="radio" name="timings" value="2" id="sel" onclick="times(2)" />Selected</label>
                      <div id="timings" style="display:none">

                                    <div class="days" id="day1">
                                        <label><input type="checkbox" value="1" name="day_select[]" id="days1" onclick="dayselect(1)" title="click here to select all day" /><input type="text" name="day[1]" value="Monday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(1)" id="adding1"> </i>
                                        <div id="ft11" class="ft1"><span><label>From</label><select id="frm11" name="frmx[1][]" class="fx1" onchange="sel_from(1, this.value, 1)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to11" class="tx1" name="to[1][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day2">
                                        <label><input type="checkbox" value="2" id="days2" onclick="dayselect(2)"  name="day_select[]" title="click here to select all day" /><input type="text" name="day[2]" value="Tuesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(2)" id="adding2"> </i> 
                                        <div id="ft21" class="ft2"><span><label>From</label><select id="frm21" name="frmx[2][]" class="fx2"  onchange="sel_from(1, this.value, 2)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to21" class="tx2" name="to[2][]"  disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day3">
                                        <label><input type="checkbox" value="3" id="days3" onclick="dayselect(3)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[3]" value="Wednesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(3)" id="adding3"> </i>

                                        <div id="ft31" class="ft3"><span><label>From</label><select class="fx3" id="frm31" name="frmx[3][]"  onchange="sel_from(1, this.value, 3)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to31" class="tx3" name="to[3][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day4">
                                        <label><input type="checkbox" value="4" id="days4" onclick="dayselect(4)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[4]" value="Thursday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(4)" id="adding4"> </i>
                                        <div id="ft41" class="ft4"><span><label>From</label><select class="fx4" id="frm41" name="frmx[4][]"  onchange="sel_from(1, this.value, 4)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to41" class="tx4" name="to[4][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day5">
                                        <label><input type="checkbox" value="5" id="days5" onclick="dayselect(5)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[5]" value="Friday" readonly class="day_input"/></label><i class="icon-plus" onclick="more(5)" id="adding5"> </i>
                                        <div id="ft51" class="ft5"><span><label>From</label><select class="fx5" id="frm51" name="frmx[5][]"  onchange="sel_from(1, this.value, 5)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to51" class="tx5" name="to[5][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>

                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day6">
                                        <label><input type="checkbox" value="6" id="days6" onclick="dayselect(6)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[6]" value="Saturday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(6)" id="adding6"> </i>
                                        <div id="ft61" class="ft6"><span><label>From</label><select id="frm61" class="fx6" name="frmx[6][]"  onchange="sel_from(1, this.value, 6)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to61" class="tx6" name="to[6][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day7">
                                        <label><input type="checkbox" value="7" id="days7" onclick="dayselect(7)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[7]" value="Sunday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(7)" id="adding7"> </i>
                                        <div id="ft71" class="ft7"><span><label>From</label><select id="frm71" class="fx7" name="frmx[7][]"  onchange="sel_from(1, this.value, 7)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to71" class="tx7" name="to[7][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>


                                    </div>
                  </div>
                </div>
                </div>-->
                
                
                
                
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"><?php echo $restaurant -> description ?> </textarea>
                  </div>
                </div>

		
                
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="saverestaurant"> Update </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'): ?>



<script>
/*function change_accord(obj){

$(".acoordian").hide();
$("#"+obj.name).show();
$( ".button_accordian" ).css( "color", "#797777" );
document.getElementById(obj.name+1).style.color=" #979797";
}*/

</script>


<script>

function edit(name,id,identity){



	//alert(identity);

	/* $('#oneditprofile').hide();
	
	
    $('#name').val(name);

      switch (identity) {
      case 'name_res':
         $('#divname').show();
        break;
      case 'city_res':
         $('#divcity').show('slowly');
        break;
      case $(this).hasClass('class3'):
        // do stuff
        break;
    }*/
    
    

//alert(name);


}


</script>
<script>
  $(document).ready(function(){
    $('#save_city').click(function(){
    var name = $.trim($('#name').val());
    if(name == '') $('#name').css('border-color','red').attr('placeholder','please enter restaurant name');
    else if($( "#locations option:selected" ).text() == '') $('#locations').css('border-color','red');
    else if($( "#locations" ).text() == '') $('#locations').css('border-color','red');
    else{
    
    alert($( "#locations option:selected" ).text());
    
    }
   //alert("hi");
        });     
      //  $('#add_city_name').val('');

    });


</script>


<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <!--<h4> <i class="icon-reorder"> </i> View Restaurant </h4>-->
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo site_url() ?>administrator/restaurants" class="icon-arrow-left"> </a> </span>
             
             <!-- <td><input type="button" class="button_accordian" id="profile1" for="ApproveButton" name="profile" value="Profile" onclick="change_accord(this)"></td>
<td><input type="button" name="food" class="button_accordian" id="food1" value="Food" onclick="change_accord(this)"></td>
<td><input type="button" name="timing" class="button_accordian" id="timing1" value="Timing" onclick="change_accord(this)"></td>
<td><input type="button" name="offer" class="button_accordian" id="offer1" value="Offer" onclick="change_accord(this)"></td>
<td><input type="button" name="hotdeals" class="button_accordian" id="hotdeals1" value="Deals Of The Week" onclick="change_accord(this)"></td>
 <script>document.getElementById("profile1").style.color=" #979797";</script> -->         
             
              </div>
              
 
              
                <div  class="acoordian" id="profile" ><!--From name to description-->
              
              <div class="editdata" id="divname" hidden>
              
              
                    <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Name : </td>
                        
                        
                        <td> <input class="span6 required" id="name" type="text" placeholder="Please enter name" name="name" value="<?php echo $restaurant -> name;?>"/></td>
                      <td class="hidden-phone" style="width:155px">
                     
                       
                     
                      </td>
                        
                      </tr>
                      <tr>
                        <td class="span3"> City : </td>
                        <td>  <?php
                    
                    //echo "<pre>";print_r($allcities);echo "</pre>";
                     $allcities[''] = 'Select Cities'; $attributes= 'id="cities" class="span6 required" tabindex="6" required';

                      echo form_dropdown('cities', $allcities, $city['id']);
                    ?></td>
                           <td class="hidden-phone" style="width:155px">
                     
              
                     
                      </td>
                      </tr>
                      
                                   <tr>
                        <td class="span3"> Locations : </td>
                        <td>                        
                            <?php $attributes= 'id="locations" class="span6 required" multiple="multiple" required';
                    
                    //echo "<pre>";print_r($locations);echo "</pre>";exit;
                      echo form_dropdown('locations[]', $respective_all_location, $restaurant_locations_selected,$attributes);
                    ?>
                        </td>
                        
                           <td class="hidden-phone" style="width:155px"> </td>
                        
                      </tr>
                       <tr>
                         <?php if($restaurant -> image) {?>
                     
                        <td class="span3"> Image : </td>
                        <td> 
                         <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    <img src="<?php echo base_url();?>uploads/thumbs/restaurants/<?php echo $restaurant->image?>" width="90" alt="image">
                    </div>
                  </div>
                        
                        
                        </td>
                          
                      
                   
                      <?php } ?>
                      
                       <td class="hidden-phone" style="width:155px">
                     
                       
                     
                      </td>
                      </tr>
                      
                      
                   
                      
                      
                      
                      
                      <tr>
                        <td class="span3"> Address : </td>
                        <td>  <textarea class="span6 required number1"  placeholder="Please enter price" name="address" /><?php echo $restaurant -> address;?></textarea> </td>
                         <td class="hidden-phone" style="width:155px">
                     
                      
                     
                      </td>
                      </tr>
                      
                      
                         <tr>
                        <td class="span3"> Description : </td>
                        <td><textarea class="span12 wysihtml5" rows="6" name="description"><?php echo $restaurant -> description ?> </textarea></td>
                         <td class="hidden-phone" style="width:155px">
 
                      </td>
                      </tr>
                      <br>
                      <br>
                      
                      
                          <tr>
                        <td class="span3"></td>
                        <td> <button type="submit" class="btn blue" id = "save_city"> <i class="icon-ok"> </i> Save </button></td>
                         <td class="hidden-phone" style="width:155px">
 
                      </td>
                      </tr>
                      
                     
                         
                       </table >
                    </tbody>
                    </div>
              
              
              
                <!-- <div class="widget-body" >
                <div id="add_result">
                  <?php if($this->session->flashdata('success')!='') echo $this->session->flashdata('success');?>
                </div>
                <form action="#" class="form-horizontal" />
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="text" placeholder="Enter Restaurant Name" class="input-long required" id="name" />
                    
                  </div>
                </form>
              </div>
            
              
              
              
              
              
              
                      
                 <div class="widget-body" >
                <div id="add_result">
                  <?php if($this->session->flashdata('success')!='') echo $this->session->flashdata('success');?>
                </div>
                <form action="#" class="form-horizontal" />
                  <label class="control-label"> City </label>
                  <div class="controls">
                    <?php
                    
                    //echo "<pre>";print_r($allcities);echo "</pre>";
                     $allcities[''] = 'Select Cities'; $attributes= 'id="cities" class="span6 required" tabindex="6" required';
                      echo form_dropdown('cities', $allcities, $city['id']);
                    ?>
                    
                     
                  </div>
               
              </div>
              
              
              
              
              
                   <div class="control-group">
                  <label class="control-label"> Locations </label>
                  <div class="controls" id="locationsd">
                    <?php $attributes= 'id="locations" class="span6 required" multiple="multiple" required';
                    
                    //echo "<pre>";print_r($locations);echo "</pre>";exit;
                      echo form_dropdown('locations[]', $respective_all_location, $restaurant_locations_selected,$attributes);
                    ?>
                  </div>
                </div>
              
              
              
                
                    
                      
                        </form>-->
                 
                
           
              
              </div>

              
        <div id="oneditprofile" >  
        
        
         
              
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Name : </td>
                        <td> <?php echo $restaurant -> name;?></td>
                      <td class="hidden-phone" style="width:155px">
                     
                        <!--<a class="btn mini purple editcity" onclick="edit('<?php echo $restaurant -> name?>','<?php echo $restaurant -> id?>','name_res');"> <i class="icon-edit"> </i> Edit </a>-->
                     
                      </td>
                        
                      </tr>
                      <tr>
                        <td class="span3"> City : </td>
                        <td> <?php echo $city['name'];?></td>
                           <td class="hidden-phone" style="width:155px">
                     
              
                     
                      </td>
                      </tr>
                      
                                   <tr>
                        <td class="span3"> Locations : </td>
                        <td>                        
                          <?php
                          //echo "<pre>";print_r($restaurant_locations);echo "</pre>";
             
                            if(is_array($restaurant_locations) && count($restaurant_locations)>0)
                            {
                              $i=1;
                          
                              foreach ($restaurant_locations as $value) {
                                 echo ($i==1) ? '': ' , '; echo $value['name'];
                                //echo '<pre>';print_r($value['name']);echo '</pre>';
                                $i++;
                              }
                            }
                            ?>
                        </td>
                        
                           <td class="hidden-phone" style="width:155px">
                     
                     
                     
                      </td>
                        
                      </tr>
                       <tr>
                         <?php if($restaurant -> image) {?>
                     
                        <td class="span3"> Image : </td>
                        <td> <img src="<?php echo base_url(); ?>uploads/thumbs/restaurants/<?php echo $restaurant -> image; ?>" alt=""></td>
                          
                      
                   
                      <?php } ?>
                      
                       <td class="hidden-phone" style="width:155px">
                     
                       
                     
                      </td>
                      </tr>
                      
                      
                 
                      
                      
                      
                      <tr>
                        <td class="span3"> Address : </td>
                        <td> <?php echo $restaurant -> address;?></td>
                         <td class="hidden-phone" style="width:155px">
                     
                      
                     
                      </td>
                      </tr>
                      
                      
                         <tr>
                        <td class="span3"> Description : </td>
                        <td> <?php echo $restaurant -> description;?></td>
                         <td class="hidden-phone" style="width:155px">
                         </td>
                      
                      </tr>
                      <br>
                      <br>
                      
                         
                       </table >
                    </tbody>
                      </div> <!--till here accord-->
                      </div>  <!--div for hiding on edit click-->
                      </div>
                        <div  class="acoordian" id="food" hidden> <!--from delivary fee to food-->
                          <div class="widget-body form">
                      <table class="table table-borderless">
                    <tbody>
                      
                  
                      
                         <tr>
                        <td class="span3"> Foods : </td>
                        <td>           <?php
            //  echo '<pre>';print_r($restaurant_select_categories);echo '</pre>';
                            if(is_array($restaurant_select_categories) && count($restaurant_select_categories)>0)
                            {
                              $i=1;
                              //echo "hello";
                              foreach ($restaurant_select_categories as $value) {
                                 echo ($i==1) ? '': ' , '; echo $value['name'];
                                //echo '<pre>';print_r($value['name']);echo '</pre>';
                                $i++;
                              }
                            }
                            ?></td>
                             <td class="hidden-phone" style="width:155px">
                     
                       
                     
                      </td>
                      </tr>
                      
         
                      
                                   
                      
                   
                   
                    </tbody>
                  </table>
                  
                  </div>
                  
                  
                   
                  
                  </div> <!--accord end from delivery fee-->
                  
                  
                  
                  
                  <div  class="acoordian" id="timing" hidden>

           <div class="widget-body form">

                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <script>
                        function times(x)
                        {
                            if (x == 2) $("#timings").show();
                            else  $("#timings").hide();
                        }
                        
                        function rem(x, y)
                        {
                        
                      
                            if (confirm('Are You Sure To Delete')){
                            //by me
                            fromarray[y][x]=0;
                            toarr[y][x]=0;
                            //end by me
                                $("#ft" + y + x).remove();
                                
                                }
                        }
                        function more(x)
                        {
                            var len = $(".ft" + x).length;
                            var i = 1;
                            var last_id = "";
                            $(".ft" + x).each(function() {
                                if (i == len)
                                    last_id = this.id;
                                i++;
                            });
                            var current_id = last_id.split('ft' + x);
                            var nextid = parseInt(current_id[1]) + 1;
                            if ($("#frm" + x + current_id[1]).val() != "" && $("#to" + x + current_id[1]).val() != "")
                            {
                                var create = '<div id="ft' + x + nextid + '" class="ft' + x + '"><span><label>From</label><select id="frm' + x + nextid + '" class="fx' + x + '" onchange="sel_from(' + nextid + ',this.value,' + x + ')" name="frmx[' + x + '][]"><option value="">Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select class="tx' + x + '" id="to' + x + nextid + '" name="to[' + x + '][]" onchange="to(' + nextid + ',this.value,' + x + ')" disabled><option value="">Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span><i class="icon-remove" onclick="rem(' + nextid + ',' + x + ')"> </i></div>';
                                $("#ft" + x + current_id[1]).after(create.trim());
                            }
                            else {
                                if ($("#frm" + x + current_id[1]).val() == "") {
                                    alert('Please select from time');
                                    $("#frm" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                                if ($("#to" + x + current_id[1]).val() == "") {
                                    alert('Please select to time');
                                    $("#to" + x + current_id[1]).css('border', '1px red solid');
                                    return false;
                                }
                            }


                        }
                        function dayselect(x)
                        {
                            //alert(document.getElementById("days"+x).checked);
                            if (document.getElementById("days" + x).checked)
                            {
                                $("#adding" + x).hide();
                                $(".ft" + x).hide();
                            }
                            else
                            {
                                $("#adding" + x).show();
                                $(".ft" + x).show();
                            }
                        }
                        
                        //by me
                    	var fromarray = [];
                        fromarray[1] = [];
                        fromarray[2] = [];
                        fromarray[3] = [];
                        fromarray[4] = [];
                        fromarray[5] = [];
                        fromarray[6] = [];
                        fromarray[7] = [];
                        
                        
                        //check value is initilized to check whether to disable "to" attribute or not
                        var checkValue=0;
                            
  
                        //
                        
                        function sel_from(x, y, z)
                        {
                            if (y != '')
                            {
                                var fm = $("#frm" + z + x).val();
                                var tr = $("#to" + z + x).val();
                               
                               //by me
                               fromarray[z][x]=y;
                               
				

                               for(var i=1;i<=x;i++){
                             
                               
                              // alert(fromarray[z][i]);
                              
                              //alert(i);
                              
                            //alert(fromarray[z][i]);
                              
                             //alert(toarr[z][i]);
                               
                               if(y>fromarray[z][i] && y<=toarr[z][i]){
                               
                               alert("The selected time is already between above time");
                               
                               fromarray[z][i]=0;
                               
                                   var a=(z*10)+x;
                        var b="frm"+a;
                       // alert(b);
                     // var selectTags = document.getelementbyid(b);
                     
                     //alert("no hello");

				document.getElementById(b).selectedIndex = 0;
				 $("#to" + z + x).attr('disabled');
				checkValue=1;
                               
                               }
                               
                               
                               }
                               
                               
                               if(checkValue==0){
                               //alert("hello");
                               $("#to" + z + x).removeAttr('disabled');
                               
                               
                               
                               }else{
                              
                               checkValue=0;
                               }
                               
                 		//alert(fromarray[z][x]);
                               /// end by me
                               
                                var k = 1;
                                var l = 1;
                                var found = "";
                                $("select#frm" + z + x + " option").each(function() {
                                    if (this.value == fm)
                                        found = k;

                                    k++;
                                });

                                $("select#to" + z + x + " option").each(function() {

                                    if (l <= found)
                                    {
                                    }
                                    l++;
                                });
                               // $("#to" + z + x).removeAttr('disabled');
                            }
                        }
                        
                        
                        
                        //by me
                          var toarr = [];
                        toarr[1] = [];
                        toarr[2] = [];
                        toarr[3] = [];
                        toarr[4] = [];
                        toarr[5] = [];
                        toarr[6] = [];
                        toarr[7] = [];
                        
                         function to(x, y, z)
                        {
                        
                        toarr[z][x]=y;
                        
                        //alert( fromarray[x]);
                        
                        if(fromarray[z][x]>y){
   
                        alert("Please enter time greater than from");
                        var a=(z*10)+x;
                        var b="to"+a;
                       // alert(b);
                     // var selectTags = document.getelementbyid(b);
 			document.getElementById(b).selectedIndex = 0;
                        
                        }else{
                        
                        
                        
                        
                        
                        }
                        
                        }
                        ///end by me
                        

                      </script>
                      <label><input type="radio" name="timings" value="1" checked onclick="times(1)" />All Days</label>
                      <label><input type="radio" name="timings" value="2" id="sel" onclick="times(2)" />Selected</label>
                      <div id="timings" style="display:none">

                                    <div class="days" id="day1">
                                        <label><input type="checkbox" value="1" name="day_select[]" id="days1" onclick="dayselect(1)" title="click here to select all day" /><input type="text" name="day[1]" value="Monday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(1)" id="adding1"> </i>
                                        <div id="ft11" class="ft1"><span><label>From</label><select id="frm11" name="frmx[1][]" class="fx1" onchange="sel_from(1, this.value, 1)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to11" class="tx1" name="to[1][]" onchange="to(1, this.value, 1)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day2">
                                        <label><input type="checkbox" value="2" id="days2" onclick="dayselect(2)"  name="day_select[]" title="click here to select all day" /><input type="text" name="day[2]" value="Tuesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(2)" id="adding2"> </i> 
                                        <div id="ft21" class="ft2"><span><label>From</label><select id="frm21" name="frmx[2][]" class="fx2"  onchange="sel_from(1, this.value, 2)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to21"  class="tx2" name="to[2][]" onchange="to(1, this.value, 2)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>

                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day3">
                                        <label><input type="checkbox" value="3" id="days3" onclick="dayselect(3)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[3]" value="Wednesday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(3)" id="adding3"> </i>
                                        <div id="ft31" class="ft3"><span><label>From</label><select class="fx3" id="frm31" name="frmx[3][]"  onchange="sel_from(1, this.value, 3)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to31" class="tx3" onchange="to(1, this.value, 3)" name="to[3][]" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day4">
                                        <label><input type="checkbox" value="4" id="days4" onclick="dayselect(4)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[4]" value="Thursday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(4)" id="adding4"> </i>
                                        <div id="ft41" class="ft4"><span><label>From</label><select class="fx4" id="frm41" name="frmx[4][]"  onchange="sel_from(1, this.value, 4)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to41" class="tx4"  name="to[4][]" onchange="to(1, this.value, 4)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day5">
                                        <label><input type="checkbox" value="5" id="days5" onclick="dayselect(5)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[5]" value="Friday" readonly class="day_input"/></label><i class="icon-plus" onclick="more(5)" id="adding5"> </i>
                                        <div id="ft51" class="ft5"><span><label>From</label><select class="fx5" id="frm51" name="frmx[5][]"  onchange="sel_from(1, this.value, 5)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to51" class="tx5" name="to[5][]" onchange="to(1, this.value, 5)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>

                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day6">
                                        <label><input type="checkbox" value="6" id="days6" onclick="dayselect(6)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[6]" value="Saturday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(6)" id="adding6"> </i>
                                        <div id="ft61" class="ft6"><span><label>From</label><select id="frm61" class="fx6" name="frmx[6][]"  onchange="sel_from(1, this.value, 6)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to61" class="tx6" name="to[6][]" onchange="to(1, this.value, 6)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>
                                    </div>
                                    <hr />
                                    <div style="clear:both"></div>
                                    <div class="days" id="day7">
                                        <label><input type="checkbox" value="7" id="days7" onclick="dayselect(7)" name="day_select[]" title="click here to select all day" /><input type="text" name="day[7]" value="Sunday" readonly class="day_input" /></label><i class="icon-plus" onclick="more(7)" id="adding7"> </i>
                                        <div id="ft71" class="ft7"><span><label>From</label><select id="frm71" class="fx7" name="frmx[7][]"  onchange="sel_from(1, this.value, 7)"><option value=''>Select From Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option></select></span><span><label>To</label><select id="to71" class="tx7" name="to[7][]" onchange="to(1, this.value, 7)" disabled><option value=''>Select To Time</option><option value="00">00:00AM</option><option value="01">01:00AM</option><option value="02">02:00AM</option><option value="03">03:00AM</option><option value="04">04:00AM</option><option value="05">05:00AM</option><option value="06">06:00AM</option><option value="07">07:00AM</option><option value="08">08:00AM</option><option value="09">09:00AM</option><option value="10">10:00AM</option><option value="11">11:00AM</option><option value="12">12:00PM</option><option value="13">01:00PM</option><option value="14">02:00PM</option><option value="15">03:00PM</option><option value="16">04:00PM</option><option value="17">05:00PM</option><option value="18">06:00PM</option><option value="19">07:00PM</option><option value="20">08:00PM</option><option value="21">09:00PM</option><option value="22">10:00PM</option><option value="23">11:00PM</option><option value="23.59">11:59PM</option></select></span></div>


                                    </div>
                  </div>
                </div>
                </div>
                 <td class="hidden-phone" style="width:155px">
                     
                        <a class="btn mini purple editcity" onclick="edit();"> <i class="icon-edit"> </i> Edit </a>
                     
                      </td>

	</div>
	</div>
	

                <div  class="acoordian" id="offer" hidden>
                
                 <div class="widget-body form">

		<h1>Offer</h1>


		</div>
		</div>
		
		<div  class="acoordian" id="hotdeals" hidden >
		
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-datepicker/css/datepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/compiled/timepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-colorpicker/css/colorpicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/daterangepicker.css" />
    

  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-daterangepicker/daterangepicker.js"></script>

 <script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		
		
		
		
		
		
		
		  <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3">   Title </td>
                      
                 <td>  <input class="span6 required" type="text" placeholder="Please enter name" name="title"/></td>
                 
                  <td class="hidden-phone" style="width:155px">
                     
                        <a class="btn mini purple editcity" onclick="edit();"> <i class="icon-edit"> </i> Edit </a>
                     
                      </td>
                 
                      </tr>
                      
                      
                      
             <tr>
                        <td class="span3">Start and end date </td>
                  	<div class="controls">
                  
                
            <td> <input type="text" class="m-ctrl-medium date-range" name="start_end_date" /></td>
            
             <td class="hidden-phone" style="width:155px">
                     
                        <a class="btn mini purple editcity" onclick="edit();"> <i class="icon-edit"> </i> Edit </a>
                     
                      </td>
            
            
            </tr>
           
                  
                  
                  
          

		 <tr>
                        <td class="span3">Image </td>
                  <div class="controls">
                  <td>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image_deal" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
             </td>
		 <td class="hidden-phone" style="width:155px">
                     
                        <a class="btn mini purple editcity" onclick="edit();"> <i class="icon-edit"> </i> Edit </a>
                     
                      </td>
		
		      <tr>
                        <td class="span3"> Description </td>

                <td>
                    <textarea  rows="6" name="deal_description"> </textarea></td>
                    
                     <td class="hidden-phone" style="width:155px">
                     
                        <a class="btn mini purple editcity" onclick="edit();"> <i class="icon-edit"> </i> Edit </a>
                     
                      </td>
                


      </tbody>              
             
</table>
    

<?php endif; ?>


<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script> 
  jQuery(document).ready(function(){
  $('#cities').change(function(){
    var city_id = $('#cities').val();
    //alert(city_id);
    if(city_id !=''){
    $.ajax({
        type : "POST",
        dataType: 'json',
        url : "<?php echo site_url()?>administrator/restaurants/get_locations",
        data : { 'city_id': city_id },
        success : function(response) {
            console.log(response);
            $('#locations').html(response.options);/*
            $('#locationsd ul.chzn-results').html(response.list);*/
        },
        error : function() {
           alert("We are unable to do your request. Please contact webadmin");
        }
    });
  }
  });
  $("#restaurant_form").validate({
    rules: {
        image:{ required: false, extension: "gif|jpeg|jpg|png"}
        },
        image: {
          image:{ extension:"File must be in gif / jpeg / jpg / png formats"}
        }
    });

  }); 
</script>

