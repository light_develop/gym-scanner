
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Cities </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>state/add" class="icon-plus"></a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> State Name </th>
              <th> State Code </th>
              <th> Country Name </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>

          <?php if(isset($states)  && count($states)){ $i = 1;?>

          <?php foreach ($states as $row)  { ?>
            <tr class="odd gradeX">

             <td> <?php echo $row->state;?> </td>
             <td> <?php echo $row->state_code;?> </td>
             <td> <?php echo $row->country_name;?> </td>
              <td class="hidden-phone">
              <a href="<?php echo ADMIN_URL;?>state/view/<?php echo $row->state_code?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editstate" href="<?php echo ADMIN_URL;?>state/edit/<?php echo $row->state_code?>"> <i class="icon-edit"> </i> Edit </a> 
                <span class="btn btn-danger" onclick='getid("<?php echo $row->state_code?>")'> <i class="icon-remove icon-white"> </i> Delete </span>                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End state listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Cities </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>state" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>state/add" class="form-horizontal" method="post" id="add_notify" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Country List </label>
                  <div class="controls">
                    <?php $countries[''] = 'Country';$attributes= 'id="country_id" class="span6 required"';
                    echo form_dropdown('country_id', $countries, '',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="state_name" value="" id="state_name"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State Code </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="state_code" value="" id="state_code"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> state List (Excel) </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="userfile" id="userfile"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
                      <option value="1"  selected="selected">Active</option>                   
                      <option value="0">Inactive</option>                   
                   </select>
                </div>
                </div> -->
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_state"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>state" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>state/edit" class="form-horizontal" method="post" id="add_notify" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Country List </label>
                  <div class="controls">
                    <?php $cntry[''] = 'Country';$attributes= 'id="country_id" class="span6 required"';
                    echo form_dropdown('country_id', $countries,$result->country_id,$attributes);
                    ?>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label"> State Name </label>
                  <div class="controls">
                   <input type="text" name="state_name" class="span6 required" value="<?php echo $result->state; ?>" />
                </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State Code </label>
                  <div class="controls">
                    <input type="text" name="state_code" class="span6 required" value="<?php echo $result->state_code ?>">
                </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">    
                  <?php $status = $result->status; ?>             
                   <select name="status" id="status" class="chosen span6 required" >                   
                      <option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
                      <option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                    
                   </select>
                </div>
                </div>            
                <br />
                <div class="form-actions">
                  <button type="submit" name="addmem" class="btn btn-success" id="saveuser"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'):

 ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> State View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>state" class="icon-arrow-left"></a></span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>  
                      <tr>
                        <td class="span3"> Country Name : </td>
                        <td> <?php echo $result->country_name; ?> </td>
                      </tr>                      
                      <tr>
                        <td class="span3"> State Name : </td>
                        <td> <?php echo $result->state; ?> </td>
                      </tr> 
                      <tr>
                        <td class="span3"> State Code : </td>
                        <td> <?php echo $result->state_code; ?> </td>
                      </tr>                                 
                      <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($result -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script>
  jQuery(document).ready(function(){

$("#add_state").click(function(){
    var cnt = $('#country_id').val();
    var state_name = $("#state_name").val();
    var state_code = $("#state_code").val();
    var userfile = $("#userfile").val();
    if(cnt == ''){
      alert('please choose Country');
      return false;
    }
    if(state_name == '' && userfile == ''){
      alert('please choose atleast one inserting option');
      return false;
    }
    if($.trim(state_name) != ''){
      if($.trim(state_code) == ''){
        alert('please give state code');
        return false;
      }else{
        return true;
      }
    }
});
 
 
  });
   function getid(id){
    
      if (confirm('Do you want to delete this state?')) 
      {
        $.ajax({
          type: 'POST',
          url: '<?php echo ADMIN_URL;?>state/delete',
          data: { id: id}
        })
          .done(function(response){  console.log(response);
              if(response == 'success'){
                window.location.reload(true);
                return true;
              }else{
                window.location.reload(true);
                return false;
              }
            });
      }
     else
      return false;           
      }
               
</script>


