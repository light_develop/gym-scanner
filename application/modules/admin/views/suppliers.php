
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All suppliers -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Suppliers </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>suppliers/add" class="icon-plus"> Add Supplier</a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> Name </th>
              <th> Mobile </th>
              <th> Email </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php
           if(isset($suppliers)  && count($suppliers)){ $i = 1; ?>
          <?php foreach ($suppliers as $row)  { ?>
            <tr class="odd gradeX">
             <td> <?php echo ucfirst($row->first_name).' '.ucfirst($row->last_name);?> </td>
             <td> <?php echo $row->mobile;?> </td>
             <td> <?php echo $row->email;?> </td>   
             <td>
                <a href="<?php echo ADMIN_URL;?>suppliers/view/<?php echo $row->id?>" class="btn mini black"> <i class="icon-eye-open"> </i></a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>suppliers/edit/<?php echo $row -> id?>"> <i class="icon-edit"> </i></a>   
                <?php if($row->status==0){?>
                  <a class="btn btn-success" id="<?php echo $row->id; ?>" onclick="status_change_supplier(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-up"> </i></a><?php } else{ ?>
                  <a class="btn btn-danger" id="<?php echo $row->id; ?>" onclick="status_change_supplier(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                  <?php } ?>
                <span class="btn btn-danger" onclick="getid(<?php echo $row->id?>)" > <i class="icon-remove icon-white"> </i></span>               
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Supplier </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>suppliers" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>suppliers/add" class="form-horizontal" method="post" id="user_profile_add" enctype = "multipart/form-data"/>
                <div class="error"> </div>
                <div class="control-group">
                  <label class="control-label"> First Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="first_name" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Last Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="last_name" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> User Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="user_name" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Password </label>
                  <div class="controls">
                   <input class="span6 required" type="password"  name="password" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email" value=""/>
                  </div>
                </div>               
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>
    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Supplier Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>suppliers" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>suppliers/edit" class="form-horizontal" method="post" id="user_profile_add" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> First Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $supplier->id;?>">
                    <input type="text" name="first_name" value="<?php echo $supplier->first_name;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Last Name </label>
                  <div class="controls">
                    <input type="text" name="last_name" value="<?php echo $supplier->last_name; ?>"/>                    
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> User Name </label>
                  <div class="controls">
                    <input type="text" name="user_name" value="<?php echo $supplier->user_name;?>"/>                    
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Password </label>
                  <div class="controls">
                    <input type="password" name="password" value="<?php echo $supplier->password;?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value="<?php echo $supplier->mobile; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="<?php echo $supplier->email; ?>">
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit" name="adduser" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'): ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Supplier View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>suppliers" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> First Name : </td>
                        <td> <?php echo ucfirst($supplier->first_name);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Last Name : </td>
                        <td> <?php echo ucfirst($supplier->last_name);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> User Name : </td>
                        <td> <?php echo ucfirst($supplier->user_name);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Mobile : </td>
                        <td> <?php echo $supplier->mobile; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Email : </td>
                        <td> <?php echo $supplier->email; ?></td>
                      </tr>                                   
											<tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($supplier->status==1)?"In Active":"Active";?></td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script>
  jQuery(document).ready(function(){

    $("#user_profile_add").validate({
      rules: {
        // simple rule, converted to {required:true}
        name: "required",
        // compound rule
        email: {
          required: true,
          email: true
        },
        
        username:"required"
      }
    });
  });  
  	           
</script>
<script type="text/javascript">
  function status_change_supplier(id,status,aid)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo ADMIN_URL;?>suppliers/change_status",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '0'){
                  $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
                  $('#'+aid).attr('onclick','status_change_supplier('+id+',1,"'+aid+'")');
                }else{
                  $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_supplier('+id+',0,"'+aid+'")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>