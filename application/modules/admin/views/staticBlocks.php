<div id="main-content">
    <div class="container-fluid">
        <link rel="stylesheet" type="text/css"
              href="<?php echo base_url(); ?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/admin/css/style_datepicker.css"/>

        <script src="<?php echo base_url(); ?>themes/admin/js/framework/main.js"></script>
        <script src="<?php echo base_url(); ?>themes/admin/js/framework/tab.js"></script>

        <script src="<?php echo base_url(); ?>themes/wysiwig/src/wysiwyg.js"></script>
        <script src="<?php echo base_url(); ?>themes/wysiwig/src/wysiwyg-editor.js"></script>
        <script src="<?php echo base_url(); ?>themes/wysiwig/src/wysiwyg-init.js"></script>
        <script src="<?php echo base_url(); ?>themes/wysiwig/src/wysiwyg-init.js"></script>
        <link href="<?php echo base_url(); ?>themes/admin/css/tabgrid.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>themes/wysiwig/src/wysiwyg-editor.css" rel="stylesheet"/>
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    </div>
<style>
    #main-content {
        box-shadow: 0 0 10px #292828;
        -moz-box-shadow: 0 0 10px #292828;
        -webkit-box-shadow: 0 0 10px #292828;
        display: flex;
    }
</style>
    <div class="container-fluid span12">
        <div class="settings_widget container-fluid">
            <div class=" container-fluid admin_language_settings">
                <div class="body">

                    <div class="navbar">
                        <div class="navbar-inner">
                            <h5><i class="icon-reorder"> </i> Statick Blocks </h5>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div id="grid-container">
                            <?php if (isset($staticBlocksInfo["static_blocks"])) : ?>
                                <?php foreach ($staticBlocksInfo["static_blocks"] as $block): ?>
                                    <li id="<?php echo $block["static_block_id"] . "_" . $block["lang_id"] ?>_header"><?php echo $block["static_block_title"]; ?></li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <?php if (isset($staticBlocksInfo["static_blocks"])) : ?>
                            <?php foreach ($staticBlocksInfo["static_blocks"] as $block): ?>
                                <div class="static-block-container"
                                     id="<?php echo $block["static_block_id"] . "_" . $block["lang_id"]; ?>_data"
                                     style="display: none">
                                    <form action="<?php echo base_url(); ?>admin/staticBlocks/edit" method="POST"
                                          onsubmit="submitNewStaticBlock(this); return false;">
                                        <input validation="notempty" name="static_block_id"
                                               value="<?php echo $block["static_block_id"] ?>"/>
                                        <input validation="notempty" name="static_block_title"
                                               value="<?php echo $block["static_block_title"] ?>"/>
                                        <?php echo $this->getLanguagesSelect(null, $block["lang_id"]); ?>
                                        <textarea validation="notempty" class="html-container"
                                                  name="static_block_content"
                                                  value=""><?php echo $block["static_block_content"]; ?></textarea>
                                        <a href="#" class="btn btn-mini btn-info" onclick="showHtmlEditor(this)">Show Html Editor</a>
                                        <a href="#" class="btn btn-mini btn-info" onclick="showWyswigEditor(this)">Show Wyswig Editor</a>
                                        <input type="hidden" name="entity_id"
                                               value="<?php echo $block["entity_id"] ?>"/>
                                        <button class="btn btn-mini btn-success" type="submit">Submit</button>
                                    </form>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <div class="static-block-container new-holder span12">
                            <?php echo form_open('admin/staticBlocks/addNew', array("id" => "static_block_form",
                                "preview-action" => 'admin/staticBlocks/preview'
                            ));
                            ?>
                            <input class="input-medium" name="static_block_title" validation="notempty"/>
                            <input class="input-medium" name="static_block_id" validation="notempty"/>
                            <?php echo $this->getLanguagesSelect(); ?>
                            <input class="page-id input-medium"/>
                            <a href="javascript:void(null) " class="btn btn-mini btn-info"
                               onclick="submitNewStaticBlock(this.parentNode, 1)">Preview</a>
                            <textarea class="html-container" name="static_block_content"
                                      validation="notempty"></textarea>


                                <a class="btn btn-mini btn-info" href="javascript:void(null)"
                                   onclick="showHtmlEditor(this)">Show Html Editor</a>
                                <a class="btn btn-mini btn-info" href="javascript:void(null)"
                                   onclick="showWyswigEditor(this)">Show Wyswig Editor</a>

                                <button class="btn btn-mini btn-success"
                                        onsubmit="submitNewStaticBlock(this.parentNode, 0)">Save!
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var tabgrid = new TabGrid("grid-container", "header", "data", "static-block-container");
        tabgrid.addAdditionalCallback(function (operation) {
            if (operation == "open") {
                $$1('.new-holder')[0].style.display = "none";
            } else {
                $$1('.new-holder')[0].style.display = "block";
            }
        });

        tabgrid.init();

        $(document).ready(function () {
            initWyswigs('.html-container'); //init all wyswigs
            var wyswigs = $$1('.static-block-container');
            $('.new-holder').show();
        });

        function showWyswigEditor(block) {
            var htmlEd = _getHtmlEditor(block);
            wyswigShow(block.parentNode, htmlEd);
            $(htmlEd).hide();

        }

        function showHtmlEditor(block) {
            var htmlEd = _getHtmlEditor(block);
            wyswigHide(block.parentNode, htmlEd);
            $(htmlEd).show();
        }

        function submitNewStaticBlock(form, preview) {
            var oldAction = form.action;
            var pageInput = form.querySelector("input.page-id");

            if (preview == 1) {
                if (!pageInput.value) {
                    form.action = oldAction;
                } else {
                    form.action = pageInput.value + "?preview=1";
                    form.querySelector("button").click();
                }
                return false;
            }

            if (validate(form.querySelectorAll("input, textarea"))) {
                submit();
            }
        }


    </script>