
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Users </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>users/add" class="icon-plus"> Add User</a> </span>
      </div>
      <div class="widget-body">
     <?php if($this->session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this ->session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this->session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this ->session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> Sl No. </th>               
              <th> First Name </th>
              <th> Last Name </th>
              <th> User Name </th>
              <th> Email </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php
           if(isset($users)  && count($users)){ $i = 1; ?>
          <?php foreach ($users as $row)  { ?>
            <tr class="odd gradeX" id="div_<?= $row->id?>">
              <td><?php echo $i;?></td>

               <td> <?php echo ucfirst($row->first_name);?> </td>
               <td> <?php echo ucfirst($row->last_name);?> </td>
               <td> <?php echo $row->user_name;?> </td>
               <td> <?php echo $row->email;?> </td>           
              <td class="hidden-phone"><div style="clear:both;">
                <a href="<?php echo ADMIN_URL;?>users/view/<?php echo $row ->id;?>" class="btn mini black"> <i class="icon-eye-open"> </i></a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>users/edit/<?php echo $row->id;?>"> <i class="icon-edit"> </i></a>   
                <?php if($row->status==0){?>
                  <a class="btn btn-info" id="<?php echo $row->id; ?>" onclick="status_change_user(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-lock"> </i></a>
                <?php } else if($row->status == 1){ ?>
                  <a class="btn btn-success" id="<?php echo $row->id; ?>" onclick="status_change_user(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-up"> </i></a>
                <?php } else if($row->status == 2){?>
                  <a class="btn btn-danger" id="<?php echo $row->id; ?>" onclick="status_change_user(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                <?php } else if($row->status == 3){?>
                  <a class="btn btn-warning" id="<?php echo $row->id; ?>" onclick="status_change_user(<?php echo $row->id; ?>,<?php echo $row->status; ?>,'<?php echo $row->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                <?php } ?>
                      <? if ($row->warning==0):?>
                          <a class="btn btn-warning" id="<?php echo $row->id; ?>" onclick="" href="<?php echo ADMIN_URL;?>users/sendWarning/<?php echo $row->id;?>" > <i class="icon-warning-sign"> Send Warning</i></a>
                      <? endif?>
                      <? if ($row->banned==0 && $row->warning == 1):?>
                          <a class="btn btn-inverse"  id="<?php echo $row->id; ?>" onclick="" href="<?php echo ADMIN_URL;?>users/banUser/<?php echo $row->id;?>" > <i class="icon-ban-circle"> Ban User</i></a>
                      <? endif?>
                      <? if ($row->banned==1 && $row->warning==1 ):?>
                          <a class="btn btn-inverse"  id="<?php echo $row->id; ?>" onclick="" href="<?php echo ADMIN_URL;?>users/unBanUser/<?php echo $row->id;?>" > <i class="icon-refresh"> Unban User</i></a>
                      <? endif?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>users/removeSelectedUser/<?php echo $row->id;?>"> <i class="icon-remove icon-white"> </i></a>
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Users </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                <?php if (validation_errors() != "") { ?>
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Errors Found !</strong> <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>

              <form action="<?php echo ADMIN_URL;?>users/add" class="form-horizontal" method="post" id="user_profile_add" enctype = "multipart/form-data"/>
                <div class="error"> </div>
                <div class="control-group">
                  <label class="control-label"> First Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="first_name" value="<?= $this->input->post('first_name');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Last Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="last_name" value="<?= $this->input->post('last_name');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> User Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="user_name" value="<?= $this->input->post('user_name');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Password </label>
                  <div class="controls">
                   <input class="span6 required" type="password"  name="password" value="<?= $this->input->post('password');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email" value="<?= $this->input->post('email');?>"/>
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>
    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                <?php if (validation_errors() != "") { ?>
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Errors Found !</strong> <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>

              <form action="<?php echo ADMIN_URL; ?>users/edit_user" class="form-horizontal" method="post" id="user_profile_add" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> First Name </label>
                  <div class="controls">
                    <input type="hidden" name="uid" value="<?php echo $user->id ?>">
                    <input class="span6 required" type="text" name="first_name"value="<?php echo $user->first_name; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Last Name </label>
                  <div class="controls">
                    <input class="span6 required" type="text" name="last_name"value="<?php echo $user->last_name; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> User Name </label>
                  <div class="controls">
                    <input type="hidden" name="old_name" value="<?php echo $user->user_name; ?>">
                    <input class="span6 required" type="text" name="user_name" value="<?php echo $user->user_name; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Password </label>
                  <div class="controls">
                    <input class="span6 required" type="password" name="password"value="<?php echo $user->password; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="<?php echo $user->email; ?>">
                  </div>
                </div>            
                <br />
                <div class="form-actions">
                  <button type="submit" name="adduser" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'): ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> First Name : </td>
                        <td> <?php echo ucfirst($user->first_name);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Last name : </td>
                        <td> <?php echo $user->last_name; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> User Name : </td>
                        <td> <?php echo ucfirst($user->user_name);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Email : </td>
                        <td> <?php echo $user->email; ?></td>
                      </tr>                                  
											<tr>
                        <td class="span3"> Payment Status : </td>
                        <td> <?php if($user->status == 0)
                                echo "Pending";
                                else if($user->status == 1)
                                  echo "Active";
                                else if($user->status == 2)
                                  echo "Rejected";
                                else echo "Hold";?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  jQuery(document).ready(function(){

    $("#user_profile_add").validate({
      rules: {
        // simple rule, converted to {required:true}
        name: "required",
        // compound rule
        email: {
          required: true,
          email: true
        },
        
        username:"required"
      }
    });
  });
</script>
<script type="text/javascript">
  function status_change_user(id,status,aid)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo ADMIN_URL;?>users/change_status",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '0'){
                  $('#'+aid).removeClass('btn btn-info').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'")');
                }else if(status == '1'){
                  $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',2,"'+aid+'")');
                }else if(status == '2'){
                  $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'")');
                }else{
                  $('#'+aid).removeClass('btn btn-warning').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>
<script>
  function del_usr(selected){
      $.ajax({
        type : "POST",
        url : "<?php echo ADMIN_URL?>users/del_usr",
        data : {
          'id':selected
        },
        success : function(data){
          if(data == 1){
            alert('sorry, User allocated to gym');
          }else{
            $('#div_'+selected).hide();
          }
        },
        error : function(){
          alert("We are unable to do your request. Please contact webadmin");
        }
      });
  }
</script>