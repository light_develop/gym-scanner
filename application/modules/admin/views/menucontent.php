
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
<?php
/**
 * [Check the mode of view, if all it will list all menus]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') :  ?>   
<!-- Start Listing All Menus -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Menu Contents</h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>menu/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
      
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Menu Name </th>
              <th> Image </th>
              <th class="hidden-phone"> Description </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php if(isset($menus) && is_array($menus) && count($menus)){ $i = 1;?>
          <?php foreach ($menus as $key => $menu) { 
        $menuname = str_replace('[@]', '<br/>',  $menu->menuname);
        $description = str_replace('[@]', '<br/><br/>',  $menu->content);
        
          ?>
            <tr class="odd gradeX">
             <td><a href="<?php echo ADMIN_URL;?>menu/view/<?php echo $menu -> id ;?>">
             <?php echo ucfirst($menuname) ; ?></a>  </td>
             <td><?php if($menu->image != ''){?>
              <img src="<?php echo base_url()?>uploads/thumbs/cms_content/<?php echo $menu -> image; ?>" alt="image" />
              <?php }?>
             </td>
             <td> <?php echo $description; ?> </td>
              <td class="hidden-phone" style="width:330px">
              <a href="<?php echo ADMIN_URL;?>menu/view/<?php echo $menu -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>menu/edit/<?php echo $menu -> id?>"> <i class="icon-edit"> </i> Edit </a>
                <span class="btn btn-danger" onclick="getid(<?php echo $menu-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                <?php if($menu->status==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>menu/deactivate/<?php echo $menu->id?>"> <i class="icon-ok icon-white"> </i>Active</a>
                
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>menu/active/<?php echo $menu->id?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
                         
                <?php endif; ?>
                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Menu Content </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>menu" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL;?>menu/add" class="form-horizontal" method="post" id="menu_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                
                
                <div class="control-group">
                  <label class="control-label"> Select Main Menu </label>
                  <div class="controls">
                    <?php $addmenus['select'] = 'Select Main Menu'; $attributes= 'id="mainmenu" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('mainmenu', $addmenus, 'select',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Sub Menu </label>
                  <div class="controls">
                    <?php $submenus['select'] = 'Select Sub Menu'; $attributes= 'id="submenu" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('submenu', $submenus, 'select',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" id="image-file"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"> </textarea>
                  </div>
                </div>   
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1"  selected="selected">Active</option>                   
																	<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>               
                <br />
                <div class="form-actions">
                  <button type="submit" name="addmenu" class="btn btn-success" id="savemenu"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Edit Menu </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>menu" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL; ?>menu/edit" class="form-horizontal" method="post" id="menu_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $menu -> id ?>">
                   <?php echo ucfirst($menu -> menuname); ?>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    <?php if($menu->image != ''){?>  
                    <span style="padding-right:10px;" id="delimg"> 
                    <img src="<?php echo base_url();?>uploads/thumbs/cms_content/<?php echo $menu->image?>" width="90" alt="image">
                    <span class="btn btn-danger" onclick="deleteImage('<?php echo $menu->image?>');"> Delete </span>  
                    </span>                    
                    <?php }?>
                    <input type="hidden" name="oldimage" value="<?php echo $menu->image;?>"/>
                    <div id="removeimgdiv"></div> 
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Menu List </label>
                  <div class="controls">
                    <?php $attributes= 'id="menu_list" class="chosen span6 required" tabindex="6" required';
                        echo form_dropdown('menu_list', $menu_list, $menu->menu_id,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"><?php echo $menu -> content ?> </textarea>
                  </div>
                </div> 
                <div class="control-group">
                  <label class="control-label"> Status </label>
                  <div class="controls">
                  
                    <?php $status = $menu->status; ?>
                   <select name="status" id="status" class="chosen span6 required" >                   
						<option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
						<option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                   
                   </select>
                   
                </div>
                </div> 
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="editmenu"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'):

 ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Menu Content </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL ?>menu" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Menu Name : </td>
                        <td> <?php echo ucfirst($menu -> menuname);?></td>
                      </tr>
																		  <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($menu -> status==1)?"Active":"In Active";?></td>
                      </tr>
                                                                  
                      <?php if($menu -> image) {?>
                      <tr>
                        <td class="span3"> Image : </td>
                        <td> <img src="<?php echo base_url(); ?>uploads/thumbs/cms_content/<?php echo $menu -> image; ?>" alt=""></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td class="span3"> Description : </td>
                        <td> <?php echo $menu -> content;?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
  </div>
</div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script> 
  jQuery(document).ready(function(){

  $("#menu_form").validate({
    rules: {
        image:{ required: false, extension: "gif|jpeg|jpg|png"}
        },
        image: {
          image:{ extension:"File must be in gif / jpeg / jpg / png formats"}
        }
    });
    $('#savemenu').click(function(){ 
       var k = $('#mainmenu').val(); 
      
     // alert(k);
      var l = $('#submenu').val(); 
      
      if(k=='select' && l=='select'){
        $("#mainmenu").after('<label for="menu_list" generated="true" class="error">Please select menu.</label>' );
        return false;
      } 
    });
 
  });
  function deleteImage(imgname)
  {
      if(confirm('Are you sure want to delete this image?'))
      {
        $('#delimg').remove();
        $('#removeimgdiv').append('<input type="hidden" name="remove_gym_image" value="'+imgname+'">');
      }
  }
  function getid(id){
  	
      if (confirm('Do you want to delete this menucontent?')) 
      {
 	   	$.ajax({
		type: 'POST',
                url: '<?php echo ADMIN_URL;?>menu/delete',
                data: { sid: id}
            })
            .done(function(response){  
				              if(response == 'success'){
					                   window.location.reload(true);
					                   return true;
				              }else{
				              	     window.location.reload(true);
					                   return false;
				             }});
  	          }
  	       else{
  	       						window.location.reload(true);
                return false;  	       	
  	       	}
  	       	}
 
</script>


