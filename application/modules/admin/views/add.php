<?php

$this->load->view('includes/header');
?>
    
    
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
<?php
/**
 * [Check the mode of view, if all it will list all foods]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') : ?>   
<!-- Start Listing All Cities -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Foods </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo site_url()?>administrator/foods/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <!-- //<?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      //<?php } ?> -->
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Food Name </th>
              <th> Image </th>
              <th class="hidden-phone"> Description </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody> <?php echo 'show something';?>
          <!-- <?php if(isset($foods) && is_array($foods) && count($foods)){ $i = 1;?>
          <?php foreach ($foods as $key => $food) { ?> -->
            <tr class="odd gradeX">
             <td><a href="<?php echo base_url();?>administrator/foods/view/<?php echo $food -> id ;?>"><?php echo $food -> name ; ?></a>  </td>
             <td> <img src="<?php echo base_url()?>uploads/thumbs/foods/<?php echo $food -> image; ?>" alt="image" /> </td>
             <td> <?php echo $food -> description; ?> </td>
              <td class="hidden-phone" style="width:155px">
              <a href="<?php echo site_url()?>administrator/foods/view/<?php echo $food -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo site_url()?>administrator/foods/edit/<?php echo $food -> id?>"> <i class="icon-edit"> </i> Edit </a>
               <!--  <a href="#" class="btn mini black"> <i class="icon-trash"> </i> Delete </a> -->
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Food Items </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo site_url() ?>administrator/foods" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo site_url() ?>administrator/foods/add" class="form-horizontal" method="post" id="food_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label">Food Item Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="name"/>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Price </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter price" name="price"/>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Food Category </label>
                  <div class="controls">
                    <?php $attributes= 'id="food_categories" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('food_categories', $food_categories, '',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"> </textarea>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label"> Preparation Time </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter Time in minutes" name="time"/>
                </div>
                </div>    
                <div class="control-group">
                  <label class="control-label"> Status </label>
                  <div class="controls">
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1" selected="selected">Active</option>                   
																	<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>               
                <br />
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="savefood"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Edit Food </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo site_url() ?>administrator/foods" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo site_url() ?>administrator/foods/edit" class="form-horizontal" method="post" id="food_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $food -> id ?>">
                   <input class="span6 required" type="text" placeholder="Please enter name" name="name" value="<?php echo $food -> name ?>" />
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Price </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter price" name="price" value="<?php echo $food -> price;?>" />
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Image </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="image" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    <img src="<?php echo base_url();?>uploads/thumbs/foods/<?php echo $food->image?>" width="90" alt="image">
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Food Categories </label>
                  <div class="controls">
                    <?php $attributes= 'id="food_categories" class="chosen span6 required" tabindex="6" required';
                    
                    // echo "<pre>";print_r($food_select_categories);echo "</pre>";
                    echo form_dropdown('food_categories', $food_categories, $food_select_categories,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span12 wysihtml5" rows="6" name="description"><?php echo $food -> description ?> </textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Preparation Time </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" placeholder="Please enter Time in minutes" name="time" value="<?php echo $food -> time;?>" />
                </div>
                </div>    
                <div class="control-group">
                  <label class="control-label"> Status </label>
                  <div class="controls">
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1" selected="selected">Active</option>                   
																	<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div> 
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="savefood"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'): ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Food Items </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo site_url() ?>administrator/foods" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Name : </td>
                        <td> <?php echo $food -> name;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Price : </td>
                        <td> <?php echo $food -> price;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Category : </td>
                        <td>                        
                          <?php
                       //  echo '<pre>';print_r($food_select_categories);print_r($food_categories);echo '</pre>';
                           // if(is_array($food_select_categories) && count($food_select_categories)>0)
                            echo $food_select_categories;                             
                            
                            ?>
                        </td>
                      </tr>
                      <?php if($food -> image) {?>
                      <tr>
                        <td class="span3"> Image : </td>
                        <td> <img src="<?php echo base_url(); ?>uploads/thumbs/foods/<?php echo $food -> image; ?>" alt=""></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td class="span3"> Description : </td>
                        <td> <?php echo $food -> description;?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
  </div>
</div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script> 
  jQuery(document).ready(function(){

  $("#food_form").validate({
    rules: {
        image:{ required: false, extension: "gif|jpeg|jpg|png"}
        },
        image: {
          image:{ extension:"File must be in gif / jpeg / jpg / png formats"}
        }
    });
  $('#savefood').click(function(){ 
      var foo = []; 
      var k = 0;
      $('#food_categories :selected').each(function(i, selected){ 
      foo[i] = $(selected).text();
      k++; 
    });
      if(k==0){
        $("#food_categories").after('<label for="food_categories" generated="true" class="error">Please select food category.</label>' );
        return false;
      }
  });

  }); 
</script>


<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript" >

        $('.add').click(function(){            
            $('#viewcms').hide();
            $('#addcms').show();
        });
        $('.view').click(function(){           
            $('#addcms').hide();
            $('#viewcms').show();
        });
        
     	$("#cmsmenu").click(function(){
      
     		  var menu = $('#menu_id').val();
                  var menuname = $('#menuname').val();
               
                  
                  $('#menuIdErr, #nameErr').html('');
                  if(!menuname)
                  {
                     // alert('please enter menu name');
                      $('#nameErr').css('color', 'red').html('please enter menu name');
                      return false;
                  }
                  
                	$.ajax({ type: 'POST',
				dataType: 'JSON',
                           	url: '<?php echo ADMIN_URL;?>cms/getmname',
                           	data: { menu_id: menu, menuname: menuname },
                           	success:function(response){
                           	                           	
                           	if(response.q == 'err')
                          		{
                          				var err = 1;
                          				alert('err');
                          					$('#nameErr').css('color', 'red').html('Menu name already exist!');     
                          					return false;                     				
                          		}
                          		else
                          		{
                          				                  if(!menu)
                  {        
                       			
                        if(confirm("Are you sure? is this main menu?"))
                        {
                        	alert('no err');
                                $(this).submit();                        		
                        }
                        else
                        {
                            $('#menuIdErr').css('color', 'red').html('please select menu name');                               
                      					return false;                 
                        }

                  }
                          		}
                           }
                      			});	
                      

                  
});


</script>
