<style type="text/css">
	.widget-title>h4, .breadcrumb>li>a:hover, .chats li.in .name {
color: #ffffff;
text-shadow: none;

	table {
		table-layout: fixed;
		width:100%
	}
	td {
		word-wrap:break-word;
	}

}
</style>
<div id="main-content">
      <div class="container-fluid">
      <div class="row-fluid"> <div class="span12"> </div> </div>
      <div class="row-fluid circle-state-overview">
		<div class="span2 responsive clearfix" data-tablet="span3" data-desktop="span2">
			<div class="circle-wrap">
				<div class="stats-circle turquoise-color">
					<a style="text-decoration: none" href="<?php echo ADMIN_URL;?>users"><i class="icon-user"></i></a>
				</div>
				<p>
					<strong>+<?php echo $res->count2; ?></strong> New Users
				</p>
			</div>
		</div>
		<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
			<div class="circle-wrap">
				<div class="stats-circle red-color">
					<a style="text-decoration: none" href="<?php echo ADMIN_URL;?>gyms"><i class="icon-tags"></i></a>
				</div>
				<p>
					<strong>+<?php echo $res->count1; ?></strong> Gyms
				</p>
			</div>
		</div>
	</div>
	<div class="row-fluid circle-state-overview">
		<div class="row-fluid">
		  <div class="span12">
		    <div class="widget">
		      <div class="widget-title" style="background:#868686;">
		        <h4> <i class="icon-tags"> </i> Latest Gyms </h4>
		      </div>
		      <div class="widget-body" id="widget">
		        <table class="table  table-striped table-bordered" style="table-layout: fixed;
		width:100%" >
		          <thead>
		            <tr>                 
		              <th> Logo </th>    
		              <th> Name </th>
		              <th> Open Time </th>
		              <th> Closing Time </th>
		              <th> Rating </th>
		              <th> Address </th>
		              <th> Specialty </th>
		              <th> Owner-Details </th>
		            </tr>
		          </thead>
		          <tbody>
		          <?php if(isset($gyms) && is_array($gyms) && count($gyms)){ $i = 1;?>
		          <?php foreach ($gyms as $key => $gym) {
		                $gymname = str_replace('[@]', '<br/>',  $gym->gymname);
		          ?>
		            <tr class="odd gradeX" style="word-wrap:break-word">
		             <td  style="word-wrap:break-word"><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym->id ;?>">
		             <?php $img = $gym->gym_logo; ?>
		             <img src="<?php echo base_url()?>uploads/gym/logos/<?php echo $img; ?> " alt="image" height="50px" width="50px" /></a>  </td>
		             <td style="word-wrap:break-word"><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym->id ;?>">
		             <?php echo ucfirst($gymname); ?></a>  </td>
		             <td style="word-wrap:break-word"><?php echo $gym->open_time; ?></td>
		             <td style="word-wrap:break-word"><?php echo $gym->close_time; ?></td>
		             <td style="word-wrap:break-word"><?php for($j = 0;$j < $gym->star; $j++){ ?> <img src="<?php echo base_url()?>themes/admin/img/star-on.png" /> <?php  }?></td>
		             <td><?php echo ucfirst($gym->gstreet).',' ; echo '<br/>';
		                       echo ucfirst($gym->city).',' ; echo '<br/>';
		             		   echo ucfirst($gym->state).',' ; echo '<br/>';
		                       echo ucfirst($gym->country_name) ;?></td>
		             <td style="word-wrap:break-word"><?php if($gym->speciality ==1){echo 'Men';} else if($gym->speciality ==2){echo 'Women';} else{echo 'All';} ?></td>
		             <td style="word-wrap:break-word" ><?php echo "Name: ". ucfirst($gym->owner) ; echo '<br/>'; echo "Ph no: ". $gym->ow_phone ; echo '<br/>'; echo "Mail id: ".$gym->ow_email ; ?></td>
		            </tr>
		            <?php $i++; } } ?>
		          </tbody>
		        </table>
		      </div>
		    </div>
		  </div>
		</div>
		</div>
	<div class="row-fluid circle-state-overview">
		<div class="row-fluid">
		  <div class="span12">
		    <div class="widget">
		      <div class="widget-title" style="background:#868686;">
		        <h4> <i class="icon-tags"> </i> Recent Users </h4>
		      </div>
		      <div class="widget-body" id="widgetr">
		        <table class="table table-condensed table-striped table-bordered" >
		          <thead>
		            <tr>  
		              <th> Sl No. </th>
		              <th> First Name </th>
		              <th> Last Name </th>
		              <th> User Name </th>
		              <th> Email </th>
		              <th> Gym Name </th>
		            </tr>
		          </thead>
		          <tbody>
		         <?php
		           if(isset($users)  && count($users)){ $i = 1; ?>
		          <?php foreach ($users as $row)  { ?>
		            <tr class="odd gradeX">
		            <td><?php echo $i;?></td>
		             <td> <?php echo ucfirst($row->first_name);?> </td>
		             <td> <?php echo ucfirst($row->last_name);?> </td>
		             <td> <?php echo ucfirst($row->user_name);?> </td>
		             <td> <?php echo $row->email;?> </td>
		             <td> <?php echo $row->gymname;?> </td>
		            </tr>
		            <?php $i++; } } ?>
		          </tbody>
		        </table>
		      </div>
		    </div>
		  </div>
		</div>
		</div>		
	</div>
    </div>

</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        setInterval(function() {
            $("#widget").load("admin/dashboard #widget");
        }, 10000);  
          setInterval(function() {
            $("#widgetr").load("admin/dashboard #widgetr");
        }, 10000);
    });
</script>
