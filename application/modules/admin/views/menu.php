<?php  
    $this->load->view('includes/header');
?>    
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
<?php
/**
 * [Check the mode of view, if all it will list all foods]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') : ?>   
<!-- Start Listing All Cities -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Menus </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL?>cms/addcms" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
         <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                     
              <th> Menu Name </th>
              <th> Submenu Name </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody> 
        <?php if(isset($menus) && is_array($menus) && count($menus)){ $i = 1;?>
          <?php foreach ($menus as $key  => $row) { ?> 
            <tr class="odd gradeX">
             <td><?php echo ucfirst($row['menuname']) ; ?></td>
             <td><?php echo ucfirst($row['submenu_name']); ?></td>
              <td class="hidden-phone" style="width:350px">
              <a href="<?php ADMIN_URL;?>cms/viewcms/<?php echo $row['id'];?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>cms/editcms/<?php echo $row['id'];?>"> <i class="icon-edit"> </i> Edit </a>
		<span class="btn btn-danger" onclick="getid(<?php echo $row['id']?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
		<?php if($row['status']==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>cms/deactivate/<?php echo $row['id']?>"> <i class="icon-ok icon-white"> </i>Active</a>
                
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>cms/active/<?php echo $row['id']?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
		<?php endif; ?>
                         

               <!--  <a href="#" class="btn mini black"> <i class="icon-trash"> </i> Delete </a> -->
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>

       <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Menu </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>cms" class="icon-arrow-left"> </a> </span>
              </div>
              
             
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL?>cms/addmenu" class="form-horizontal" method="post" id="menu_form" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label">Menu Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" placeholder="Please enter name" onblur="checkexist()" id="menuname" name="menuname" value=""/>
				    <span id="err_text"></span>
                </div>
                <input type="hidden" name='id' value="">
                </div>
                <br />
                
                <div class="control-group">
                  <label class="control-label">Select Main Menu </label>
                  <div class="controls">
                                  
                     <?php
                    
                     $addmenu['0'] = 'Main Menu'; 
                     $attributes= 'id="menuid" onchange="checkexist()" class="span6 required" tabindex="6" ';
		     echo form_dropdown('id', $addmenu,'0',$attributes);
                    ?>
                </div>
                </div>
                <br />             
             
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="savemenu"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Menu </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>cms" class="icon-arrow-left"> </a> </span>
              </div>
              
             
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL; ?>cms/editmenu" class="form-horizontal" method="post" id="menu_form" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label">Menu Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" id="menuname" onblur="checkexist()" name" name="menuname" value="<?php echo $details->menuname?>"/>
                   <!-- <span id="err_text"></span> -->                
                </div>
                <input type="hidden" name='mid' value="<?php echo $details->id?>">
                </div>
                <br />
                
                <div class="control-group">
                  <label class="control-label">Select Restaurant </label>
                  <div class="controls">
                                           
                     <?php
                    
                    //echo "<pre>";print_r($allcities);echo "</pre>";
                     $addmenus['0'] = 'Main Menu'; 
                     $attributes= 'id="menuid" onchange="checkexist()" class="span6 required" tabindex="6" required';

                      echo form_dropdown('id', $addmenus,$details->submenu_id,$attributes );
                    ?>
                </div>
                </div>
                <br />
             
             
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="savemenu"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'): ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Menu  </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL; ?>cms" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Menu Name : </td>
                        <td> <?php echo ucfirst($details -> menuname);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> SubMenu : </td>
                        <td> <?php if(isset($submenu)) { echo ucfirst($submenu -> menuname);} else echo 'Main Menu';?></td>
                      </tr>
                      <tr>
                    </tbody>
                  </table>
                </div>
  </div>
</div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script> 


  jQuery(document).ready(function(){

	  $("#menu_form").validate({
		rules: {
		    "name":"required"
		    },
		messages: {
		  "menuname": {
		    required: "Enter a menu name"
		  }
		}  
	  });
    }); 
  function getid(id){
  	
      //alert(id);  
      if (confirm('Do you want to delete this menu?')) 
      {
 	   	$.ajax({
		type: 'POST',
                url: '<?php echo ADMIN_URL;?>cms/delete',
                data: { sid: id}
            })
            .done(function(response){  
				              if(response == 'success'){
					                   window.location.reload(true);
					                   return true;
				              }else{
					                   window.location.reload(true);
					                   return false;
				             }});
  	          }
  	       else{
  	       						window.location.reload(true);
                return false;  	       	
  	       	}
  	       	}
  	            

	
        function checkexist()
        {
            
            var menuname = $('#menuname').val();
		var id = $('#menuid').val();
		
		$.ajax({
			type: 'POST',
            url: '<?php echo ADMIN_URL;?>cms/getmname',
            data: { menuname: menuname , sid: id}
            })
            .done(function(response){   
				if(response == 1){
					//alert('response');
					//$('#err_text').css('color', 'red').html(response); 
					$("#savemenu").removeAttr('disabled');
					return true;
				}else{ 
                                    
					$('#err_text').css('color', 'red').html('Menu Name Already Exist'); 
					$("#savemenu").attr('disabled','disabled');
					
					return false;
				} });
           
        }
</script>


<?php $this->load->view('includes/footer'); ?>


