<style>
   .restrict-offer-popup {position: fixed; width: 60%; /*height: 60%;*/ left: 20%; top: 20%; background-color: white; border: 2px solid grey; }
   .restrict-offer-popup textarea {left: 10%; top: 10%; width: 70%; height: 250px; position: relative; }
   .restrict-offer-popup button {top: 80%; left: 85%; position: absolute}
</style>
<script>
    function sendRestrictEmail(button) {
        var message = jQuery("#restrict_message");
        var textarea = button.parentNode.querySelector("textarea");
        var email = textarea.getAttribute("email");
        var message = textarea.value;
        $.ajax({
            type: "POST",
            'url' : '<?php echo ADMIN_URL;?>offers/send_restrict',
            'async' : false,
            'data' : {
                "message" : message,
                "email" : email
            }
        }).responseText;
    }
</script>
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 <div class="restrict-offer-popup" style="display: none">
     <h3 style="text-align: center">Restrict message text</h3>
     <textarea id="restrict_message" style="resize: none">
         Dear, User
Your offer  was rejected because it failed our terms and conditions.But we want to work with you so you can correct your offer and get it live as soon as possible. Ok?

The specific reason your offer was rejected was: [reason]

You should:

Please correct the above issue and we will get you up and running in no time.

With best regards,
Gymscanner Customer Relations
www.gymscanner.com
     </textarea>
     <button class="btn btn-info" onclick="sendRestrictEmail(this); $(this.parentNode).hide(); ">Send</button>
 </div>

<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>

<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Offers </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>offers/add_offer" class="icon-plus"></a> </span>
      </div>
      <div class="widget-body">
     <?php if($this->session->flashdata('success')!=''){?>

        <div><h4 class="success"><?php echo $this->session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this->session->flashdata('delete')!=''){?>

        <div><h4 class="error"><?php echo $this->session->flashdata('delete');?></h4></div>
      <?php } ?>

      <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th>Gym Name</th>
              <th> Details </th>
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>

          <?php

          //echo "<pre>";print_r($offers);echo "</pre>";exit;
           if(isset($offers)  && count($offers)){ $i = 1;

          ?>
          <?php foreach ($offers as $row)  { ?>
            <tr class="odd gradeX" id="ofr_<?php echo $row->id;?>">
             <td><?php echo $row->gymname; ?></td>
             <?php $res = $this->db->where(array('offer_relation_id'=>$row->id,'status !='=>'3'))->get('offer_description')->result();?>
              <td>
              <table class="table table-striped table-bordered">
                <thead>
                <tr>                  
                  <th>Duration </th>
                  <th>Membership </th>
                  <th>Discount </th>
                  <th>Offer </th>
                  <th>Description </th>
                  <th class="hidden-phone">Status </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($res as $key => $value){?>
                <tr id="div_<?php echo $value->id;?>">                  
                  <td><?php echo $value->duration; ?></td>
                  <td><?php echo $value->membership; ?></td>
                  <td><?php echo $value->discount; ?></td>
                  <td><?php echo $value->offer; ?></td>
                  <td><?php echo $value->description; ?></td>
                  <td>
                    <?php if($value->status==0){?>
                      <a class="btn btn-info" id="<?php echo $value->id; ?>" onclick="status_change_user(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>', '<?php echo $row->email?>')" > <i class="icon-lock"> </i></a>
                    <?php } else if($value->status == 1){ ?>
                      <a class="btn btn-success" id="<?php echo $value->id; ?>" onclick="status_change_user(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>', '<?php echo $row->email?>')" > <i class="icon-thumbs-up"> </i></a>
                    <?php } else if($value->status == 2){?>
                      <a class="btn btn-danger" id="<?php echo $value->id; ?>" onclick="status_change_user(<?php echo $value->id; ?>,<?php echo $value->status; ?>,'<?php echo $value->id; ?>', '<?php echo $row->email?>')" > <i class="icon-thumbs-down"> </i></a>
                    <?php } ?>
                      <span class="btn btn-danger" onclick="del_row(<?php echo $value->id;?>)" > <i class="icon-remove icon-white"> </i></span> 
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </td>  
            <td class="hidden-phone">
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>offers/edit/<?php echo $row->id;?>"> <i class="icon-edit"> </i></a>   
                <span class="btn btn-danger" onclick="del_offr(<?php echo $row->id;?>)" > <i class="icon-remove icon-white"> </i></span> 
            </td>
          </tr>
          <?php $i++; } } ?>
        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Offer </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>offers" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>offers/add" class="form-horizontal" method="post" id="add_notify" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                <div class="control-group">
                  <label class="control-label"> Select Facility </label>
                  <div class="controls">
                    <?php $attributes = 'id="gym_id" class="required" onchange="chk_gym(this.value)" '; 
                        $gyms[''] = 'Choose Facility';
                        echo form_dropdown('gym',$gyms,'',$attributes);?>
                        <span class="error_gym"></span>
                  </div>
                </div>       
                
                <div class="control-group">
                  <label class="control-label"> Offer Details </label>
                  <div class="controls">
                    <table class="table table-striped table-bordered new_tab">
                      <tr>
                        <td>Duration</td>
                        <td>Membership</td>
                        <td>Discount<br/> (if any)</td>
                        <td>Exclusive Offer <br/>(Compulsory)</td>
                        <td>Customer Get</td>
                        <td id="add_more"><b>+ Add</b></td>
                      </tr>
                      <tr class="row_cal row_cen">
                        <td style="text-align:center;"><input type="text" class="span12" name="duration[]" required/><p><lable>Example: 1 Month</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span6" name="membership[]" required/><p><lable>Example: US$ 30</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span3" name="discount[]" /><p><lable>Example: 10% off</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span12" name="offer[]" required/><p><lable>Example: 1 Week Free</lable></p></td>
                        <td style="text-align:center;"></td>
                        <td id="clear_0"></td>
                      </tr>
                      <tr class="row_cal row_cen">
                        <td style="text-align:center;"><input type="text" class="span12" name="duration[]" /><p><lable>Example: 1 Month</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span6" name="membership[]" /><p><lable>Example: US$ 30</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span3" name="discount[]" /><p><lable>Example: 10% off</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span12" name="offer[]" /><p><lable>Example: 1 Week Free</lable></p></td>
                        <td style="text-align:center;"></td>
                        <td id="clear_0"></td>
                      </tr>
                      <tr class="row_cal row_cen">
                        <td style="text-align:center;"><input type="text" class="span12" name="duration[]" /><p><lable>Example: 1 Month</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span6" name="membership[]" /><p><lable>Example: US$ 30</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span3" name="discount[]" /><p><lable>Example: 10% off</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span12" name="offer[]" /><p><lable>Example: 1 Week Free</lable></p></td>
                        <td style="text-align:center;"></td>
                        <td id="clear_0"></td>
                      </tr>
                      <tr class="row_cal row_cen">
                        <td style="text-align:center;"><input type="text" class="span12" name="duration[]" /><p><lable>Example: 1 Month</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span6" name="membership[]" /><p><lable>Example: US$ 30</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span3" name="discount[]" /><p><lable>Example: 10% off</lable></p></td>
                        <td style="text-align:center;"><input type="text" class="span12" name="offer[]" /><p><lable>Example: 1 Week Free</lable></p></td>
                        <td style="text-align:center;"></td>
                        <td id="clear_0"></td>
                      </tr>
                    </table>
                  </div>
                </div>                

                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="sub_new"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'):?>

    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Edit Offer </h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>offers" class="icon-arrow-left"></a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>offers/update_offr" class="form-horizontal" method="post" id="add_notify" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?> 
                
                  <div class="control-group">
                    <label class="control-label"> Select Gym </label>
                    <div class="controls">
                      <?php if(isset($details)){ ?>
                        <?php 
                        $attributes1 = 'class="form_big" onchange="chk_edit_gym(this.value)" '; 
                        echo form_dropdown('gym',$gyms,$details[0]->gym_id,$attributes1); ?>
                        <span class="error_gym"></span>

                      <input type="hidden" id="old_gym" name="old_gym" value="<?php echo $details[0]->gym_id;?>"/>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label"> Offer Details </label>
                    <div class="controls">
                      <table border="2px" class="table table-striped table-bordered new_tab">
                        <tr>
                          <td>Duration</td>
                          <td>Membership</td>
                          <td>Discount<br/> (if any)</td>
                          <td>Exclusive Offer <br/>(Compulsory)</td>
                          <td>Customer Get</td>
                          <td id="edit_more"><b>+ Add</b></td>
                        </tr>
                        <?php $i = 0; //echo count($details);
                          foreach ($details as $key => $value) { ?>
                            <input type="hidden" name="offr_id[]" value="<?php echo $value->id?>"/>
                            <input type="hidden" name="relation_id" value="<?php echo $details[0]->offer_relation_id?>"/>
                            <tr class="row_cal center_mar">
                              <td style="text-align:center;"><input type="text" class="span12" name="duration[]" value="<?php echo $value->duration;?>" /></td>
                              <td style="text-align:center;"><input type="text" class="span6" name="membership[]" value="<?php echo $value->membership;?>"/></td>
                              <td style="text-align:center;"><input type="text" class="span3" name="discount[]" value="<?php echo $value->discount;?>"/></td>
                              <td style="text-align:center;"><input type="text" class="span12" name="offer[]" value="<?php echo $value->offer;?>"/></td>
                              <td style="text-align:center;"><?php echo $value->description;?></td>
                              <td id="clear_0"></td>
                            </tr>
                        <?php $i++; } ?>
                        <?php while($i < 4){?>
                        <tr class="row_cal row_cen">
                          <td style="text-align:center;"><input type="text" class="span12" name="duration[]" /><p><lable>Example: 1 Month</lable></p></td>
                          <td style="text-align:center;"><input type="text" class="span6" name="membership[]" /><p><lable>Example: US$ 30</lable></p></td>
                          <td style="text-align:center;"><input type="text" class="span3" name="discount[]" /><p><lable>Example: 10% off</lable></p></td>
                          <td style="text-align:center;"><input type="text" class="span12" name="offer[]" /><p><lable>Example: 1 Week Free</lable></p></td>
                          <td style="text-align:center;"></td>
                          <td id="clear_0"></td>
                        </tr>
                        <?php $i++; }?>
                      </table> 
                    </div>
                  </div>
                     
                <br />
                <div class="form-actions">
                  <button type="submit" name="addmem" class="btn btn-success" id="edit_sub"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>

<script type="text/javascript">
  var isanyerror=false;
  $(document).on('click','#sub_new',function(){
    var gym = $('#gym_id').val();
    if($.trim(gym) == ''){
      var result1 = 'Please Choose facility';
      $(".error_gym").text(result1).css('color','red');
      return false;
    }else{
  if(!isanyerror)
      return true;
      else
      return false;
    }
  });
</script>
<script type="text/javascript">
  var cnt = 0;
  $(document).on('click','#add_more',function(){
    cnt = cnt + 1; 
    $('.new_tab').append('<tr class="row_cal row_cen" id="new_'+cnt+'"><td style="text-align:center;"><input class="span12" type="text" name="duration[]"/><p><lable>Example: 1 Month</lable></p></td><td style="text-align:center;"><input class="span6" type="text" name="membership[]"/><p><lable>Example: US$ 30</lable></p></td><td style="text-align:center;"><input class="span3" type="text" name="discount[]"/><p><lable>Example: 10% off</lable></p></td><td style="text-align:center;"><input class="span12" type="text" name="offer[]"/><p><lable>Example: 1 Week Free</lable></p></td><td style="text-align:center;"><input class="center_mar" type="hidden" name="description[]"/></td><td><button type="button" class="btn btn-warning " onclick="remove_btn('+cnt+')" >X</button></td></tr>')
  });
  function remove_btn(id){
      $('#new_'+id).remove();
  }
</script>

<script type="text/javascript">

  $(document).on('focusout', '.row_cal input', function(event){  
    $('.row_cal').each(function(){
      var first=$(this).find('td:first-child input').val();
      var second=$(this).find('td:nth-child(2) input').val();
      var third=$(this).find('td:nth-child(3) input').val();
      var fourth=$(this).find('td:nth-child(4) input').val();
      if($.trim(first)!='' && $.trim(fourth)!='')
      var total=first+' Membership + '+fourth;
      if($.trim(second)!='')
      total+=' for US$ '+(second-(second*third/100));
      //$(this).find('td:nth-child(5) td').val(total);
      $(this).find('td:nth-child(5)').text(total);
    });
    });
</script>
<script>
  function chk_gym(gym){
    $('.error_gym').text('');
    var gym = $('#gym_id').val();
    if($.trim(gym) == ''){
      var result1 = 'Please Choose facility';
      $(".error_gym").text(result1).css('color','red');
      return false;
    }else{
      var result1 = $.ajax({
                   'url' : '<?php echo ADMIN_URL;?>offers/check_gym/' + gym,
                   'async' : false
             }).responseText;
         $(".error_gym").text(result1).css('color','red');
         if($.trim(result1)!='')
         isanyerror=true;
         else
         isanyerror=false;
    }
  }
</script>
<script type="text/javascript">
  var isediterror=false;
  $(document).on('click','#edit_sub',function(){
  if(!isediterror)
      return true;
      else
      return false;
  });
</script>
<script type="text/javascript">
  var cnt = 0;
  $(document).on('click','#edit_more',function(){
    cnt = cnt + 1; 
    $('.new_tab').append('<tr class="row_cal row_cen" id="new_'+cnt+'"><td style="text-align:center;"><input class="span12" type="text" name="duration[]"/><p><lable>Example: 1 Month</lable></p></td><td style="text-align:center;"><input class="span6" type="text" name="membership[]"/><p><lable>Example: US$ 30</lable></p></td><td style="text-align:center;"><input class="span3" type="text" name="discount[]"/><p><lable>Example: 10% off</lable></p></td><td style="text-align:center;"><input class="span12" type="text" name="offer[]"/><p><lable>Example: 1 Week Free</lable></p></td><td style="text-align:center;"><input class="center_mar" type="hidden" name="description[]"/></td><td><button type="button" class="btn btn-warning " onclick="remove_btn('+cnt+')" >X</button></td></tr>')
  });
  function remove_btn(id){
      $('#new_'+id).remove();
  }
</script>

<script>
  function chk_edit_gym(gym){
    var old_gym = $('#old_gym').val();
    $('.error_gym').text('');
    if(gym != old_gym){
      var result = $.ajax({
                   'url' : '<?php echo ADMIN_URL;?>offers/check_gym/' + gym,
                   'async' : false
             }).responseText;
         $(".error_gym").text(result).css('color','red');
         if($.trim(result)!='')
         isediterror=true;
         else
         isediterror=false;
    }else{
      isediterror=false;
    }
  } 
</script>

<script type="text/javascript">

  function status_change_user(id,status,aid, email)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo ADMIN_URL;?>offers/change_status",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '0'){
                    $('.restrict-offer-popup').hide();
                  $('#'+aid).removeClass('btn btn-info').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'", "' + email + '")');
                }else if(status == '1'){
                  $('.restrict-offer-popup').show();
                  $('.restrict-offer-popup textarea').attr("email", email);
                  $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',2,"'+aid+'", "' + email + '")');
                }else if(status == '2'){
                    $('.restrict-offer-popup').hide();
                  $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_user('+id+',1,"'+aid+'", "' + email + '")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>
<script>
  function del_row(selected){
      $.ajax({
        type : "POST",
        url : "<?php echo ADMIN_URL?>offers/del_row",
        data : {
          'id':selected
        },
        success : function(data){
          if(data == 1){
            alert('sorry, Users allocated to this offer');
          }else{
            $('#div_'+selected).hide();
          }
        },
        error : function(){
          alert("We are unable to do your request. Please contact webadmin");
        }
      });
  }
</script>
<script>
  function del_offr(selected){
      $.ajax({
        type : "POST",
        url : "<?php echo ADMIN_URL?>offers/del_offr",
        data : {
          'id':selected
        },
        success : function(data){
          if(data == 1){
            alert('sorry, Users allocated to this offer');
          }else{
            $('#ofr_'+selected).hide();
          }
        },
        error : function(){
          alert("We are unable to do your request. Please contact webadmin");
        }
      });
  }
</script>