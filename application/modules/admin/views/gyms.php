<style>
    table {
        table-layout: fixed;
        width:100%
    }
    td {
        word-wrap:break-word;
    }
</style>
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
      <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
      <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
      <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
      <!-- Dependencies: JQuery and GMaps API should be loaded first
      <script src="js/jquery-1.7.2.min.js"></script> -->
      <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

      <!-- CSS and JS for our Map code
      <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/jquery-gmaps-latlon-picker.css"/>
      <script src="<?php echo base_url();?>themes/admin/js/jquery-gmaps-latlon-picker.js"></script>
<?php
/**
 * [Check the mode of view, if all it will list all gym]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') :  ?>
<!-- Start Listing All gyms -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Gyms </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>gyms/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">


          <table class="table table-condensed table-bordered" id="sample_1">
              <thead>

              <th> Logo </th>
              <th> Name </th>
              <th> Open Time </th>
              <th> Close Time </th>
              <th> Rating </th>
              <th> Address </th>
              <th> Specialty </th>
              <th> Price </th>
              <th> Contact Details </th>
              <th style="width:170px;"> Actions </th>

              </thead>
              <tbody>



              <?php if(isset($gyms) && is_array($gyms) && count($gyms)){ $i = 1;?>
                  <?php foreach ($gyms as $key => $gym) {
                      $gymname = str_replace('[@]', '<br/>',  $gym->gymname);
                      ?>
                      <tr class="" id="div_<?= $gym->id;?>">
                          <td><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym -> id ;?>">
                                  <?php $img = $gym->gym_logo; ?>
                                  <img src="<?php echo base_url()?>uploads/gym/logos/<?php echo $img; ?> " alt="image" height="auto" width="auto" /></a>  </td>
                          <td><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym -> id ;?>"><?php echo ucfirst($gymname) ; ?></a></td>
                          <td ><?php echo $gym->open_time; ?></td>
                          <td><?php echo $gym->close_time; ?></td>
                          <td><?php for($j = 0;$j < $gym->star; $j++){ ?> <img src="<?php echo base_url()?>themes/admin/img/star-on.png" /> <?php  }?></td>
                          <td><?php echo ucfirst($gym->gstreet).', ' ;
                              echo ucfirst($gym->city).', ' ;
                              echo ucfirst($gym->state).', ' ;
                              echo ucfirst($gym->country_name).', ' ;
                              echo $gym->gzip ;?></td>
                          <td><?php if($gym->speciality ==1){echo 'Men';} else if($gym->speciality ==2){echo 'Women';} else{echo 'All';} ?></td>
                          </td>
                          <td><?=$gym->price;?></td>
                          <td><?php echo "Name: ". ucfirst($gym->owner) ; echo '<br/>'; echo "Ph no: ". $gym->ow_phone ; echo '<br/>'; echo "Mail id: ".$gym->ow_email ; ?></td>
                          <td>
                              <a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym ->id;?>" class="btn mini black"> <i class="icon-eye-open"> </i></a>
                              <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>gyms/edit/<?php echo $gym->id;?>"> <i class="icon-edit"> </i></a>
                              <?php if($gym->status==0){?>
                                  <a class="btn btn-info" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-lock"> </i></a>
                              <?php } else if($gym->status == 1){ ?>
                                  <a class="btn btn-success" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-thumbs-up"> </i></a>
                              <?php } else if($gym->status == 2){?>
                                  <a class="btn btn-danger" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                              <?php } else if($gym->status == 3){?>
                                  <a class="btn btn-warning" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                              <?php } ?>
                              <span class="btn btn-danger" onclick="del_gym(<?php echo $gym->id;?>)" > <i class="icon-remove icon-white"> </i></span>
                          </td>
                      </tr>
                      <?php $i++; } } ?>




              </tbody>
          </table>





      <?php if($this ->session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>

       <?php if($this ->session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>

      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Gym Details </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>gyms" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL;?>gyms/add" class="form-horizontal" method="post" id="gym_form" enctype = "multipart/form-data" >
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <?php if($this -> session -> flashdata('failure')!=''){?>
                  <div><h4 class="error"><?php echo $this -> session -> flashdata('failure');?></h4></div>
                <?php } ?>
                <span id="ytlInfo"></span>
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Name" id="gymname" name="gymname" value="<?php echo set_value('gymname');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Logo </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="logo" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Images </label>
                  <div class="controls">
                    <input type="file" name="upload_file[]">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_images" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <div id="add_more"></div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Video </label>
                  <div class="controls">
                    <input name="gym_video[]" id="youtube" class="span6 youtube" type="text" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_urls" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <div id="add_videos"></div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <div id="datetimepicker3" style="float:left;">
                      <input data-format="hh:mm:ss" type="text" class="span6 required" name="open_time" placeholder="Open Time" readonly/>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                    <div id="datetimepicker4" style="float:left;">
                      <input data-format="hh:mm:ss" type="text" class="span6 required" name="close_time" placeholder="Close Time" readonly/>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Rating </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Rating" id="star" name="star" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Specialty </label>
                  <div class="controls">
                    <select name="speciality" id="specialty" class="chosen span6 required" >
					     <option value="1"  selected="selected">Men</option>
					     <option value="2">Women</option>
					     <option value="3">All</option>
                   </select>
                  </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Comments </label>
                  <div class="controls">
                    <input class="span6 " type="text" placeholder=" Comment" id="comments" name="comments" value=""/>
                  </div>
                </div> -->
                <div class="control-group">
                  <label class="control-label"> Country </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter Country" id="gcountry" name="gcountry" value=""/> -->
                    <?php $cntry[''] = 'Country';$attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="span6 required"';
                          echo form_dropdown('cntry', $cntry, '',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State </label>
                  <div class="controls">
                    <?php $attributes= 'id="state"  class="span6 required" onchange="get_cities(this.value)"';
                      echo form_dropdown('state', array(), NULL,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter City" id="gcity" name="gcity" value=""/> -->
                    <?php $attributes= 'id="city"  class="span6 required" ';
                      echo form_dropdown('city', array(), NULL,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Street </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Street" id="gstreet" name="gstreet" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Zip code </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Zip Code" id="gzip" name="gzip" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Category </label>
                  <div class="controls">
                   <?php $cat['']='Category';$attributes= 'id="cat_id" class="span6 required" ';
                      echo form_dropdown('cat_id', $cat, '',$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Advanced </label>
                  <div class="controls">
                    <input class="checkbox" type="checkbox" name="adv[pilates]" value="1" >PILATES/YOGA
                    <input class="checkbox" type="checkbox" name="adv[mma]" value="1" >MMA
                    <input class="checkbox" type="checkbox" name="adv[boxing]" value="1" >BOXING
                    <input class="checkbox" type="checkbox" name="adv[cross]" value="1" >CROSS TRAINING
                    <input class="checkbox" type="checkbox" name="adv[spinning]" value="1" >SPINNING
                    <input class="checkbox" type="checkbox" name="adv[gymnastics]" value="1" >GYMNASTICS
                    <input class="checkbox" type="checkbox" name="adv[martial]" value="1" >MARTIAL ARTS
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Price Range </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter pricerange " id="pricerange" name="pricerange" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Price </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter price" id="price" name="price" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Contact Person </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter owner" id="owner" name="owner" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Phone No </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Ph No" id="ow_phone" name="ow_phone" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Email" id="ow_email" name="ow_email" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span6 required" name="description"></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="type">Location</label>
                  <div class="controls">
                      <fieldset class="gllpLatlonPicker" id="custom_id" >
                          <input type="text" name="search_location" class="gllpSearchField" value="" required/>
                          <input type="button" name="search" class="gllpSearchButton" value="search">
                          <br/><br/>
                          <div class="gllpMap">Google Maps</div>
                          <input type="hidden" class="gllpLatitude" name="latitude" value="60"/>
                          <input type="hidden" class="gllpLongitude" name="longitude" value="30"/>
                          <input type="hidden" class="gllpZoom" name="zoom" value="12"/>
                      </fieldset>
                  </div> <!-- /controls -->
                </div>
                <div class="form-actions">
                  <button type="submit" name="addgym" class="btn btn-success" id="savegym"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Edit Gym </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>gyms" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL; ?>gyms/edit" class="form-horizontal" method="post" id="gym_edit_form" enctype = "multipart/form-data"/>
                <?php if($this->session-> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <span id="ytlInfo"></span>
                <div class="control-group">
                  <label class="control-label"> Logo </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="logo" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                      <input type="hidden" name="old_image" value="<?=$gym->gym_logo?>" />

                     <?php if($gym->gym_logo!='') { ?>
                    <img src="<?=base_url()?>uploads/gym/logos/<?=$gym->gym_logo?>" width="90" alt="image">
                    <?php }?>
                    </div>
                  </div>
                </div>

                <br />
                <div>
                  <label class="control-label"> Made By </label>
                  <div >
                    <input type="radio" name="made_by" value="1" class="radio" checked> Admin &nbsp;
                    <input type="radio" name="made_by" value="2" class="radio"> Supplier
                    </div>
                </div>

                <br />
                <div class="control-group">
                  <label class="control-label"> Select Suppliers </label>
                  <div class="controls">
					   <?php $attributes= 'id="is_supplier" class="chosen span6"';
                      echo form_dropdown('is_supplier', $suppliers, $gym->is_supplier, $attributes);
                    ?>

					</div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $gym->id ?>"/>
                    <input type="text" name="gymname" value="<?php echo $gym->gymname; ?>"/>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Gym Images </label>
                  <div class="controls">
                      <?php foreach ($gimgs as $key => $value) {
                          $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
                        <span style="position:relative" id="img_<?php echo $value->id;?>">
                            <img src="<?php echo $image;?>" width="100px" height="50"> &nbsp;&nbsp;
                            <i class="icon-remove" name="<?php echo $value->id?>" id="<?php echo $value->gym_images?>" style="position:absolute;margin-top:-10px;margin-left:-10px;color:red;"></i></span>
                      <?php } ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="file" name="upload_file[]">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_images" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Video </label>
                    <?php if($gym) {
                      $videos = $gym->gym_video;
                      $res = explode('~',$gym->gym_video);
                        foreach($res as $value){ ?>
                        <div class="controls">
                          <input name="gym_video[]" class="span6 youtube" type="text" value="<?php echo $value;?>" />
                        </div>
                    <?php } } ?>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_urls" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <div id="add_videos"></div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <div id="datetimepicker3" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="open_time" placeholder="Open Time" readonly value="<?php echo $gym->open_time;?>" />
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                    <div id="datetimepicker4" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="close_time" placeholder="Close Time" readonly value="<?php echo $gym->close_time;?>" />
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Rating </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Rating" id="star" name="star" value="<?php echo $gym->star;?>"/>
                  </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Comments </label>
                  <div class="controls">
                    <input class="span6 required" type="text" id="comments" name="comments" value="<?php echo $gym->comments;?>"/>
                  </div>
                </div> -->
                <div class="control-group">
                  <label class="control-label"> Country </label>
                  <div class="controls">
                    <?php $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="chosen span6 required"';
                      echo form_dropdown('cntry', $cntry, $gym->gcountry,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State </label>
                  <div class="controls">
                    <?php $attributes= 'id="state" class="span6 required" tabindex="6" required onchange="get_cities(this.value)"';
                         echo form_dropdown('state', $states, $gym->gstate,$attributes);
                     ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City </label>
                  <div class="controls">
                     <?php $attributes= 'id="city" class="span6 required" tabindex="6" required';
                         echo form_dropdown('city', $city, $gym->gcity,$attributes);
                     ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Street </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Street" id="gstreet" name="gstreet" value="<?php echo ucfirst($gym->gstreet);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Zip code </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Zip Code" id="gzip" name="gzip" value="<?php echo ucfirst($gym->gzip);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Specialty </label>
                  <div class="controls">
                    <?php $specialty = $gym->speciality; ?>
                    <select name="speciality" id="specialty" class="chosen span6 required" >
        					    <option value="1"  <?php if($specialty == 1){ ?> selected="selected" <?php } ?>>Men Only</option>
        					    <option value="2" <?php if($specialty == 2){ ?> selected="selected" <?php } ?>>Women Only</option>
        					    <option value="3" <?php if($specialty == 3){ ?> selected="selected" <?php } ?>>Mixed</option>
                   </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Category </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter Country" id="gcountry" name="gcountry" value="<?php echo ucfirst($gym->gcountry);?>"/> -->
                    <?php $attributes= 'id="cat_id" class="span6 required" ';
                      echo form_dropdown('cat_id', $cat, $gym->category,$attributes);
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Advanced </label>
                  <div class="controls">
                    <input class="checkbox" type="checkbox" name="adv[pilates]" value="1" <?php if (in_array('pilates', $adv)) echo "checked"; ?> />PILATES/YOGA
                    <input class="checkbox" type="checkbox" name="adv[mma]" value="1" <?php if (in_array('mma', $adv)) echo "checked"; ?> />MMA
                    <input class="checkbox" type="checkbox" name="adv[boxing]" value="1" <?php if (in_array('boxing', $adv)) echo "checked"; ?> />BOXING
                    <input class="checkbox" type="checkbox" name="adv[cross]" value="1" <?php if (in_array('cross', $adv)) echo "checked"; ?> />CROSS TRAINING
                    <input class="checkbox" type="checkbox" name="adv[spinning]" value="1" <?php if (in_array('spinning', $adv)) echo "checked"; ?> />SPINNING
                    <input class="checkbox" type="checkbox" name="adv[gymnastics]" value="1" <?php if (in_array('gymnastics', $adv)) echo "checked"; ?> />GYMNASTICS
                    <input class="checkbox" type="checkbox" name="adv[martial]" value="1" <?php if (in_array('martial', $adv)) echo "checked"; ?> />MARTIAL ARTS
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Price Range </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Pricerange " id="pricerange" name="pricerange" value="<?php echo $gym->price_range;?>"/>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label"> Price </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter price" id="price" name="price" value="<?php echo $gym->price;?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Contact Person </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Owner" id="owner" name="owner" value="<?php echo ucfirst($gym->owner);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Phone No </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Ph No" id="ow_phone" name="ow_phone" value="<?php echo ucfirst($gym->ow_phone);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Email" id="ow_email" name="ow_email" value="<?php echo ucfirst($gym->ow_email);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Description </label>
                  <div class="controls">
                    <textarea class="span6 required" name="description"><?php echo $gym->description;?></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="type">Location</label>
                  <div class="controls">
                      <fieldset class="gllpLatlonPicker" id="custom_id" >
                          <input type="text" name="search_location" class="gllpSearchField" value="<?php echo ucfirst($gym->location);?>" required/>
                          <input type="button" name="search" class="gllpSearchButton" value="search">
                          <br/><br/>
                          <div class="gllpMap">Google Maps</div>
                          <input type="hidden" class="gllpLatitude" name="latitude" value="<?php echo ucfirst($gym->gmap_lat);?>"/>
                          <input type="hidden" class="gllpLongitude" name="longitude" value="<?php echo ucfirst($gym->gmap_long);?>"/>
                          <input type="hidden" class="gllpZoom" name="zoom" value="10"/>
                      </fieldset>
                  </div> <!-- /controls -->
                </div>
                <br />
                <div class="form-actions">
                  <button type="submit" name="addgym" class="btn btn-success" id="editgym"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'): ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Gym Details </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL ?>gyms" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Gym Logo : </td>
                        <td> <?php $img = $gym->gym_logo;?>
                             <img src="<?php echo base_url()?>uploads/gym/logos/<?php echo $img; ?> " alt="image" height="70px" width="100px" />
                        </td>
                      </tr>
                      <tr>
                        <td class="span3"> Name : </td>
                        <td> <?php echo ucfirst($gym->gymname);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Images : </td>
                        <td>
                          <?php foreach ($gimgs as $key => $value) {
                            $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
                              <img src="<?php echo $image;?>" width="100px" height="50"> &nbsp;&nbsp;
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="span3"> Gym Video : </td>
                        <td>
                          <?php
                            if($gym->gym_video != '') {
                              $videos = $gym->gym_video;
                              $res = explode('~',$gym->gym_video);
                              foreach($res as $value){
                                $var = explode('v=',$value);
                                $new = explode('/',$value);
                                if($new[3] ||$var[1]) {
                                  if(strpos($value,'vimeo') !== false)
                                  {?>
                                      <iframe src="http://player.vimeo.com/video/<?=$new[3]?>" width="220" height="145" frameborder="1"></iframe>
                                  <?php }

                                  if (strpos($value,'youtube') !== false)
                                  { ?>
                                      <iframe width="220" height="145" src="http://www.youtube.com/embed/<?=$var[1]?>" frameborder="1"> </iframe>
                                  <?php }
                                } else {?>

                            <iframe width="220" height="145" src="<?=$product->video_url?>" frameborder="1"> </iframe>

                          <?php } } }?>
                        </td>
                      </tr>
                      <tr>
                        <td class="span3"> Open Time : </td>
                        <td> <?php echo $gym ->open_time;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Timings : </td>
                        <td> <?php echo $gym ->close_time;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Rating : </td>
                        <td> <?php for($j = 0;$j < $gym->star; $j++){ ?> <img src="<?php echo base_url()?>themes/admin/img/star-on.png" /> <?php  }?></td>
                      </tr>
                       <!-- <tr>
                        <td class="span3"> Comments : </td>
                        <td> <?php echo $gym->comments;?>
                          </td>
                      </tr> -->
                      <tr>
                        <td class="span3"> Address : </td>
                        <td> <?php echo ucfirst($gym->gstreet).',' ;
			                       echo ucfirst($gym->city).',';
			                       echo ucfirst($gym->state).',';
			                       echo ucfirst($gym->country_name).',';
			                       echo $gym->gzip ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Map : </td>
                        <td> <?php echo 'Latitude : '; echo $gym->gmap_lat ; echo '<br />';
                                   echo 'Longitude : '; echo $gym->gmap_long ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Specialty : </td>
                        <td> <?php if($gym->speciality ==1){echo 'Men';} else if($gym->speciality ==2){echo 'Women';} else{echo 'All';} ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Category : </td>
                        <td> <?php echo ucfirst($gym->categoryname) ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Price Range : </td>
                        <td> <?php echo ucfirst($gym->price_range) ; ?></td>
                      </tr>

                      <tr>
                        <td class="span3"> Price : </td>
                        <td> <?php echo $gym->price; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Contact Person : </td>
                        <td> <?php echo ucfirst($gym->owner) ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Phone : </td>
                        <td> <?php echo $gym->ow_phone ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Email : </td>
                        <td> <?php echo $gym->ow_email ;?></td>
                      </tr>
					  <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($gym -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
        </div>
    </div>
       <?php endif; ?>
  </div>
</div>
  </div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js"></script>
<script>
  jQuery(document).ready(function(){

    $("#gym_form").validate({

      rules: {
        "gymname":"required",
        "timings":"required",
        "star":{required:true,digits: true,min:1,max:5},
        "upload_file[]":{ required: true, extension: "gif|jpeg|jpg|png"},
        "gstreet":"required",
        "logo":"required",
        "gcity":"required",
        "gstate":"required",
        "gcountry":"required",
        "gzip":"required",
        "speciality":"required",
        "owner":"required",
        "price":{required:true,digits: true},
        "pricerange":{required:true,digits: true,min:1,max:5},
        "ow_phone":{required:true,digits: true,minlength:10},
        "ow_email":{required:true,email: true}
      }
    });
  });
  function load_dropdown_content(selected_value){

    if(selected_value!=''){
      var result = $.ajax({
        'url' : '<?php echo ADMIN_URL;?>gyms/get_states/' + selected_value,
        'async' : false
      }).responseText;
      $("#state").replaceWith(result);
    }
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
        'url' : '<?php echo ADMIN_URL;?>gyms/get_cities/' + state,
        'async' : false
      }).responseText;
      $("#city").replaceWith(result);
    }
  }
  function deleteImage(id, imgname)
  {
    if(confirm('Are you sure want to delete this image?'))
    {
      $('#delimg'+id).remove();
      $('#removeimgdiv').append('<input type="hidden" name="remove_gym_image[]" value="'+imgname+'">');
    }
  }
  function getid(id){
    if (confirm('Do you want to delete this gym?'))
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo ADMIN_URL;?>gyms/delete',
        data: { sid: id}
      })
          .done(function(response){
            if(response == 'success'){
              window.location.reload(true);
              return true;
            }else{
              window.location.reload(true);
              return false;
            }});
    }
    else{
      window.location.reload(true);
      return false;
    }
  }
</script>
<script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
      //pick12HourFormat : true
    });
  });
</script>
<script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickDate: false
    });
  });
</script>
<script>
  $(function (){
    var img_count = 0;
    $('#add_images').on("click",function(){
      img_count = img_count+1;
      $("#add_more").append('<div class="control-group" id="divname'+img_count+'"><label class="control-label" for="address"></label><div class="controls"><input type="file" name="upload_file[]" class="span4" /><button type="button" class="btn btn-warning " onclick="remove_btn('+img_count+')" >Remove</button></div></div>');
      $("#divname"+img_count).hide();
      //$("#jcrop_target"+img_count).hide();

      $( "#divname"+img_count ).slideDown( "slow", function() {
        // Animation complete.
      });
    });
  });
</script>
<script type="text/javascript">
  function remove_btn(id){

    $( "#divname"+id).slideUp( "slow", function() {
      $("#divname"+id).remove();
    });
  }
</script>
<script type="text/javascript">
  function status_change_gym(id,status,aid)
  {
    $.ajax({
      type : "POST",
      url : "<?php echo ADMIN_URL;?>gyms/change_status",
      data : {
        'id': id,
        'status':status
      },
      success : function(data) {
        if(status == '0'){
          $('#'+aid).removeClass('btn btn-info').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
          $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
        }else if(status == '1'){
          $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
          $('#'+aid).attr('onclick','status_change_gym('+id+',2,"'+aid+'")');
        }else if(status == '2'){
          $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
          $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
        }else{
          $('#'+aid).removeClass('btn btn-warning').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
          $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
        }
      },
      error : function() {
        alert("We are unable to do your request. Please contact webadmin");
      }
    });
  }
</script>
<script type="text/javascript">
  $('.icon-remove').click (function () {

    var id = $(this).attr('name');
    var image = $(this).attr('id');
    var result = $.ajax({
      'url' : '<?php echo ADMIN_URL?>gyms/del_img/' + id  + '/' + image,
      'async' : false
    }).responseText;
    $('#img_'+id).hide();
    return false;
  });

</script>
<script>
  function del_gym(selected){
    $.ajax({
      type : "POST",
      url : "<?php echo ADMIN_URL?>gyms/del_gym",
      data : {
        'id':selected
      },
      success : function(data){
        if(data == 1){
          alert('sorry, Users allocated to this cant delete');
        }else{
          $('#div_'+selected).hide();
        }
      },
      error : function(){
        alert("We are unable to do your request. Please contact webadmin");
      }
    });
  }
</script>
<script type="text/javascript">
  function ytVidId(url) {
    var contain1=url.indexOf('youtube');
    var contain2=url.indexOf('vimeo');
    if(contain1>-1)
      var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
    else if(contain2 > -1)
      var p = /http:\/\/(?:www\.|player\.)?(vimeo|youtube)\.com\/(?:embed\/|video\/)?(.*?)(?:\z|$|\?)/;
    else
      return false;
    return (url.match(p)) ? RegExp.$1 : false;
  }

  $(document).on("change keyup input",'.youtube', function() {
    //console.log('hi');
    //$('.ytlInfo').html('');
    var url = $(this).val();
    if (ytVidId(url) !== false) {
      $('#ytlInfo').html('');
    } else {
      $('#ytlInfo').html('give valid url').css('color','red');
    }
  });
</script>
<script>
  $(function (){
    var url_count = 0;
    $('#add_urls').on("click",function(){
      url_count = url_count+1;
      $("#add_videos").append('<div class="control-group" id="div'+url_count+'"><label class="control-label" for="video"></label><div class="controls"><input type="text" required class="span6 youtube" name="gym_video[]" /><button type="button" class="btn btn-warning " onclick="remove_url('+url_count+')" >Remove</button></div></div>');
      $("#div"+url_count).hide();

      $( "#div"+url_count ).slideDown( "slow", function() {
        // Animation complete.
      });
    });
  });
  function remove_url(id){
    $( "#div"+id).slideUp( "slow", function() {
      $("#div"+id).remove();
    });
  }
</script>

