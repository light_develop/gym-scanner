
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Users -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Users </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>users/add" class="icon-plus"> Add User</a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this -> session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> Name </th>
              <th> Mobile </th>
              <th> Email </th>
              <th> Gym Name </th>
              <th> Category </th>
              <th> Membership </th>
              <th> Price </th>
              <th> Joining date </th>
              <th> End date </th>
             <!-- <th class="hidden-phone"> User Group </th>-->
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php
           if(isset($users)  && count($users)){ $i = 1; ?>
          <?php foreach ($users as $row)  { ?>
            <tr class="odd gradeX">
             <td> <?php echo ucfirst($row->username);?> </td>
             <td> <?php echo $row->mobile;?> </td>
             <td> <?php echo $row->email;?> </td>           
             <td> <?php echo $row->gymname;?> </td>
             <td> <?php echo $row->categoryname;?> </td>
             <td> <?php echo $row->membership. " months";?> </td>
             <td> <?php echo $row->price;?> </td>
             <td> <?php echo $row->join_date;?> </td>
             <td> <?php echo $row->end_date;?> </td>
              <td class="hidden-phone"><div style="clear:both;">
              <a href="<?php echo ADMIN_URL;?>users/view/<?php echo $row -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>users/edit/<?php echo $row -> id?>"> <i class="icon-edit"> </i> Edit </a> 
                <!-- <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>users/payment/<?php echo $row -> id?>"> <i class="icon-eye-open"> </i> Payments </a>                   -->
                </div>&nbsp;<div style="clear:both;">
                <span class="btn btn-danger" onclick="getid(<?php echo $row-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                <?php if($row->status==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>users/deactivate/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Active</a>
                
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>users/active/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
                </div>        
                <?php endif; ?>                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Users </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>users/add" class="form-horizontal" method="post" id="user_profile_add" enctype = "multipart/form-data"/>
                <div class="error"> </div>
                <div class="control-group">
                  <label class="control-label"> User Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="username" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Category </label>
                    <div class="controls">
                     <?php $cat[''] = 'Select Category'; $attributes= 'id="cat_id" class="chosen span6 required" onchange="get_gyms(this.value)" ';
                       echo form_dropdown('cat_id', $cat, '',$attributes);
                     ?>
                    </div>
                </div>                
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                    <div class="controls">
                    <?php $attributes= 'id="gym_id"  class="chosen span6 required" onchange="get_ofrs(this.value)" ';
                          echo form_dropdown('gym_id', array(), NULL,$attributes);                     
                    ?>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Offers </label>
                    <div class="controls">
                      <div class="setofrs">
                        <table width="100%" border="0" style="font-family:Calibri;" cellspacing="0" cellpadding="0" id="mytab">
                        <tr class="trhd">
                          <td width="20%" class="brd">Durattion</td>
                          <td width="20%" class="brd">Price</td>
                          <td width="20%" class="brd">Exlusive Offer</td>
                          <td width="20%" class="brd">Offer Price</td>
                          <td width="20%" class="brd">&nbsp;</td>
                        </tr>
                        </table>
                      </div>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Joining Date </label>
                  <div class="controls">
                    <input class="span6 required " type="text" id="datepicker" name="datepicker" value="" readonly />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Payment Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
											<option value="1" selected="selected">Active</option>                   
											<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>
    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User Edit</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>users/edit" class="form-horizontal" method="post" id="user_profile_add" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $user -> id ?>">
                   <?php echo ucfirst($user->username); ?>
                </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value="<?php echo $user->mobile; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="<?php echo $user->email; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Category </label>
                    <div class="controls">
                     <?php $attributes= 'id="cat_id" class="chosen span6 required"  onchange="get_gyms(this.value)" tabindex="6" ';
                       echo form_dropdown('cat_id', $cat,$user->category,$attributes);
                     ?>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                    <div class="controls">
                      <?php $gym = $this->session->userdata('gym');
                        if(isset($gym) && $gym != ''){ 
                          $attributes= 'id="gym_id" class="chosen span6 required" onchange="get_ofrs(this.value)"';
                          echo form_dropdown('gym_id', $gymbycat,$user->gym_id,$attributes);
                        }else { 
                          $attributes= 'id="gym_id"  class="chosen span6 required" onchange="get_ofrs(this.value)"';
                          echo form_dropdown('gym_id', array(), NULL,$attributes);                     
                        } ?>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Offers </label>
                  <div class="controls">    
                   <div class="setofrs">
                    <table width="100%" border="0" style="font-family:Calibri;" cellspacing="0" cellpadding="0" id="mytab">
                      <tr class="trhd">
                        <td width="20%" class="brd">Durattion</td>
                        <td width="20%" class="brd">Price</td>
                        <td width="20%" class="brd">Exlusive Offer</td>
                        <td width="20%" class="brd">&nbsp;</td>
                      </tr>
                      <?php if (isset($ofr_res))
                        {  
                            $newprice = explode(',',$ofr_res->price);
                            foreach ($newprice as $row) {
                              $resprice = explode('-',$row);?>
                              <tr>
                                <td class="brd" style="border-left:1px solid #dbdbdb;"><?php echo $resprice[1];?></td>
                                <td class="brd"><?php echo $resprice[2];?></td>
                                <td class="brd"><?php echo $resprice[3]."%";?></td>
                                <td class="brd "  ><input type="radio" name="mem_id" value="<?php echo $resprice[0];?>" <?php if($resprice[0] == $user->membership){echo 'checked="checked"';}?> style='opacity:1 !important;height:13px;'/></td>
                              </tr>
                          <?php }
                       } ?>
                    </table>
                  </div>
                </div>
                </div> 
                <div class="control-group">
                  <label class="control-label"> Joining date </label>
                  <div class="controls">
                    <input class="span6 required" type="text" id="datepicker" name="datepicker" readonly value="<?php echo $user->join_date; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Payment Status</label>
                  <div class="controls">    
                  <?php $status = $user->payment_status; ?>             
                   <select name="status" id="status" class="chosen span6 required" >                   
  										<option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
  										<option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                    
                   </select>
                </div>
                </div>            
                <br />
                <div class="form-actions">
                  <button type="submit" name="adduser" class="btn btn-success" id="add_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'): ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>users" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> User Name : </td>
                        <td> <?php echo ucfirst($user->username);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Mobile : </td>
                        <td> <?php echo $user->mobile; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Email : </td>
                        <td> <?php echo $user->email; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Gym Name : </td>
                        <td> <?php echo $user->gymname ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Category : </td>
                        <td> <?php echo $user->categoryname ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Membership : </td>
                        <td> <?php echo $user->membershipname." months";?></td>
                      </tr> 
                      <tr>
                         <td class="span3"> Price : </td>
                         <td> <?php echo $user->price;?></td>
                      </tr>  
                       <tr>
                         <td class="span3"> Joining date : </td>
                         <td> <?php echo $user->join_date;?></td>
                      </tr>  
                       <tr>
                         <td class="span3"> End date : </td>
                         <td> <?php echo $user->end_date;?></td>
                      </tr>                                      
											<tr>
                        <td class="span3"> Payment Status : </td>
                        <td> <?php echo ($user -> payment_status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  jQuery(document).ready(function(){

    $("#user_profile_add").validate({
      rules: {
        // simple rule, converted to {required:true}
        name: "required",
        // compound rule
        email: {
          required: true,
          email: true
        },
        
        username:"required"
      }
    });
  });
   function getid(id){
  	  
      if (confirm('Do you want to delete this menu?')) 
      {
 	   	$.ajax({
		            type: 'POST',
                url: '<?php echo ADMIN_URL;?>users/delete',
                data: { id: id}
            })
            .done(function(response){  
				              if(response == 'success'){
					                   window.location.reload(true);
					                   return true;
				              }else{
					                   window.location.reload(true);
					                   return false;
				             }});
  	          }
  	       else{
                return false;  	       	
  	       	}
  	       	}
  function get_gyms(selectedval){
    if(selectedval != ''){
         var result = $.ajax({
               'url' : '<?php echo ADMIN_URL;?>users/gymsByCat/' + selectedval,
               'async' : false
         }).responseText;
       $("#gym_id").replaceWith(result);
    }
    else{
      alert('choose any category');
      return false;
    }
  }
  function get_ofrs(selectedgym){
    if(selectedgym != ''){
         var result = $.ajax({
               'url' : '<?php echo ADMIN_URL;?>users/ofrsByGym/' + selectedgym,
               'async' : false
         }).responseText;
      $(".setofrs").html('');
      $(".setofrs").append(result);
    }
    else{
      alert('choose any Gym');
      return false;
    }
  }
  	           
</script>
<script type="text/javascript">
  $(function() {
    $( "#datepicker" ).datepicker({ minDate: 0, maxDate: "+1M +10D" });
  });
  
</script>
<script type="text/javascript">
  $(document).on('click','#add_profile',function(){ 

     if($("input:radio[name='mem_id']").is(":checked")) {
      var memid = $('input:radio[name=mem_id]:checked').val();    
    }else{  
      alert('Please choose any offer');
      return false;
      }     
         
      });
</script>
