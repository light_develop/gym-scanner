<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"></html><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Admin Dashboard</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/bootstrap-timepicker/compiled/timepicker.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_responsive.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/css/style_default.css" rel="stylesheet" id="style_color" />
<link href="<?php echo base_url()?>themes/admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>themes/admin/assets/uniform/css/uniform.default.css" />
<link href="<?php echo base_url()?>themes/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="<?php echo base_url()?>themes/admin/assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>themes/admin/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body class="fixed-top">
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="<?php echo ADMIN_URL;?>dashboard"><!-- <img src="<?php echo base_url()?>themes/admin/img/logo.png"
					alt="Admin Lab" /> --><b>Admin</b></a><a class="btn btn-navbar collapsed"
					id="main_menu_trigger" data-toggle="collapse"
					data-target=".nav-collapse"><span class="icon-bar"></span><span
					class="icon-bar"></span><span class="icon-bar"></span><span
					class="arrow"></span></a>
				<div id="top_menu" class="nav notify-row">
				</div>
				<div class="top-nav ">  <?php //print_r($this->session->userdata['user']->username);?>
					<ul class="nav pull-right top-menu">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="" alt="" /><!--<img src="<?php echo base_url()?>uploads/thumbs/users/<?php echo $user-> image?>" alt="" />-->
							<span class="username"> <?php echo ucwords($this->session->userdata['user']->username);?> </span><b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo ADMIN_URL;?>dashboard/profile"><i class="icon-user"></i> My Profile</a></li>
								<li><a href="<?php echo ADMIN_URL;?>dashboard/password"><i class="icon-tasks"></i> Password Change</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo ADMIN_URL;?>logout"><i class="icon-key"></i> Log Out</a></li>
							</ul></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="container" class="row-fluid">
		<div id="sidebar" class="nav-collapse collapse">
			<div class="sidebar-toggler hidden-phone"></div>
			<div class="navbar-inverse">
				<form class="navbar-search visible-phone" action="" />
				<input type="text" class="search-query" placeholder="Search" />
				<form action=""></form>
			</div>
			<ul class="sidebar-menu" >
				<li id="dashboardm"><a href="<?php echo ADMIN_URL;?>dashboard" class="">
                <span class="icon-box"><i class="icon-dashboard" id="dashboardm"></i></span> Dashboard</a></li>
	
                <li id="bannersm"><a href="<?php echo ADMIN_URL;?>banners" class="">
                <span class="icon-box"><i class="icon-th-list" id="bannersm"></i></span> Banners</a></li>

                <li class="has-sub" id="gymlocationsm">
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-th-list" ></i></span>Gym Locations
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li id="countrym"><a class="" href="<?php echo ADMIN_URL;?>country">Country List</a></li>
                        <li id="citym"><a class="" href="<?php echo ADMIN_URL;?>city">City List</a></li>
                    </ul>
                </li>
                <li class="has-sub" id="cmsmenum">
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-th-list"></i></span>CMS
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li id="cmsm"><a class="" href="<?php echo ADMIN_URL;?>cms">Menus</a></li>
                        <li id="menum"><a class="" href="<?php echo ADMIN_URL;?>menu">Menu Content</a></li>
                    </ul>
                </li>

                <li id="gymsm">
                    <a href="<?php echo ADMIN_URL;?>gyms" class="">
                    <span class="icon-box"><i class="icon-th-list" id="gymsm"></i></span> Gym </a>
                </li>
     
                <li id="usersm">
                    <a href="<?php echo ADMIN_URL;?>users" class="">
                    <span class="icon-box"><i class="icon-th-list" id="usersm"></i></span> Users </a>
                </li>
                <li id="contact_detailsm">
                    <a href="<?php echo ADMIN_URL;?>contact_details" class="">
                    <span class="icon-box"><i class="icon-th-list" id="contact_detailsm"></i></span> Contact Details </a>
                </li>
                <li id="notificationsm">
                    <a href="<?php echo ADMIN_URL;?>notifications" class="">
                    <span class="icon-box"><i class="icon-th-list" id="notificationsm"></i></span> Notification Settings </a>
                </li>

                <li id="offersm">
                    <a href="<?php echo ADMIN_URL;?>offers" class="">
                    <span class="icon-box"><i class="icon-th-list" id="offersm"></i></span> Gym Offers </a>
                </li>            		
            </ul>
        </div>
<script type="text/javascript" >
    var menu = '#<?=$this -> router -> fetch_class()?>m';
    //alert(menu);
    if(menu == '#dashboardm')
        {
            $('#dashboardm').addClass('active');

        }
    if(menu == '#bannersm'){
        $('#bannersm').addClass('active');        
    }
    if(menu == '#gymsm'){
        $('#gymsm').addClass('active');        
    }
    if(menu == '#usersm'){
        $('#usersm').addClass('active');        
    }
    if(menu == '#notificationsm'){
        $('#notificationsm').addClass('active');        
    }
    if(menu == '#offersm'){
        $('#offersm').addClass('active');        
    }
    if(menu == '#countrym' || menu == '#citym'){
        $('#gymlocationsm').addClass('active');        
    }
    if(menu == '#cmsm' || menu == '#menum'){
        $('#cmsmenum').addClass('active');        
    }
    $('#<?=$this -> router -> fetch_class()?>m').addClass('active');
</script>
