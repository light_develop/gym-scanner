<?php

$this->load->view('includes/header');
?>
    
    <div id="main-content">
	<div class="container-fluid">
	<div class="row-fluid"> <div class="span12"> </div> </div>
	<div class="row-fluid circle-state-overview">
      
				<div class="span2 responsive clearfix add"  data-tablet="span3" data-desktop="span2">
					<div class="widget-title">
						
						<p>
							<strong >Add Cms</strong> 
						</p>
					</div>
				</div>
				<div class="span2 responsive view" data-tablet="span3"	data-desktop="span2">
					<div class="widget-title">
						
						<p>
							<strong>View Cms</a></strong> 
						</p>
					</div>
				</div>
			
			</div>
			<div class="widget-body" align="center" >
			    <div id="addcms"> 
        
               <form name="cmsmenu" method="post" action="<?php echo ADMIN_URL;?>cms/addcms" id="cmsmenu">               
                        <table>
                            <tr role="row"><td> Menu :</td><td>
                                    <select name="menu_id" id="menu_id" >
									<option value="">-- Select Menu -- </option>
									<?php foreach($menus as $row){?>
									<option value="<?php echo $row->id?>"><?php echo $row->menuname;?></option>
									<?php } ?></select> <span id="menuIdErr"></span>
                                 
                                 </td></tr>
							<tr role="row"><td>Menu Name :</td>
    
    							<td><input type="text" id="menuname" name="menuname" placeholder="Enter menu name" /> 
								<span id="nameErr"></span></td>
							</tr>
                             <tr role="row"><td></td><td><input type="submit" name="submit" value="Add" /></td></tr>                        
                        </table>               
               </form>          
                                
        </div>		
        <div id="viewcms">
	    <form name="viewcmsmenu" method="post" action="<?php echo ADMIN_URL;?>cms/viewcms" id="viewcmsmenu">  
            	<table>
			<tr role="row">
				<td>sr.no</td>
				<td>Menu Name</td>
				<td>SubMenu</td>
				<td>View</td>
				<td>Delete</td>
			</tr>
			<?php $i=1; foreach($viewmenus as $row){ ?>
			<tr role="row">
				<td><?php echo $i;?></td>
				<td><?php echo $row->menuname;?></td>
				<td><?php $menu = $row->submenu_id; if($menu == 0){ echo "Main Menu";} 
								else {echo "Sub Menu";}?></td>
				<td><a href="<?php echo ADMIN_URL;?>/cms/edit/<?php echo $row->id;?>">
					<input type="button" name="view" value="View" /></a></td>
				<td><a href="<?php echo ADMIN_URL;?>/cms/delete/<?php echo $row->id;?>">
					<input type="button" name="delete" value="Delete" /></a></td>
			</tr>
			<?php $i++; } ?>
		</table>  
	    </form>
        </div>			
			</div>
      
      </div>
    </div>

<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript" >

        $('.add').click(function(){            
            $('#viewcms').hide();
            $('#addcms').show();
        });
        $('.view').click(function(){           
            $('#addcms').hide();
            $('#viewcms').show();
        });
        
     	$("#cmsmenu").click(function(){
      
     		  var menu = $('#menu_id').val();
                  var menuname = $('#menuname').val();
               
                  
                  $('#menuIdErr, #nameErr').html('');
                  if(!menuname)
                  {
                     // alert('please enter menu name');
                      $('#nameErr').css('color', 'red').html('please enter menu name');
                      return false;
                  }
                  
                	$.ajax({
  																							type: 'POST',
  																							dataType: 'JSON',
                           url: '<?php echo ADMIN_URL;?>cms/getmname',
                           data: { menu_id: menu, menuname: menuname },
                           success:function(response){
                           	                           	
                           	if(response.q == 'err')
                          		{
                          				var err = 1;
                          				alert('err');
                          					$('#nameErr').css('color', 'red').html('Menu name already exist!');     
                          					return false;                     				
                          		}
                          		else
                          		{
                          				                  if(!menu)
                  {        
                       			
                        if(confirm("Are you sure? is this main menu?"))
                        {
                        	alert('no err');
                                $(this).submit();                        		
                        }
                        else
                        {
                            $('#menuIdErr').css('color', 'red').html('please select menu name');                               
                      					return false;                 
                        }

                  }
                          		}
                           }
                      			});	
                      

                  
});


</script>
