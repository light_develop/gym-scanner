
<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid"> <div class="span12"> </div> </div>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
      <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/chosen-bootstrap/chosen/chosen.css" />
      <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
      <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
      <!-- Dependencies: JQuery and GMaps API should be loaded first 
      <script src="js/jquery-1.7.2.min.js"></script> -->
      <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

      <!-- CSS and JS for our Map code 
      <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script> -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/jquery-gmaps-latlon-picker.css"/>
      <script src="<?php echo base_url();?>themes/admin/js/jquery-gmaps-latlon-picker.js"></script>
<?php
/**
 * [Check the mode of view, if all it will list all gym]
 * @var [string]
 */
if(isset($mode) && $mode == 'all') :  ?>   
<!-- Start Listing All gyms -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Gyms </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>gyms/add" class="icon-plus"> </a> </span>
      </div>
      <div class="widget-body">
      <?php if($this ->session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>
      
       <?php if($this ->session -> flashdata('delete')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('delete');?></h4></div>
      <?php } ?>
      
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>                 
              <th> Logo </th>    
              <th> Name </th>
              <th> Open Time </th>
              <th> Close Time </th>
              <th> Price </th>
              <th> Rating </th>
              <th> Address </th>
              <th> Specialty </th>
              <th> Owner-Details </th>
              <th style="width:170px;"> Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php if(isset($gyms) && is_array($gyms) && count($gyms)){ $i = 1;?>
          <?php foreach ($gyms as $key => $gym) { 
                $gymname = str_replace('[@]', '<br/>',  $gym->gymname);        
          ?>
            <tr class="odd gradeX">
             <td><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym -> id ;?>">
             <?php $img = $gym->gym_logo; ?>
             <img src="<?php echo base_url()?>uploads/gym/logos/<?php echo $img; ?> " alt="image" height="100px" width="100px" /></a>  </td>
             <td><a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym -> id ;?>"><?php echo ucfirst($gymname) ; ?></a></td>
             <td><?php echo $gym->open_time; ?></td>
             <td><?php echo $gym->close_time; ?></td>
             <td><?php echo $gym->price ; ?></td>
             <td><?php for($j = 0;$j < $gym->star; $j++){ ?> <img src="<?php echo base_url()?>themes/admin/img/star-on.png" /> <?php  }?></td>
             <td><?php echo ucfirst($gym->gstreet).', ' ;
                       echo ucfirst($gym->city).', ' ; 
                       echo ucfirst($gym->state).', ' ;
                       echo ucfirst($gym->country_name).', ' ; 
                       echo $gym->gzip ;?></td>
             <td><?php if($gym->speciality ==1){echo 'Men';} else if($gym->speciality ==2){echo 'Women';} else{echo 'All';} ?></td>
             <td</td>
             <td><?php echo "Name: ". ucfirst($gym->owner) ; echo '<br/>'; echo "Ph no: ". $gym->ow_phone ; echo '<br/>'; echo "Mail id: ".$gym->ow_email ; ?></td>
             <td>
              <a href="<?php echo ADMIN_URL;?>gyms/view/<?php echo $gym -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i></a>
              <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>gyms/edit/<?php echo $gym -> id?>"> <i class="icon-edit"> </i></a>   
              <?php if($gym->status==0){?>
                <a class="btn btn-success" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-thumbs-up"> </i></a><?php } else{ ?>
                <a class="btn btn-danger" id="<?php echo $gym->id; ?>" onclick="status_change_gym(<?php echo $gym->id; ?>,<?php echo $gym->status; ?>,'<?php echo $gym->id; ?>')" > <i class="icon-thumbs-down"> </i></a>
                <?php } ?>
              <span class="btn btn-danger" onclick="getid(<?php echo $gym-> id?>)" > <i class="icon-remove icon-white"> </i></span>               
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block --> 
<?php elseif( isset($mode) && $mode == 'add'): ?>

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add Gym Details </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>gyms" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL;?>gyms/add" class="form-horizontal" method="post" id="gym_form" enctype = "multipart/form-data"/>
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <?php if($this -> session -> flashdata('failure')!=''){?>
                  <div><h4 class="error"><?php echo $this -> session -> flashdata('failure');?></h4></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Name" id="gymname" name="gymname" value="<?php echo set_value('gymname');?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Logo </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="logo" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Images </label>
                  <div class="controls">
                    <input type="file" name="upload_file[]">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_images" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <div id="add_more"></div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Video </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <input name="MAX_FILE_SIZE" value="100000000000000"  type="hidden"/>
                          <span class="fileupload-new"> Select video </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="gym_video" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <div id="datetimepicker3" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="open_time" placeholder="Open Time" readonly></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                    <div id="datetimepicker4" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="close_time" placeholder="Close Time" readonly></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Rating </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Rating" id="star" name="star" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Specialty </label>
                  <div class="controls">
                    <select name="speciality" id="specialty" class="chosen span6 required" >                   
					     <option value="1"  selected="selected">Men</option>                   
					     <option value="2">Women</option>   
					     <option value="3">All</option>                   
                   </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Comments </label>
                  <div class="controls">
                    <input class="span6 " type="text" placeholder=" Comment" id="comments" name="comments" value=""/>
                  </div>
                </div>
                 <div class="control-group">
                  <label class="control-label"> Membership </label>
                  <div class="controls">
                      <table border="1px">
                        
                        <?php foreach ($mem as $row):?>
                          <tr>
                            <td align="center" width="150px"><?php echo "<b>".$row->name." months</b>"?></td>
                            <td align="center"><input type="hidden" name="mem_id[]" value="<?php echo $row->id?>"/>
                              <input class="number" type="text" placeholder="Price" id="price<?php echo $row->id?>" name="price[]" value=""/></td>
                            <td ><input class="number" type="text" placeholder="Offer" id="offer" name="offer[]" value=""/></td>
                          </tr>
                        <?php endforeach;?>
                      </table>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Country </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter Country" id="gcountry" name="gcountry" value=""/> -->
                    <?php $cntry[''] = 'Country';$attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="span6 required"';
                          echo form_dropdown('cntry', $cntry, '',$attributes);
                    ?>              
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State </label>
                  <div class="controls">
                    <?php $attributes= 'id="state"  class="span6 required" ';                      
                      echo form_dropdown('state', array(), NULL,$attributes);                      
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter City" id="gcity" name="gcity" value=""/> -->
                    <?php $attributes= 'id="city"  class="span6 required" ';                      
                      echo form_dropdown('city', array(), NULL,$attributes);                      
                    ?>                  
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Street </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Street" id="gstreet" name="gstreet" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Zip code </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Zip Code" id="gzip" name="gzip" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Category </label>
                  <div class="controls">
                   <?php $cat['']='Category';$attributes= 'id="cat_id" class="span6 required" ';
                      echo form_dropdown('cat_id', $cat, '',$attributes);
                    ?> 
                  </div>
                </div>
                <!-- <div class="control-group">
                  <label class="control-label"> Advanced </label>
                  <div class="controls">
                    <input class="checkbox" type="checkbox" name="adv['PILATES']" value="1" >PILATES/YOGA
                    <input class="checkbox" type="checkbox" name="adv['MMA']" value="1" >MMA 
                  </div>
                </div> -->
                <div class="control-group">
                  <label class="control-label"> Price Range </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Pricerange " id="pricerange" name="pricerange" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Owner Name </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Owner" id="owner" name="owner" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Phone No </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Ph No" id="ow_phone" name="ow_phone" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Email" id="ow_email" name="ow_email" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="type">Location</label>
                  <div class="controls">  
                      <fieldset class="gllpLatlonPicker" id="custom_id" >
                          <input type="text" name="search_location" class="gllpSearchField" value="" required/>
                          <input type="button" name="search" class="gllpSearchButton" value="search">
                          <br/><br/>
                          <div class="gllpMap">Google Maps</div>
                          <input type="hidden" class="gllpLatitude" name="latitude" value="60"/>
                          <input type="hidden" class="gllpLongitude" name="longitude" value="30"/>
                          <input type="hidden" class="gllpZoom" name="zoom" value="12"/>                                                       
                      </fieldset>                                                     
                  </div> <!-- /controls -->
                </div>
                <div class="form-actions">
                  <button type="submit" name="addgym" class="btn btn-success" id="savegym"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?> 

        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Edit Gym </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>gyms" class="icon-arrow-left"> </a> </span>
              </div>
              <div class="widget-body form">
                <form action="<?php echo ADMIN_URL; ?>gyms/edit" class="form-horizontal" method="post" id="gym_edit_form" enctype = "multipart/form-data"/>
                <?php if($this->session-> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Logo </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select file </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="logo" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                      <input type="hidden" name="old_image" value="<?=$gym->gym_logo?>" />

                     <?php if($gym->gym_logo!='') { ?>
                    <img src="<?=base_url()?>uploads/gym/logos/<?=$gym->gym_logo?>" width="90" alt="image">
                    <?php }?>
                    </div>
                  </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $gym -> id ?>">
                   <?php echo ucfirst($gym->gymname); ?>
                </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Gym Images </label>
                  <div class="controls">
                      <?php foreach ($gimgs as $key => $value) { 
                          $image = base_url().'uploads/gym/images/'.$value->gym_images;?>
                        <span style="position:relative" id="img_<?php echo $value->id;?>">
                            <img src="<?php echo $image;?>" width="100px" height="50"> &nbsp;&nbsp;
                            <i class="icon-remove" name="<?php echo $value->id?>" id="<?php echo $value->gym_images?>" style="position:absolute;margin-top:-10px;margin-left:-10px;color:red;"></i></span>                      
                      <?php } ?>                        
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="file" name="upload_file[]">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"></label>
                  <div class="controls">
                    <input type="button" id="add_images" value="Add more" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Video </label>
                  <div class="controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="input-append">
                        <div class="uneditable-input">
                          <i class="icon-file fileupload-exists"> </i>
                          <span class="fileupload-preview"> </span>
                        </div>
                        <span class="btn btn-file">
                          <span class="fileupload-new"> Select video </span>
                          <span class="fileupload-exists"> Change </span>
                          <input type="file" class="default" name="gym_video" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"> Remove </a>
                      </div>
                      <input type="hidden" name="old_video" value="<?=$gym->gym_video?>" />

                     <?php if($gym->gym_video!='') { 
                      $video = $gym->gym_video;?>
                        <object width="200" height="100" data="<?php echo base_url()?>uploads/thumbs/gym/videos/<?php echo $video;?>">
                          <param name="autoplay" value="true">
                        </object>
                    <?php }?>
                    </div>
                  </div>
                </div>
                <br />
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                    <div id="datetimepicker3" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="open_time" placeholder="Open Time" readonly value="<?php echo $gym->open_time;?>" ></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                    <div id="datetimepicker4" style="float:left;">
                    <input data-format="hh:mm:ss" type="text" class="span6 required" name="close_time" placeholder="Close Time" readonly value="<?php echo $gym->close_time;?>" ></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Rating </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Rating" id="star" name="star" value="<?php echo $gym->star;?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Price </label>
                  <div class="controls">
                    <table border="1px">                        
                      <?php foreach ($mem as $row):?>
                        <tr>
                          <td align="center" width="150px"><?php echo "<b>".$row->name."</b>"?></td>
                          <td align="center"><input type="hidden" name="mem_id[]" value="<?php echo $row->id?>" />
                          <?php $price = $gym->price;
                            $newprice = explode(',',$price); ?>                                
                                <input class="number" type="text" placeholder="Price" id="price" name="price[]" 
                                value="<?php for($p =0;$p<count($newprice);$p++){
                              $resprice = explode('-',$newprice[$p]);
                              if($resprice[0] == $row->id){ echo $resprice[2]; } } ?>"/>                  
                          </td>
                          <td align="center">
                          <?php $offer = $gym->price;
                            $newoffer = explode(',',$offer); ?>                                
                                <input class="number" type="text" placeholder="Offer" id="offer" name="offer[]" 
                                value="<?php for($p =0;$p<count($newoffer);$p++){
                              $resoffer = explode('-',$newoffer[$p]);
                              if($resoffer[0] == $row->id){ echo $resoffer[3]; } } ?>"/>                            
                          </td>
                     <?php endforeach;?>
                      </table>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Comments </label>
                  <div class="controls">
                    <input class="span6 required" type="text" id="comments" name="comments" value="<?php echo $gym->comments;?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Country </label>
                  <div class="controls">
                    <?php $attributes= 'id="cntry" onchange="load_dropdown_content(this.value)" class="chosen span6 required"';
                      echo form_dropdown('cntry', $cntry, $gym->gcountry,$attributes);
                    ?>                  
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> State </label>
                  <div class="controls">
                    <?php $attributes= 'id="state" class="span6 required" tabindex="6" required';
                         echo form_dropdown('state', $states, $gym->gstate,$attributes);
                     ?> 
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> City </label>
                  <div class="controls">
                     <?php $attributes= 'id="city" class="span6 required" tabindex="6" required';
                         echo form_dropdown('city', $city, $gym->gcity,$attributes);
                     ?>                  
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Street </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Street" id="gstreet" name="gstreet" value="<?php echo ucfirst($gym->gstreet);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Zip code </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Zip Code" id="gzip" name="gzip" value="<?php echo ucfirst($gym->gzip);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Select Specialty </label>
                  <div class="controls">
                    <?php $specialty = $gym->speciality; ?>
                    <select name="speciality" id="specialty" class="chosen span6 required" >                   
        					    <option value="1"  <?php if($specialty == 1){ ?> selected="selected" <?php } ?>>Men</option>                   
        					    <option value="2" <?php if($specialty == 2){ ?> selected="selected" <?php } ?>>Women</option>   
        					    <option value="3" <?php if($specialty == 3){ ?> selected="selected" <?php } ?>>All</option>                   
                   </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Category </label>
                  <div class="controls">
                    <!-- <input class="span6 required" type="text" placeholder="Please enter Country" id="gcountry" name="gcountry" value="<?php echo ucfirst($gym->gcountry);?>"/> -->
                    <?php $attributes= 'id="cat_id" class="span6 required" ';
                      echo form_dropdown('cat_id', $cat, $gym->category,$attributes);
                    ?>                  
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Price Range </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Pricerange " id="pricerange" name="pricerange" value="<?php echo $gym->price_range;?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Owner Name </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Owner" id="owner" name="owner" value="<?php echo ucfirst($gym->owner);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Phone No </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Ph No" id="ow_phone" name="ow_phone" value="<?php echo ucfirst($gym->ow_phone);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required" type="text" placeholder="Please enter Email" id="ow_email" name="ow_email" value="<?php echo ucfirst($gym->ow_email);?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="type">Location</label>
                  <div class="controls">  
                      <fieldset class="gllpLatlonPicker" id="custom_id" >
                          <input type="text" name="search_location" class="gllpSearchField" value="<?php echo ucfirst($gym->location);?>" required/>
                          <input type="button" name="search" class="gllpSearchButton" value="search">
                          <br/><br/>
                          <div class="gllpMap">Google Maps</div>
                          <input type="hidden" class="gllpLatitude" name="latitude" value="<?php echo ucfirst($gym->gmap_lat);?>"/>
                          <input type="hidden" class="gllpLongitude" name="longitude" value="<?php echo ucfirst($gym->gmap_long);?>"/>
                          <input type="hidden" class="gllpZoom" name="zoom" value="10"/>                                                       
                      </fieldset>                                                     
                  </div> <!-- /controls -->
                </div>             
                <br />
                <div class="form-actions">
                  <button type="submit" name="addgym" class="btn btn-success" id="editgym"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>

<?php elseif( isset($mode) && $mode == 'view'): ?>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> View Gym Details </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL ?>gyms" class="icon-arrow-left"> </a> </span>
      </div>
      <div class="widget-body form">
                  <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> Gym Logo : </td>
                        <td> <?php $img = $gym->gym_logo;?>
                             <img src="<?php echo base_url()?>uploads/gym/logos/<?php echo $img; ?> " alt="image" height="70px" width="100px" />
                        </td>
                      </tr>
                      <tr>
                        <td class="span3"> Name : </td>
                        <td> <?php echo ucfirst($gym->gymname);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Images : </td>
                        <td> <?php $imgs = $gimgs->images; 
                       $eachimg = explode(',',$imgs);
                       for($s = 0; $s < count($eachimg); $s++){?> 
                       <img src="<?php echo base_url()?>uploads/thumbs/gym/<?php echo $eachimg[$s]; ?> " alt="image" height="70px" width="100px" />
                       <?php } ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Gym Video : </td>
                        <td> <?php $video = $gym->gym_video;?>
                          <object width="200" height="100" data="<?php echo base_url()?>uploads/thumbs/gym/videos/<?php echo $video;?>">
                            <param name="autoplay" value="true">
                          </object>                        
                      </td>
                      </tr>
                      <tr>
                        <td class="span3"> Open Time : </td>
                        <td> <?php echo $gym ->open_time;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Timings : </td>
                        <td> <?php echo $gym ->close_time;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Rating : </td>
                        <td> <?php for($j = 0;$j < $gym->star; $j++){ ?> <img src="<?php echo base_url()?>themes/admin/img/star-on.png" /> <?php  }?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Price : </td>
                        <td> 
                          <table> 
                            <tr>
                              <td align="center"><?php echo "<b> Duration </b>"?></td>
                              <td align="center"><?php echo "<b> Price </b>"?></td>
                              <td align="center"><?php echo "<b> Offer </b>"?></td>
                            </tr>                     
                            <?php $price = $gym->price;
                              $newprice = explode(',',$price); 
                              for($p =0;$p<count($newprice);$p++){
                                $resprice = explode('-',$newprice[$p]);?>                                
                                  <tr>
                                    <td align="center"><?php echo "<b>".$resprice[1]." months"."</b>"?></td>
                                    <td align="center"><?php echo $resprice[2]; ?></td>
                                    <td align="center"><?php echo $resprice[3]. "%";?></td>
                                  </tr>
                              <?php } ?>
                          </table>
                        </td>
                      </tr>
                       <tr>
                        <td class="span3"> Comments : </td>
                        <td> <?php echo $gym->comments;?>
                          </td>
                      </tr>
                      <tr>
                        <td class="span3"> Address : </td>
                        <td> <?php echo ucfirst($gym->gstreet).',' ;
			                       echo ucfirst($gym->city).','; 
			                       echo ucfirst($gym->state).',';
			                       echo ucfirst($gym->country_name).',';
			                       echo $gym->gzip ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Map : </td>
                        <td> <?php echo 'Latitude : '; echo $gym->gmap_lat ; echo '<br />';
                                   echo 'Longitude : '; echo $gym->gmap_long ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Specialty : </td>
                        <td> <?php if($gym->speciality ==1){echo 'Men';} else if($gym->speciality ==2){echo 'Women';} else{echo 'All';} ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Category : </td>
                        <td> <?php echo ucfirst($gym->categoryname) ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Price Range : </td>
                        <td> <?php echo ucfirst($gym->price_range) ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Owner : </td>
                        <td> <?php echo ucfirst($gym->owner) ; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Ow-Phone : </td>
                        <td> <?php echo $gym->ow_phone ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Ow-Email : </td>
                        <td> <?php echo $gym->ow_email ;?></td>
                      </tr>
					  <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($gym -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
        </div>
    </div>
       <?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js"></script>
<script> 
  jQuery(document).ready(function(){

  $("#gym_form").validate({
    
		rules: {
		    "gymname":"required",
		    "timings":"required",
		    "star":{required:true,digits: true,min:1,max:5},
        "upload_file[]":{ required: true, extension: "gif|jpeg|jpg|png"}, 
		    "gstreet":"required",
        "logo":"required",
		    "gcity":"required",
		    "gstate":"required",
		    "gcountry":"required",
		    "gzip":"required",
		    "speciality":"required",
		    "owner":"required",
        "price":{required:true,digits: true},
        "pricerange":{required:true,digits: true,min:1,max:5},
		    "ow_phone":{required:true,digits: true,minlength:10},
		    "ow_email":{required:true,email: true}
		    }		 
	  });
    }); 
  function load_dropdown_content(selected_value){ 

       if(selected_value!=''){
    	       var result = $.ajax({
        	         'url' : '<?php echo ADMIN_URL;?>gyms/get_states/' + selected_value,
        	         'async' : false
    	       }).responseText;
    	   $("#state").replaceWith(result);
      }
  }
  function get_cities(state){
    if(state != ''){
      var result = $.ajax({
                   'url' : '<?php echo ADMIN_URL;?>gyms/get_cities/' + state,
                   'async' : false
             }).responseText;
         $("#city").replaceWith(result);
    }
  }
  function deleteImage(id, imgname)
  {
      if(confirm('Are you sure want to delete this image?'))
      {
        $('#delimg'+id).remove();
        $('#removeimgdiv').append('<input type="hidden" name="remove_gym_image[]" value="'+imgname+'">');
      }
  }
  function getid(id){
      if (confirm('Do you want to delete this gym?')) 
      {
 	   	$.ajax({
		        type: 'POST',
                url: '<?php echo ADMIN_URL;?>gyms/delete',
                data: { sid: id}
            })
            .done(function(response){  
	              if(response == 'success'){
		                window.location.reload(true);
		                return true;
	              }else{
		                window.location.reload(true);
		                return false;
	             }});
  	          }
  	       else{
  	       		window.location.reload(true);
                return false;  	       	
  	       	}
  	 }
</script>
<script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
      //pick12HourFormat : true
    });
  });
</script>
<script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickDate: false
    });
  });
</script>
<script>
    $(function (){
        var img_count = 0;
        $('#add_images').on("click",function(){
            img_count = img_count+1;
            $("#add_more").append('<div class="control-group" id="divname'+img_count+'"><label class="control-label" for="address"></label><div class="controls"><input type="file" name="upload_file[]" class="span4" /><button type="button" class="btn btn-warning " onclick="remove_btn('+img_count+')" >Remove</button></div></div>');
            $("#divname"+img_count).hide();
            //$("#jcrop_target"+img_count).hide();

            $( "#divname"+img_count ).slideDown( "slow", function() {
            // Animation complete.
            });
        });
    });
</script>
<script type="text/javascript">
    function remove_btn(id){

    $( "#divname"+id).slideUp( "slow", function() {
        $("#divname"+id).remove();
      });
    }
</script>
<script type="text/javascript">
  function status_change_gym(id,status,aid)
  {
    $.ajax({
            type : "POST",
            url : "<?php echo ADMIN_URL;?>gyms/change_status",
            data : {
                        'id': id,
                        'status':status
                   },
            success : function(data) {
                if(status == '0'){
                  $('#'+aid).removeClass('btn btn-success').addClass('btn btn-danger').html('<i class="icon-thumbs-down"></i>');
                  $('#'+aid).attr('onclick','status_change_gym('+id+',1,"'+aid+'")');
                }else{
                  $('#'+aid).removeClass('btn btn-danger').addClass('btn btn-success').html('<i class="icon-thumbs-up"></i>');
                  $('#'+aid).attr('onclick','status_change_gym('+id+',0,"'+aid+'")');
                }
            },
            error : function() {
               alert("We are unable to do your request. Please contact webadmin");
            }
        });
  }
</script>
<script type="text/javascript">
    $('.icon-remove').click (function () {

     var id = $(this).attr('name');
     var image = $(this).attr('id');
         var result = $.ajax({
            'url' : '<?php echo ADMIN_URL?>gyms/del_img/' + id  + '/' + image,
            'async' : false
        }).responseText;
           $('#img_'+id).hide();
     return false;
     });

</script>
