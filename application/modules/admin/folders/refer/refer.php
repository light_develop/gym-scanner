
<div id="main-content">
  <div class="container-fluid">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/style_datepicker.css" />
  <link href="<?php echo base_url();?>themes/admin/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 
<?php
/**
 * [Check the mode of view, if all it will list all food_categories]
 * @var [string]
 */

if(isset($mode) && $mode == 'all'):?>
<!-- Start Listing All Cities -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-title">
        <h4> <i class="icon-reorder"> </i> All Refer Users </h4>
        <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
        <span class="tools"> <a href="<?php echo ADMIN_URL;?>refer/add" class="icon-plus"> Add Friend</a> </span>
      </div>
      <div class="widget-body">
     <?php if($this -> session -> flashdata('success')!=''){?>
        <div><h4 class="success"><?php echo $this -> session -> flashdata('success');?></h4></div>
      <?php } ?>


       <?php if($this -> session -> flashdata('error')!=''){?>
        <div><h4 class="error"><?php echo $this -> session -> flashdata('error');?></h4></div>
      <?php } ?>
        <table class="table table-striped table-bordered" id="sample_1">
          <thead>
            <tr>
              <th> Name </th>
              <th> Mobile </th>
              <th> Email</th>
              <th> Gym Name </th>
              <th> Timing </th>
              <th> Referred By </th>
             <!-- <th class="hidden-phone"> User Group </th>-->
              <th class="hidden-phone">Actions </th>
            </tr>
          </thead>
          <tbody>

          <?php

         // echo "<pre>";print_r($users);echo "</pre>";exit;
           if(isset($users)  && count($users)){ $i = 1;

          ?>

          <?php foreach ($users as $row)  {


          ?>
            <tr class="odd gradeX">

             <td> <?php echo ucfirst($row->firstname)." ". ucfirst($row->lastname);?> </td>
             <td> <?php echo $row->mobile;?> </td>
             <td> <?php echo $row->email;?> </td>
             <td> <?php echo $row->gymname;?> </td>
             <td> <?php echo $row->timing;?> </td>
             <td> <?php echo $row->referred_by;?> </td>
              <td><div style="clear:both;">
              <a href="<?php echo ADMIN_URL;?>refer/view/<?php echo $row -> id?>" class="btn mini black"> <i class="icon-eye-open"> </i>  View </a>
                <a class="btn mini purple editcity" href="<?php echo ADMIN_URL;?>refer/edit/<?php echo $row -> id?>"> <i class="icon-edit"> </i> Edit </a> 
                </div>&nbsp;<div style="clear:both;">
                <span class="btn btn-danger" onclick="getid(<?php echo $row-> id?>)" > <i class="icon-remove icon-white"> </i> Delete </span>
                <?php if($row->status==1):?>
                <a class="btn btn-success" href="<?php echo ADMIN_URL;?>refer/deactivate/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Active</a>
                
                <?php else:?>
                <a class="btn btn-danger" href="<?php echo ADMIN_URL;?>refer/active/<?php echo $row->id?>"> <i class="icon-ok icon-white"> </i>Inactive</a>
                </div>        
                <?php endif; ?>
                              
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End city listing block -->
<?php elseif( isset($mode) && $mode == 'add'): ?>

         <div class="row-fluid">
          <div class="span12">
            <div class="widget">
            <div class="widget-title">
                <h4> <i class="icon-reorder"> </i> Add New Friend </h4>
                <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                <span class="tools"> <a href="<?php echo ADMIN_URL; ?>refer" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL;?>users/add" class="form-horizontal" method="post" id="user_profile_add" enctype = "multipart/form-data"/>
                <div class="error"> </div>
                <div class="control-group">
                  <label class="control-label"> First Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text"  name="firstname" value=""/>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label"> Last Name </label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="lastname" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Age </label>
                  <div class="controls">
                   <input class="span6 required" type="text" name="age" value=""/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Gender </label>
                  <div class="controls">
                    <label class="radio">
                      <input type="radio" name="gender" value="1" checked/="" /> Male </label>
                    <label class="radio">
                      <input type="radio" name="gender" value="2"  /> Female </label>                   
                    </label>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value="">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Referred By </label>
                    <div class="controls">
                     <input class="span6 required" type="text" name="refferedby" value="" >
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                    <div class="controls">
                     <?php $gyms['select'] = 'Select Gym'; $attributes= 'id="gym_id" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('gym_id', $gyms, 'select',$attributes);
                    ?>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                    <div class="controls">
                     <input class="span6 required" type="text" name="timing" value="" >
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">                 
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1"  selected="selected">Active</option>                   
																	<option value="0">Inactive</option>                   
                   </select>
                </div>
                </div>              
                <div class="form-actions">
                  <button type="submit" class="btn btn-success" id="update_profile"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
<?php elseif( isset($mode) && $mode == 'edit'): ?>

    <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> Edit Friend </h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>refer" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">

              <form action="<?php echo ADMIN_URL; ?>users/edit" class="form-horizontal" method="post" id="user_profile_add" />
                <?php if($this -> session -> flashdata('error') !='') { ?>
                  <div class="error"> <?php echo $this -> session -> flashdata('error'); ?></div>
                <?php } ?>
                <div class="control-group">
                  <label class="control-label"> Name </label>
                  <div class="controls">
                    <input type="hidden" name="id" value="<?php echo $user -> id ?>">
                   <?php echo ucfirst($user->firstname)." ". ucfirst($user->lastname); ?>
                </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Gender </label>
                  <div class="controls">
                    <label class="radio">
                      <input type="radio" name="gender" value="1" <?php if($user->gender == 1){echo "checked";} ?> /> Male </label>
                    <label class="radio">
                      <input type="radio" name="gender" value="2"  <?php if($user->gender == 2){echo "checked";} ?> />  Female </label>                   
                    </label>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Age </label>
                  <div class="controls">
                   <input class="span6 required number" type="text" name="age" value="<?php echo $user->age; ?>"/>
                   <span>In cms</span>
                  </div>
                </div>                
                <div class="control-group">
                  <label class="control-label"> Mobile No. </label>
                  <div class="controls">
                    <input class="span6 required number" minlength="10" maxlength="13" type="text" name="mobile" value="<?php echo $user->mobile; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Email </label>
                  <div class="controls">
                    <input class="span6 required email" type="text" name="email"value="<?php echo $user->email; ?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Timings </label>
                  <div class="controls">
                   <input class="span6" type="text" name="timing" value="<?php echo $user->timing; ?>"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Gym Name </label>
                    <div class="controls">
                     <?php $attributes= 'id="gym_id" class="chosen span6 required"  tabindex="6" required';
                      echo form_dropdown('gym_id', $gyms,$user->gym_id,$attributes);
                    ?>
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Referred By </label>
                    <div class="controls">
                     <input class="span6 required" type="text" name="refferedby" value="<?php echo $user->refferedby; ?>" >
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label"> Status</label>
                  <div class="controls">    
                  <?php $status = $user->status; ?>             
                   <select name="status" id="status" class="chosen span6 required" >                   
																	<option value="1" <?php if($status == 1){ ?> selected="selected" <?php } ?>>Active</option>                   
																	   <option value="0" <?php if($status == 0){ ?> selected="selected" <?php } ?>>Inactive</option>                    
                   </select>
                </div>
                </div>           
                <br />
                <div class="form-actions">
                  <button type="submit" name="adduser" class="btn btn-success" id="saverefer"> Submit </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
 <?php elseif( isset($mode) && $mode == 'view'):

 ?>
        <div class="row-fluid">
          <div class="span12">
            <div class="widget">
              <div class="widget-title">
                <h4> <i class="icon-user"> </i> User View</h4>
                 <span class="tools"> <a href="javascript:;" class="icon-chevron-down"> </a> </span>
                  <span class="tools"> <a href="<?php echo ADMIN_URL; ?>refer" class="icon-arrow-left"> Back </a> </span>
              </div>
              <div class="widget-body form">
                   <table class="table table-borderless">
                    <tbody>
                      <tr>
                        <td class="span3"> User Name : </td>
                        <td> <?php echo ucfirst($user->firstname)." ". ucfirst($user->lastname);?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Gender : </td>
                        <td> <?php echo($user->gender == 1)?"Male":"Female";?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Age : </td>
                        <td> <?php echo $user->age;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Mobile : </td>
                        <td> <?php echo $user->mobile; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Email : </td>
                        <td> <?php echo $user->email; ?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Refferedby : </td>
                        <td> <?php echo $user->refferedby ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Gym Name : </td>
                        <td> <?php echo $user->gymname ;?></td>
                      </tr>
                      <tr>
                        <td class="span3"> Timings : </td>
                        <td> <?php echo $user->timing ;?></td>
                      </tr>
																		  <tr>
                        <td class="span3"> Status : </td>
                        <td> <?php echo ($user -> status==1)?"Active":"In Active";?></td>
                      </tr>
                    </tbody>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
<?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"> </script>
<script type="text/javascript" src="<?php echo base_url()?>themes/admin/assets/bootstrap/js/bootstrap-fileupload.js"> </script>
<script>
  jQuery(document).ready(function(){

$("#user_profile_add").validate({
  rules: {
    // simple rule, converted to {required:true}
    name: "required",
    // compound rule
    email: {
      required: true,
      email: true
    },
    
    username:"required"
  }
});

 $(function() {
    $( "#datepicker" ).datepicker({
         changeMonth: true,
         changeYear: true,
         yearRange: '1900:+0'
         //yearrange: '1900:'+ new Date().getFullYear()
    });
   
 }); 
 
  });
   function getid(id){
  	
      //alert(id);  
      if (confirm('Do you want to delete this menu?')) 
      {
 	   	$.ajax({
		type: 'POST',
                url: '<?php echo ADMIN_URL;?>refer/delete',
                data: { id: id}
            })
            .done(function(response){  
				              if(response == 'success'){
					                   window.location.reload(true);
					                   return true;
				              }else{
					                   window.location.reload(true);
					                   return false;
				             }});
  	          }
  	       else{
  	       						window.location.reload(true);
                return false;  	       	
  	       	}
  	       	}
  	           
</script>
