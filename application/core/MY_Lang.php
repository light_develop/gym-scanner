<?php


class MY_Lang extends CI_Lang
{
    public $init = false;
    public $result = array();

    public function load($langId) {
        $CI = & get_instance();

        $data = ($result = $CI->db->get_where("languages_translate", array("lang_id" => $langId))->result_array()) ? $result : array();

        foreach($data as $langLine) {
            $this->result[$langLine["main_lang"]] = $langLine["second_lang"];
        }

    }

    function line($line = '')
    {
        return (isset($this->result[$line])) ? $this->result[$line] : $line;
    }
}