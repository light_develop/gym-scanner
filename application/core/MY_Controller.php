<?php 

class MY_Controller extends CI_Controller
{
	public $data = array();
	
	public function __construct()
	{

		parent::__construct();

		$this->lang = new MY_Lang();
        $this->loadLanguage($this->lang);

		//$user = $this->session->userdata('user');
	//	print_r($user); exit;
    //	if(!isset($user) ||  !is_object($user)) redirect('administrator'); 
    	//else    $login_test = $this->session->userdata('user');
	}

    public function loadLanguage(MY_Lang $lang) {
        if(isset($_COOKIE["lang"])) {
            $lang->load($_COOKIE["lang"]);
        }
    }


    public function dev($data = '')
	{

		echo "<pre>";
		if($data)
		{
			print_r($data);
		}
		else
		{
			if(@$_FILES)
			{
				print_r($_FILES);
				echo "<hr/>";
			}
			print_r($_POST);
		}	
		exit;			
	}
	public function change_dbdate_format($date)
	{
		$timeArr = explode(' ', $date);			
		$dateArr = explode('-', $timeArr[0]);		
		$dateformat = $dateArr[2].'-'.$dateArr[1].'-'.$dateArr[0].((isset($timeArr[1]))?' '.isset($timeArr[1]):"");		
		
		return $dateformat;
	}
}



