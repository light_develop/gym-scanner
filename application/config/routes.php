<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user";
//$route[''] = "user/$1";
$route['about'] 			= "user/pages/about";
$route['support'] 	= "user/customersupport";
$route['faq'] 		= "user/pages/faq";
$route['pers_trns'] 	= "user/pages/trainer";
$route['refunds'] 	= "user/pages/refund";
$route['privacy_policy'] 	= "user/pages/privacy";
$route['terms_cond'] 	= "user/pages/termscond";
$route['contact'] 	= "user/pages/contact";

$route['facility'] = "user/supplier/login_redirect";
$route['new_facility'] = "user/supplier/add";
$route['get_users'] = "user/supplier/get_users";
$route['add_users'] = "user/supplier/add_user";
$route['edit_users'] = "user/supplier/user_edit";
$route['edit_gym'] = "user/supplier/edit_gym";
$route['add_gym'] = "user/supplier/add_gym";
$route['src_url'] = "user/supplier/src_url";
$route['users'] = "user/supplier/get_users";
$route['view_user/(:num)'] = "user/supplier/view_user/$1";
$route['edit_user/(:num)'] = "user/supplier/edit_user/$1";
$route['view_details/(:num)'] = "user/supplier/view_details/$1";
$route['edit_details/(:num)'] = "user/supplier/edit_details/$1";
$route['view_customers/(:num)'] = "user/supplier/view_customers/$1";


$route['facility_offers'] = "user/asoffers";
$route['new_offer'] = "user/asoffers/add_offer";
$route['add_offer'] = "user/asoffers/add";
$route['update_offer'] = "user/asoffers/update_offr";
$route['edit_offer/(:num)'] = "user/asoffers/edit/$1";
$route['choose_ofr'] = "user/asoffers/offr_submit";
$route['view_offer/(:num)'] = "user/asoffers/view/$1";
$route['review'] = "user/asoffers/review";
$route['view_offers/(:num)'] = "user/asoffers/view_offers/$1";

$route['change_pwd'] = "user/change_pwd";
$route['profile/(:num)'] = "user/profile_view/$1";
$route['edit_profile'] = "user/edit_profile";
$route['chng_pwd'] = "user/chng_pwd";
$route['search'] = "user/search";
$route['fac_det/(:num)'] = "user/gym_imgs/$1";
$route['fac_det/(:num)/image'] = "user/gym_imgs/$1";
$route['fac_det/(:num)/video'] = "user/gym_imgs/$1";
$route['fac_det/user/get_gymname'] = "user/get_gymname";

$route['charge_pay'] = "webservices/payment";
$route['charge_payment'] = "webservices/charge_payment";
//$route['admin'] = "admin/dashboard";
//$route['login'] = "admin/login";

$route['404_override'] = '';


//$route['login'] = 'welcome/auth/login';
//$route['logout'] = 'welcome/auth/logout';

/* End of file routes.php */
/* Location: ./application/config/routes.php */