<?php
    $apiLoginID = "2EEA5870";// Get API ID from TaxCloud
    $apiKey = "B8283100-9A7D-4B9B-BEEF-F1DDB6DA46CA";//Get API KEY from TaxCloud

    class Adapter
    {
        const WSDL = "https://api.taxcloud.net/1.0/?wsdl";
        const API_ID = "2EEA5870";
        const API_KEY = "B8283100-9A7D-4B9B-BEEF-F1DDB6DA46CA";

        public function getSoapClient() {
            return new SoapClient(self::WSDL);
        }

        public function authorize() {
            $_client = $this->getSoapClient();

            $cartItem =  new StdClass();
            $cartItem->ItemID = "2"; //$this->_getTime();
            $cartItem->Index = 0;
            $cartItem->displayName = "displayName";
            $cartItem->TIC = "0000";
            $cartItem->Price = 1000.00;
            $cartItem->Qty = 1;

            $cartItems = array($cartItem);
            $cart = new StdClass();
            $cart->cartID = 12;
            $cart->cartItems = $cartItems;

            $wrapper = new StdClass();
            $wrapper->deliveredBySeller = false;
            $wrapper->cartItems = $cartItems;
            $wrapper->customerID = 12;

            $address = new StdClass();
            $address->Address1 = "3205 South Judkins Street";
            $address->City = "Minnesota";
            $address->State = "VT";
            $address->Zip5 = "05001";

            $du = new StdClass();
            $du->Address1 = "3206 South Judkins Street";
            $du->City = "Minnesota";
            $du->State = "VT";
            $du->Zip5 = "05001";

            $wrapper->apiKey = self::API_KEY;
            $wrapper->apiLoginID = self::API_ID;
            $wrapper->origin = $address;
            $wrapper->destination = $du;

            var_dump((array)$_client->__soapCall("Lookup", array($wrapper)));
        }

        protected function _getTime() {
            $time = new DateTime();
            return $time->format('Y.m.d H:i:s');
        }

        protected function _prepareRequest($request) {
            $result = array();

            foreach($request as $param=>$value) {
                switch(gettype($value)) {
                    case "string":
                        $result[] = new SoapVar($value, XSD_STRING, null, null, $param);
                        break;
                    case "object":
                        $result[] = new SoapVar($value, XSD_DATE, null, null, $param);
                        break;
                }

            }

            return $result;
        }
    }

    $adapter = new Adapter();
    $adapter->authorize();
